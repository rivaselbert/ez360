package com.sister.ivana.webapi.elasticsearch;


import java.util.List;

public class InventoryResults {
    public static class VehicleHit {
        public VehicleData _source;
    }

    public static class HitCollection {
        public int total;
        public List<VehicleHit> hits;
    }

    public static class VehicleData {
        public String vin;
        public String project_id;
        public String library_id;
        public String year;
        public String make;
        public String model;
        public String stock_no;
        public String miles;
        public String new_used;
        public List<String> display_pics;
        public List<List<String>> spin_detail_pics;
        public List<String> detail_pics;
        public List<String> third_party_pics;
        public List<String> stock_pics;
        public List<List<String>> spin_pics;
        public List<String> thumbnail_pics;
    }

    public HitCollection hits;
}
