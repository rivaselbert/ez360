package com.sister.ivana.webapi;


import android.content.Context;

import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.logging.AppLoggingInterceptor;
import com.sister.ivana.webapi.elasticsearch.MagazineResults;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Path;

public class WebService {
    private static final String BASE_URL = "http://backend.prod.sister.tv/";
    private static WebApi api;

    private static final String BASE_DEBUG_URL = "http://ivana.prod.sister.tv/";
    private static WebApi debugApi;

    public static WebApi getApi(Context context) {
        if (api == null) {
            // We use a long read timeout because the endpoints that process media from AWS
            // do it synchronously and it can take awhile.
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                    .addInterceptor(new AppLoggingInterceptor(new AppSharedPreferences(context.getApplicationContext())))
                    .readTimeout(5, TimeUnit.MINUTES);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(clientBuilder.build())
                    .build();
            api = retrofit.create(WebApi.class);
        }

        return api;
    }

    /**
     * Temporary API for testing. Will be migrated to getApi().
     */
    public static WebApi getDebugApi(Context context) {
        if (debugApi == null) {
            // We use a long read timeout because the endpoints that process media from AWS
            // do it synchronously and it can take awhile.
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                    .addInterceptor(new AppLoggingInterceptor(new AppSharedPreferences(context.getApplicationContext())))
                    .readTimeout(5, TimeUnit.MINUTES);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_DEBUG_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(clientBuilder.build())
                    .build();
            debugApi = retrofit.create(WebApi.class);
        }

        return debugApi;
    }
}
