package com.sister.ivana.webapi.elasticsearch;


import java.util.List;

public class MagazineResults {
    public static class MagazineShotData {
        public String feature_title;
        public String feature_description;
        public String reference_image;
        public String shooting_instruction;
        public String feature_code;
    }

    public static class MagazineData {
        public String project_id;
        public String vehicle_type;
        public String year;
        public String make;
        public String model;

        public List<MagazineShotData> features;
    }

    public static class MagazineHit {
        public MagazineData _source;
    }

    public static class HitCollection {
        public List<MagazineHit> hits;
    }

    public HitCollection hits;
}
