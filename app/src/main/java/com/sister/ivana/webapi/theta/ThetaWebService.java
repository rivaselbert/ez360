package com.sister.ivana.webapi.theta;


import android.content.Context;

import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.logging.AppLoggingInterceptor;

import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Interface for communicating with Theta S camera
 */
public class ThetaWebService {
    private static final String BASE_URL = "http://192.168.1.1/osc/";
    private static ThetaWebApi api;

    // Debug version that logs HTTP requests and responses
    public static ThetaWebApi getApi(Context context) {
        if (api == null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new AppLoggingInterceptor(new AppSharedPreferences(context.getApplicationContext())))
                    .build();

            Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create());

            api = retrofitBuilder.build().create(ThetaWebApi.class);
        }

        return api;
    }

    public static Call<ThetaResponse> startSession(Context context) {
        ThetaCommand command = new ThetaCommand();
        command.name = "camera.startSession";
        return getApi(context).execute(command);
    }

    public static Call<ThetaResponse> setHDR(Context context, String sessionID) {
        ThetaCommand command = new ThetaCommand();
        command.name = "camera.setOptions";

        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("sessionId", sessionID);

        HashMap<String, String> options = new HashMap<>();
        options.put("_filter", "hdr");
        parameters.put("options", options);

        command.parameters = parameters;

        return getApi(context).execute(command);
    }

    public static Call<ThetaResponse> takePicture(Context context, String sessionID) {
        ThetaCommand command = new ThetaCommand();
        command.name = "camera.takePicture";

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("sessionId", sessionID);
        command.parameters = parameters;

        return getApi(context).execute(command);
    }

    public static Call<ThetaResponse> delete(Context context, String fileURI) {
        ThetaCommand command = new ThetaCommand();
        command.name = "camera.delete";

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("fileUri", fileURI);
        command.parameters = parameters;

        return getApi(context).execute(command);
    }

    public static Call<ResponseBody> getImage(Context context, String fileURI) {
        ThetaCommand command = new ThetaCommand();
        command.name = "camera.getImage";

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("fileUri", fileURI);
        command.parameters = parameters;

        return getApi(context).getImage(command);
    }

    public static Call<ThetaUpdatesResponse> checkForUpdates(Context context, String stateFingerprint) {
        ThetaUpdatesRequest request = new ThetaUpdatesRequest();
        request.stateFingerprint = stateFingerprint;
        return getApi(context).checkForUpdates(request);
    }
}
