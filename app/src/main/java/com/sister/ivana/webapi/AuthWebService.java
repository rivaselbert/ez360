package com.sister.ivana.webapi;


import android.content.Context;

import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.logging.AppLoggingInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AuthWebService {
    private static final String BASE_URL = "http://auth.sister.tv/";
    private static AuthWebApi api;

    public static AuthWebApi getApi(Context context) {
        if (api == null) {
            // We use a long read timeout because the endpoints that process media from AWS
            // do it synchronously and it can take awhile.
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                    .addInterceptor(new AppLoggingInterceptor(new AppSharedPreferences(context.getApplicationContext())))
                    .readTimeout(5, TimeUnit.MINUTES);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(clientBuilder.build())
                    .build();
            api = retrofit.create(AuthWebApi.class);
        }

        return api;
    }

    public Call<UserDetail> getUserDetail(Context context, String authorization, String user) {
        // SiSTeR wants to allow people to enter mixed case email addresses but their back end
        // expects all lowercase
        return getApi(context).getUserDetail(authorization, user.toLowerCase());
    }
}
