package com.sister.ivana.webapi.theta;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;

public interface ThetaWebApi {
    @GET("info")
    Call<ResponseBody> getInfo();

    @POST("commands/execute")
    Call<ThetaResponse> execute(@Body ThetaCommand command);

    @POST("state")
    Call<ThetaStateResponse> getState();

    @POST("checkForUpdates")
    Call<ThetaUpdatesResponse> checkForUpdates(@Body ThetaUpdatesRequest request);

    @POST("commands/execute")
    @Streaming
    Call<ResponseBody> getImage(@Body ThetaCommand command);
}
