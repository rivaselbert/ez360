package com.sister.ivana.webapi;


import java.util.ArrayList;

public class ExteriorSpinBody extends AppVersionBody {
    public String library_id;
    public String project_id;
    public String vin;
    public boolean replace;
    public ArrayList<String> spin_urls;
    public String camera_id;
    public String camera_role;
    public boolean used_turntable;
    public int automated_picture_count;
    public int spin_duration;
    public int extracted_picture_count;
}
