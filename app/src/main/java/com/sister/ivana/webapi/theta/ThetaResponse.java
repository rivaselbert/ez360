package com.sister.ivana.webapi.theta;


public class ThetaResponse {
    public enum ExecutionStatus {DONE, IN_PROGRESS, ERROR, UNKNOWN}

    static public class Results {
        public String sessionId;
        public Integer timeout;
    }

    public String name;
    public String state;
    public Results results;

    public ExecutionStatus getExecutionStatus() {
        if (state.contentEquals("done")) {
            return ExecutionStatus.DONE;
        } else if (state.contains("inProgress")) {
            return ExecutionStatus.IN_PROGRESS;
        } else if (state.contains("error")) {
            return ExecutionStatus.ERROR;
        } else {
            return ExecutionStatus.UNKNOWN;
        }

    }
}
