package com.sister.ivana.webapi;


import java.util.ArrayList;

public class AddPoiBody extends AppVersionBody {
    public static class Coordinate {
        public double x;
        public double y;
    }

    public String library_id;
    public String project_id;
    public String vin;
    public String image_url;
    public String title;
    public String description;
    public boolean add_to_poi;
    public int spin_number;
    public int spin_index;
    public String camera_id;
    public Coordinate picture_coordinate;
}
