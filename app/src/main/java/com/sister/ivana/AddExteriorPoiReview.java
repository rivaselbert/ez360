package com.sister.ivana;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sister.ivana.dialog.AppDialogFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AddExteriorPoiReview extends DialogFragment implements DeletePhotoDialogFragment.DeletePhotoListener {
    public interface AddExteriorPoiReviewListener {
        void onAddExteriorPoiReviewSubmit(String pathname, boolean addToMagazine);
        void onAddExteriorPoiReviewCancel();
    }

    @BindView(R.id.topContainer) ViewGroup topContainer;
    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.addtoMagazineCheckBox) CheckBox addPoiCheckBox;

    private static final String KEY_IMAGE_FILENAME = "KEY_IMAGE_FILENAME";
    private Unbinder unbinder;
    private String imagePathname;

    public static AddExteriorPoiReview newInstance(String imagePathname) {
        AddExteriorPoiReview fragment = new AddExteriorPoiReview();
        Bundle args = new Bundle();
        args.putString(KEY_IMAGE_FILENAME, imagePathname);
        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int)(frame.width() * 0.9);
        final int height = (int)(frame.height() * 0.95);
        getDialog().getWindow().setLayout(width, height);
        topContainer.requestLayout();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_external_poi_review, null);
        unbinder = ButterKnife.bind(this, view);

        final Dialog dialog = new Dialog(getActivity(), R.style.AppDialogTheme);
        dialog.setContentView(view);

        imagePathname = getArguments().getString(KEY_IMAGE_FILENAME);

        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                params.width = (int) (1.6 * (double)imageView.getMeasuredHeight());

                Picasso picasso = Picasso.get();
                File f = new File(imagePathname);
                picasso.load(f)
                        .fit()
                        .into(imageView);

            }
        });

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.negative_button)
    void onClickNegativeButton() {
        final AddExteriorPoiReviewListener listener = (AddExteriorPoiReviewListener) getActivity();
        listener.onAddExteriorPoiReviewCancel();
        dismiss();
    }

    @OnClick(R.id.positive_button)
    void onClickPositiveButton() {
        final AddExteriorPoiReviewListener listener = (AddExteriorPoiReviewListener) getActivity();
        listener.onAddExteriorPoiReviewSubmit(imagePathname, addPoiCheckBox.isChecked());
        dismiss();
    }

    @OnClick(R.id.button_delete)
    void onDeleteClick() {
        final DeletePhotoDialogFragment fragment = DeletePhotoDialogFragment.newInstance();
        fragment.setTargetFragment(this, 1);
        fragment.show(getActivity().getFragmentManager(), "delete_photo");
    }

    @Override
    public void onDeletePhotoPositiveClick() {
        dismiss();

        if (new File(imagePathname).delete()) {
            Timber.i("user deleted picture for add external poi: %s", imagePathname);
        } else {
            Timber.e("failed deleting picture for add external poi: %s", imagePathname);
        }
    }

    @Override
    public void onDeletePhotoNegativeClick() {
    }
}
