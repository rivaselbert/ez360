package com.sister.ivana;

import android.app.Dialog;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.VideoView;

import com.sister.ivana.settings.ItemSelectedListener;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class LongClickCompleteDialogFragment extends DialogFragment {

    @BindView(R.id.fullScreenVideo) VideoView fullScreenVideo;

    private ItemSelectedListener listener;

    public void setListener(ItemSelectedListener listener) {
        this.listener = listener;
    }

    private Unbinder unbinder;

    public String url;

    public static LongClickCompleteDialogFragment newInstance(String url) {
        LongClickCompleteDialogFragment fragment = new LongClickCompleteDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.url = url;
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int)(frame.width() * 0.9);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity(), R.style.AppDialogTheme);
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_long_click_complete, null);
        dialog.setContentView(view);
        unbinder = ButterKnife.bind(this, dialog);

        File videoFile = new File(url);
        Uri video = Uri.fromFile(videoFile);
        fullScreenVideo.setVideoURI(video);
        fullScreenVideo.seekTo(100);

        return dialog;
    }

    @OnClick(R.id.video_play_button)
    void onClickVideoButton() {
        fullScreenVideo.start();
    }

    @OnClick(R.id.negative_button)
    void onClickNegativeButton() {
        File videoFile = new File(url);
        videoFile.delete();
        dismiss();
    }

    @OnClick(R.id.positive_button)
    void onClickPositiveButton() {
        if (listener != null) {
            listener.itemSelected(null);
        }
        dismiss();
    }

}
