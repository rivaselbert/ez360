package com.sister.ivana.settings.types;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.sister.ivana.R;
import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Arrays;
import java.util.Map;

public class SwitchRowHolder extends BaseRowHolder {

    public static final int LAYOUT_ID = R.layout.setting_switch;

    public final View view;
    public final TextView title;
    public final TextView subtitle;
    public final Switch switch1;
    public final View disabledOverlay;
    private boolean enabled;

    public SwitchRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view, adapter);
        this.view = view;

        title = (TextView) view.findViewById(R.id.title);
        subtitle = (TextView) view.findViewById(R.id.subtitle);
        switch1 = (Switch) view.findViewById(R.id.switch1);
        disabledOverlay = view.findViewById(R.id.disabled_overlay);

        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enabled) {
                    switch1.setChecked(!switch1.isChecked());
                }
            }
        });

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                //boolean value = switch1.isChecked();
                boolean value = isChecked;
                String subtitleValue = getDisplayValue(value);

                rowValue.storeValue(value);
                subtitle.setText(subtitleValue);

                if (SwitchRowHolder.this.adapter.getListener() != null) {
                    SwitchRowHolder.this.adapter.getListener().itemChanged(rowValue.item);
                }
            }
        });
    }

    @Override
    public void bind(Context context, final Map<String,String> item) {
        super.bind(context, item);
        rowValue = new SwitchRowValue(item, adapter);

        Boolean value = (Boolean) rowValue.retrieveValue();
        String subtitleValue = getDisplayValue(value);

        title.setText(item.get("title"));
        subtitle.setText(subtitleValue);
        switch1.setChecked(value);

        enabled = true;
        if ("true".equals(item.get("disabled"))) {
            enabled = false;
        }

        switch1.setEnabled(enabled);
        disabledOverlay.setVisibility(enabled ? View.GONE : View.VISIBLE);
    }

    private String getDisplayValue(boolean value) {
        String subtitleValue = rowValue.item.get("subtitle");
        String valuesId = rowValue.item.get("values");
        if (valuesId != null) {
            Resources res = adapter.getContext().getResources();
            final String[] values = res.getStringArray(Integer.parseInt(valuesId));
            int valueIndex = Arrays.asList(values).indexOf(Boolean.toString(value));

            String valuesDisplayId = rowValue.item.get("values_display");
            if (valuesDisplayId != null) {
                final String[] valuesDisplay = res.getStringArray(Integer.parseInt(valuesDisplayId));
                subtitleValue = valuesDisplay[valueIndex];
            }
        }
        return subtitleValue;
    }
}
