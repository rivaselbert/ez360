package com.sister.ivana.settings;

public abstract class ItemSelectedListener {
    public abstract void itemSelected(String item);
}
