package com.sister.ivana.settings.types;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Map;

public class TextEntryRowValue extends BaseRowValue {

    public TextEntryRowValue(Map<String,String> item, BaseSettingsAdapter adapter) {
        super(item, adapter);
    }

    @Override
    public Object retrieveValue() {
        String key = item.get("key");
        String value = null;
        if (key != null) {
            JsonObject serverSettings = adapter.getSharedPreferences().retrieveServerSettings();
            value = adapter.getSharedPreferences().getKeyValue(key);
        }
        return value;
    }

    @Override
    public Object retrieveDefaultValue(JsonObject serverSettings) {
        String defaultValueString = item.get("default");
        Object defaultValueObject = getServerDefaultSetting(serverSettings);
        if (defaultValueObject != null && defaultValueObject instanceof String) {
            defaultValueString = (String) defaultValueObject;
        }
        return defaultValueString;
    }

    @Override
    public void storeValue(Object value) {
        String key = item.get("key");
        if (key != null && value instanceof String) {
            adapter.getSharedPreferences().putKeyValue(key, (String) value);
        }
    }

    @Override
    public Object getServerDefaultSetting(JsonObject serverSettings) {
        JsonElement element = super.getServerDefaultJsonElement(serverSettings);
        if (element != null) {
            return element.getAsString();
        }
        return null;
    }

}
