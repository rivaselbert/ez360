package com.sister.ivana.settings;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.settings.types.AnotherScreenRowHolder;
import com.sister.ivana.settings.types.BaseRowHolder;
import com.sister.ivana.settings.types.BaseRowValue;
import com.sister.ivana.settings.types.ButtonRowHolder;
import com.sister.ivana.settings.types.HeaderRowHolder;
import com.sister.ivana.settings.types.NumberEntryRowHolder;
import com.sister.ivana.settings.types.NumberEntryRowValue;
import com.sister.ivana.settings.types.NumberSelectRowHolder;
import com.sister.ivana.settings.types.NumberSelectRowValue;
import com.sister.ivana.settings.types.ResetButtonRowHolder;
import com.sister.ivana.settings.types.SwitchRowHolder;
import com.sister.ivana.settings.types.SwitchRowValue;
import com.sister.ivana.settings.types.TextEntryRowHolder;
import com.sister.ivana.settings.types.TextEntryRowValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BaseSettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected ArrayList<Map<String, String>> settings = new ArrayList<Map<String, String>>();
    protected Context context;
    protected Activity activity;
    protected SettingsItemChangeListener listener;
    protected AppSharedPreferences sharedPreferences;

    public BaseSettingsAdapter(Context context, Activity activity, SettingsItemChangeListener listener) {
        this.context = context;
        this.activity = activity;
        this.listener = listener;

        sharedPreferences = new AppSharedPreferences(activity);
    }

    public Context getContext() {
        return context;
    }

    public Activity getActivity() {
        return activity;
    }

    public AppSharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public SettingsItemChangeListener getListener() {
        return listener;
    }

    protected Map<String,String> createMap(String[][] mapStrings) {
        Map<String,String> map = new HashMap<String,String>();
        for (String[] keyValue : mapStrings) {
            if (keyValue.length == 2) {
                map.put(keyValue[0], keyValue[1]);
            }
        }
        return map;
    }

    public ArrayList<Map<String, String>> getAllSettings() {
        ArrayList<Map<String, String>> allSettings = new ArrayList<Map<String, String>>();
        return allSettings;
    }

    public JsonObject putSettingsAsJson(JsonObject jsonObject) {
        for (Map<String, String> setting : getAllSettings()) {
            String localOnly = setting.get("local_only");
            if (localOnly == null || !"true".equalsIgnoreCase(localOnly)) {
                String id = setting.get("layout");
                BaseRowValue rowValue = (BaseRowValue) createRowValue(setting, Integer.parseInt(id));
                rowValue.item = setting;

                String key = setting.get("key");
                if (key != null) {
                    Object value = rowValue.retrieveValue();
                    jsonObject = add(jsonObject, key, value);
                }
            }
        }
        return jsonObject;
    }

    public JsonObject putDefaultsAsJson(JsonObject jsonObject) {
        JsonObject serverSettings = sharedPreferences.retrieveServerSettings();
        for (Map<String, String> setting : getAllSettings()) {
            String localOnly = setting.get("local_only");
            if (localOnly == null || !"true".equalsIgnoreCase(localOnly)) {
                String id = setting.get("layout");
                BaseRowValue rowValue = (BaseRowValue) createRowValue(setting, Integer.parseInt(id));
                rowValue.item = setting;

                String key = setting.get("key");
                if (key != null) {
                    Object value = rowValue.retrieveDefaultValue(serverSettings);
                    jsonObject = add(jsonObject, key, value);
                }
            }
        }
        return jsonObject;
    }

    private JsonObject add(JsonObject jsonObject, String key, Object value) {
        if (value instanceof String) {
            jsonObject.addProperty(key, (String)value);
        } else if (value instanceof Integer) {
            jsonObject.addProperty(key, (Integer)value);
        } else if (value instanceof Boolean) {
            jsonObject.addProperty(key, (Boolean)value);
        }
        return jsonObject;
    }

    public void resetAllToDefaults() {
        JsonObject serverSettings = sharedPreferences.retrieveServerSettings();
        for (Map<String, String> setting : getAllSettings()) {
            String id = setting.get("layout");
            BaseRowValue rowValue = createRowValue(setting, Integer.parseInt(id));
            rowValue.item = setting;

            Object serverDefaultValue = rowValue.getServerDefaultSetting(serverSettings);
            if (serverDefaultValue != null) {
                rowValue.storeValue(serverDefaultValue);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        Map<String,String> map = settings.get(position);
        String id = map.get("layout");
        return Integer.parseInt(id);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case HeaderRowHolder.LAYOUT_ID:
                return new HeaderRowHolder(view, this);
            case SwitchRowHolder.LAYOUT_ID:
                return new SwitchRowHolder(view, this);
            case NumberSelectRowHolder.LAYOUT_ID:
                return new NumberSelectRowHolder(view, this);
            case NumberEntryRowHolder.LAYOUT_ID:
                return new NumberEntryRowHolder(view, this);
            case AnotherScreenRowHolder.LAYOUT_ID:
                return new AnotherScreenRowHolder(view, this);
            case ResetButtonRowHolder.LAYOUT_ID:
                return new ResetButtonRowHolder(view, this);
            case ButtonRowHolder.LAYOUT_ID:
                return new ButtonRowHolder(view, this);
            case TextEntryRowHolder.LAYOUT_ID:
                return new TextEntryRowHolder(view, this);
        }
        return new NumberSelectRowHolder(view, this);
    }

    public BaseRowValue createRowValue(Map<String, String> setting, int viewType) {
        switch (viewType) {
            case HeaderRowHolder.LAYOUT_ID:
                return new BaseRowValue(setting, this);
            case SwitchRowHolder.LAYOUT_ID:
                return new SwitchRowValue(setting, this);
            case NumberSelectRowHolder.LAYOUT_ID:
                return new NumberSelectRowValue(setting, this);
            case NumberEntryRowHolder.LAYOUT_ID:
                return new NumberEntryRowValue(setting, this);
            case AnotherScreenRowHolder.LAYOUT_ID:
                return new BaseRowValue(setting, this);
            case TextEntryRowHolder.LAYOUT_ID:
                return new TextEntryRowValue(setting, this);
        }
        return new BaseRowValue(setting, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        BaseRowHolder holder = (BaseRowHolder) viewHolder;
        holder.bind(context, settings.get(position));
    }

    @Override
    public int getItemCount() {
        return settings.size();
    }

}
