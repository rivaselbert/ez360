package com.sister.ivana.settings;

import android.app.Activity;
import android.content.Context;

import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.R;
import com.sister.ivana.Util;
import com.sister.ivana.settings.types.ButtonRowHolder;
import com.sister.ivana.settings.types.HeaderRowHolder;
import com.sister.ivana.settings.types.NumberEntryRowHolder;
import com.sister.ivana.settings.types.NumberSelectRowHolder;
import com.sister.ivana.settings.types.ResetButtonRowHolder;
import com.sister.ivana.settings.types.SwitchRowHolder;
import com.sister.ivana.settings.types.TextEntryRowHolder;

import java.util.ArrayList;
import java.util.Map;

public class AdvancedSettingsAdapter extends BaseSettingsAdapter {

    private boolean setupInProgress = false;

    public AdvancedSettingsAdapter(Context context, Activity activity, SettingsItemChangeListener listener) {
        super(context, activity, listener);


        setupSettings();
    }

    public void setupSettings() {
        if (!setupInProgress) {
            setupInProgress = true;
            settings.clear();
            addSettings(settings);
            setupInProgress = false;
        }
    }

    @Override
    public ArrayList<Map<String, String>> getAllSettings() {
        ArrayList<Map<String, String>> allSettings = new ArrayList<Map<String, String>>();
        addSettings(allSettings);
        return allSettings;
    }

    private void addSettings(ArrayList<Map<String, String>> settingsParam) {
        settingsParam.add(createMap(new String[][] {
                {"title", "Advanced"},
                {"layout", HeaderRowHolder.LAYOUT_ID+""}
        }));

        final boolean vinSharingAvailable = !sharedPreferences.isMasterSlaveEnabled() || sharedPreferences.isMaster();
        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_SHARED_INPUT},
                {"title", "Shared Input"},
                {"subtitle", "Share VIN selection with other camera?"},
                {"default", "Yes - Share VIN selection with other camera?"},
                {"values", R.array.yes_no_values+""},
                {"values_display", R.array.shared_input_display+""},
                {"disabled", vinSharingAvailable ? "false" : "true"},
                {"layout", SwitchRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_MASTER_SLAVE_ENABLED},
                {"local_only", "true"},
                {"title", "Set Master / Slave ?"},
                {"subtitle", "A master device can cause the slaves to shoot automatically"},
                {"default", "Master"},
                {"values", R.array.is_master_values+""},
                {"layout", SwitchRowHolder.LAYOUT_ID+""}
        }));

        if (sharedPreferences.getBoolKeyValue(AppSharedPreferences.KEY_MASTER_SLAVE_ENABLED, false)) {
            settingsParam.add(createMap(new String[][] {
                    {"key", AppSharedPreferences.KEY_DEVICE_ROLE},
                    {"local_only", "true"},
                    {"title", "This device is"},
                    {"subtitle", "Master"},
                    {"default", "Master"},
                    {"values", R.array.device_role_values+""},
                    {"values_display", R.array.device_role_display+""},
                    {"dialog_title", "Select device role"},
                    {"dialog_button", R.string.dialog_button_ok+""},
                    {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
            }));

            settingsParam.add(createMap(new String[][] {
                    {"key", AppSharedPreferences.KEY_DEVICE_ALIAS},
                    {"local_only", "true"},
                    {"title", "Alias"},
                    {"subtitle", " "},
                    {"default", " "},
                    {"dialog_title", "Enter device alias"},
                    {"dialog_button", R.string.dialog_button_ok+""},
                    {"layout", TextEntryRowHolder.LAYOUT_ID+""}
            }));

            if (sharedPreferences.isMaster()) {
                settingsParam.add(createMap(new String[][] {
                        {"key", AppSharedPreferences.KEY_MASTER_TRIGGER},
                        {"local_only", "true"},
                        {"title", "Trigger Slaves"},
                        {"default", "On"},
                        {"values", R.array.on_off_values + ""},
                        {"values_display", R.array.on_off_display + ""},
                        {"layout", SwitchRowHolder.LAYOUT_ID + ""}
                }));
            }

            settingsParam.add(createMap(new String[][] {
                    {"key", AppSharedPreferences.KEY_UDP_COMMUNICATIONS_PORT},
                    {"title", "Communications Port"},
                    {"subtitle", "11111"},
                    {"default", "11111"},
                    {"dialog_title", "Communications Port"},
                    {"dialog_button", R.string.dialog_button_ok+""},
                    {"layout", NumberEntryRowHolder.LAYOUT_ID+""}
            }));
        }

//        settingsParam.add(createMap(new String[][] {
//                {"key", AppSharedPreferences.KEY_IS_FIXED_CAMERA},
//                {"title", "Fixed or portable camera?"},
//                {"subtitle", "Fixed"},
//                {"values", R.array.is_fixed_values+""},
//                {"values_display", R.array.is_fixed_display+""},
//                {"layout", SwitchRowHolder.LAYOUT_ID+""}
//        }));

        settingsParam.add(createMap(new String[][] {
                {"title", "Pictures setting"},
                {"layout", HeaderRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_PICTURE_SIZE},
                {"title", "Pictures size"},
                {"subtitle", "2 - Recommended size"},
                {"default", "2 - Recommended size"},
                {"values", R.array.picture_size_values+""},
                {"values_display", R.array.picture_size_display+""},
                {"dialog_title", "Select picture size"},
                {"dialog_button", R.string.dialog_button_ok+""},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_COMPRESSION_RATIO},
                {"title", "Compression ratio"},
                {"subtitle", "5 - fastest transfer (recommended)"},
                {"default", "5 - fastest transfer (recommended)"},
                {"values", R.array.picture_compression_values+""},
                {"values_display", R.array.picture_compression_display+""},
                {"dialog_title", "Select picture compression"},
                {"dialog_button", R.string.dialog_button_ok+""},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"title", "Support"},
                {"layout", HeaderRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_IS_LOGGING_ENABLED},
                {"title", "Diagnostic Logging"},
                {"subtitle", "Enabled"},
                {"default", "Enabled"},
                {"values", R.array.is_logging_enabled_values+""},
                {"values_display", R.array.is_logging_enabled_display+""},
                {"layout", SwitchRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"title", "Device ID"},
                {"subtitle", Util.getLogDeviceID(context)},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"title", "Send Logs"},
                {"layout", ButtonRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"layout", ResetButtonRowHolder.LAYOUT_ID+""}
        }));

//        settingsParam.add(createMap(new String[][] {
//                {"title", "Video dimension"},
//                {"subtitle", "1"},
//                {"values", R.array.how_many_spins_values+""},
//                {"dialog_title", "Select video dimension"},
//                {"dialog_button", R.string.dialog_button_ok+""},
//                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
//        }));
//
//        settingsParam.add(createMap(new String[][] {
//                {"title", "Video quality"},
//                {"subtitle", "1"},
//                {"values", R.array.how_many_spins_values+""},
//                {"dialog_title", "Select video compression"},
//                {"dialog_button", R.string.dialog_button_ok+""},
//                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
//        }));
    }

}
