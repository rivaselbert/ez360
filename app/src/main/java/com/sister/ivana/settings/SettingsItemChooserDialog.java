package com.sister.ivana.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.sister.ivana.R;
import com.sister.ivana.dialog.AppDialogFragment;

public class SettingsItemChooserDialog extends AppDialogFragment {

    public String[] items;
    private String selectedItem;
    private SettingsAdapter adapter;
    private ItemSelectedListener listener;

    public static SettingsItemChooserDialog newInstance(String[] items, String current, String title, int buttonTitle) {
        SettingsItemChooserDialog fragment = new SettingsItemChooserDialog();

        fragment.items = items;
        fragment.selectedItem = current;
        Bundle args = new Bundle();
        setTitle(args, title);
        setContentView(args, R.layout.dialog_select_item);
        setPositiveButtonText(args, buttonTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(ItemSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        RecyclerView listView = (RecyclerView) dialog.findViewById(R.id.list);
        listView.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        listView.setLayoutManager(linearLayoutManager);

        adapter = new SettingsAdapter(getContext());
        listView.setAdapter(adapter);

        return dialog;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (listener != null) {
            if (selectedItem != null) {
                listener.itemSelected(selectedItem);
            }
        }
        dismiss();
    }




    public class SettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Context context;

        public SettingsAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getItemViewType(int position) {
            return SimpleRowHolder.LAYOUT_ID;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
            return new SimpleRowHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            SimpleRowHolder holder = (SimpleRowHolder) viewHolder;
            holder.bind(context, items[position]);
        }

        @Override
        public int getItemCount() {
            return items.length;
        }

        public class SimpleRowHolder extends RecyclerView.ViewHolder {

            public static final int LAYOUT_ID = R.layout.simple_list_item_radio_button;

            public final View view;
            public final RadioButton radioButton;

            public SimpleRowHolder(View view) {
                super(view);
                this.view = view;

                radioButton = (RadioButton) view.findViewById(android.R.id.text1);

                this.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedItem = radioButton.getText().toString();
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            public void bind(Context context, final String item) {
                radioButton.setText(item);
                radioButton.setChecked(selectedItem != null && selectedItem.equals(item));
            }
        }
    }
}
