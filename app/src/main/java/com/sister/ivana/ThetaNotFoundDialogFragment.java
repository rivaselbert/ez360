package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.widget.CheckBox;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class ThetaNotFoundDialogFragment extends AppDialogFragment {
    private Unbinder unbinder;

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int) (frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    public static ThetaNotFoundDialogFragment newInstance() {
        ThetaNotFoundDialogFragment fragment = new ThetaNotFoundDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_theta_connect_fail_not_found_title);
        AppDialogFragment.setContentView(args, R.layout.dialog_theta_not_found);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_ok);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        return dialog;
    }


}
