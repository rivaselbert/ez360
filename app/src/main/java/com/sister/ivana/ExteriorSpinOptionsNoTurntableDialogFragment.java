package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class ExteriorSpinOptionsNoTurntableDialogFragment extends AppDialogFragment {
    public enum SpinMethod {
        AUTOMATIC_PICTURES(0), VIDEO(1), MANUAL_PICTURES(2);

        private final int code;

        SpinMethod(int code) {
            this.code = code;
        }

        int getCode() {
            return code;
        }

        public static SpinMethod create(int code) {
            switch (code) {
                case 1:
                    return VIDEO;
                case 2:
                    return MANUAL_PICTURES;
                default:
                    return AUTOMATIC_PICTURES;
            }
        }
    }

    public interface SpinOptionsNoTurntableListener {

        void onSpinOptionsNoTurntableManualPictures();

        void onSpinOptionsNoTurntableAutomaticPictures(int frames, int interval);

        void onSpinOptionsNoTurntableCutFromVideo(int frames, boolean createVideo);
    }

    private static final int MIN_PICTURE_INTERVAL = 1;
    private static final int MAX_PICTURE_INTERVAL = 5;

    private static final int MIN_PICTURE_COUNT = 5;
    private static final int MAX_PICTURE_COUNT = 100;

    @BindView(R.id.shoot_pictures_manually) RadioButton shootPicturesManually;
    @BindView(R.id.shoot_pictures_automatically) RadioButton shootPicturesAutomatically;
    @BindView(R.id.cut_pictures_from_video) RadioButton cutPicturesFromVideo;
    @BindView(R.id.picture_interval_edittext) EditText pictureIntervalEditText;
    @BindView(R.id.extract_picture_count_edittext) EditText extractPictureCountEditText;
    @BindView(R.id.auto_picture_count_edittext) EditText autoPictureCountEditText;
    @BindView(R.id.shoot_automatically_label_prefix) TextView shootAutomaticallyLabelPrefix;
    @BindView(R.id.shoot_automatically_label_suffix) TextView getShootAutomaticallyLabelSuffix;
    @BindView(R.id.create_video_checkbox) CheckBox createVideoCheckbox;

    private Unbinder unbinder;
    private AppSharedPreferences preferences;
    private final TextWatcher pictureIntervalWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            updateIntervalSuffix();
        }
    };

    public static ExteriorSpinOptionsNoTurntableDialogFragment newInstance() {
        ExteriorSpinOptionsNoTurntableDialogFragment fragment = new ExteriorSpinOptionsNoTurntableDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_spinit_options_no_turntable_title);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_ok);
        AppDialogFragment.setContentView(args, R.layout.dialog_exterior_spin_options_no_turntable);
        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int)(frame.width() * 0.9);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);

        preferences = new AppSharedPreferences(getContext());
        SpinMethod method = preferences.getNoTurntableSpinMethod();
        switch (method) {
            case AUTOMATIC_PICTURES:
                setChecked(shootPicturesAutomatically);
                break;

            case VIDEO:
                setChecked(cutPicturesFromVideo);
                break;

            case MANUAL_PICTURES:
                setChecked(shootPicturesManually);
                break;

            default:
                setChecked(shootPicturesAutomatically);
                break;
        }

        final int interval = preferences.getSpinNoTurntablePictureInterval();
        final String displayInterval = String.valueOf(interval);
        pictureIntervalEditText.setText(displayInterval);
        updateIntervalSuffix();
        pictureIntervalEditText.setSelection(displayInterval.length());
        pictureIntervalEditText.addTextChangedListener(pictureIntervalWatcher);

        final String extractCount = String.valueOf(preferences.getSpinNoTurntableExtractPictureCount());
        extractPictureCountEditText.setText(extractCount);

        final String autoCount = String.valueOf(preferences.getSpinNoTurntableAutoPictureCount());
        autoPictureCountEditText.setText(autoCount);

        final boolean videoEnabled = preferences.getSpinNoTurntableVideoEnabled();
        createVideoCheckbox.setChecked(videoEnabled);

        // Start with cursor at end of text
        extractPictureCountEditText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                extractPictureCountEditText.setSelection(extractPictureCountEditText.getText().length());
                extractPictureCountEditText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        autoPictureCountEditText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                autoPictureCountEditText.setSelection(autoPictureCountEditText.getText().length());
                autoPictureCountEditText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnCheckedChanged(R.id.shoot_pictures_manually)
    public void onShootManuallyChanged() {
        if (shootPicturesManually.isChecked()) {
            setChecked(shootPicturesManually);
            preferences.putNoTurntableSpinMethod(SpinMethod.MANUAL_PICTURES);
        }
    }

    @OnCheckedChanged(R.id.shoot_pictures_automatically)
    public void onShootAutomaticallyChanged() {
        if (shootPicturesAutomatically.isChecked()) {
            setChecked(shootPicturesAutomatically);
            preferences.putNoTurntableSpinMethod(SpinMethod.AUTOMATIC_PICTURES);
        }
    }

    @OnCheckedChanged(R.id.cut_pictures_from_video)
    public void onCutPicturesFromVideoChanged() {
        if (cutPicturesFromVideo.isChecked()) {
            setChecked(cutPicturesFromVideo);
            preferences.putNoTurntableSpinMethod(SpinMethod.VIDEO);
        }
    }

    @OnCheckedChanged(R.id.create_video_checkbox)
    public void onCreateVideoChanged() {
        preferences.putSpinNoTurntableVideoEnabled(createVideoCheckbox.isChecked());
    }

    private void setChecked(RadioButton checkbox) {
        shootPicturesManually.setChecked(checkbox == shootPicturesManually);
        shootPicturesAutomatically.setChecked(checkbox == shootPicturesAutomatically);
        cutPicturesFromVideo.setChecked(checkbox == cutPicturesFromVideo);

        if (shootPicturesManually.isChecked()) {
            final int color = getResources().getColor(R.color.disabled_text);
            shootAutomaticallyLabelPrefix.setTextColor(color);
            getShootAutomaticallyLabelSuffix.setTextColor(color);
            pictureIntervalEditText.setTextColor(color);
            pictureIntervalEditText.setBackgroundResource(R.drawable.editable_number_background_disabled);
            pictureIntervalEditText.setEnabled(false);
        } if (shootPicturesAutomatically.isChecked()) {
            final int color = getResources().getColor(android.R.color.white);
            shootAutomaticallyLabelPrefix.setTextColor(color);
            getShootAutomaticallyLabelSuffix.setTextColor(color);
            pictureIntervalEditText.setTextColor(color);
            pictureIntervalEditText.setEnabled(true);
            pictureIntervalEditText.setBackgroundResource(R.drawable.editable_number_background);
            autoPictureCountEditText.requestFocus();
        } else {
            final int color = getResources().getColor(R.color.disabled_text);
            shootAutomaticallyLabelPrefix.setTextColor(color);
            getShootAutomaticallyLabelSuffix.setTextColor(color);
            pictureIntervalEditText.setTextColor(color);
            pictureIntervalEditText.setEnabled(false);
            pictureIntervalEditText.setBackgroundResource(R.drawable.editable_number_background_disabled);
            extractPictureCountEditText.requestFocus();
        }

        if (cutPicturesFromVideo.isChecked()) {
            createVideoCheckbox.setEnabled(true);
            createVideoCheckbox.setTextColor(getResources().getColor(android.R.color.white));
        } else {
            createVideoCheckbox.setEnabled(false);
            createVideoCheckbox.setTextColor(getResources().getColor(R.color.disabled_text));
        }
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i != DialogInterface.BUTTON_POSITIVE) {
            return;
        }

        final SpinOptionsNoTurntableListener listener = (SpinOptionsNoTurntableListener) getActivity();

        if (shootPicturesManually.isChecked()) {
            listener.onSpinOptionsNoTurntableManualPictures();
        } else if (shootPicturesAutomatically.isChecked()) {
            int interval = 0;
            try {
                interval = Integer.parseInt(pictureIntervalEditText.getText().toString());
            } catch (NumberFormatException e) {
                Util.logError("Poorly formatted interval", e);
            }

            if (interval < MIN_PICTURE_INTERVAL || interval > MAX_PICTURE_INTERVAL) {
                final String message = String.format("Picture interval must be between %s and %s seconds", MIN_PICTURE_INTERVAL, MAX_PICTURE_INTERVAL);
                AppDialogFragment.show((AppCompatActivity) getActivity(), message);
                return;
            }

            int count = 0;
            try {
                count = Integer.parseInt(autoPictureCountEditText.getText().toString());
            } catch (NumberFormatException e) {
                Util.logError("Poorly formatted auto picture count", e);
            }

            if (count < MIN_PICTURE_COUNT || count > MAX_PICTURE_COUNT) {
                final String message = String.format("Picture count must be between %s and %s", MIN_PICTURE_COUNT, MAX_PICTURE_COUNT);
                AppDialogFragment.show((AppCompatActivity) getActivity(), message);
                return;
            }

            preferences.putSpinNoTurntablePictureInterval(interval);
            preferences.putSpinNoTurntableAutoPictureCount(count);

            listener.onSpinOptionsNoTurntableAutomaticPictures(count, interval);
        } else if (cutPicturesFromVideo.isChecked()) {
            int count = 0;
            try {
                count = Integer.parseInt(extractPictureCountEditText.getText().toString());
            } catch (NumberFormatException e) {
                Util.logError("Poorly formatted extracted picture count", e);
            }

            if (count < MIN_PICTURE_COUNT || count > MAX_PICTURE_COUNT) {
                final String message = String.format("Picture count must be between %s and %s", MIN_PICTURE_COUNT, MAX_PICTURE_COUNT);
                AppDialogFragment.show((AppCompatActivity) getActivity(), message);
                return;
            }

            preferences.putSpinNoTurntableExtractPictureCount(count);
            listener.onSpinOptionsNoTurntableCutFromVideo(count, createVideoCheckbox.isChecked());
        }

        dismiss();
    }

    private void updateIntervalSuffix() {
        if (TextUtils.isEmpty(pictureIntervalEditText.getText())) {
            getShootAutomaticallyLabelSuffix.setText("Seconds");
            return;
        }

        int interval = 0;
        try {
            interval = Integer.parseInt(pictureIntervalEditText.getText().toString());
        } catch (NumberFormatException e) {
            Util.logError("Poorly formatted number in watcher", e);
        }

        if (interval == 1) {
            getShootAutomaticallyLabelSuffix.setText("Second");
        } else {
            getShootAutomaticallyLabelSuffix.setText("Seconds");
        }
    }


}
