package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.widget.CheckBox;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class LongClickIntroDialogFragment extends AppDialogFragment {
    public interface LongClickIntroListener {
        void onLongClickIntroPositiveClick(int magazineID, int requestCode);

        void onLongClickIntroNegativeClick();
    }

    @BindView(R.id.long_click_checkbox)
    CheckBox longClickCheckBox;

    private Unbinder unbinder;

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int) (frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    public static LongClickIntroDialogFragment newInstance() {
        LongClickIntroDialogFragment fragment = new LongClickIntroDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_button_long_click);
        AppDialogFragment.setContentView(args, R.layout.dialog_long_click);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_next);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_back);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        // Save preferences
        final AppSharedPreferences preferences = new AppSharedPreferences(getContext());

        if (i == DialogInterface.BUTTON_POSITIVE) {
            preferences.putDontShowLongClickMessage(longClickCheckBox.isChecked());
            LongClickIntroListener listener = (LongClickIntroListener) getActivity();
            listener.onLongClickIntroPositiveClick(0, 0);
        } else {
            preferences.putDontShowLongClickMessage(longClickCheckBox.isChecked());
            LongClickIntroListener listener = (LongClickIntroListener) getActivity();
            listener.onLongClickIntroNegativeClick();
        }
        dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        return dialog;
    }


}
