package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.sister.ivana.dialog.AppDialogFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class VinConfirmationDialogFragment extends AppDialogFragment {
    public interface VinConfirmationListener {
        void onVinConfirmationPositiveClick(String vin, String project, String library);

        void onVinConfirmationNegativeClick();
    }

    private static final String KEY_VIN = "KEY_VIN";
    private static final String KEY_STOCK = "KEY_STOCK";
    private static final String KEY_YEAR = "KEY_YEAR";
    private static final String KEY_MAKE = "KEY_MAKE";
    private static final String KEY_MODEL = "KEY_MODEL";
    private static final String KEY_THUMBNAIL = "KEY_THUMBNAIL";
    private static final String KEY_PROJECT = "KEY_PROJECT";
    private static final String KEY_LIBRARY = "KEY_LIBRARY";
    private Unbinder unbinder;
    private String vin;
    private String project;
    private String library;

    @BindView(R.id.description_text) TextView descriptionTextView;
    @BindView(R.id.stock_text) TextView stockTextView;
    @BindView(R.id.vin_text) TextView vinTextView;
    @BindView(R.id.thumbnail) ImageView thumbnail;

    public static VinConfirmationDialogFragment newInstance(String vin, String stock, String year, String make, String model, String thumbnail, String project, String library) {
        VinConfirmationDialogFragment fragment = new VinConfirmationDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_VIN, vin);
        args.putString(KEY_STOCK, stock);
        args.putString(KEY_MAKE, make);
        args.putString(KEY_MODEL, model);
        args.putString(KEY_YEAR, year);
        args.putString(KEY_THUMBNAIL, thumbnail);
        args.putString(KEY_PROJECT, project);
        args.putString(KEY_LIBRARY, library);
        AppDialogFragment.setTitle(args, R.string.dialog_vin_confirmaton_title);
        AppDialogFragment.setContentView(args, R.layout.dialog_vin_confirmation);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_button_yes);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);

        vin = getArguments().getString(KEY_VIN);
        project = getArguments().getString(KEY_PROJECT);
        library = getArguments().getString(KEY_LIBRARY);
        populateFields(getArguments());

        // start loading image
        String url = getArguments().getString(KEY_THUMBNAIL);
        if (TextUtils.isEmpty(url)) {
            url = "http://sdk.sister.tv.s3.amazonaws.com/img/image_not_available.jpg";
        }

        final float width = getResources().getDimension(R.dimen.vin_confirmation_thumbnail_width);
        final float height = getResources().getDimension(R.dimen.vin_confirmation_thumbnail_height);
        Picasso.get()
                .load(url)
                .error(R.drawable.image_not_available)
                .resize(Math.round(width), Math.round(height))
                .centerCrop()
                .into(thumbnail);

        return dialog;
    }

    public void onResume() {
        super.onResume();

        // Work around fact that dialogs seem to have a maximum width that does not take
        // width of content into account. Calculate width based on fixed size of components
        // in layout.
        final int widthDP = 150 + 12 + 360;
        final float scale = getResources().getDisplayMetrics().density;
        getDialog().getWindow().setLayout((int)(widthDP * scale), WRAP_CONTENT);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == DialogInterface.BUTTON_POSITIVE) {
            VinConfirmationListener listener = (VinConfirmationListener) getActivity();
            listener.onVinConfirmationPositiveClick(vin, project, library);
        } else if (i == DialogInterface.BUTTON_NEGATIVE) {
            VinConfirmationListener listener = (VinConfirmationListener) getActivity();
            listener.onVinConfirmationNegativeClick();
        }
        dismiss();
    }

    private void populateFields(Bundle bundle) {
        final String stock = bundle.getString(KEY_STOCK);
        final String year = bundle.getString(KEY_YEAR);
        final String make = bundle.getString(KEY_MAKE);
        final String model = bundle.getString(KEY_MODEL);

        descriptionTextView.setText(Util.getVehicleDisplayString(year, make, model));

        if (!TextUtils.isEmpty(stock)) {
            stockTextView.setText("Stock: " + stock);
        } else {
            stockTextView.setText("Stock:");
        }

        if (!TextUtils.isEmpty(vin)) {
            vinTextView.setText("Vin: " + vin);
        } else {
            vinTextView.setText("Vin:");
        }
    }
}
