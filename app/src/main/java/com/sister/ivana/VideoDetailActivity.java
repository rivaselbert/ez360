package com.sister.ivana;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoDetailActivity extends AppCompatActivity implements DeletePhotoDialogFragment.DeletePhotoListener {
    public static final int RESULT_DELETE = 101;

    public static final String EXTRA_SHOT_INDEX = "EXTRA_SHOT_INDEX";

    private final float aspectRatio = 4f / 3f;
    private File video;
    private int index;

    private static final String EXTRA_VIDEO_PATHNAME = "EXTRA_VIDEO_PATHNAME";
    private static final String EXTRA_INDEX = "EXTRA_INDEX";

    @BindView(R.id.top_layout) RelativeLayout topLayout;
    @BindView(R.id.video_view) VideoView videoView;
    @BindView(R.id.button_close) ImageView closeButton;
    @BindView(R.id.button_delete) ImageView deleteButton;

    public static Intent createIntent(Context context, File video, int index) {
        Intent intent = new Intent(context, VideoDetailActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATHNAME, video.getAbsolutePath());
        intent.putExtra(EXTRA_INDEX, index);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        ButterKnife.bind(this);

        final String pathname = getIntent().getStringExtra(EXTRA_VIDEO_PATHNAME);
        if (pathname != null) {
            video = new File(pathname);
        }
        index = getIntent().getIntExtra(EXTRA_INDEX, -1);
    }

    @Override
    protected void onStart() {
        super.onStart();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int height = (int) (metrics.heightPixels * 0.80);
        int width = Math.round(aspectRatio * (float) metrics.heightPixels);

        getWindow().setLayout(width, height);
        topLayout.requestLayout();

        if (video != null) {
            final Uri uri = Uri.fromFile(video);
            videoView.setVideoURI(Uri.fromFile(video));
            videoView.seekTo(100);
        }
    }

    @OnClick(R.id.video_play_button)
    void onClickVideoButton() {
        videoView.start();
    }

    @OnClick(R.id.button_close)
    void onCloseClick() {
        finish();
    }

    @OnClick(R.id.button_delete)
    void onDeleteClick() {
        final DeletePhotoDialogFragment fragment = DeletePhotoDialogFragment.newInstance();
        fragment.show(getFragmentManager(), "delete_photo");
    }

    @Override
    public void onDeletePhotoPositiveClick() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_SHOT_INDEX, index);
        setResult(RESULT_DELETE, intent);
        finish();
    }

    @Override
    public void onDeletePhotoNegativeClick() {
    }
}
