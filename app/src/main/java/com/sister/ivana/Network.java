package com.sister.ivana;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

import java.util.List;

import static android.content.Context.WIFI_SERVICE;

public class Network {
    private final Context context;

    public Network(Context context) {
        this.context = context;
    }


    public boolean haveWiFi() {
        final ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = manager.getActiveNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public String getCurrentSSID() {
        String currentSSID = "";

        WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
        final WifiInfo currentInfo = wifiManager.getConnectionInfo();
        if (currentInfo != null) {
            final String ssid = currentInfo.getSSID();
            return stripQuotes(ssid);
        }

        return currentSSID;
    }

    public boolean connectToNetwork(String ssid) {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
        final int networkID = getNetworkID(wifiManager, ssid);
        if (networkID == -1) {
            Util.logError("Failed finding network ID for " + ssid);
            return false;
        }

        if (!wifiManager.disconnect()) {
            Util.logError("Failed disconnecting from current network while connecting to " + ssid);
            return false;
        }

        if (!wifiManager.enableNetwork(networkID, true)) {
            Util.logError(String.format("Failed enabling network %s %s", ssid, networkID));
            return false;
        }

        return true;
    }

    /**
     * Try to connect back to original WiFi access point after camera operation.
     */
    public void connectToLastSSID() {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
        if (!wifiManager.disconnect()) {
            Util.logError("Failed disconnecting from current network");
        }

        removeCameraNetworks(wifiManager);

        // Try to connect back to original access point after camera operation.
        final String ssid = new AppSharedPreferences(context).getLastSSID();
        if (!TextUtils.isEmpty(ssid)) {
            final int networkID = getNetworkID(wifiManager, ssid);
            if (networkID != -1) {
                if (!wifiManager.enableNetwork(networkID, true)) {
                    Util.logError(String.format("Failed enabling network %s %s", ssid, networkID));
                }
            } else {
                Util.logError("Failed finding network ID for " + ssid);

            }
        } else {
            Util.logError("Could not find last SSID");
        }
    }

    public boolean isConnected(String ssid) {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo == null) {
            return false;
        }

        // Immediately after connecting, we get a connection info with ssid of <unknown ssid>
        // and a state of COMPLETED, even though we are not connected to the new network yet,
        // so ignore that.
        final String connectedSSID = wifiInfo.getSSID();
        if (connectedSSID == null || !stripQuotes(connectedSSID).equalsIgnoreCase(ssid)) {
            return false;
        }

        final SupplicantState state = wifiInfo.getSupplicantState();
        if (state != SupplicantState.COMPLETED) {
            return false;
        }

        // After connecting to the WiFi access point we still can't communicate with
        // the camera until we have an active network.
        final ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    public static boolean networkIsConfigured(WifiManager wifiManager, String ssid) {
        return getNetworkID(wifiManager, ssid) != -1;
    }

    private static String stripQuotes(String string) {
        if (string == null) {
            return null;
        }

        if (string.startsWith("\"") && string.endsWith("\"")) {
            if (string.length() > 2) {
                string = string.substring(1, string.length() - 1);
            } else {
                string = "";
            }
        }

        return string;
    }

    private static void removeCameraNetworks(WifiManager wifiManager) {
        List<WifiConfiguration> networks = wifiManager.getConfiguredNetworks();
        if (networks == null) {
            return;
        }

        // I've seen cases where we had multiple networks in the Control Panel with the same
        // SSID, maybe because earlier versions of the app did not check whether the
        // network existed before creating it. We handle that by deleting all existing camera
        // networks which should be fine since we re-create them after each scan.
        for (WifiConfiguration network : networks) {
            if (!TextUtils.isEmpty(network.SSID)) {
                final String ssid = stripQuotes(network.SSID);
                if (ThetaScanner.isThetaNetwork(ssid)) {
                    if (!wifiManager.removeNetwork(network.networkId)) {
                        Util.logError(String.format("Failed removing camera network %s %s", ssid, network.SSID));
                    }
                }
            }
        }
    }

    private static int getNetworkID(WifiManager wifiManager, String ssid) {
        int networkID = -1;

        List<WifiConfiguration> networks = wifiManager.getConfiguredNetworks();
        if (networks == null) {
            return networkID;
        }

        for (WifiConfiguration network : networks) {
            if (!TextUtils.isEmpty(network.SSID)) {
                if (stripQuotes(network.SSID).contentEquals(ssid)) {
                    networkID = network.networkId;
                    break;
                }
            }
        }

        return networkID;
    }
}
