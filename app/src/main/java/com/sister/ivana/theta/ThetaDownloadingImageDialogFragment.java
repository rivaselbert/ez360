package com.sister.ivana.theta;

import android.app.Dialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.sister.ivana.R;
import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ThetaDownloadingImageDialogFragment extends AppDialogFragment {
    private Unbinder unbinder;
    @BindView(R.id.progress) ProgressBar progressBar;

    public static ThetaDownloadingImageDialogFragment newInstance() {
        ThetaDownloadingImageDialogFragment fragment = new ThetaDownloadingImageDialogFragment();
        fragment.setCancelable(false);
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, "Step 3: Transfer Media");
        AppDialogFragment.setContentView(args, R.layout.dialog_theta_downloading);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.green), PorterDuff.Mode.MULTIPLY);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
