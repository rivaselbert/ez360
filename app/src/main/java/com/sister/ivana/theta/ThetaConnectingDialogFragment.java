package com.sister.ivana.theta;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.sister.ivana.R;
import com.sister.ivana.ThetaController;
import com.sister.ivana.VehicleActivity;
import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ThetaConnectingDialogFragment extends AppDialogFragment {
    private Unbinder unbinder;
    @BindView(R.id.progress) ProgressBar progressBar;

    public static ThetaConnectingDialogFragment newInstance() {
        ThetaConnectingDialogFragment fragment = new ThetaConnectingDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setContentView(args, R.layout.dialog_theta_connecting);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_cancel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.green), PorterDuff.Mode.MULTIPLY);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        onCancel();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        onCancel();
    }

    private void onCancel() {
        VehicleActivity activity = (VehicleActivity) getActivity();
        ThetaController controller = activity.getThetaController();
        if (controller != null) {
            controller.onConnectingDialogCancel();
        }
        dismiss();
    }
}
