package com.sister.ivana.theta;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.sister.ivana.R;
import com.sister.ivana.ThetaController;
import com.sister.ivana.VehicleActivity;
import com.sister.ivana.dialog.AppDialogFragment;

import java.util.List;

public class ThetaConnectionInfoDialog extends AppDialogFragment {

    public List<String> listCameraSSID;
    private String selectedCamera;

    public static ThetaConnectionInfoDialog newInstance(List<String> listCameraSSID) {
        ThetaConnectionInfoDialog fragment = new ThetaConnectionInfoDialog();

        fragment.listCameraSSID = listCameraSSID;
        Bundle args = new Bundle();
        setTitle(args, R.string.dialog_theta_connection_info_title);
        setContentView(args, R.layout.dialog_theta_connect);
        setPositiveButtonText(args, R.string.dialog_theta_connection_info_connect);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        final ListView listView = (ListView) dialog.findViewById(R.id.list);
        String[] stringArray = new String[listCameraSSID.size()];
        stringArray = listCameraSSID.toArray(stringArray);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                stringArray);

        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedCamera = (String) listView.getItemAtPosition(position);
            }
        });

        return dialog;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (selectedCamera != null) {
            VehicleActivity activity = (VehicleActivity) getActivity();
            ThetaController controller = activity.getThetaController();
            if (controller != null) {
                controller.onConnectionInfoPositiveClick(selectedCamera);
            }
            dismiss();
        }
    }
}
