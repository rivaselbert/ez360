package com.sister.ivana.theta;

import android.content.DialogInterface;
import android.os.Bundle;

import com.sister.ivana.R;
import com.sister.ivana.ThetaController;
import com.sister.ivana.VehicleActivity;
import com.sister.ivana.dialog.AppDialogFragment;

public class ThetaReadyToShootDialog extends AppDialogFragment {
    public static ThetaReadyToShootDialog newInstance(String cameraSSID) {
        ThetaReadyToShootDialog fragment = new ThetaReadyToShootDialog();

        Bundle args = new Bundle();
        setTitle(args, "Step 2: Shoot");

        final String message = String.format("Make sure %s is positioned correctly and press shoot.", cameraSSID);
        setMessage(args, message);

        setPositiveButtonText(args, R.string.dialog_theta_ready_to_shoot_positive_button);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        VehicleActivity activity = (VehicleActivity) getActivity();
        ThetaController controller = activity.getThetaController();
        if (controller != null) {
            if (i == DialogInterface.BUTTON_POSITIVE) {
                controller.onReadyToShootPositiveClick();
            } else {
                onCancel();
            }
        }
        dismiss();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        onCancel();
    }

    private void onCancel() {
        VehicleActivity activity = (VehicleActivity) getActivity();
        ThetaController controller = activity.getThetaController();
        if (controller != null) {
            controller.onReadyToShootCancel();
        }
    }
}
