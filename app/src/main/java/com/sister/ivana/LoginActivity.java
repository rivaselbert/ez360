package com.sister.ivana;

import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;
import com.netcompss.ffmpeg4android.GeneralUtils;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;
import com.sister.ivana.database.Project;
import com.sister.ivana.dialog.AppDialogFragment;
import com.sister.ivana.logging.CrashReporter;
import com.sister.ivana.logging.LogUploader;
import com.sister.ivana.logging.LogUploaderService;
import com.sister.ivana.settings.SettingsServerComms;
import com.sister.ivana.webapi.AuthWebService;
import com.sister.ivana.webapi.Token;
import com.sister.ivana.webapi.TokenRequest;
import com.sister.ivana.webapi.UserDetail;
import com.sister.ivana.webapi.WebService;
import com.sister.ivana.webapi.elasticsearch.ESWebService;
import com.sister.ivana.webapi.elasticsearch.ProjectResults;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class LoginActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper> {
    @BindView(R.id.user_id_field) EditText userEditText;
    @BindView(R.id.password_field) EditText passwordEditText;
    @BindView(R.id.remember_me_enabled) CheckBox rememberMeCheckBox;

    private ProgressDialog progressDialog;
    private Call<Token> tokenRequest;
    private Call<UserDetail> userDetailRequest;
    private Call<ProjectResults> projectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Copy ffmpeg license to where library expects it to be
        GeneralUtils.copyLicenseFromAssetsToSDIfNeeded(this, getApplicationContext().getFilesDir().getAbsolutePath() + "/");
        AppSharedPreferences.setDefaultValues(this);

        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e, "Failed getting version info");
        }
        Timber.i("Version name: %s code: %d", packageInfo != null ? packageInfo.versionName : "", packageInfo != null ? packageInfo.versionCode : 0);

        AppSharedPreferences sharedPreferences = new AppSharedPreferences(this);
        if (sharedPreferences.isLoggedIn()) {
            if (sharedPreferences.isMasterSlaveEnabled() && !sharedPreferences.isMaster()) {
                TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(new Intent(LoginActivity.this, CameraBlankActivity.class))
                        .startActivities();
            } else {
                Intent intentEnv = new Intent(LoginActivity.this, EnvironmentSettingsActivity.class);
                intentEnv.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(intentEnv)
                        .addNextIntentWithParentStack(new Intent(LoginActivity.this, VinSearchActivity.class))
                        .startActivities();
            }
            finish();
            return;
        }

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent) {
                if (action == EditorInfo.IME_ACTION_DONE) {
                    onLogin();
                    return true;
                }

                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        final String username = preferences.getRememberMeUsername();
        final String password = preferences.getRememberMePassword();
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            userEditText.setText(username);
            passwordEditText.setText(password);
        }

        rememberMeCheckBox.setChecked(true);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (tokenRequest != null) {
            tokenRequest.cancel();
            tokenRequest = null;
        }

        if (userDetailRequest != null) {
            userDetailRequest.cancel();
            userDetailRequest = null;
        }

        if (projectRequest != null) {
            projectRequest.cancel();
            projectRequest = null;
        }
    }

    @OnClick(R.id.login_button)
    public void onLoginClick() {
        onLogin();
    }

    private void onLogin() {
        final String username = userEditText.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            AppDialogFragment.show(this, R.string.dialog_login_no_username_title);
            return;
        }

        final String password = passwordEditText.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            AppDialogFragment.show(this, R.string.dialog_login_no_password_title);
            return;
        }

        if (!new Network(this).haveWiFi()) {
            AppDialogFragment.show(this, R.string.dialog_no_wifi_title, R.string.dialog_login_requires_wifi_message);
            return;
        }

        if (Util.connectedToTheta(this)) {
            AppDialogFragment.show(this, R.string.dialog_no_wifi_title, R.string.dialog_login_theta_detected);
            return;
        }

        startTokenRequest(username, password);
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Logging in");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void startTokenRequest(final String username, final String password) {
        TokenRequest requestData = new TokenRequest(username, password);

        showProgressDialog();
        tokenRequest = AuthWebService.getApi(this).postTokenRequest(requestData);
        tokenRequest.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                tokenRequest = null;
                final Token token = response.body();
                if (token == null) {
                    // Only dismiss on error because otherwise we are starting another network request.
                    hideProgressDialog();
                    displayError();
                    return;
                }

                startUserDetailRequest(username, password, token.token);
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                tokenRequest = null;
                hideProgressDialog();
                displayError();
            }

            private void displayError() {
                AppDialogFragment.show(LoginActivity.this, R.string.dialog_login_error_title, R.string.dialog_login_error_message);
            }
        });
    }

    private void startUserDetailRequest(final String username, final String password, final String token) {
        userDetailRequest = new AuthWebService().getUserDetail(this, "Token " + token, username);
        userDetailRequest.enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {
                userDetailRequest = null;
                final UserDetail detail = response.body();
                if (detail == null) {
                    hideProgressDialog();
                    displayError();
                    return;
                }

                if (!detail.is_active) {
                    hideProgressDialog();
                    AppDialogFragment.show(LoginActivity.this, R.string.dialog_inactive_user_title, R.string.dialog_inactive_user_message);
                    return;
                }

                List<Project> projects = getProjects(detail.dealer_permissions);
                if (projects.isEmpty()) {
                    hideProgressDialog();
                    AppDialogFragment.show(LoginActivity.this, "Missing Projects", "No projects are configured for user. Please contact SiSTeR Technologies.");
                    Util.logError("No projects for user");
                    return;
                }

                startNextProjectRequest(username, password, token, projects);
            }

            @Override
            public void onFailure(Call<UserDetail> call, Throwable t) {
                userDetailRequest = null;
                hideProgressDialog();
                displayError();
            }

            private void displayError() {
                AppDialogFragment.show(LoginActivity.this, R.string.dialog_login_error_title, R.string.dialog_login_error_message);
            }
        });
    }

    private void completeLogin(String username, String password, String token, List<Project> projects) {
        try {
            TableUtils.clearTable(getConnectionSource(), Project.class);
            Dao<Project, ?> dao = getHelper().getProjectDao();
            for (Project project : projects) {
                dao.create(project);
            }

            // The log file S3 path is created using the project and library so we wait until we are
            // logged in to initialize the logger.
            LogUploaderService.initialize(getApplicationContext());

            final AppSharedPreferences preferences = new AppSharedPreferences(LoginActivity.this);
            preferences.putCurrentUser(username);
            preferences.putToken(token);
            CrashReporter.setIdentifiers(getApplicationContext(), getHelper());

            if (rememberMeCheckBox.isChecked()) {
                preferences.putRememberMeUsername(username);
                preferences.putRememberMePassword(password);
            } else {
                preferences.putRememberMeUsername("");
                preferences.putRememberMePassword("");
            }

            new SettingsServerComms(this, getHelper()).startGetSettingsRequest(new SettingsServerComms.GetSettingsCompleteListener() {
                @Override
                public void onCompletion(boolean success) {
                    // Keep going regardless of whether we successfully downloaded a settings documents
                    // because we will try again later. We do a request here just so that the turntable
                    // option on the next screen will agree with what is in the settings document.
                    startActivity(new Intent(LoginActivity.this, EnvironmentSettingsActivity.class));
                }
            });
        } catch (SQLException e) {
            Util.logError("Database error creating user projects");
            AppDialogFragment.show(LoginActivity.this, R.string.dialog_projects_update_title, R.string.dialog_projects_update_message);
        } finally {
            hideProgressDialog();
        }
    }

    /*
     * Kick off network requests to get dealer name for each project.
     */
    private void startNextProjectRequest(String username, String password, String token, List<Project> projects) {
        for (Project project : projects) {
            if (project.getDealerName().isEmpty()) {
                startProjectRequest(project, username, password, token, projects);
                return;
            }
        }
        // If we got this far we have dealer names for all projects.
        completeLogin(username, password, token, projects);
    }

    private void startProjectRequest(final Project project, final String username, final String password, final String token, final List<Project> projects) {
        projectRequest = new ESWebService().getProjectSettings(this, project.getProject());
        projectRequest.enqueue(new Callback<ProjectResults>() {
            @Override
            public void onResponse(Call<ProjectResults> call, Response<ProjectResults> response) {
                projectRequest = null;
                final ProjectResults results = response.body();
                if (results == null) {
                    hideProgressDialog();
                    displayError();
                    return;
                }

                if (results.hits == null || results.hits.hits == null || results.hits.hits.isEmpty()) {
                    hideProgressDialog();
                    AppDialogFragment.show(LoginActivity.this, "Invalid Project", "An invalid project is configured for this user. Please contact SiSTeR Technologies.");
                    Util.logError("No project hits");
                    return;
                }

                ProjectResults.ProjectHit hit = results.hits.hits.get(0);
                if (hit._source == null || TextUtils.isEmpty(hit._source.dealer_name)) {
                    hideProgressDialog();
                    AppDialogFragment.show(LoginActivity.this, "Invalid Project", "An invalid project is configured for this user. Please contact SiSTeR Technologies.");
                    Util.logError("No project hits");
                    return;
                }

                project.setDealerName(hit._source.dealer_name);
                startNextProjectRequest(username, password, token, projects);
            }

            @Override
            public void onFailure(Call<ProjectResults> call, Throwable t) {
                projectRequest = null;
                hideProgressDialog();
                displayError();
            }

            private void displayError() {
                AppDialogFragment.show(LoginActivity.this, R.string.dialog_login_error_title, R.string.dialog_login_error_message);
            }
        });
    }

    private List<Project> getProjects(List<String> permissions) {
        ArrayList<Project> projects = new ArrayList<>();

        if (permissions == null) {
            return projects;
        }

        for (String permission : permissions) {
            // Permission looks like library:project, e.g., demolib:demoprj
            final String[] components = permission.split(":");
            if (components.length == 2) {
                final String library = components[0];
                final String project = components[1];
                projects.add(new Project(library, project));
            }
        }

        return projects;
    }
}
