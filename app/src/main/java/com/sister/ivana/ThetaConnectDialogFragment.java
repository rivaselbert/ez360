package com.sister.ivana;

import android.os.Bundle;

import com.sister.ivana.dialog.AppDialogFragment;

public class ThetaConnectDialogFragment extends AppDialogFragment {
    public static ThetaConnectDialogFragment newInstance() {
        ThetaConnectDialogFragment fragment = new ThetaConnectDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_no_theta_ssid_title);
        AppDialogFragment.setContentView(args, R.layout.dialog_missing_theta_ssid);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_ok);
        fragment.setArguments(args);
        return fragment;
    }
}
