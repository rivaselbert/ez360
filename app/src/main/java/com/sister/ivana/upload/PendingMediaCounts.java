package com.sister.ivana.upload;


public class PendingMediaCounts {
    private final String vin;
    private final long picturesCount;
    private final long fullMotionVideoCount;
    private final long spinItPictureCount;
    private final long ez360PictureCount;

    public PendingMediaCounts(String vin, long picturesCount, long fullMotionVideoCount, long spinItPictureCount, long ez360PictureCount) {
        this.vin = vin;
        this.picturesCount = picturesCount;
        this.fullMotionVideoCount = fullMotionVideoCount;
        this.spinItPictureCount = spinItPictureCount;
        this.ez360PictureCount = ez360PictureCount;
    }

    public PendingMediaCounts(String vin) {
        this.vin = vin;
        this.picturesCount = 0;
        this.fullMotionVideoCount = 0;
        this.spinItPictureCount = 0;
        this.ez360PictureCount = 0;
    }

    public String getVin() {
        return vin;
    }

    public long getPicturesCount() {
        return picturesCount;
    }

    public long getFullMotionVideoCount() {
        return fullMotionVideoCount;
    }

    public long getSpinItPictureCount() {
        return spinItPictureCount;
    }

    public long getEz360PictureCount() {
        return ez360PictureCount;
    }
}
