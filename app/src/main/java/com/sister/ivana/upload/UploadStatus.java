package com.sister.ivana.upload;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

import com.sister.ivana.Util;

import timber.log.Timber;

public class UploadStatus {
    public static final String ACTION_UPDATE_VIDEO_COMPRESSION = "UploadStatus.ACTION_UPDATE_VIDEO_COMPRESSION";
    public static final String ACTION_RESET_VIDEO_COMPRESSION = "UploadStatus.ACTION_RESET_VIDEO_COMPRESSION";
    public static final String ACTION_UPDATE_FRAME_EXTRACTION = "UploadStatus.ACTION_UPDATE_FRAME_EXTRACTION";
    public static final String ACTION_RESET_FRAME_EXTRACTION = "UploadStatus.ACTION_RESET_FRAME_EXTRACTION";

    public static final String EXTRA_YEAR = "UploadStatus.EXTRA_YEAR";
    public static final String EXTRA_MAKE = "UploadStatus.EXTRA_MAKE";
    public static final String EXTRA_MODEL = "UploadStatus.EXTRA_MODEL";

    public interface VehicleOperation {
        String getYear();

        String getMake();

        String getModel();
    }

    public static class Upload implements VehicleOperation {
        public enum State {
            UPLOADING_MEDIA, PROCESSING_MEDIA
        }

        private final String year;
        private final String make;
        private final String model;
        private final UploadType uploadType;
        private final State state;

        // Count of total files and completed files for this group.
        private final int totalFiles;
        private final int completedFiles;

        // Percent of current file that has been uploaded.
        private final int currentProgress;

        public Upload(String year, String make, String model, UploadType uploadType, int totalFiles, int completedFiles, int currentProgress) {
            this.year = year;
            this.make = make;
            this.model = model;
            this.uploadType = uploadType;
            this.totalFiles = totalFiles;
            this.completedFiles = completedFiles;
            this.currentProgress = currentProgress;
            this.state = State.UPLOADING_MEDIA;
        }

        public Upload(String year, String make, String model, State state) {
            this.year = year;
            this.make = make;
            this.model = model;
            this.uploadType = null;
            this.state = state;
            completedFiles = totalFiles = currentProgress = 0;
        }

        public String getYear() {
            return year;
        }

        public String getMake() {
            return make;
        }

        public String getModel() {
            return model;
        }

        public State getState() {
            return state;
        }

        public int getTotalFiles() {
            return totalFiles;
        }

        public int getCompletedFiles() {
            return completedFiles;
        }

        public int getCurrentProgress() {
            return currentProgress;
        }

        public String getDisplayUnits() {
            return uploadType.getDisplayString(totalFiles);
        }
    }

    public static class VideoCompressionOperation implements VehicleOperation {
        private final String year;
        private final String make;
        private final String model;

        public VideoCompressionOperation(String year, String make, String model) {
            this.year = year;
            this.make = make;
            this.model = model;
        }

        public String getYear() {
            return year;
        }

        public String getMake() {
            return make;
        }

        public String getModel() {
            return model;
        }
    }

    public static class FrameExtractionOperation implements VehicleOperation {
        private final String year;
        private final String make;
        private final String model;

        public FrameExtractionOperation(String year, String make, String model) {
            this.year = year;
            this.make = make;
            this.model = model;
        }

        public String getYear() {
            return year;
        }

        public String getMake() {
            return make;
        }

        public String getModel() {
            return model;
        }
    }

    private Upload currentUpload;
    private VideoCompressionOperation currentVideoCompressionOperation;
    private FrameExtractionOperation currentFrameExtractionOperation;

    // Number of vehicles that have any pending uploads
    private int pendingVehicleCount;

    private static UploadStatus status;
    private final Handler mainHandler;
    private final BroadcastReceiver broadcastReceiver;

    private UploadStatus(Context context) {
        mainHandler = new Handler(context.getApplicationContext().getMainLooper());
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                receiveBroadcast(intent);
            }
        };

        context.registerReceiver(broadcastReceiver, new IntentFilter(ACTION_UPDATE_VIDEO_COMPRESSION));
        context.registerReceiver(broadcastReceiver, new IntentFilter(ACTION_RESET_VIDEO_COMPRESSION));
        context.registerReceiver(broadcastReceiver, new IntentFilter(ACTION_UPDATE_FRAME_EXTRACTION));
        context.registerReceiver(broadcastReceiver, new IntentFilter(ACTION_RESET_FRAME_EXTRACTION));
    }

    public static UploadStatus getStatus(Context context) {
        if (status == null) {
            status = new UploadStatus(context.getApplicationContext());
        }

        return status;
    }

    public void updateVehicle(Upload upload) {
        currentUpload = upload;
        postUpdate();
    }

    public void updatePendingVehicleCount(int count) {
        pendingVehicleCount = count;
        postUpdate();
    }

    public void resetVehicle() {
        updateVehicle(null);
    }

    public void updateVideoCompressionOperation(VideoCompressionOperation operation) {
        currentVideoCompressionOperation = operation;
        postUpdate();
    }

    public VideoCompressionOperation getCurrentVideoCompressionOperation() {
        return currentVideoCompressionOperation;
    }

    public void resetVideoCompressionOperation() {
        updateVideoCompressionOperation(null);
    }

    public void updateFrameExtractionOperation(FrameExtractionOperation operation) {
        currentFrameExtractionOperation = operation;
        postUpdate();
    }

    public FrameExtractionOperation getCurrentFrameExtractionOperation() {
        return currentFrameExtractionOperation;
    }

    public void resetFrameExtractionOperation() {
        updateFrameExtractionOperation(null);
    }

    public Upload getCurrentUpload() {
        return currentUpload;
    }

    public int getPendingVehicleCount() {
        return pendingVehicleCount;
    }

    private void receiveBroadcast(Intent intent) {
        final String action = intent.getAction();
        if (action == null) {
            Timber.e("Received broadcast with null action");
            return;
        }

        if (action.contentEquals(ACTION_UPDATE_VIDEO_COMPRESSION)) {
            final String year = intent.getStringExtra(EXTRA_YEAR);
            final String make = intent.getStringExtra(EXTRA_MAKE);
            final String model = intent.getStringExtra(EXTRA_MODEL);

            final VideoCompressionOperation operation = new VideoCompressionOperation(year, make, model);
            updateVideoCompressionOperation(operation);
        } else if (action.contentEquals(ACTION_RESET_VIDEO_COMPRESSION)) {
            resetVideoCompressionOperation();
        } else if (action.contentEquals(ACTION_UPDATE_FRAME_EXTRACTION)) {
            final String year = intent.getStringExtra(EXTRA_YEAR);
            final String make = intent.getStringExtra(EXTRA_MAKE);
            final String model = intent.getStringExtra(EXTRA_MODEL);

            final FrameExtractionOperation operation = new FrameExtractionOperation(year, make, model);
            updateFrameExtractionOperation(operation);

        } else if (action.contentEquals(ACTION_RESET_FRAME_EXTRACTION)){
            resetFrameExtractionOperation();
        }
    }

    private void postUpdate() {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                Util.getBus().post(UploadStatus.this);
            }
        });
    }
}
