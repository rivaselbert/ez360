package com.sister.ivana.upload;


import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.j256.ormlite.stmt.PreparedQuery;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.Util;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.UploadFile;
import com.sister.ivana.database.UploadGroup;
import com.sister.ivana.logging.AppLogger;
import com.sister.ivana.logging.LogUploader;

import net.sourceforge.opencamera.ImageDescription;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

import static com.sister.ivana.upload.UploadType.create;

class Uploader extends Handler {
    interface UploaderListener {
        void uploadsComplete(boolean reschedule);

        void uploadEnqueueComplete(int startID);
    }

    private static class ProgressData {
        int id;
        long bytesCurrent;
        long bytesTotal;
    }

    private static class MediaTransferListener implements TransferListener {
        private Uploader uploader;

        public MediaTransferListener(Uploader uploader) {
            this.uploader = uploader;
        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Timber.d("onStateChanged id: %d state: %s", id, state);
            if (uploader != null) {
                uploader.sendStateChanged(id, state);
            }
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Timber.d("onProgressChanged id: %d bytesCurrent: %d bytesTotal: %d", id, bytesCurrent, bytesTotal);
            if (uploader != null) {
                uploader.sendProgressChanged(id, bytesCurrent, bytesTotal);
            }
        }

        @Override
        public void onError(int id, Exception ex) {
            Timber.e(ex, "onError id: " + id);
        }
    }

    private static final int MESSAGE_TYPE_ENQUEUE_UPLOAD = 1;
    private static final int MESSAGE_TYPE_UPLOAD_STATE_CHANGE = 2;
    private static final int MESSAGE_TYPE_UPLOAD_PROGRESS_CHANGE = 3;
    private static final int MESSAGE_TYPE_UPLOADS_ENABLED_CHANGE = 4;
    private static final int MESSAGE_TYPE_START = 5;
    private static final int MESSAGE_TYPE_STOP = 6;
    private static final int MESSAGE_TYPE_DELETE_FILES = 7;
    private static final int MESSAGE_TYPE_COMPRESSION_COMPLETE = 8;
    private static final int MESSAGE_TYPE_FRAME_EXTRACTION_COMPLETE = 9;

    private final AmazonS3Client amazonS3Client;
    private final TransferUtility transferUtility;
    private final UploaderListener uploadListener;
    private final Handler mainHandler;
    private final UploadStatus uploadStatus;
    private final Context context;
    private final UploadQueue uploadQueue;
    private Call<Void> completionRequest;
    private UploadFile currentFile;
    private UploadGroup currentExtraction;
    private final MediaTransferListener listener;

    public Uploader(Looper looper,
                    UploaderListener uploadListener,
                    Context context,
                    DatabaseHelper helper) throws SQLException {
        super(looper);
        this.context = context.getApplicationContext();
        listener = new MediaTransferListener(this);
        amazonS3Client = new AmazonS3Client(new BasicAWSCredentials(AppSharedPreferences.AWS_ACCESS, AppSharedPreferences.AWS_SECRET));
        transferUtility = new TransferUtility(amazonS3Client, context);
        uploadStatus = UploadStatus.getStatus(context);
        uploadQueue = new UploadQueue(context, helper);
        this.uploadListener = uploadListener;
        mainHandler = new Handler(context.getMainLooper());
    }

    private static String getMessageTypeString(int type) {
        String str;

        switch (type) {
            case MESSAGE_TYPE_ENQUEUE_UPLOAD:
                str = "MESSAGE_TYPE_ENQUEUE_UPLOAD";
                break;
            case MESSAGE_TYPE_UPLOAD_STATE_CHANGE:
                str = "MESSAGE_TYPE_UPLOAD_STATE_CHANGE";
                break;
            case MESSAGE_TYPE_UPLOAD_PROGRESS_CHANGE:
                str = "MESSAGE_TYPE_UPLOAD_PROGRESS_CHANGE";
                break;
            case MESSAGE_TYPE_UPLOADS_ENABLED_CHANGE:
                str = "MESSAGE_TYPE_UPLOADS_ENABLED_CHANGE";
                break;
            case MESSAGE_TYPE_START:
                str = "MESSAGE_TYPE_START";
                break;
            case MESSAGE_TYPE_STOP:
                str = "MESSAGE_TYPE_STOP";
                break;
            case MESSAGE_TYPE_DELETE_FILES:
                str = "MESSAGE_TYPE_DELETE_FILES";
                break;
            case MESSAGE_TYPE_COMPRESSION_COMPLETE:
                str = "MESSAGE_TYPE_COMPRESSION_COMPLETE";
                break;
            case MESSAGE_TYPE_FRAME_EXTRACTION_COMPLETE:
                str = "MESSAGE_TYPE_FRAME_EXTRACTION_COMPLETE";
                break;
            default:
                str = Integer.toString(type);
        }
        return str;
    }

    @Override
    public void handleMessage(Message message) {
        Timber.d("handleMessage message: %s", getMessageTypeString(message.what));

        Intent intent;
        switch (message.what) {
            case MESSAGE_TYPE_START:
                processQueue();
                break;

            case MESSAGE_TYPE_ENQUEUE_UPLOAD:
                intent = (Intent) message.obj;
                // All we do here is add the upload information to the database. The rest of the
                // execution is controlled via the JobScheduler.
                uploadQueue.enqueue(intent);

                final int startID = message.arg1;
                notifyUploadEnqueueComplete(startID);
                break;

            case MESSAGE_TYPE_DELETE_FILES:
                intent = (Intent) message.obj;
                deleteFiles(intent.getStringArrayListExtra(UploaderService.EXTRA_PATHNAMES));
                notifyUploadEnqueueComplete(message.arg1);
                break;

            case MESSAGE_TYPE_COMPRESSION_COMPLETE:
                intent = (Intent) message.obj;
                long requestCode = intent.getLongExtra(UploaderService.EXTRA_COMPRESSION_REQUEST_CODE, 0);
                String pathname = intent.getStringExtra(UploaderService.EXTRA_COMPRESSION_PATHNAME);
                boolean success = intent.getBooleanExtra(UploaderService.EXTRA_COMPRESSION_SUCCESS, false);
                uploadQueue.updateCompressionStatus(requestCode, pathname, success);

                // This is handled like an enqueue operation since new content should be ready for
                // upload.
                notifyUploadEnqueueComplete(message.arg1);
                break;

            case MESSAGE_TYPE_FRAME_EXTRACTION_COMPLETE:
                intent = (Intent) message.obj;
                long extractionCode = intent.getLongExtra(UploaderService.EXTRA_FRAME_EXTRACTION_REQUEST_CODE, 0);
                boolean extractionSuccess = intent.getBooleanExtra(UploaderService.EXTRA_FRAME_EXTRACTION_SUCCESS, false);
                ArrayList<ImageDescription> pictures = intent.getParcelableArrayListExtra(UploaderService.EXTRA_PICTURES);
                uploadQueue.updateFrameExtractionStatus(extractionCode, extractionSuccess, pictures);

                // This is handled like an enqueue operation since new images should be ready.
                notifyUploadEnqueueComplete(message.arg1);
                break;

            case MESSAGE_TYPE_UPLOAD_STATE_CHANGE:
                onStateChanged(message.arg1, (TransferState) message.obj);
                break;

            case MESSAGE_TYPE_UPLOAD_PROGRESS_CHANGE:
                final ProgressData data = (ProgressData) message.obj;
                onProgressChanged(data.id, data.bytesCurrent, data.bytesTotal);
                break;

            case MESSAGE_TYPE_UPLOADS_ENABLED_CHANGE:
                processQueue();
                break;

            case MESSAGE_TYPE_STOP:
                stopTransfers();
                synchronized (this) {
                    notifyAll();
                }
                break;
        }
    }

    private void stopTransfers() {
        List<TransferObserver> uploads = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        for (TransferObserver observer : uploads) {
            if (!LogUploader.isLogTransfer(observer)) {
                Timber.d("pausing %s", observer);
                observer.cleanTransferListener();
                transferUtility.pause(observer.getId());
            }
        }

        if (completionRequest != null) {
            Timber.d("stopTransfers canceling completionRequest");

            // This is potentially a long operation and anything we do here blocks the main thread
            // so just cancel it.
            completionRequest.cancel();
        }

        uploadStatus.resetVehicle();
    }

    public void sendUploadIntent(Intent intent, int startID) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_ENQUEUE_UPLOAD;
        message.arg1 = startID;
        message.obj = intent;
        sendUploaderMessage(message);
    }

    public void sendDeleteIntent(Intent intent, int startID) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_DELETE_FILES;
        message.arg1 = startID;
        message.obj = intent;
        sendUploaderMessage(message);
    }

    public void sendCompressionCompleteIntent(Intent intent, int startID) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_COMPRESSION_COMPLETE;
        message.arg1 = startID;
        message.obj = intent;
        sendUploaderMessage(message);
    }

    public void sendFrameExtractionCompleteIntent(Intent intent, int startID) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_FRAME_EXTRACTION_COMPLETE;
        message.arg1 = startID;
        message.obj = intent;
        sendUploaderMessage(message);
    }

    public void start() {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_START;
        sendUploaderMessage(message);
    }

    public void sendProcessQueue() {
        start();
    }

    synchronized public void stop() {
        Timber.d("stop entered");
        // Sometimes the transfer utility sends us updates after we have called cleanTransferUtility
        // and are unable to handle them because the looper has been shut down. Setting uploader
        // to null prevents this. No synchronization is needed because stop gets called on the
        // main thread and the transfer utility makes its callbacks on the main thread
        listener.uploader = null;

        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_STOP;
        sendUploaderMessage(message);

        // JobScheduler stops the service after we process this stop request so we have
        // to wait here.
        for (; ; ) {
            try {
                wait();

                Timber.d("stopping looper");
                getLooper().quit();
                return;
            } catch (InterruptedException ignored) {

            }
        }
    }

    /*
     * Tell event loop that the enabled status might have changed.
     */
    void sendPreferencesChanged() {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_UPLOADS_ENABLED_CHANGE;
        sendUploaderMessage(message);
    }

    private void sendStateChanged(int id, TransferState state) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_UPLOAD_STATE_CHANGE;
        message.arg1 = id;
        message.obj = state;
        sendUploaderMessage(message);
    }

    private void sendProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_UPLOAD_PROGRESS_CHANGE;
        final ProgressData data = new ProgressData();
        data.id = id;
        data.bytesCurrent = bytesCurrent;
        data.bytesTotal = bytesTotal;
        message.obj = data;
        sendUploaderMessage(message);
    }

    synchronized private void sendUploaderMessage(Message message) {
        sendMessage(message);
    }

    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    private void processQueue() {
        Timber.d("processQueue: entered");

        if (!new AppSharedPreferences(context).areUploadsEnabled()) {
            Timber.d("processQueue: uploads disabled");
            notifyUploadsComplete(false);
            return;
        }

        if (!isExternalStorageReadable()) {
            Timber.d("processQueue: external storage not readable");
            notifyUploadsComplete(true);
            return;
        }

        uploadQueue.deleteCompletedGroups();

        if (completionRequest != null) {
            Timber.d("processQueue: waiting for pending competion request %s", completionRequest);
            return;
        }

        if (currentFile != null) {
            // Row might have been updated when compression operation completed.
            uploadQueue.refresh(currentFile);

            Timber.d("processQueue: currentFile: %s", currentFile);
            processCurrentFile();
            return;
        }

        if (currentExtraction != null) {
            // Row might have been updated when frame extraction completed.
            uploadQueue.refresh(currentExtraction);

            if (currentExtraction.getUploadStatus() != UploadGroup.UploadGroupStatus.EXTRACTION_PENDING) {
                Timber.d("processQueue: found completed extraction %s", currentExtraction);
                currentExtraction = null;
            } else {
                Timber.d("processQueue: frame extraction in progress for %s", currentExtraction);
                processFrameExtraction(currentExtraction);
                return;
            }
        }

        // Process groups that have uploaded all files.
        final UploadGroup uploadGroup = uploadQueue.nextGroup();
        if (uploadGroup != null) {
            Timber.d("processQueue: uploadGroup: %s", uploadGroup);
            startCompletionRequest(uploadGroup);
            killBackgroundProcesses();
            return;
        }

        // Process paused file uploads
        if (resumeExistingTransfers()) {
            Timber.d("processQueue: resumed transfers");
            return;
        }

        // We pick either the next frame extraction or the next file to upload based on timestamp
        // so that we extract all of the frames in a group and then upload them before extracting
        // the next group.
        final UploadGroup nextExtraction = uploadQueue.nextFrameExtractionGroup();
        final UploadFile nextFile = uploadQueue.nextFile();

        if (nextExtraction != null &&
                (nextFile == null
                        || UploadType.create(nextExtraction.getUploadType()).getUploadPriority() < UploadType.create(nextFile.getUploadGroup().getUploadType()).getUploadPriority()
                        || nextExtraction.getTimestamp() < nextFile.getTimestamp())) {
            currentExtraction = nextExtraction;
            Timber.d("processQueue: next frame extraction: %s", currentExtraction);
            processFrameExtraction(currentExtraction);
            return;
        }

        currentFile = nextFile;
        if (currentFile != null) {
            Timber.d("processQueue: nextFile: %s", currentFile);
            processCurrentFile();
            return;
        }

        Timber.d("processQueue: completing");

        // If we got this far there is nothing left to do.
        final boolean reschedule = uploadQueue.uploadFailuresExist();
        notifyUploadsComplete(reschedule);
    }

    private void processCurrentFile() {
        if (!new File(currentFile.getPathname()).exists()) {
            // We are not sure yet why, but at one site we had several vehicles that were
            // stuck uploading because the image file no longer existed. If we get into
            // this state remove the record for the file that doesn't exist and keep going.
            Timber.e("processFile found nonexistent file %s", currentFile);
            uploadQueue.removeUploadFile(currentFile);
            currentFile = null;
            processQueue();
            return;
        }

        final Integer transferID = currentFile.getTransferID();
        if (transferID != null) {
            // Transfer is in progress so we need to wait for it to complete.
            Timber.d("processFile: transfer in progress id: %d file: %s", transferID, currentFile.getPathname());
            return;
        }

        if (!currentFile.getCompressedPathname().isEmpty()) {
            Timber.d("processFile: compression complete for file: %s", currentFile.getPathname());
            upload(currentFile);
            return;
        }

        final UploadType uploadType = UploadType.create(currentFile.getUploadGroup().getUploadType());
        if (!uploadType.requiresCompression()) {
            Timber.d("processFile: compression not required for file: %s", currentFile.getPathname());
            upload(currentFile);
            return;
        }

        if (currentFile.getCompressionRequestCode() == 0) {
            Timber.d("processFile: compression required for file: %s", currentFile.getPathname());

            if (UploadType.create(currentFile.getUploadGroup().getUploadType()) != UploadType.FULL_MOTION_VIDEO) {
                // The photo compression happens very quickly (~100ms) so it looks jerky to display
                // a separate status in the status bar. Instead we'll lump it together with the
                // upload status.
                publishUpdate(currentFile, 0);
            }
            
            uploadQueue.startCompressionRequest(currentFile);
            return;
        }

        final long timeout;
        if (uploadType.equals(UploadType.FULL_MOTION_VIDEO) || (uploadType.equals(UploadType.ANIMATED_PICTURE) && currentFile.getPathname().endsWith(".mp4"))) {
            timeout = MediaProcessingService.VIDEO_COMPRESSION_TIMEOUT_MS;
        } else {
            timeout = MediaProcessingService.PICTURE_COMPRESSION_TIMEOUT_MS;
        }

        final long now = System.currentTimeMillis();
        final long timestamp = currentFile.getCompressionRequestTimestamp();
        if (now - timestamp > timeout) {
            Timber.d("processFile: compression timed out timestamp: %d now: %d file: %s", timestamp, now, currentFile.getPathname());
            uploadQueue.startCompressionRequest(currentFile);
            return;
        }

        Timber.d("processFile: compression in progress id: %d file: %s", currentFile.getCompressionRequestCode(), currentFile.getPathname());
    }

    private void killBackgroundProcesses() {
        final ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        manager.killBackgroundProcesses(context.getPackageName());
    }

    private void processFrameExtraction(UploadGroup uploadGroup) {
        if (uploadGroup.getExtractionRequestCode() == 0) {
            Timber.d("processFrameExtraction: extraction required for file: %s", uploadGroup.getSourceVideoPathname());
            uploadQueue.startExtractionRequest(uploadGroup);
            return;
        }

        final long now = System.currentTimeMillis();
        final long timestamp = uploadGroup.getExtractionRequestTimestamp();
        if (now - timestamp > MediaProcessingService.FRAME_EXTRACTION_TIMEOUT_MS) {
            Timber.d("processFrameExtraction: extraction timed out timestamp: %d now: %d file: %s", timestamp, now, uploadGroup.getSourceVideoPathname());
            uploadQueue.startExtractionRequest(uploadGroup);
            return;
        }

        Timber.d("processFrameExtraction: extraction in progress id: %d file: %s", uploadGroup.getExtractionRequestCode(), uploadGroup.getSourceVideoPathname());
    }

    private boolean resumeExistingTransfers() {
        List<TransferObserver> uploads = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        if (uploads.isEmpty()) {
            Timber.d("no existing uploads to resume");
            return false;
        }

        // Existing transfers can be in almost any state here. We try to pause it when we stop,
        // but it seems like there are race conditions in the transfer utility and we can't always
        // count on them being in the PAUSED state.
        boolean transferInProgress = false;
        for (TransferObserver observer : uploads) {
            Timber.d("found existing transfer %s", AppLogger.getTransferObserverString(observer));

            if (LogUploader.isLogTransfer(observer)) {
                Timber.d("ignoring log file upload");
                continue;
            }

            // Make sure we are an observer in case we are starting the service with an
            // existing upload.
            observer.setTransferListener(listener);
            final TransferState state = observer.getState();
            switch (state) {
                case PAUSED:
                    // We pause transfers when we lose Wi-Fi or when the user disabled uploads.
                    currentFile = uploadQueue.getUploadFile(observer.getId());
                    transferUtility.resume(observer.getId());

                    // Indicate that we want to wait for this transfer to reach a final state
                    // of COMPLETED or FAILED.
                    transferInProgress = true;
                    break;

                case COMPLETED:
                    // This could happen if the transfer utility completed an upload while
                    // this service wasn't running.
                    onStateChanged(observer.getId(), state);
                    break;

                case FAILED:
                    onStateChanged(observer.getId(), state);
                    break;

                default:
                    // Any other state should be a transitive state that will eventually result
                    // in COMPLETED or FAILED so indicate that we are uploading.
                    transferInProgress = true;
            }
        }

        return transferInProgress;
    }

    private void notifyUploadsComplete(final boolean reschedule) {
        // Reset error status so that we'll retry next time we run.
        uploadQueue.resetErrorStatus();

        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                uploadListener.uploadsComplete(reschedule);
            }
        });
    }

    private void notifyUploadEnqueueComplete(final int startID) {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                uploadListener.uploadEnqueueComplete(startID);
            }
        });
    }

    private void upload(UploadFile uploadFile) {
        final File file;
        if (!uploadFile.getCompressedPathname().isEmpty()) {
            file = new File(uploadFile.getCompressedPathname());
        } else {
            file = new File(uploadFile.getPathname());
        }

        if (!file.exists()) {
            Timber.i("upload file no longer exists: %s", file.getAbsoluteFile());
            // If the file has been removed from external media then delete this record.
            currentFile = null;
            uploadQueue.removeUploadFile(uploadFile);

            processQueue();
            return;
        }

        final UploadType uploadType = create(uploadFile.getUploadGroup().getUploadType());
        final TransferObserver observer = transferUtility.upload(uploadType.getAWSBucket(), uploadFile.getAWSKey(), file, CannedAccessControlList.PublicRead);

        Timber.d("upload observer: %s", AppLogger.getTransferObserverString(observer));

        uploadQueue.setTransferID(uploadFile, observer.getId());
        observer.setTransferListener(listener);
    }

    private void startCompletionRequest(final UploadGroup group) {
        Timber.d("startCompletionRequest group: %s", group);

        publishProcessingStart(group);
        final UploadType uploadType = UploadType.create(group.getUploadType());

        completionRequest = uploadType.getUploadRequest(context, amazonS3Client, group);
        completionRequest.enqueue(new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response != null) {
                    final int code = response.code();
                    Timber.d("startCompletionRequest HTTP code: %d", code);
                    if (code < 200 || code >= 300) {
                        Util.logError("Error posting media: " + code);

                        post(new Runnable() {
                            @Override
                            public void run() {
                                handleCompletionFailure(group);
                            }
                        });
                        return;
                    }

                    post(new Runnable() {
                        @Override
                        public void run() {
                            handleCompletionSuccess(group);
                        }
                    });
                } else {
                    Util.logError("Invalid response posting media");
                    post(new Runnable() {
                        @Override
                        public void run() {
                            handleCompletionFailure(group);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                completionRequest = null;
                Util.logError("Failed posting media", t);
                post(new Runnable() {
                    @Override
                    public void run() {
                        handleCompletionFailure(group);
                    }
                });
            }
        });
    }

    private void handleCompletionSuccess(UploadGroup group) {
        Timber.d("handleCompletionSuccess group: %s", group);
        completionRequest = null;
        uploadStatus.resetVehicle();
        uploadQueue.setUploadGroupComplete(group);
        processQueue();
    }

    private void handleCompletionFailure(UploadGroup group) {
        Timber.d("handleCompletionFailure group: %s", group);
        completionRequest = null;
        uploadStatus.resetVehicle();
        uploadQueue.setUploadGroupFailed(group);
        processQueue();
    }

    private void onStateChanged(int id, TransferState state) {
        Timber.d("onStateChanged id:%d state: %s", id, state);
        switch (state) {
            case COMPLETED:
                completeFileUpload(id, true);
                break;

            case FAILED:
                completeFileUpload(id, false);
                break;

            case IN_PROGRESS:
                onProgressChanged(id, 0, 1);
                break;

            case PAUSED:
                processQueue();
                break;
        }
    }

    private void completeFileUpload(int id, boolean successful) {
        transferUtility.deleteTransferRecord(id);

        final UploadFile file = uploadQueue.getUploadFile(id);
        if (file != null) {
            if (successful) {
                uploadQueue.setUploadComplete(file);
            } else {
                uploadQueue.setUploadFailed(file);
            }

            uploadQueue.updateGroupCompletionStatus(file.getUploadGroup());
        }

        currentFile = null;
        processQueue();
    }

    private void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        final int progress = Math.round(((float) bytesCurrent / bytesTotal) * 100);
        final UploadFile file = uploadQueue.getUploadFile(id);
        if (file != null) {
            publishUpdate(file, progress);
        }
    }

    private void publishUpdate(UploadFile uploadFile, int progress) {
        final UploadGroup group = uploadFile.getUploadGroup();

        int complete = 0;
        int total = 0;
        for (UploadFile file : group.getFiles()) {
            if (file.isUploadComplete()) {
                complete++;
            }
            total++;
        }

        final UploadStatus.Upload upload = new UploadStatus.Upload(group.getYear(),
                group.getMake(),
                group.getModel(),
                create(group.getUploadType()),
                total,
                complete,
                progress);
        uploadStatus.updateVehicle(upload);
    }

    private void publishProcessingStart(UploadGroup group) {
        final UploadStatus.Upload upload = new UploadStatus.Upload(group.getYear(), group.getMake(), group.getModel(), UploadStatus.Upload.State.PROCESSING_MEDIA);
        uploadStatus.updateVehicle(upload);
    }

    private void deleteFiles(ArrayList<String> pathnames) {
        for (String pathname : pathnames) {
            if (currentFile != null && (currentFile.getPathname().contentEquals(pathname) || currentFile.getCompressedPathname().contentEquals(pathname))) {
                // Don't try to delete the file we are working on so that we don't put the S3
                // service in a weird state.
                continue;
            }

            uploadQueue.removePendingUploadFile(pathname);
        }
    }
}
