package com.sister.ivana.upload;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;

import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.Util;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseJobService;

import net.sourceforge.opencamera.ImageDescription;

import org.w3c.dom.Text;

import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class UploaderService extends OrmLiteBaseJobService<DatabaseHelper> {
    private static final String ACTION_SCHEDULE_UPLOADER = "ACTION_SCHEDULE_UPLOADER";
    private static final String ACTION_ENQUEUE_MEDIA = "ACTION_ENQUEUE_MEDIA";
    private static final String ACTION_DELETE_MEDIA = "ACTION_DELETE_MEDIA";
    private static final String ACTION_RECEIVE_MEDIA_COMPRESSION_RESULT = "ACTION_RECEIVE_MEDIA_COMPRESSION_RESULT";
    private static final String ACTION_RECEIVE_FRAME_EXTRACTION_RESULT = "ACTION_RECEIVE_FRAME_EXTRACTION_RESULT";
    private static final String ACTION_RECEIVE_GIF_CONVERSION_RESULT = "ACTION_RECEIVE_GIF_CONVERSION_RESULT";
    static final String EXTRA_VIN = "EXTRA_VIN";
    static final String EXTRA_PROJECT = "EXTRA_PROJECT";
    static final String EXTRA_LIBRARY = "EXTRA_LIBRARY";
    static final String EXTRA_YEAR = "EXTRA_YEAR";
    static final String EXTRA_MAKE = "EXTRA_MAKE";
    static final String EXTRA_MODEL = "EXTRA_MODEL";
    static final String EXTRA_REPLACE = "EXTRA_REPLACE";
    static final String EXTRA_PATHNAMES = "EXTRA_PATHNAMES";
    static final String EXTRA_UPLOAD_TYPE = "EXTRA_UPLOAD_TYPE";
    static final String EXTRA_PRIORITY = "EXTRA_PRIORITY";
    static final String EXTRA_COMPRESSION_REQUEST_CODE = "EXTRA_COMPRESSION_REQUEST_CODE";
    static final String EXTRA_COMPRESSION_SUCCESS = "EXTRA_COMPRESSION_SUCCESS";
    static final String EXTRA_COMPRESSION_PATHNAME = "EXTRA_COMPRESSION_PATHNAME";
    static final String EXTRA_PICTURES = "EXTRA_PICTURES";
    static final String EXTRA_USED_TURNTABLE = "EXTRA_USED_TURNTABLE";
    static final String EXTRA_EXTRACT_PICTURES = "EXTRA_EXTRACT_PICTURES";
    static final String EXTRA_FRAME_COUNT = "EXTRA_FRAME_COUNT";
    static final String EXTRA_FRAME_EXTRACTION_REQUEST_CODE = "EXTRA_FRAME_EXTRACTION_REQUEST_CODE";
    static final String EXTRA_FRAME_EXTRACTION_SUCCESS = "EXTRA_FRAME_EXTRACTION_SUCCESS";
    static final String EXTRA_VIDEO_SOURCE = "EXTRA_VIDEO_SOURCE";
    static final String EXTRA_AUTOMATED_PICTURE_COUNT = "EXTRA_AUTOMATED_PICTURE_COUNT";
    static final String EXTRA_SPIN_DURATION = "EXTRA_SPIN_DURATION";
    static final String EXTRA_EXTRACTED_PICTURE_COUNT = "EXTRA_EXTRACTED_PICTURE_COUNT";
    static final String EXTRA_CAMERA_ROLE = "EXTRA_CAMERA_ROLE";
    static final String EXTRA_SPIN_NUMBER = "EXTRA_SPIN_NUMBER";
    static final String EXTRA_SPIN_INDEX = "EXTRA_SPIN_INDEX";
    static final String EXTRA_ADD_TO_MAGAZINE = "EXTRA_ADD_TO_MAGAZINE";
    static final String EXTRA_TITLE = "EXTRA_TITLE";
    static final String EXTRA_DESCRIPTION = "EXTRA_DESCRIPTION";
    static final String EXTRA_COORDINATE_X = "EXTRA_COORDINATE_X";
    static final String EXTRA_COORDINATE_Y = "EXTRA_COORDINATE_Y";

    private static final int SCHEDULER_JOB_ID = 111;

    private Uploader uploader;
    private JobParameters jobParameters;
    private boolean isScheduledJobRunning;
    private AppSharedPreferences preferences;

    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            uploader.sendPreferencesChanged();
        }
    };

    private final Uploader.UploaderListener listener = new Uploader.UploaderListener() {
        @Override
        public void uploadsComplete(boolean reschedule) {
            Timber.d("uploadsComplete reschedule: %b jobParameters: %s", reschedule, jobParameters);

            if (jobParameters != null) {
                jobFinished(jobParameters, reschedule);
                jobParameters = null;
            }
        }

        @Override
        public void uploadEnqueueComplete(int startID) {
            Timber.d("uploadEnqueueComplete startID: %d", startID);

            scheduleService(UploaderService.this);

            // Note that this only controls the lifetime controlled by intents. The job scheduler
            // uses binding and so is unaffected by this.
            stopSelf(startID);
        }
    };

    private void scheduleService(Context context) {
        final boolean uploadsEnabled = new AppSharedPreferences(context).areUploadsEnabled();

        Timber.d("scheduleService uploadsEnabled: %b isScheduledJobRunning: %b", uploadsEnabled, isScheduledJobRunning);

        if (isScheduledJobRunning) {
            // If the service is already running because of the job scheduler just send a command
            // to process the queue.
            if (uploader != null) {
                uploader.sendProcessQueue();
            }
        } else if (uploadsEnabled) {
            final JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

            // We used to schedule the job every time since the docs say that it will replace the
            // pending job. However, if you replace the existing job sometimes your job won't run
            // again for many minutes so it works better to check if a job is pending.
            List<JobInfo> jobs = scheduler.getAllPendingJobs();
            for (JobInfo info : jobs) {
                if (info.getId() == SCHEDULER_JOB_ID) {
                    return;
                }
            }

            // Schedule to run when Wi-Fi is available. Scheduling the job when it is already running
            // causes the scheduler to stop the job so we keep track of our running status to guard against that.
            final JobInfo.Builder builder = new JobInfo.Builder(SCHEDULER_JOB_ID, new ComponentName(context, UploaderService.class));
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
            builder.setPersisted(true);
            builder.setBackoffCriteria(3000, JobInfo.BACKOFF_POLICY_LINEAR);

            int result = scheduler.schedule(builder.build());
            Timber.i("Scheduled job to run: %d", result);
        }
    }

    private static Intent createBaseIntent(Context context, UploadType uploadType, String vin, String year, String make, String model, String project, String library, boolean replace) {
        if (TextUtils.isEmpty(vin)
                || TextUtils.isEmpty(project)
                || TextUtils.isEmpty(library)) {
            throw new InvalidParameterException("Bad upload parameters");

        }
        final Intent intent = new Intent(context, UploaderService.class);
        intent.setAction(ACTION_ENQUEUE_MEDIA);
        intent.putExtra(EXTRA_UPLOAD_TYPE, uploadType.getDatabaseCode());
        intent.putExtra(EXTRA_PRIORITY, uploadType.getUploadPriority());
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_PROJECT, project);
        intent.putExtra(EXTRA_YEAR, year);
        intent.putExtra(EXTRA_MAKE, make);
        intent.putExtra(EXTRA_MODEL, model);
        intent.putExtra(EXTRA_LIBRARY, library);
        intent.putExtra(EXTRA_REPLACE, replace);
        intent.putExtra(EXTRA_CAMERA_ROLE, "");
        return intent;
    }

    public static Intent createFullMotionVideoIntent(Context context, String vin, String year, String make, String model, String project, String library, boolean replace, String pathname) {
        if (TextUtils.isEmpty(pathname)) {
            throw new InvalidParameterException("Missing pathname");
        }

        final Intent intent = createBaseIntent(context, UploadType.FULL_MOTION_VIDEO, vin, year, make, model, project, library, replace);

        ArrayList<String> pathnames = new ArrayList<>();
        pathnames.add(pathname);
        intent.putExtra(EXTRA_PATHNAMES, pathnames);

        return intent;
    }

    public static Intent createDetailPicturesIntent(Context context, String vin, String year, String make, String model, String project, String library, boolean replace, ArrayList<ImageDescription> images) {
        if (images == null || images.isEmpty()) {
            throw new InvalidParameterException("Bad upload parameters");
        }

        final Intent intent = createBaseIntent(context, UploadType.DETAIL_IMAGES, vin, year, make, model, project, library, replace);
        intent.putExtra(EXTRA_PICTURES, images);
        return intent;
    }

    public static Intent createAddPoiIntent(Context context, String vin, String year, String make, String model, String project, String library, boolean replace, String title, String description, String pathname, int spinNumber, int spinIndex, boolean addToMagazine, double x, double y) {
        if (TextUtils.isEmpty(pathname)) {
            throw new InvalidParameterException("Missing pathname");
        }

        final Intent intent = createBaseIntent(context, UploadType.ADD_EXTERIOR_POI, vin, year, make, model, project, library, replace);
        ArrayList<String> pathnames = new ArrayList<>();
        pathnames.add(pathname);
        intent.putExtra(EXTRA_PATHNAMES, pathnames);
        intent.putExtra(EXTRA_TITLE, title);
        intent.putExtra(EXTRA_DESCRIPTION, description);
        intent.putExtra(EXTRA_SPIN_NUMBER, spinNumber);
        intent.putExtra(EXTRA_SPIN_INDEX, spinIndex);
        intent.putExtra(EXTRA_ADD_TO_MAGAZINE, addToMagazine);
        intent.putExtra(EXTRA_COORDINATE_X, x);
        intent.putExtra(EXTRA_COORDINATE_Y, y);

        return intent;
    }

    public static Intent createPipIntent(Context context, String vin, String year, String make, String model, String project, String library, boolean replace, String wideImagePathname, String detailImagePathname, double x, double y) {
        if (TextUtils.isEmpty(wideImagePathname) || TextUtils.isEmpty(detailImagePathname)) {
            throw new InvalidParameterException("Missing pathname");
        }

        final Intent intent = createBaseIntent(context, UploadType.PIP, vin, year, make, model, project, library, replace);
        ArrayList<String> pathnames = new ArrayList<>();
        pathnames.add(wideImagePathname);
        pathnames.add(detailImagePathname);
        intent.putExtra(EXTRA_PATHNAMES, pathnames);
        intent.putExtra(EXTRA_COORDINATE_X, x);
        intent.putExtra(EXTRA_COORDINATE_Y, y);

        return intent;
    }

    public static Intent createSpinItPicturesIntent(Context context,
                                                    String vin,
                                                    String year,
                                                    String make,
                                                    String model,
                                                    String project,
                                                    String library,
                                                    boolean replace,
                                                    boolean usedTurntable,
                                                    ArrayList<ImageDescription> images,
                                                    int automatedPictureCount,
                                                    int spinDuration,
                                                    int extractedPictureCount) {
        if (images == null || images.isEmpty()) {
            throw new InvalidParameterException("Bad upload parameters for spin it pictures request");
        }

        final Intent intent = createBaseIntent(context, UploadType.SPIN_IT_IMAGES, vin, year, make, model, project, library, replace);
        intent.putExtra(EXTRA_PICTURES, images);
        intent.putExtra(EXTRA_USED_TURNTABLE, usedTurntable);
        intent.putExtra(EXTRA_AUTOMATED_PICTURE_COUNT, automatedPictureCount);
        intent.putExtra(EXTRA_SPIN_DURATION, spinDuration);
        intent.putExtra(EXTRA_EXTRACTED_PICTURE_COUNT, extractedPictureCount);
        return intent;
    }

    public static Intent createSpinItVideoIntent(Context context,
                                                 String vin,
                                                 String year,
                                                 String make,
                                                 String model,
                                                 String project,
                                                 String library,
                                                 boolean replace,
                                                 boolean usedTurntable,
                                                 String videoPathname,
                                                 int frameCount,
                                                 int automatedPictureCount,
                                                 int spinDuration,
                                                 int extractedPictureCount,
                                                 String cameraRole) {
        if (TextUtils.isEmpty(videoPathname)) {
            throw new InvalidParameterException("Missing pathname");
        }

        final Intent intent = createBaseIntent(context, UploadType.SPIN_IT_IMAGES, vin, year, make, model, project, library, replace);
        intent.putExtra(EXTRA_EXTRACT_PICTURES, true);
        intent.putExtra(EXTRA_USED_TURNTABLE, usedTurntable);
        intent.putExtra(EXTRA_VIDEO_SOURCE, videoPathname);
        intent.putExtra(EXTRA_FRAME_COUNT, frameCount);
        intent.putExtra(EXTRA_AUTOMATED_PICTURE_COUNT, automatedPictureCount);
        intent.putExtra(EXTRA_SPIN_DURATION, spinDuration);
        intent.putExtra(EXTRA_EXTRACTED_PICTURE_COUNT, extractedPictureCount);
        intent.putExtra(EXTRA_CAMERA_ROLE, cameraRole);

        return intent;
    }

    public static Intent createEZ360PictureIntent(Context context, String vin, String year, String make, String model, String project, String library, boolean replace, String pathname) {
        if (TextUtils.isEmpty(pathname)) {
            throw new InvalidParameterException("Missing pathname");
        }

        final Intent intent = createBaseIntent(context, UploadType.EZ360, vin, year, make, model, project, library, replace);

        ArrayList<String> pathnames = new ArrayList<>();
        pathnames.add(pathname);
        intent.putExtra(EXTRA_PATHNAMES, pathnames);

        return intent;

    }

    public static Intent createAnimatedPictureIntent(Context context, String vin, String year, String make, String model, String project, String library, boolean replace, String video, ArrayList<String> images) {
        final Intent intent = createBaseIntent(context, UploadType.ANIMATED_PICTURE, vin, year, make, model, project, library, replace);
        ArrayList<String> pathnames = new ArrayList<>(images.size() + 1);
        if (!TextUtils.isEmpty(video)) {
            pathnames.add(video);
        }
        for (String image : images) {
            pathnames.add(image);
        }
        intent.putExtra(EXTRA_PATHNAMES, pathnames);
        return intent;
    }

    public static Intent createDeleteMediaIntent(Context context, ArrayList<String> pathnames) {
        if (pathnames == null || pathnames.isEmpty()) {
            throw new InvalidParameterException("Bad upload parameters for delete media request");
        }

        final Intent intent = new Intent(context, UploaderService.class);
        intent.setAction(ACTION_DELETE_MEDIA);
        intent.putExtra(EXTRA_PATHNAMES, pathnames);

        return intent;
    }

    public static Intent createScheduleJobIntent(Context context) {
        final Intent intent = new Intent(context, UploaderService.class);
        intent.setAction(ACTION_SCHEDULE_UPLOADER);
        return intent;
    }

    public static Intent createMediaCompressionCompleteIntent(Context context, long requestCode, boolean success, String pathname) {
        final Intent intent = new Intent(context, UploaderService.class);
        intent.setAction(ACTION_RECEIVE_MEDIA_COMPRESSION_RESULT);
        intent.putExtra(EXTRA_COMPRESSION_REQUEST_CODE, requestCode);
        intent.putExtra(EXTRA_COMPRESSION_SUCCESS, success);
        intent.putExtra(EXTRA_COMPRESSION_PATHNAME, pathname);
        return intent;
    }

    public static Intent createFrameExtractionCompleteIntent(Context context, long requestCode, boolean success, ArrayList<ImageDescription> pictures) {
        final Intent intent = new Intent(context, UploaderService.class);
        intent.setAction(ACTION_RECEIVE_FRAME_EXTRACTION_RESULT);
        intent.putExtra(EXTRA_FRAME_EXTRACTION_REQUEST_CODE, requestCode);
        intent.putExtra(EXTRA_FRAME_EXTRACTION_SUCCESS, success);
        intent.putExtra(EXTRA_PICTURES, pictures);

        return intent;
    }

    public static Intent createGifConversionCompleteIntent(Context context, long requestCode, boolean success, String pathname) {
        final Intent intent = new Intent(context, UploaderService.class);
        intent.setAction(ACTION_RECEIVE_GIF_CONVERSION_RESULT);
        intent.putExtra(EXTRA_COMPRESSION_REQUEST_CODE, requestCode);
        intent.putExtra(EXTRA_COMPRESSION_SUCCESS, success);
        intent.putExtra(EXTRA_COMPRESSION_PATHNAME, pathname);
        return intent;
    }

    @Override
    public boolean onStartJob(JobParameters parameters) {
        Timber.d("onStartJob entered");

        isScheduledJobRunning = true;
        jobParameters = parameters;
        uploader.start();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Timber.d("onStopJob entered");

        isScheduledJobRunning = false;
        return true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("onCreate entered");

        HandlerThread thread = new HandlerThread("UploaderServiceThread" + System.currentTimeMillis());
        thread.start();

        try {
            uploader = new Uploader(thread.getLooper(), listener, getApplicationContext(), getHelper());
        } catch (SQLException e) {
            Util.logError("Error creating upload dao", e);
            stopSelf();
        }

        preferences = new AppSharedPreferences(this);
        preferences.registerOnSharedPreferenceChangeListener(preferenceListener);
    }

    @Override
    public void onDestroy() {
        Timber.d("onDestroy entered");

        preferences.unregisterOnSharedPreferenceChangeListener(preferenceListener);
        if (uploader != null) {
            uploader.stop();
        }

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            final String action = intent.getAction();
            Timber.d("onStartCommand action: %s startId: %d", action, startId);

            if (ACTION_ENQUEUE_MEDIA.equals(action)) {
                uploader.sendUploadIntent(intent, startId);
            } else if (ACTION_DELETE_MEDIA.equals(action)) {
                uploader.sendDeleteIntent(intent, startId);
            } else if (ACTION_RECEIVE_MEDIA_COMPRESSION_RESULT.equals(action)) {
                uploader.sendCompressionCompleteIntent(intent, startId);
            } else if (ACTION_RECEIVE_GIF_CONVERSION_RESULT.equals(action)) {
                uploader.sendCompressionCompleteIntent(intent, startId);
            } else if (ACTION_RECEIVE_FRAME_EXTRACTION_RESULT.equals(action)) {
                uploader.sendFrameExtractionCompleteIntent(intent, startId);
            } else if (ACTION_SCHEDULE_UPLOADER.equals(action)) {
                // We handle this via an intent so we can check whether the job is running
                // before scheduling it.
                scheduleService(UploaderService.this);
                stopSelf(startId);
            }
        }
        return START_NOT_STICKY;
    }
}
