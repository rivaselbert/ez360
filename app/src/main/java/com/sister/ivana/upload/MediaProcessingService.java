package com.sister.ivana.upload;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.sister.ivana.FFmpeg;
import com.sister.ivana.logging.LogReceiver;

import net.sourceforge.opencamera.ImageDescription;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Do not use Timber calls in this class. It runs in a separate process and should send all of its
 * logging to the main process using LogReceiver.
 */

public class MediaProcessingService extends IntentService {
    // We periodically retry old compression requests in case the app crashes while requests are
    // pending. Once a request has been around for longer than this threshold we retry it. It is
    // OK if we send more than one request for an image. Typically it takes a few hundred ms to
    // compress an image so this should be a safe timeout value.
    public static long PICTURE_COMPRESSION_TIMEOUT_MS = 15000;

    // The video compression can take quite awhile. For a one minute video it frequently takes three
    // or more minutes. Consequently we'll use a long timeout.
    public static long VIDEO_COMPRESSION_TIMEOUT_MS = 15 * 60 * 1000;

    // This usually takes about one minute.
    public static long FRAME_EXTRACTION_TIMEOUT_MS = 10 * 60 * 1000;

    private static final String TAG = "MediaProcessingService";
    private static final String ACTION_COMPRESS_PICTURE = "ACTION_COMPRESS_PICTURE";
    private static final String ACTION_COMPRESS_VIDEO = "ACTION_COMPRESS_VIDEO";
    private static final String ACTION_EXTRACT_FRAMES = "ACTION_EXTRACT_FRAMES";
    private static final String ACTION_CONVERT_GIF = "ACTION_CONVERT_GIF";

    private static final String EXTRA_REQUEST_CODE = "EXTRA_REQUEST_CODE";
    private static final String EXTRA_SOURCE_PATHNAME = "EXTRA_SOURCE_PATHNAME";
    private static final String EXTRA_DESTINATION_PATHNAME = "EXTRA_DESTINATION_PATHNAME";
    private static final String EXTRA_PICTURE_QUALITY = "EXTRA_PICTURE_QUALITY";
    private static final String EXTRA_YEAR = "EXTRA_YEAR";
    private static final String EXTRA_MAKE = "EXTRA_MAKE";
    private static final String EXTRA_MODEL = "EXTRA_MODEL";
    private static final String EXTRA_VIN = "EXTRA_VIN";
    private static final String EXTRA_FRAME_COUNT = "EXTRA_FRAME_COUNT";

    public static Intent createPictureIntent(Context context, long requestCode, String source, String destination, int quality) {
        Intent intent = new Intent(context, MediaProcessingService.class);
        intent.setAction(ACTION_COMPRESS_PICTURE);
        intent.putExtra(EXTRA_REQUEST_CODE, requestCode);
        intent.putExtra(EXTRA_SOURCE_PATHNAME, source);
        intent.putExtra(EXTRA_DESTINATION_PATHNAME, destination);
        intent.putExtra(EXTRA_PICTURE_QUALITY, quality);
        return intent;
    }

    public static Intent createVideoIntent(Context context, long requestCode, String source, String destination, String year, String make, String model) {
        Intent intent = new Intent(context, MediaProcessingService.class);
        intent.setAction(ACTION_COMPRESS_VIDEO);
        intent.putExtra(EXTRA_REQUEST_CODE, requestCode);
        intent.putExtra(EXTRA_SOURCE_PATHNAME, source);
        intent.putExtra(EXTRA_DESTINATION_PATHNAME, destination);
        intent.putExtra(EXTRA_YEAR, year);
        intent.putExtra(EXTRA_MAKE, make);
        intent.putExtra(EXTRA_MODEL, model);
        return intent;
    }

    public static Intent createFrameExtractionIntent(Context context, long requestCode, String source, String vin, int frameCount, String year, String make, String model) {
        Intent intent = new Intent(context, MediaProcessingService.class);
        intent.setAction(ACTION_EXTRACT_FRAMES);
        intent.putExtra(EXTRA_REQUEST_CODE, requestCode);
        intent.putExtra(EXTRA_SOURCE_PATHNAME, source);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_FRAME_COUNT, frameCount);
        intent.putExtra(EXTRA_YEAR, year);
        intent.putExtra(EXTRA_MAKE, make);
        intent.putExtra(EXTRA_MODEL, model);
        return intent;
    }

    public static Intent createGifConversionIntent(Context context, long requestCode, String source, String destination, int quality) {
        Intent intent = new Intent(context, MediaProcessingService.class);
        intent.setAction(ACTION_CONVERT_GIF);
        intent.putExtra(EXTRA_REQUEST_CODE, requestCode);
        intent.putExtra(EXTRA_SOURCE_PATHNAME, source);
        intent.putExtra(EXTRA_DESTINATION_PATHNAME, destination);
        intent.putExtra(EXTRA_PICTURE_QUALITY, quality);
        return intent;
    }

    public MediaProcessingService() {
        super("MediaProcessingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            LogReceiver.i(this, TAG, "onHandleIntent received null intent");
            return;
        }

        final String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            LogReceiver.w(this, TAG, "onHandleIntent received empty action");
            return;
        }

        if (action.equals(ACTION_COMPRESS_PICTURE)) {
            final String source = intent.getStringExtra(EXTRA_SOURCE_PATHNAME);
            final String destination = intent.getStringExtra(EXTRA_DESTINATION_PATHNAME);
            final int quality = intent.getIntExtra(EXTRA_PICTURE_QUALITY, 100);
            final long requestCode = intent.getLongExtra(EXTRA_REQUEST_CODE, 0);
            compressPicture(requestCode, source, destination, quality);
        } else if (action.equals(ACTION_COMPRESS_VIDEO)) {
            final String source = intent.getStringExtra(EXTRA_SOURCE_PATHNAME);
            final String destination = intent.getStringExtra(EXTRA_DESTINATION_PATHNAME);
            final long requestCode = intent.getLongExtra(EXTRA_REQUEST_CODE, 0);
            final String year = intent.getStringExtra(EXTRA_YEAR);
            final String make = intent.getStringExtra(EXTRA_MAKE);
            final String model = intent.getStringExtra(EXTRA_MODEL);
            compressVideo(requestCode, source, destination, year, make, model);
        } else if (action.equals(ACTION_EXTRACT_FRAMES)) {
            final String source = intent.getStringExtra(EXTRA_SOURCE_PATHNAME);
            final String vin = intent.getStringExtra(EXTRA_VIN);
            final long requestCode = intent.getLongExtra(EXTRA_REQUEST_CODE, 0);
            final int frameCount = intent.getIntExtra(EXTRA_FRAME_COUNT, 0);
            final String year = intent.getStringExtra(EXTRA_YEAR);
            final String make = intent.getStringExtra(EXTRA_MAKE);
            final String model = intent.getStringExtra(EXTRA_MODEL);
            extractFrames(requestCode, source, vin, frameCount, year, make, model);
        } else if (action.equals(ACTION_CONVERT_GIF)) {
            final String source = intent.getStringExtra(EXTRA_SOURCE_PATHNAME);
            final String vin = intent.getStringExtra(EXTRA_VIN);
            final long requestCode = intent.getLongExtra(EXTRA_REQUEST_CODE, 0);
            final int frameCount = intent.getIntExtra(EXTRA_FRAME_COUNT, 0);
            final String year = intent.getStringExtra(EXTRA_YEAR);
            final String make = intent.getStringExtra(EXTRA_MAKE);
            final String model = intent.getStringExtra(EXTRA_MODEL);
            convertGif(requestCode, source, vin, frameCount, year, make, model);
        }
    }

    private void compressPicture(long requestCode, String source, String destination, int quality) {
        LogReceiver.d(this, TAG, "compressPicture requestCode: %d source: %s destination: %s quality: %d", requestCode, source, destination, quality);

        boolean success = false;
        boolean sendResponse = true;

        OutputStream outputStream = null;
        try {
            final File sourceFile = new File(source);
            if (!sourceFile.exists()) {
                // This can happen because the upload queue will periodically try to resend
                // requests so we might get an extra request after the original has already
                // completed and the file has been deleted. In that case we do not want to send
                // a response.
                LogReceiver.d(this, TAG, "source file doesn't exist: %s", source);
                sendResponse = false;
                return;
            }

            final File destinationFile = new File(destination);
            if (destinationFile.exists()) {
                // Could happen if we crash while trying to compress an image and then retry.
                LogReceiver.d(this, TAG, "destination file exists: %s", destination);
                if (!destinationFile.delete()) {
                    LogReceiver.d(this, TAG, "failed deleting destination file: %s", destination);

                    // Try again later.
                    sendResponse = false;
                    return;
                }
            }

            final Bitmap bitmap = BitmapFactory.decodeFile(source);
            outputStream = new FileOutputStream(destination);
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream)) {
                LogReceiver.d(this, TAG, "Compressed photo: %s", source);
                success = true;
            } else {
                LogReceiver.e(this, TAG, "Failed compressing photo %s: ", source);
            }
        } catch (FileNotFoundException e) {
            LogReceiver.e(this, TAG, e, "Exception compressing photo");
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException ignore) {
                }
            }

            // Send result back to uploader service.
            if (sendResponse) {
                final Intent intent = UploaderService.createMediaCompressionCompleteIntent(this, requestCode, success, destination);
                startService(intent);
            }
        }
    }

    private void compressVideo(long requestCode, String source, String destination, String year, String make, String model) {
        LogReceiver.d(this, TAG, "compressVideo requestCode: %d source: %s destination: %s year: %s make: %s model: %s", requestCode, source, destination, year, make, model);

        final File sourceFile = new File(source);
        if (!sourceFile.exists()) {
            LogReceiver.d(this, TAG, "source file doesn't exist: %s", source);
            return;
        }

        final File destinationFile = new File(destination);
        if (destinationFile.exists()) {
            // Could happen if we crash while trying to compress a video and then retry.
            LogReceiver.d(this, TAG, "destination file exists: %s", destination);
            if (!destinationFile.delete()) {
                LogReceiver.d(this, TAG, "failed deleting destination file: %s", destination);
                return;
            }
        }

        final Intent updateIntent = new Intent(UploadStatus.ACTION_UPDATE_VIDEO_COMPRESSION);
        updateIntent.putExtra(UploadStatus.EXTRA_YEAR, year);
        updateIntent.putExtra(UploadStatus.EXTRA_MAKE, make);
        updateIntent.putExtra(UploadStatus.EXTRA_MODEL, model);
        sendBroadcast(updateIntent);

        FFmpeg.getFFmpeg().compressFullMotionVideo(this, source, destination);
        LogReceiver.d(this, TAG, "video compression complete");

        sendBroadcast(new Intent(UploadStatus.ACTION_RESET_VIDEO_COMPRESSION));

        // Send result back to uploader service.
        final Intent intent = UploaderService.createMediaCompressionCompleteIntent(this, requestCode, true, destination);
        startService(intent);
    }

    private void extractFrames(long requestCode, String source, String vin, int frameCount, String year, String make, String model) {
        LogReceiver.d(this, TAG, "extractFrames requestCode: %d source: %s vin: %s frameCount: %d", requestCode, source, vin, frameCount);

        final Intent updateIntent = new Intent(UploadStatus.ACTION_UPDATE_FRAME_EXTRACTION);
        updateIntent.putExtra(UploadStatus.EXTRA_YEAR, year);
        updateIntent.putExtra(UploadStatus.EXTRA_MAKE, make);
        updateIntent.putExtra(UploadStatus.EXTRA_MODEL, model);
        sendBroadcast(updateIntent);

        ArrayList<ImageDescription> pictures = FFmpeg.getFFmpeg().extractFrames(this, source, vin, frameCount);
        LogReceiver.d(this, TAG, "frame extraction complete");

        sendBroadcast(new Intent(UploadStatus.ACTION_RESET_FRAME_EXTRACTION));

        final Intent intent = UploaderService.createFrameExtractionCompleteIntent(this, requestCode, !pictures.isEmpty(), pictures);
        startService(intent);
    }

    private void convertGif(long requestCode, String source, String vin, int frameCount, String year, String make, String model) {
        LogReceiver.d(this, TAG, "convert gif requestCode: %d source: %s vin: %s frameCount: %d", requestCode, source, vin, frameCount);

        final Intent updateIntent = new Intent(UploadStatus.ACTION_UPDATE_VIDEO_COMPRESSION);
        updateIntent.putExtra(UploadStatus.EXTRA_YEAR, year);
        updateIntent.putExtra(UploadStatus.EXTRA_MAKE, make);
        updateIntent.putExtra(UploadStatus.EXTRA_MODEL, model);
        sendBroadcast(updateIntent);

        String destination = source.replace(".mp4", ".gif");
        ImageDescription imageDescription = FFmpeg.getFFmpeg().convertGif(this, source, destination);
        LogReceiver.d(this, TAG, "gif conversion complete");

        sendBroadcast(new Intent(UploadStatus.ACTION_RESET_VIDEO_COMPRESSION));

        final Intent intent = UploaderService.createGifConversionCompleteIntent(this, requestCode, imageDescription != null, destination);
        startService(intent);
    }
}
