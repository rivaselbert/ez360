package com.sister.ivana.upload;


import android.content.Context;
import android.text.TextUtils;

import com.amazonaws.services.s3.AmazonS3Client;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.CameraRole;
import com.sister.ivana.database.UploadFile;
import com.sister.ivana.database.UploadGroup;
import com.sister.ivana.webapi.AddPoiBody;
import com.sister.ivana.webapi.AnimatedPictureBody;
import com.sister.ivana.webapi.DetailsBody;
import com.sister.ivana.webapi.ExteriorSpinBody;
import com.sister.ivana.webapi.FullMotionVideoUploadDescription;
import com.sister.ivana.webapi.PanoramaBody;
import com.sister.ivana.webapi.PipBody;
import com.sister.ivana.webapi.WebService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;

public enum UploadType {
    // SiSTeR wants 360 images and video to upload only when other images have been uploaded so
    // they have a corresponding priority.
    DETAIL_IMAGES("sistertech-ivana", 0, 0, "dp", "jpg", "exterior picture", "exterior pictures", true),
    EZ360("sistertech-ivana", 1, 2, "pano_theta", "jpg", "Theta picture", "Theta pictures", false),
    SPIN_IT_IMAGES("sistertech-ivana", 0, 3, "", "jpg", "spin it picture", "spin it pictures", true),
    FULL_MOTION_VIDEO("sistertech-fullmo-raw", 2, 4, "fm", "mp4", "fullmo video", "fullmo videos", true),
    ANIMATED_PICTURE("sistertech-ivana", 0, 5, "animated", "", "animated picture", "animated pictures", true),
    ADD_EXTERIOR_POI("sistertech-ivana", 0, 7, "dp", "jpg", "exterior picture", "exterior pictures", true),
    PIP("sistertech-ivana", 0, 8, "pip", "jpg", "pip picture", "pip pictures", true);

    private final String awsBucket;
    private final int uploadPriority;
    private final int databaseCode;
    private final String mediaCode;
    private final String fileExtension;
    private final String singularDisplayString;
    private final String pluralDisplayString;
    private final boolean requiresCompression;

    UploadType(String AWSBucket, int uploadPriority, int databaseCode, String mediaCode, String fileExtension, String singularDisplayString, String pluralDisplayString, boolean requiresCompression) {
        this.awsBucket = AWSBucket;
        this.uploadPriority = uploadPriority;
        this.databaseCode = databaseCode;
        this.mediaCode = mediaCode;
        this.fileExtension = fileExtension;
        this.singularDisplayString = singularDisplayString;
        this.pluralDisplayString = pluralDisplayString;
        this.requiresCompression = requiresCompression;
    }

    public static UploadType create(int databaseCode) {
        if (databaseCode == DETAIL_IMAGES.getDatabaseCode()) {
            return DETAIL_IMAGES;
        } else if (databaseCode == EZ360.getDatabaseCode()) {
            return EZ360;
        } else if (databaseCode == SPIN_IT_IMAGES.getDatabaseCode()) {
            return SPIN_IT_IMAGES;
        } else if (databaseCode == FULL_MOTION_VIDEO.getDatabaseCode()) {
            return FULL_MOTION_VIDEO;
        } else if (databaseCode == ANIMATED_PICTURE.getDatabaseCode()) {
            return ANIMATED_PICTURE;
        } else if (databaseCode == ADD_EXTERIOR_POI.getDatabaseCode()) {
            return ADD_EXTERIOR_POI;
        } else if (databaseCode == PIP.getDatabaseCode()) {
            return PIP;
        } else {
            return null;
        }
    }

    public String getAWSBucket() {
        return awsBucket;
    }

    public String getAWSKey(String library, String project, String vin, String cameraRole, int sequenceNumber, boolean usedTurntable, boolean capturedVideo) {
        final String key;

        switch (this) {
            case EZ360:
                key = String.format(Locale.US, "%s/%s/%s/%s.%s.%s.%s",
                        library,
                        project,
                        vin,
                        vin,
                        mediaCode,
                        new SimpleDateFormat("yyyyMMddHHMMSS", Locale.US).format(new Date()),
                        fileExtension);
                break;

            case SPIN_IT_IMAGES:
                // Media code for spin it images depends on whether a turntable was used for taking
                // the photos and whether pictures are extracted from video
                final String mediaType;
                if (usedTurntable) {
                    mediaType = capturedVideo ? "sp_turntable" : "sp_turntable_pic";
                } else {
                    mediaType = capturedVideo ? "sp_walkaround_vid" : "sp_walkaround_pic";
                }

                if (TextUtils.isEmpty(cameraRole) || cameraRole.contentEquals(CameraRole.MASTER.getSymbol())) {
                    key = String.format(Locale.US, "%s/%s/%s/%s.%s.%d.%s.%s",
                            library,
                            project,
                            vin,
                            vin,
                            mediaType,
                            sequenceNumber,
                            new SimpleDateFormat("yyyyMMddHHMMSS", Locale.US).format(new Date()),
                            fileExtension);
                } else {
                    key = String.format(Locale.US, "%s/%s/%s/%s/%s.%s.%d.%s.%s",
                            library,
                            project,
                            vin,
                            cameraRole,
                            vin,
                            mediaType,
                            sequenceNumber,
                            new SimpleDateFormat("yyyyMMddHHMMSS", Locale.US).format(new Date()),
                            fileExtension);
                }

                break;

            default:
                key = String.format(Locale.US, "%s/%s/%s/%s.%s.%d.%s.%s",
                        library,
                        project,
                        vin,
                        vin,
                        mediaCode,
                        sequenceNumber,
                        new SimpleDateFormat("yyyyMMddHHMMSS", Locale.US).format(new Date()),
                        fileExtension);
        }

        return key;
    }

    /**
     * Items with lower priority are uploaded first. SiSTeR wants photos uploaded before video
     * because it gives user better experience with web app (they watch for pictures to appear
     * before moving on to next car).
     */
    public int getUploadPriority() {
        return uploadPriority;
    }

    public int getDatabaseCode() {
        return databaseCode;
    }

    public String getDisplayString(int count) {
        return (count == 1) ? singularDisplayString : pluralDisplayString;
    }

    public Call<Void> getUploadRequest(Context context, AmazonS3Client client, UploadGroup group) {
        Call<Void> call;

        switch (this) {
            case EZ360:
                call = WebService.getDebugApi(context).postInteriorPanorama(createEZ360UploadDescription(client, group));
                break;
            case DETAIL_IMAGES:
                call = WebService.getDebugApi(context).postDetails(createDetailImagesDescription(client, group));
                break;
            case ANIMATED_PICTURE:
                call = WebService.getDebugApi(context).postAnimatedPicture(createAnimatedPictureDescription(client, group));
                break;
            case FULL_MOTION_VIDEO:
                call = WebService.getDebugApi(context).postFullMotionVideo(createFullMotionVideoDescription(client, group));
                break;
            case SPIN_IT_IMAGES:
                call = WebService.getDebugApi(context).postExteriorSpin(createSpinItPicturesUploadDescription(context, client, group));
                break;
            case ADD_EXTERIOR_POI:
                call = WebService.getDebugApi(context).postAddPoi(createAddPoiDescription(context, client, group));
                break;
            case PIP:
                call = WebService.getDebugApi(context).postPip(createPipDescription(context, client, group));
                break;
            default:
                call = null;
        }

        return call;
    }

    private PanoramaBody createEZ360UploadDescription(AmazonS3Client amazonS3Client, UploadGroup group) {
        PanoramaBody description = new PanoramaBody();
        description.library_id = group.getLibrary();
        description.project_id = group.getProject();
        description.vin = group.getVin();
        description.replace = group.isReplace();

        final String awsBucket = create(group.getUploadType()).getAWSBucket();
        for (UploadFile file : group.getFiles()) {
            // There should be only one file here.
            description.interior_panorama_url = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
        }

        return description;
    }

    private DetailsBody createDetailImagesDescription(AmazonS3Client amazonS3Client, UploadGroup group) {
        class AnimatedGif {
            String pictureURL;
            String thumbnailURL;
        }
        DetailsBody description = new DetailsBody();
        description.library_id = group.getLibrary();
        description.project_id = group.getProject();
        description.vin = group.getVin();
        description.replace = group.isReplace();

        AnimatedGif animatedGIF = null;
        ArrayList<DetailsBody.Image> images = new ArrayList<>();
        for (UploadFile file : group.getFiles()) {
            DetailsBody.Image image = new DetailsBody.Image();
            image.animated_picture_url = "";
            image.animated_picture_thumbnail_url = "";
            image.image_url = "";
            image.image_url = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
            image.feature_code = file.getFeatureCode();
            image.feature_label = file.getFeatureTitle();
            images.add(image);
        }

        description.detail_pic_object = new DetailsBody.Images();
        description.detail_pic_object.images = images;

        return description;
    }

    private AnimatedPictureBody createAnimatedPictureDescription(AmazonS3Client amazonS3Client, UploadGroup group) {
        final AnimatedPictureBody body = new AnimatedPictureBody();

        body.library_id = group.getLibrary();
        body.project_id = group.getProject();
        body.vin = group.getVin();
        body.replace = group.isReplace();
        body.video = "";
        ArrayList<String> images = new ArrayList<>();
        for (UploadFile file : group.getFiles()) {
            if (file.getPathname().endsWith(".mp4")) {
                body.video = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
            } else {
                images.add(amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey()));
            }
        }

        body.images = images;

        return body;
    }

    private AddPoiBody createAddPoiDescription(Context context, AmazonS3Client amazonS3Client, UploadGroup group) {
        AddPoiBody body = new AddPoiBody();
        body.library_id = group.getLibrary();
        body.project_id = group.getProject();
        body.vin = group.getVin();
        body.camera_id = new AppSharedPreferences(context).getCameraID();

        final String awsBucket = create(group.getUploadType()).getAWSBucket();
        for (UploadFile file : group.getFiles()) {
            // There should be only one file here.
            body.image_url = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
            body.add_to_poi = file.getAddToMagazine();
            body.title = file.getTitle();
            body.description = file.getDescription();
            body.spin_number = file.getSpinNumber();
            body.spin_index = file.getSpinIndex();

            AddPoiBody.Coordinate coordinate = new AddPoiBody.Coordinate();
            coordinate.x = file.getX();
            coordinate.y = file.getY();
            body.picture_coordinate = coordinate;
        }

        return body;
    }

    private PipBody createPipDescription(Context context, AmazonS3Client amazonS3Client, UploadGroup group) {
        PipBody body = new PipBody();

        body.library_id = group.getLibrary();
        body.project_id = group.getProject();
        body.vin = group.getVin();
        body.camera_id = new AppSharedPreferences(context).getCameraID();

        int i = 0;
        final String awsBucket = create(group.getUploadType()).getAWSBucket();
        for (UploadFile file : group.getFiles()) {
            if (i == 0) {
                body.wide_image_url = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
            } else if (i == 1) {
                body.detail_image_url = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
                PipBody.Coordinate coordinate = new PipBody.Coordinate();
                coordinate.x = file.getX();
                coordinate.y = file.getY();
                body.picture_coordinate = coordinate;
            }
            i++;
        }

        return body;
    }

    private ExteriorSpinBody createSpinItPicturesUploadDescription(Context context, AmazonS3Client amazonS3Client, UploadGroup group) {
        ExteriorSpinBody description = new ExteriorSpinBody();
        description.library_id = group.getLibrary();
        description.project_id = group.getProject();
        description.vin = group.getVin();
        description.replace = group.isReplace();
        description.camera_id = new AppSharedPreferences(context).getCameraID();
        description.camera_role = group.getCameraRole();
        description.used_turntable = group.getUsedTurntable();
        description.automated_picture_count = group.getAutomatedPictureCount();
        description.spin_duration = group.getSpinDuration();
        description.extracted_picture_count = group.getExtractedPictureCount();

        final String awsBucket = create(group.getUploadType()).getAWSBucket();
        description.spin_urls = new ArrayList<>();

        for (UploadFile file : group.getFiles()) {
            final String url = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
            description.spin_urls.add(url);
        }

        return description;
    }

    private FullMotionVideoUploadDescription createFullMotionVideoDescription(AmazonS3Client amazonS3Client, UploadGroup group) {
        FullMotionVideoUploadDescription description = new FullMotionVideoUploadDescription();

        description.vin = group.getVin();
        description.project_id = group.getProject();
        description.library_id = group.getLibrary();
        description.replace = group.isReplace();

        final String awsBucket = create(group.getUploadType()).getAWSBucket();
        for (UploadFile file : group.getFiles()) {
            // There should be only one file here.
            description.fullmo_raw = amazonS3Client.getResourceUrl(awsBucket, file.getAWSKey());
        }

        // These fields are hard coded in PixZero so I left it that way here.
        description.user = "demo@sister.tv";

        return description;
    }

    public boolean requiresCompression() {
        return requiresCompression;
    }
}
