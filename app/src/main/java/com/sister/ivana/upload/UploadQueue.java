package com.sister.ivana.upload;


import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.PreparedUpdate;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.FileUtil;
import com.sister.ivana.Util;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.UploadFile;
import com.sister.ivana.database.UploadGroup;

import net.sourceforge.opencamera.ImageDescription;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;

import timber.log.Timber;

public class UploadQueue {
    private final DatabaseHelper databaseHelper;
    private final Handler mainHandler;
    private final UploadStatus uploadStatus;
    private final PreparedQuery<UploadFile> nextFileQuery;
    private final PreparedQuery<UploadGroup> nextGroupQuery;
    private final PreparedQuery<UploadGroup> nextFrameExtractionGroupQuery;
    private final PreparedQuery<UploadFile> fileQuery;
    private final PreparedQuery<UploadGroup> completedGroupQuery;
    private final SelectArg transferArg = new SelectArg();
    private final SelectArg vinArg = new SelectArg();
    private final SelectArg uploadTypeArg = new SelectArg();
    private final PreparedUpdate<UploadFile> resetFileErrorUpdate;
    private final PreparedUpdate<UploadGroup> resetGroupErrorUpdate;
    private final PreparedQuery<UploadFile> fileFailureQuery;
    private final PreparedQuery<UploadGroup> groupFailureQuery;
    private final String pendingCountQuery;
    private final PreparedQuery<UploadFile> mediaCountsQuery;
    private final PreparedQuery<UploadFile> pendingFileQuery;
    private final PreparedQuery<UploadFile> pendingCompressionFileQuery;
    private final PreparedQuery<UploadGroup> pendingFrameExtractionQuery;
    private final SelectArg pathnameArg = new SelectArg();
    private final SelectArg compressedPathnameArg = new SelectArg();
    private final SelectArg compressionRequestCode = new SelectArg();
    private final SelectArg frameExtractionRequestCode = new SelectArg();
    private final SelectArg videoPathnameArg = new SelectArg();
    private final PreparedQuery<UploadFile> nextRequestCodeQuery;
    private final PreparedQuery<UploadGroup> nextExtractionRequestCodeQuery;
    private final PreparedQuery<UploadFile> pendingVideoUploadQuery;
    private final PreparedQuery<UploadGroup> sourceVideoQuery;
    private final Context context;

    public UploadQueue(Context context, DatabaseHelper helper) throws SQLException {
        this.context = context;
        this.databaseHelper = helper;
        uploadStatus = UploadStatus.getStatus(context);
        nextFileQuery = prepareNextFileQuery();
        nextGroupQuery = prepareNextGroupQuery();
        nextFrameExtractionGroupQuery = prepareNextFrameExtractionGroupQuery();
        fileQuery = prepareFileQuery();
        resetFileErrorUpdate = prepareResetFileErrorUpdate();
        resetGroupErrorUpdate = prepareResetGroupErrorUpdate();
        completedGroupQuery = prepareCompletedGroupQuery();
        fileFailureQuery = prepareFailedFileQuery();
        groupFailureQuery = prepareFailedGroupQuery();
        pendingCountQuery = preparePendingCountQuery();
        mainHandler = new Handler(context.getMainLooper());
        mediaCountsQuery = prepareMediaCountQuery(helper, vinArg, uploadTypeArg);
        pendingFileQuery = preparePendingFileQuery();
        pendingCompressionFileQuery = preparePendingCompressionFileQuery();
        pendingFrameExtractionQuery = preparePendingFrameExtractionQuery();
        nextRequestCodeQuery = prepareNextRequestCodeQuery();
        nextExtractionRequestCodeQuery = prepareNextExtractionRequestCodeQuery();
        pendingVideoUploadQuery = preparePendingVideoUploadQuery();
        sourceVideoQuery = prepareSourceVideoQuery();
    }

    public static PendingMediaCounts getCounts(DatabaseHelper helper, String vin) {
        try {
            SelectArg vinArg = new SelectArg();
            SelectArg uploadTypeArg = new SelectArg();
            PreparedQuery<UploadFile> query = prepareMediaCountQuery(helper, vinArg, uploadTypeArg);

            vinArg.setValue(vin);
            return getMediaCounts(helper.getUploadFileDao(), helper.getUploadGroupDao(), query, vin, uploadTypeArg);
        } catch (SQLException e) {
            Util.logError("Error getting media counts");
        }

        return new PendingMediaCounts(vin);
    }

    public static List<String> getMediaFiles(DatabaseHelper helper, UploadType uploadType, String vin) {
        HashSet<String> pathnames = new HashSet<>();

        try {
            QueryBuilder<UploadGroup, String> builder = helper.getUploadGroupDao().queryBuilder();
            builder.where()
                    .eq(UploadGroup.COLUMN_VIN, vin)
                    .and()
                    .eq(UploadGroup.COLUMN_UPLOAD_TYPE, uploadType.getDatabaseCode());

            List<UploadGroup> groups = builder.query();
            for (UploadGroup group : groups) {
                for (UploadFile uploadFile : group.getFiles()) {
                    if (!uploadFile.isUploadComplete()) {
                        final String compressedPathname = uploadFile.getCompressedPathname();
                        final String pathname = uploadFile.getPathname();
                        if (!TextUtils.isEmpty(compressedPathname)) {
                            pathnames.add(compressedPathname);
                        } else if (!TextUtils.isEmpty(pathname)) {
                            pathnames.add(pathname);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            Util.logError("Error querying for pending uploads");
        }

        ArrayList<String> files = new ArrayList<>(pathnames.size());
        for (String pathname : pathnames) {
            files.add(pathname);
        }

        return files;
    }

    private static PendingMediaCounts getMediaCounts(Dao<UploadFile, String> uploadFileDao, Dao<UploadGroup, String> uploadGroupDao, PreparedQuery<UploadFile> query, String vin, SelectArg uploadTypeArg) throws SQLException {
        uploadTypeArg.setValue(UploadType.DETAIL_IMAGES.getDatabaseCode());
        long detailCount = uploadFileDao.countOf(query);

        uploadTypeArg.setValue(UploadType.ADD_EXTERIOR_POI.getDatabaseCode());
        detailCount += uploadFileDao.countOf(query);

        uploadTypeArg.setValue(UploadType.FULL_MOTION_VIDEO.getDatabaseCode());
        final long fullMotionVideoCount = uploadFileDao.countOf(query);

        uploadTypeArg.setValue(UploadType.SPIN_IT_IMAGES.getDatabaseCode());
        final long spinItPictureCount = uploadFileDao.countOf(query);

        final long spinItUnextractedCount = getSpinItUnextractedCount(uploadGroupDao, vin);

        uploadTypeArg.setValue(UploadType.EZ360.getDatabaseCode());
        final long ez360PictureCount = uploadFileDao.countOf(query);

        return new PendingMediaCounts(vin, detailCount, fullMotionVideoCount, spinItPictureCount + spinItUnextractedCount, ez360PictureCount);
    }

    private static int getSpinItUnextractedCount(Dao<UploadGroup, String> uploadGroupDao, String vin) {
        int count = 0;

        try {
            SelectArg vinArg = new SelectArg();
            vinArg.setValue(vin);
            PreparedQuery<UploadGroup> query = prepareSpinitUnextractedQuery(uploadGroupDao, vinArg);
            for (UploadGroup group : uploadGroupDao.query(query)) {
                count += group.getFrameCount();
            }
        } catch (SQLException e) {
            Timber.e(e, "Error counting unextracted spin pictures");
        }

        return count;
    }

    private static PreparedQuery<UploadFile> prepareMediaCountQuery(DatabaseHelper helper, SelectArg vinArg, SelectArg uploadTypeArg) throws SQLException {
        QueryBuilder<UploadGroup, String> groupBuilder = helper.getUploadGroupDao().queryBuilder();
        groupBuilder.where()
                .eq(UploadGroup.COLUMN_VIN, vinArg)
                .and()
                .eq(UploadGroup.COLUMN_UPLOAD_TYPE, uploadTypeArg);

        QueryBuilder<UploadFile, String> fileBuilder = helper.getUploadFileDao().queryBuilder();
        fileBuilder.setCountOf(true)
                .where()
                .not().eq(UploadFile.COLUMN_UPLOAD_STATUS, UploadFile.UploadFileStatus.COMPLETE.getDatabaseCode());
        fileBuilder.join(groupBuilder);

        return fileBuilder.prepare();
    }

    private static PreparedQuery<UploadGroup> prepareSpinitUnextractedQuery(Dao<UploadGroup, String> dao, SelectArg vinArg) throws SQLException {
        QueryBuilder<UploadGroup, String> builder = dao.queryBuilder();
        builder.where()
                .eq(UploadGroup.COLUMN_VIN, vinArg)
                .and()
                .eq(UploadGroup.COLUMN_UPLOAD_TYPE, UploadType.SPIN_IT_IMAGES.getDatabaseCode())
                .and()
                .eq(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.EXTRACTION_PENDING.getDatabaseCode());

        return builder.prepare();
    }

    public void enqueue(Intent intent) {
        try {
            final String vin = intent.getStringExtra(UploaderService.EXTRA_VIN);
            final String project = intent.getStringExtra(UploaderService.EXTRA_PROJECT);
            final String library = intent.getStringExtra(UploaderService.EXTRA_LIBRARY);
            final String year = intent.getStringExtra(UploaderService.EXTRA_YEAR);
            final String make = intent.getStringExtra(UploaderService.EXTRA_MAKE);
            final String model = intent.getStringExtra(UploaderService.EXTRA_MODEL);
            final boolean replace = intent.getBooleanExtra(UploaderService.EXTRA_REPLACE, false);
            final int uploadTypeCode = intent.getIntExtra(UploaderService.EXTRA_UPLOAD_TYPE, 0);
            final int priority = intent.getIntExtra(UploaderService.EXTRA_PRIORITY, 0);
            final boolean usedTurntable = intent.getBooleanExtra(UploaderService.EXTRA_USED_TURNTABLE, false);
            final boolean needExtraction = intent.getBooleanExtra(UploaderService.EXTRA_EXTRACT_PICTURES, false);
            final int automatedPictureCount = intent.getIntExtra(UploaderService.EXTRA_AUTOMATED_PICTURE_COUNT, 0);
            final int spinDuration = intent.getIntExtra(UploaderService.EXTRA_SPIN_DURATION, 0);
            final int extractedPictureCount = intent.getIntExtra(UploaderService.EXTRA_EXTRACTED_PICTURE_COUNT, 0);
            final long timestamp = System.currentTimeMillis();
            final String cameraRole = Util.enforceNonNull(intent.getStringExtra(UploaderService.EXTRA_CAMERA_ROLE));
            final UploadGroup uploadGroup = new UploadGroup(vin, project, library, year, make, model, replace, uploadTypeCode, timestamp, usedTurntable, automatedPictureCount, spinDuration, extractedPictureCount, cameraRole);
            final ArrayList<UploadFile> uploadFiles = new ArrayList<>();
            final int spinNumber = intent.getIntExtra(UploaderService.EXTRA_SPIN_NUMBER, 0);
            final int spinIndex = intent.getIntExtra(UploaderService.EXTRA_SPIN_INDEX, 0);
            final boolean addToMagazine = intent.getBooleanExtra(UploaderService.EXTRA_ADD_TO_MAGAZINE, false);
            final String title = intent.getStringExtra(UploaderService.EXTRA_TITLE);
            final String poiDescription = intent.getStringExtra(UploaderService.EXTRA_DESCRIPTION);
            final double x = intent.getDoubleExtra(UploaderService.EXTRA_COORDINATE_X, 0);
            final double y = intent.getDoubleExtra(UploaderService.EXTRA_COORDINATE_Y, 0);

            Timber.d("enqueue adding group %s", uploadGroup);

            // For now we can have either EXTRA_PICTURES or EXTRA_PATHNAMES.
            List<ImageDescription> pictures = intent.getParcelableArrayListExtra(UploaderService.EXTRA_PICTURES);
            if (pictures != null) {
                int sequence = 0;
                for (ImageDescription description : pictures) {
                    final String awsKey = UploadType.create(uploadTypeCode).getAWSKey(library, project, vin, cameraRole, sequence++, usedTurntable, needExtraction);
                    int uploadSubTypeCode = UploadType.DETAIL_IMAGES.getDatabaseCode();
                    final UploadFile uploadFile = new UploadFile(uploadGroup, description.image.getAbsolutePath(), awsKey, description.featureCode, description.featureTitle, priority, timestamp, description.sequenceNumber, uploadSubTypeCode);
                    
                    uploadFiles.add(uploadFile);
                    Timber.d("enqueue adding file %s", uploadFile);
                }
            }

            List<String> pathnames = intent.getStringArrayListExtra(UploaderService.EXTRA_PATHNAMES);
            if (pathnames != null) {
                int sequence = 0;
                for (String pathname : pathnames) {
                    String awsKey = UploadType.create(uploadTypeCode).getAWSKey(library, project, vin, cameraRole, sequence++, usedTurntable, needExtraction);
                    if (uploadTypeCode == UploadType.ANIMATED_PICTURE.getDatabaseCode()) {
                        // Animated pictures include both video and pictures so preserve the existing extension.
                        final int index = pathname.lastIndexOf('.');
                        if (index > -1) {
                            awsKey += pathname.substring(index + 1);
                        }
                    }

                    final UploadFile uploadFile;
                    if (uploadTypeCode == UploadType.ADD_EXTERIOR_POI.getDatabaseCode()) {
                        uploadFile = new UploadFile(uploadGroup, pathname, awsKey, priority, timestamp, title, poiDescription, spinNumber, spinIndex, addToMagazine, x, y);
                    } else if (uploadTypeCode == UploadType.PIP.getDatabaseCode()) {
                        if (sequence == 0) {
                            uploadFile = new UploadFile(uploadGroup, pathname, awsKey, priority, timestamp);
                        } else {
                            uploadFile = new UploadFile(uploadGroup, pathname, awsKey, priority, timestamp, x, y);
                        }
                    } else {
                        uploadFile = new UploadFile(uploadGroup, pathname, awsKey, priority, timestamp);
                    }

                    uploadFiles.add(uploadFile);
                    Timber.d("enqueue adding file %s", uploadFile);
                }
            }

            if (needExtraction) {
                uploadGroup.setUploadStatus(UploadGroup.UploadGroupStatus.EXTRACTION_PENDING);
                uploadGroup.setSourceVideoPathname(intent.getStringExtra(UploaderService.EXTRA_VIDEO_SOURCE));
                uploadGroup.setFrameCount(intent.getIntExtra(UploaderService.EXTRA_FRAME_COUNT, 0));
            }

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {
                public Void call() throws Exception {
                    databaseHelper.getUploadGroupDao().createOrUpdate(uploadGroup);

                    for (UploadFile file : uploadFiles) {
                        databaseHelper.getUploadFileDao().createOrUpdate(file);
                    }
                    return null;
                }
            });


            publishMediaCounts(vin);
            publishPendingVehicleCount();

        } catch (SQLException e) {
            Util.logError("Failed adding to upload table", e);
        }
    }

    public void startCompressionRequest(UploadFile file) {
        try {
            final long requestCode = getNextRequestCode();
            startCompressionRequest(file, requestCode);
        } catch (SQLException e) {
            Timber.e(e, "Failed starting compression request for %s", file.getPathname());
        }
    }

    private void startCompressionRequest(UploadFile file, long requestCode) throws SQLException {
        final UploadGroup group = file.getUploadGroup();
        final int uploadTypeCode = group.getUploadType();

        Timber.d("startCompressionRequest requestCode: %d file: %s group: %s", requestCode, file, group);

        final Intent intent;
        final int quality = new AppSharedPreferences(context).getDetailPictureQuality();
        if (uploadTypeCode == UploadType.DETAIL_IMAGES.getDatabaseCode()
                || uploadTypeCode == UploadType.ADD_EXTERIOR_POI.getDatabaseCode()
                || uploadTypeCode == UploadType.SPIN_IT_IMAGES.getDatabaseCode()
                || (uploadTypeCode == UploadType.ANIMATED_PICTURE.getDatabaseCode() && file.getPathname().endsWith(".jpg"))
                || uploadTypeCode == UploadType.PIP.getDatabaseCode()) {
            final String pathname = file.getPathname();
            intent = MediaProcessingService.createPictureIntent(context, requestCode, pathname, pathname + ".compressed", quality);
        } else if ((uploadTypeCode == UploadType.FULL_MOTION_VIDEO.getDatabaseCode() && TextUtils.isEmpty(file.getCompressedPathname()))
                || (uploadTypeCode == UploadType.ANIMATED_PICTURE.getDatabaseCode() && file.getPathname().endsWith(".mp4"))) {
            final String source = file.getPathname();
            final String destination = FileUtil.getNextFullMotionVideoFile(group.getVin()).getAbsolutePath();
            intent = MediaProcessingService.createVideoIntent(context, requestCode, source, destination, group.getYear(), group.getMake(), group.getModel());
        } else {
            intent = null;
        }

        if (intent != null) {
            file.setCompressionRequestCode(requestCode);
            databaseHelper.getUploadFileDao().update(file);
            context.startService(intent);
        }
    }

    public void startExtractionRequest(UploadGroup group) {
        try {
            final long requestCode = getNextFrameExtractionCode();
            startExtractionRequest(group, requestCode);
        } catch (SQLException e) {
            Timber.e(e, "Failed starting extraction request for %s", group.getSourceVideoPathname());
        }
    }

    private void startExtractionRequest(UploadGroup group, long requestCode) throws SQLException {
        Timber.d("startExtractionRequest requestCode: %d group: %s", requestCode, group);

        final Intent intent = MediaProcessingService.createFrameExtractionIntent(
                context,
                requestCode,
                group.getSourceVideoPathname(),
                group.getVin(),
                group.getFrameCount(),
                group.getYear(),
                group.getMake(),
                group.getModel());
        group.setExtractionRequestCode(requestCode);
        databaseHelper.getUploadGroupDao().update(group);
        context.startService(intent);
    }

    public void updateGroupCompletionStatus(UploadGroup uploadGroup) {
        final boolean isComplete = isGroupComplete(uploadGroup);

        Timber.d("updateGroupCompletionStatus isComplete: %b group: %s", isComplete, uploadGroup);

        if (isComplete) {
            setUploadGroupPending(uploadGroup);
        }
    }

    public void updateCompressionStatus(long requestCode, String pathname, boolean success) {
        Timber.d("updateCompressionStatus requestCode: %d success: %b pathname: %s", requestCode, success, pathname);

        try {
            compressionRequestCode.setValue(requestCode);
            UploadFile uploadFile = databaseHelper.getUploadFileDao().queryForFirst(pendingCompressionFileQuery);
            if (uploadFile == null) {
                // This can happen if the user deletes the media before it is compressed or if a previous
                // operation crashed or timed out. It isn't safe to delete the compressed image because
                // a retry might have been started. This probably doesn't happen enough to be a problem
                // but if it does we can create some type of delete queue.
                Timber.d("Could not find source file for compression result: %s", pathname);
                return;
            }

            if (success) {
                uploadFile.putCompressedPathname(pathname);
                databaseHelper.getUploadFileDao().update(uploadFile);
            }

        } catch (SQLException e) {
            Timber.e(e, "Error updating compression status");
        }
    }

    public void updateFrameExtractionStatus(long requestCode, boolean success, ArrayList<ImageDescription> pictures) {
        Timber.d("updateFrameExtractionStatus requestCode: %d success: %b", requestCode, success);

        try {
            frameExtractionRequestCode.setValue(requestCode);
            final UploadGroup uploadGroup = databaseHelper.getUploadGroupDao().queryForFirst(pendingFrameExtractionQuery);
            Timber.d("updateFrameExtractionStatus uploadGroup: %s", uploadGroup);
            if (uploadGroup == null) {
                // This can happen if the user deletes the media before frames are extracted or if
                // a previous operation crashed or timed out. It isn't safe to delete the source
                // image because a retry might have started. This probably doesn't happen enough to
                // be a problem but if it does we can create some type of delete queue.
                return;
            }

            if (!success) {
                // Reset code and we will try again later.
                uploadGroup.setExtractionRequestCode(0);
                return;
            }

            final UploadType uploadType = UploadType.create(uploadGroup.getUploadType());
            final int priority = uploadType.getUploadPriority();
            final String library = uploadGroup.getLibrary();
            final String project = uploadGroup.getProject();
            final String vin = uploadGroup.getVin();
            final boolean usedTurntable = uploadGroup.getUsedTurntable();
            int sequence = 0;

            // We use the timestamp of the group so that the files will be uploaded right
            // after extraction.
            final long timestamp = uploadGroup.getTimestamp();

            final ArrayList<UploadFile> uploadFiles = new ArrayList<>();
            for (ImageDescription picture : pictures) {
                final String awsKey = uploadType.getAWSKey(library, project, vin, uploadGroup.getCameraRole(), sequence++, usedTurntable, true);
                final UploadFile uploadFile = new UploadFile(uploadGroup, picture.image.getAbsolutePath(), awsKey, priority, timestamp);
                uploadFiles.add(uploadFile);
            }

            // Indicate files are ready to upload.
            uploadGroup.setUploadStatus(UploadGroup.UploadGroupStatus.FILES_PENDING);

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {
                public Void call() throws Exception {
                    databaseHelper.getUploadGroupDao().createOrUpdate(uploadGroup);

                    for (UploadFile file : uploadFiles) {
                        databaseHelper.getUploadFileDao().createOrUpdate(file);
                    }
                    return null;
                }
            });

            publishMediaCounts(vin);
        } catch (SQLException e) {
            Timber.e(e, "Error updating frame extraction status");
        }
    }

    public void refresh(UploadFile file) {
        try {
            databaseHelper.getUploadFileDao().refresh(file);
        } catch (SQLException e) {
            Timber.e(e, "Error refreshing file: %s", file);
        }
    }

    public void refresh(UploadGroup uploadGroup) {
        try {
            databaseHelper.getUploadGroupDao().refresh(uploadGroup);
        } catch (SQLException e) {
            Timber.e(e, "Error refreshing upload group: %s", uploadGroup);
        }
    }

    private long getNextRequestCode() throws SQLException {
        UploadFile file = databaseHelper.getUploadFileDao().queryForFirst(nextRequestCodeQuery);
        if (file == null) {
            return 1;
        } else {
            return file.getCompressionRequestCode() + 1;
        }
    }

    private long getNextFrameExtractionCode() throws SQLException {
        UploadGroup group = databaseHelper.getUploadGroupDao().queryForFirst(nextExtractionRequestCodeQuery);
        if (group == null) {
            return 1;
        } else {
            return group.getExtractionRequestCode() + 1;
        }
    }

    private boolean isGroupComplete(UploadGroup uploadGroup) {
        boolean isComplete = true;

        try {
            databaseHelper.getUploadGroupDao().refresh(uploadGroup);
            for (UploadFile file : uploadGroup.getFiles()) {
                if (!file.isUploadComplete()) {
                    isComplete = false;
                    break;
                }
            }
        } catch (SQLException e) {
            Util.logError("Error refreshing upload group", e);
            isComplete = false;
        }

        return isComplete;
    }

    public UploadGroup nextGroup() {
        UploadGroup group = null;

        try {
            group = databaseHelper.getUploadGroupDao().queryForFirst(nextGroupQuery);
            Timber.d("nextGroup found %s", group);
        } catch (SQLException e) {
            Util.logError("Error querying for next upload", e);
        }

        return group;
    }

    public UploadGroup nextFrameExtractionGroup() {
        UploadGroup group = null;

        try {
            group = databaseHelper.getUploadGroupDao().queryForFirst(nextFrameExtractionGroupQuery);
            Timber.d("nextGroup waiting for frame extraction found %s", group);
        } catch (SQLException e) {
            Timber.e(e, "Error querying for next group waiting for frame extraction");
        }

        return group;
    }

    public UploadFile nextFile() {
        UploadFile uploadFile = null;

        try {
            uploadFile = databaseHelper.getUploadFileDao().queryForFirst(nextFileQuery);
            Timber.d("nextFile found %s", uploadFile);
        } catch (SQLException e) {
            Util.logError("Error querying for next upload", e);
        }

        return uploadFile;
    }

    public boolean uploadFailuresExist() {
        try {
            if (databaseHelper.getUploadFileDao().queryForFirst(fileFailureQuery) != null) {
                Timber.d("uploadFailuresExist found file failure");
                return true;
            }

            if (databaseHelper.getUploadGroupDao().queryForFirst(groupFailureQuery) != null) {
                Timber.d("uploadFailuresExist found group failure");
                return true;
            }

            Timber.d("uploadFailuresExist no failures");
            return false;

        } catch (SQLException e) {
            Util.logError("Error querying for failed uploads");
        }

        return false;
    }

    private int getPendingVehicleCount() {
        int count = 0;

        try {
            GenericRawResults<String[]> rawResults = databaseHelper.getUploadFileDao().queryRaw(pendingCountQuery);
            String[] results = rawResults.getFirstResult();
            if (results != null && results.length > 0) {
                count = Integer.valueOf(results[0]);
            }
        } catch (SQLException e) {
            Util.logError("Error getting pending vehicle count", e);
        }

        return count;
    }

    public void setUploadComplete(UploadFile uploadFile) {
        Timber.d("setUploadComplete %s", uploadFile);

        setUploadStatus(uploadFile, UploadFile.UploadFileStatus.COMPLETE);
        publishMediaCounts(uploadFile.getUploadGroup().getVin());
    }

    public void setUploadFailed(UploadFile uploadFile) {
        Timber.d("setUploadFailed %s", uploadFile);

        setUploadStatus(uploadFile, UploadFile.UploadFileStatus.FAILED);
    }

    public void deleteCompletedGroups() {
        Timber.d("deleteCompletedGroups entered");

        try {
            final List<UploadGroup> groups = databaseHelper.getUploadGroupDao().query(completedGroupQuery);
            for (UploadGroup group : groups) {
                deleteUploadGroup(group);
            }
        } catch (SQLException e) {
            Util.logError("Error getting completed groups", e);
        }
    }

    private void deleteUploadGroup(final UploadGroup group) {
        Timber.d("deleteUploadGroup %s", group);
        try {
            for (UploadFile uploadFile : group.getFiles()) {
                final String pathname = uploadFile.getPathname();
                boolean shouldDelete = true;
                if (UploadType.create(group.getUploadType()) == UploadType.FULL_MOTION_VIDEO) {
                    // Don't delete this video if it is the source of a pending frame extraction.
                    videoPathnameArg.setValue(pathname);
                    if (databaseHelper.getUploadGroupDao().countOf(sourceVideoQuery) > 0) {
                        shouldDelete = false;
                    }
                }

                if (shouldDelete) {
                    deleteFile(pathname);
                }
                deleteFile(uploadFile.getCompressedPathname());
            }

            final String sourceVideoPathname = group.getSourceVideoPathname();

            TransactionManager.callInTransaction(databaseHelper.getConnectionSource(), new Callable<Object>() {
                public Void call() throws Exception {
                    databaseHelper.getUploadFileDao().delete(group.getFiles());
                    databaseHelper.getUploadGroupDao().delete(group);
                    return null;
                }
            });

            if (!TextUtils.isEmpty(sourceVideoPathname)) {
                // The source video could be used for both the video and upload and for
                // extracting frames so make sure it isn't still referenced before
                // deleting the file.
                pathnameArg.setValue(sourceVideoPathname);
                if (databaseHelper.getUploadFileDao().countOf(pendingVideoUploadQuery) == 0) {
                    Timber.d("Deleting file %s", sourceVideoPathname);
                    if (!new File(sourceVideoPathname).delete()) {
                        Timber.i("Failed deleting video file %s", sourceVideoPathname);
                    }
                }
            }
        } catch (SQLException e) {
            Timber.e(e, "Error deleting group %s", group);
        }
    }

    private void setUploadGroupPending(UploadGroup uploadGroup) {
        setUploadGroupStatus(uploadGroup, UploadGroup.UploadGroupStatus.PENDING);
    }

    public void setUploadGroupFailed(UploadGroup uploadGroup) {
        setUploadGroupStatus(uploadGroup, UploadGroup.UploadGroupStatus.FAILED);
    }

    public void setUploadGroupComplete(UploadGroup uploadGroup) {
        setUploadGroupStatus(uploadGroup, UploadGroup.UploadGroupStatus.COMPLETE);
        publishPendingVehicleCount();
    }

    private void setUploadGroupStatus(UploadGroup uploadGroup, UploadGroup.UploadGroupStatus status) {
        Timber.d("setUploadGroupStatus group: %s status: %s", uploadGroup, status);
        try {
            uploadGroup.setUploadStatus(status);
            databaseHelper.getUploadGroupDao().update(uploadGroup);
        } catch (SQLException e) {
            Util.logError("Error updating group status", e);
        }
    }

    public void removeUploadFile(UploadFile uploadFile) {
        Timber.d("removeUploadFile removing %s", uploadFile);
        try {
            final UploadGroup group = uploadFile.getUploadGroup();
            deleteFile(uploadFile.getPathname());
            deleteFile(uploadFile.getCompressedPathname());
            databaseHelper.getUploadFileDao().delete(uploadFile);

            databaseHelper.getUploadGroupDao().refresh(group);
            if (group.getFiles().isEmpty()) {
                deleteUploadGroup(group);
            } else {
                // See if this makes the group upload complete.
                updateGroupCompletionStatus(group);
            }

            publishPendingVehicleCount();
            publishMediaCounts(group.getVin());
        } catch (SQLException e) {
            Util.logError("Error deleting group", e);
        }
    }

    private void deleteFile(String pathname) {
        if (TextUtils.isEmpty(pathname)) {
            return;
        }

        final File file = new File(pathname);
        if (file.exists()) {
            Timber.d("deleting file %s", file.getAbsolutePath());
            if (!file.delete()) {
                Timber.e("failed deleting file %s", file.getAbsolutePath());
            }
        }
    }

    public void removePendingUploadFile(String pathname) {
        Timber.d("removePendingUploadFile %s", pathname);
        try {
            pathnameArg.setValue(pathname);
            compressedPathnameArg.setValue(pathname);
            final UploadFile uploadFile = databaseHelper.getUploadFileDao().queryForFirst(pendingFileQuery);
            if (uploadFile != null) {
                removeUploadFile(uploadFile);
            }
        } catch (SQLException e) {
            Util.logError("Error deleting group", e);
        }
    }

    /**
     * Reset errors for both files and groups
     */
    public void resetErrorStatus() {
        Timber.d("resetErrorStatus entered");
        try {
            databaseHelper.getUploadFileDao().update(resetFileErrorUpdate);
            databaseHelper.getUploadGroupDao().update(resetGroupErrorUpdate);
        } catch (SQLException e) {
            Util.logError("Error resetting file upload error", e);
        }
    }

    public void setTransferID(UploadFile uploadFile, int transferID) {
        Timber.d("setTransferID id: %d file: %s", transferID, uploadFile);
        uploadFile.setTransferID(transferID);
        try {
            databaseHelper.getUploadFileDao().update(uploadFile);
        } catch (SQLException e) {
            Util.logError("Error setting transfer ID", e);
        }
    }

    public UploadFile getUploadFile(int transferID) {
        UploadFile file = null;

        try {
            transferArg.setValue(transferID);
            file = databaseHelper.getUploadFileDao().queryForFirst(fileQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return file;
    }

    private void setUploadStatus(UploadFile uploadFile, UploadFile.UploadFileStatus status) {
        uploadFile.setUploadStatus(status);
        uploadFile.resetTransferID();
        try {
            databaseHelper.getUploadFileDao().update(uploadFile);
        } catch (SQLException e) {
            Util.logError("Error updating completion status", e);
        }
    }

    private void publishPendingVehicleCount() {
        final int count = getPendingVehicleCount();
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                uploadStatus.updatePendingVehicleCount(count);
            }
        });
    }

    private void publishMediaCounts(String vin) {
        PendingMediaCounts counts;
        try {
            vinArg.setValue(vin);
            counts = getMediaCounts(databaseHelper.getUploadFileDao(), databaseHelper.getUploadGroupDao(), mediaCountsQuery, vin, uploadTypeArg);
        } catch (SQLException e) {
            Util.logError("Error publishing media counts");
            counts = new PendingMediaCounts(vin);
        }

        final PendingMediaCounts mediaCounts = counts;
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                Util.getBus().post(mediaCounts);
            }
        });
    }

    private PreparedQuery<UploadFile> prepareNextFileQuery() throws SQLException {
        QueryBuilder<UploadFile, String> builder = databaseHelper.getUploadFileDao().queryBuilder();
        builder.where().eq(UploadFile.COLUMN_UPLOAD_STATUS, UploadFile.UploadFileStatus.PENDING.getDatabaseCode());
        builder.orderBy(UploadFile.COLUMN_PRIORITY, true)
                .orderBy(UploadFile.COLUMN_TIMESTAMP, true);
        return builder.prepare();
    }

    private PreparedQuery<UploadGroup> prepareNextGroupQuery() throws SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder.where().eq(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.PENDING.getDatabaseCode());
        builder.orderBy(UploadGroup.COLUMN_TIMESTAMP, true);
        return builder.prepare();
    }

    private PreparedQuery<UploadGroup> prepareNextFrameExtractionGroupQuery() throws SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder.where().eq(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.EXTRACTION_PENDING.getDatabaseCode());
        builder.orderBy(UploadGroup.COLUMN_TIMESTAMP, true);
        return builder.prepare();
    }

    private PreparedQuery<UploadGroup> prepareCompletedGroupQuery() throws SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder.where().eq(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.COMPLETE.getDatabaseCode());
        builder.orderBy(UploadGroup.COLUMN_TIMESTAMP, true);
        return builder.prepare();
    }

    private PreparedQuery<UploadFile> prepareFileQuery() throws SQLException {
        QueryBuilder<UploadFile, String> builder = databaseHelper.getUploadFileDao().queryBuilder();
        builder.where().eq(UploadFile.COLUMN_TRANSFER_ID, transferArg);
        return builder.prepare();
    }

    private PreparedQuery<UploadFile> preparePendingFileQuery() throws SQLException {
        QueryBuilder<UploadFile, String> fileBuilder = databaseHelper.getUploadFileDao().queryBuilder();
        Where<UploadFile, String> where = fileBuilder.where();
        where.and(
                where.or(
                        where.eq(UploadFile.COLUMN_PATHNAME, pathnameArg),
                        where.eq(UploadFile.COLUMN_COMPRESSED_PATHNAME, compressedPathnameArg)),
                where.eq(UploadFile.COLUMN_UPLOAD_STATUS, UploadFile.UploadFileStatus.PENDING.getDatabaseCode())
        );

        return fileBuilder.prepare();
    }

    private PreparedQuery<UploadFile> preparePendingVideoUploadQuery() throws SQLException {
        QueryBuilder<UploadFile, String> fileBuilder = databaseHelper.getUploadFileDao().queryBuilder();
        fileBuilder.where().eq(UploadFile.COLUMN_PATHNAME, pathnameArg);
        fileBuilder.setCountOf(true);
        return fileBuilder.prepare();
    }

    private PreparedQuery<UploadGroup> prepareSourceVideoQuery() throws  SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder.where().eq(UploadGroup.COLUMN_SOURCE_VIDEO_PATHNAME, videoPathnameArg);
        builder.setCountOf(true);
        return builder.prepare();
    }

    private PreparedQuery<UploadFile> preparePendingCompressionFileQuery() throws SQLException {
        QueryBuilder<UploadFile, String> fileBuilder = databaseHelper.getUploadFileDao().queryBuilder();
        fileBuilder
                .where()
                .eq(UploadFile.COLUMN_COMPRESSION_REQUEST_CODE, compressionRequestCode);
        return fileBuilder.prepare();
    }

    private PreparedQuery<UploadGroup> preparePendingFrameExtractionQuery() throws SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder
                .where()
                .eq(UploadGroup.COLUMN_EXTRACTION_REQUEST_CODE, frameExtractionRequestCode);
        return builder.prepare();
    }

    private PreparedQuery<UploadFile> prepareNextRequestCodeQuery() throws SQLException {
        QueryBuilder<UploadFile, String> builder = databaseHelper.getUploadFileDao().queryBuilder();
        builder.orderBy(UploadFile.COLUMN_COMPRESSION_REQUEST_CODE, false);
        return builder.prepare();
    }

    private PreparedQuery<UploadGroup> prepareNextExtractionRequestCodeQuery() throws SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder.orderBy(UploadGroup.COLUMN_EXTRACTION_REQUEST_CODE, false);
        return builder.prepare();
    }


    private PreparedQuery<UploadFile> prepareFailedFileQuery() throws SQLException {
        QueryBuilder<UploadFile, String> builder = databaseHelper.getUploadFileDao().queryBuilder();
        builder.where().eq(UploadFile.COLUMN_UPLOAD_STATUS, UploadFile.UploadFileStatus.FAILED.getDatabaseCode());
        return builder.prepare();
    }

    private PreparedQuery<UploadGroup> prepareFailedGroupQuery() throws SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder.where().eq(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.FAILED.getDatabaseCode());
        return builder.prepare();
    }

    private PreparedUpdate<UploadFile> prepareResetFileErrorUpdate() throws SQLException {
        UpdateBuilder<UploadFile, String> builder = databaseHelper.getUploadFileDao().updateBuilder();
        builder.updateColumnValue(UploadFile.COLUMN_UPLOAD_STATUS, UploadFile.UploadFileStatus.PENDING.getDatabaseCode())
                .where().eq(UploadFile.COLUMN_UPLOAD_STATUS, UploadFile.UploadFileStatus.FAILED.getDatabaseCode());
        return builder.prepare();
    }

    private PreparedUpdate<UploadGroup> prepareResetGroupErrorUpdate() throws SQLException {
        UpdateBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().updateBuilder();
        builder.updateColumnValue(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.PENDING.getDatabaseCode())
                .where().eq(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.FAILED.getDatabaseCode());
        return builder.prepare();
    }

    private String preparePendingCountQuery() throws SQLException {
        QueryBuilder<UploadGroup, String> builder = databaseHelper.getUploadGroupDao().queryBuilder();
        builder.selectRaw("COUNT(DISTINCT vin)").where().not().eq(UploadGroup.COLUMN_UPLOAD_STATUS, UploadGroup.UploadGroupStatus.COMPLETE.getDatabaseCode());
        return builder.prepareStatementString();
    }
}
