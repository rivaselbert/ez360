package com.sister.ivana;


import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
    private static final String ROOT_DIRECTORY = "iVana";
    private static final String DETAIL_PICTURES_DIRECTORY = "PICTURES";
    private static final String ANIMATED_PICTURES_DIRECTORY = "ANIMATED_PICTURES";
    private static final String SPINIT_DIRECTORY = "SPINIT";
    private static final String SPINIT_PICTURES_DIRECTORY = "SPINITPICS";
    private static final String FULLMO_DIRECTORY = "FULLMO";
    private static final String EZ360_DIRECTORY = "EZ360";
    private static final String IMAGES_DIRECTORY = "IMAGES";
    private static final String BANNERS_DIRECTORY = "BANNERS";
    private static final String LOG_DIRECTORY = "LOG";

    private static File getDirectory(String... components) {
        File directory = new File(Environment.getExternalStorageDirectory(), ROOT_DIRECTORY);
        for (String component : components) {
            directory = new File(directory, component);
        }

        if (!directory.exists() && !directory.mkdirs()) {
            Util.logError("Error making directory " + directory.getAbsolutePath());
        }

        return directory;
    }

    public static File getAnimatedPicturesDirectory(String vin) {
        return getDirectory(ANIMATED_PICTURES_DIRECTORY, vin);
    }

    public static File getSpinitDirectory(String vin) {
        return getDirectory(SPINIT_DIRECTORY, vin);
    }

    public static File getSpinItPicturesDirectory(String vin) {
        return getDirectory(SPINIT_PICTURES_DIRECTORY, vin);
    }

    public static File getFullMotionDirectory(String vin) {
        return getDirectory(FULLMO_DIRECTORY, vin);
    }

    public static File getEZ360Directory(String vin) {
        return getDirectory(EZ360_DIRECTORY, vin);
    }

    public static File getDetailPicturesDirectory(String vin) {
        return getDirectory(DETAIL_PICTURES_DIRECTORY, vin);
    }

    public static File getImagesDirectory() {
        return getDirectory(IMAGES_DIRECTORY);
    }

    public static File getBannersDirectory() {
        return getDirectory(BANNERS_DIRECTORY);
    }

    public static File getBannerFile(String project) {
        return new File(getDirectory(BANNERS_DIRECTORY, project.toUpperCase()), "BANNER.png");
    }

    public static File getNextFullMotionVideoFile(String vin) {
        final String format = String.format("%s.fmr.%%d.mp4", vin.toUpperCase());
        return getNextFile(getFullMotionDirectory(vin), format);
    }

    public static File getNextAnimatedPictureFile(String vin) {
        final String format = String.format("%s.fmr.%%d.mp4", vin.toUpperCase());
        return getNextFile(getAnimatedPicturesDirectory(vin), format);
    }

    public static File getNextSpinItVideoFile(String vin) {
        final String format = String.format("%s.fmr.%%d.mp4", vin.toUpperCase());
        return getNextFile(getSpinitDirectory(vin), format);
    }

    public static File getNextEZ360File(String vin) {
        final String format = String.format("%s.ez360.%%d.JPG", vin.toUpperCase());
        return getNextFile(getEZ360Directory(vin), format);
    }

    @SuppressLint("SimpleDateFormat")
    public static File getNextSpinItPictureFile(String vin) {
        final String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        final String format = String.format("IMG_%s_%%d.jpg", timestamp);
        return getNextFile(getSpinItPicturesDirectory(vin), format);
    }

    @SuppressLint("SimpleDateFormat")
    public static File getNextDetailPictureFile(String vin) {
        final String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        final String format = String.format("IMG_%s_%%d.jpg", timestamp);
        return getNextFile(getDetailPicturesDirectory(vin), format);
    }

    public static File getLocalImageFile(Uri uri) {
        return new File(getImagesDirectory(), uri.getLastPathSegment());
    }

    private static File getNextFile(File directory, String filenameFormat) {
        for (int i = 1; ; i++) {
            final String filename = String.format(filenameFormat, i);
            final File file = new File(directory, filename);
            if (!file.exists()) {
                return file;
            }
        }
    }

    public static File getLogFile(String filename) {
        return new File(getDirectory(LOG_DIRECTORY), filename);
    }

    public static File getLogDirectory() {
        return getDirectory(LOG_DIRECTORY);
    }
}
