package com.sister.ivana;

import android.content.DialogInterface;
import android.os.Bundle;

import com.sister.ivana.dialog.AppDialogFragment;

public class CreateVehicleDialogFragment extends AppDialogFragment {
    public interface CreateVehicleListener {
        void onCreateVehiclePositiveClick(String vin);

        void onCreateVehicleNegativeClick();
    }

    private static final String KEY_VIN = "KEY_VIN";

    public static CreateVehicleDialogFragment newInstance(String vin) {
        CreateVehicleDialogFragment fragment = new CreateVehicleDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_VIN, vin);
        final String text = String.format("Sorry, I cannot find %s in the app's inventory. Should I add it?\n\nHint: Did you download the inventory today?", vin);
        AppDialogFragment.setMessage(args, text);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_button_yes);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        final String vin = getArguments().getString(KEY_VIN);
        CreateVehicleListener listener = (CreateVehicleListener) getActivity();
        if (i == DialogInterface.BUTTON_POSITIVE) {
            listener.onCreateVehiclePositiveClick(vin);
            dismiss();
        } else if (i == DialogInterface.BUTTON_NEGATIVE) {
            listener.onCreateVehicleNegativeClick();
            dismiss();
        }
        dismiss();
    }
}
