package com.sister.ivana;

import android.content.Context;

/**
 * Created by csgulley on 5/1/18.
 */

public enum CameraRole {
    MASTER("master"),
    SPIN_2("spin_2"),
    SPIN_3("spin_3"),
    CLOSE_UP_1("closeup_1"),
    CLOSE_UP_2("closeup_2"),
    CLOSE_UP_3("closeup_3");

    private final String symbol;

    CameraRole(String symbol) {
        this.symbol = symbol;
    }

    public static CameraRole create(int code) {
        /*
         * Note that these numeric codes need to stay in sync with the values that the Settings
         * code uses.
        */
        switch (code) {
            case 1:
                return MASTER;
            case 2:
                return SPIN_2;
            case 3:
                return SPIN_3;
            case 4:
                return CLOSE_UP_1;
            case 5:
                return CLOSE_UP_2;
            case 6:
                return CLOSE_UP_3;
            default:
                return null;
        }
    }

    public String getSymbol() {
        return symbol;
    }

    public int getCode() {
        int code = 0;

        switch (this) {
            case MASTER:
                code = 1;
                break;

            case SPIN_2:
                code = 2;
                break;

            case SPIN_3:
                code = 3;
                break;

            case CLOSE_UP_1:
                code = 4;
                break;

            case CLOSE_UP_2:
                code = 5;
                break;

            case CLOSE_UP_3:
                code = 6;
                break;
        }

        return code;
    }

    public String getTitle(Context context) {
        final String[] titles = context.getResources().getStringArray(R.array.device_role_display);
        final int index = getCode() - 1;
        return index < titles.length ? titles[index] : "";
    }
}
