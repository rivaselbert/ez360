package com.sister.ivana;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sister.ivana.dialog.AppDialogFragment;
import com.squareup.picasso.Picasso;

import net.sourceforge.opencamera.CameraActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AddExteriorPoiSelectPosition extends DialogFragment {
    public interface AddExteriorPoiSelectPositionDialogListener {

        void onAddExteriorPoiSelectedPosition(float x, float y);

        void onAddExteriorPoiSelectPositionCancel();

    }

    static class OnDragTouchListener implements View.OnTouchListener {

        /**
         * Callback used to indicate when the drag is finished
         */
        public interface OnDragActionListener {
            /**
             * Called when drag event is started
             *
             * @param view The view dragged
             */
            void onDragStart(View view);

            /**
             * Called when drag event is completed
             *
             * @param view The view dragged
             */
            void onDragEnd(View view);
        }

        private View mView;
        private View mParent;
        private boolean isDragging;
        private boolean isInitialized = false;

        private int width;
        private float xWhenAttached;
        private float maxLeft;
        private float maxRight;
        private float dX;

        private int height;
        private float yWhenAttached;
        private float maxTop;
        private float maxBottom;
        private float dY;

        private OnDragActionListener mOnDragActionListener;

        public OnDragTouchListener(View view) {
            this(view, (View) view.getParent(), null);
        }

        public OnDragTouchListener(View view, View parent) {
            this(view, parent, null);
        }

        public OnDragTouchListener(View view, OnDragActionListener onDragActionListener) {
            this(view, (View) view.getParent(), onDragActionListener);
        }

        public OnDragTouchListener(View view, View parent, OnDragActionListener onDragActionListener) {
            initListener(view, parent);
            setOnDragActionListener(onDragActionListener);
        }

        public void setOnDragActionListener(OnDragActionListener onDragActionListener) {
            mOnDragActionListener = onDragActionListener;
        }

        public void initListener(View view, View parent) {
            mView = view;
            mParent = parent;
            isDragging = false;
            isInitialized = false;
        }

        public void updateBounds() {
            updateViewBounds();
            updateParentBounds();
            isInitialized = true;
        }

        public void updateViewBounds() {
            width = mView.getWidth();
            xWhenAttached = mView.getX();
            dX = 0;

            height = mView.getHeight();
            yWhenAttached = mView.getY();
            dY = 0;
        }

        public void updateParentBounds() {
            maxLeft = 0;
            maxRight = maxLeft + mParent.getWidth();

            maxTop = 0;
            maxBottom = maxTop + mParent.getHeight();
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (isDragging) {
                float[] bounds = new float[4];
                // LEFT
                bounds[0] = event.getRawX() + dX;
                if (bounds[0] < maxLeft) {
                    bounds[0] = maxLeft;
                }
                // RIGHT
                bounds[2] = bounds[0] + width;
                if (bounds[2] > maxRight) {
                    bounds[2] = maxRight;
                    bounds[0] = bounds[2] - width;
                }
                // TOP
                bounds[1] = event.getRawY() + dY;
                if (bounds[1] < maxTop) {
                    bounds[1] = maxTop;
                }
                // BOTTOM
                bounds[3] = bounds[1] + height;
                if (bounds[3] > maxBottom) {
                    bounds[3] = maxBottom;
                    bounds[1] = bounds[3] - height;
                }

                switch (event.getAction()) {
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        onDragFinish();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mView.animate().x(bounds[0]).y(bounds[1]).setDuration(0).start();
                        break;
                }
                return true;
            } else {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        isDragging = true;
                        if (!isInitialized) {
                            updateBounds();
                        }
                        dX = v.getX() - event.getRawX();
                        dY = v.getY() - event.getRawY();
                        if (mOnDragActionListener != null) {
                            mOnDragActionListener.onDragStart(mView);
                        }
                        return true;
                }
            }
            return false;
        }

        private void onDragFinish() {
            if (mOnDragActionListener != null) {
                mOnDragActionListener.onDragEnd(mView);
            }

            dX = 0;
            dY = 0;
            isDragging = false;
        }
    }

    @BindView(R.id.title_text) TextView titleTextView;
    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.cursorImageView) ImageView cursorImageView;

    private static final String KEY_IMAGE_URL = "KEY_IMAGE_URL";
    private String imageUrl;

    private Unbinder unbinder;

    public static AddExteriorPoiSelectPosition newInstance(String imageUrl) {
        AddExteriorPoiSelectPosition fragment = new AddExteriorPoiSelectPosition();
        Bundle args = new Bundle();
        args.putString(KEY_IMAGE_URL, imageUrl);
        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int width = (int) (getResources().getDimension(R.dimen.shot_selector_width));
        width += (int) Math.ceil(32 * metrics.density);

        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_external_poi_select_position, null);
        unbinder = ButterKnife.bind(this, view);
        final Dialog dialog = new Dialog(getActivity(), R.style.AppDialogTheme);

        titleTextView.setText("Move dot to desired position");

        dialog.setContentView(view);

        imageUrl = getArguments().getString(KEY_IMAGE_URL);

        Picasso picasso = Picasso.get();
        final Uri uri = Uri.parse(imageUrl);
        picasso.load(uri)
                .fit()
                .into(imageView);

        cursorImageView.setOnTouchListener(new OnDragTouchListener(cursorImageView));

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.negative_button)
    void onClickNegativeButton() {
        final AddExteriorPoiSelectPositionDialogListener listener = (AddExteriorPoiSelectPositionDialogListener) getActivity();
        listener.onAddExteriorPoiSelectPositionCancel();
        dismiss();
    }

    @OnClick(R.id.positive_button)
    void onClickPositiveButton() {
        float centerX = cursorImageView.getX() + cursorImageView.getWidth() / 2;
        float centerY = cursorImageView.getY() + cursorImageView.getHeight() / 2;

        if (centerX < imageView.getX()
                || centerX > imageView.getX() + imageView.getWidth()
                || centerY < imageView.getY()
                || centerY > imageView.getY() + imageView.getHeight()) {
            AppDialogFragment.show((AppCompatActivity) getActivity(), "Move the dot over a spot in the photo");
            return;
        }

        float normalizedX = (centerX - imageView.getX()) / imageView.getWidth();
        float normalizedY = (centerY - imageView.getY()) / imageView.getHeight();

        final AddExteriorPoiSelectPositionDialogListener listener = (AddExteriorPoiSelectPositionDialogListener) getActivity();
        listener.onAddExteriorPoiSelectedPosition(normalizedX, normalizedY);
        dismiss();
    }
}
