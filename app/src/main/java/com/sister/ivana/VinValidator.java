package com.sister.ivana;


public class VinValidator {
    private final String vin;

    public VinValidator(String vin) {
        this.vin = vin != null ? vin.toUpperCase() : "";
    }

    public boolean isValid() {
        return vin.matches("[[A-Z0-9]&&[^IOQ]]{17}");
    }

    public int getErrorString() {
        final int error;

        if (vin.length() != 17) {
            error = R.string.vin_error_length;
        } else if (vin.contains("I") || vin.contains("O") || vin.contains("Q")) {
            error = R.string.vin_error_disallowed_characters;
        } else {
            error = R.string.vin_error_general;
        }

        return error;
    }
}
