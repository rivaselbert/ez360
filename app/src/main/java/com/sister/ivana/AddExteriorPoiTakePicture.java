package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AddExteriorPoiTakePicture extends AppDialogFragment {
    public interface AddExteriorPoiTakePictureListener {
        void onAddExteriorPoiTakePicturePositiveClick();
    }

    @BindView(R.id.dont_show_again_checkbox) CheckBox dontShowAgainCheckBox;

    private Unbinder unbinder;

    public static AddExteriorPoiTakePicture newInstance() {
        AddExteriorPoiTakePicture fragment = new AddExteriorPoiTakePicture();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_add_exterior_poi_take_picture_title);
        AppDialogFragment.setContentView(args, R.layout.dialog_add_exterior_poi_take_picture);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_next);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_back);
        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int) (frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == DialogInterface.BUTTON_POSITIVE) {
            if (dontShowAgainCheckBox.isChecked()) {
                new AppSharedPreferences(getContext()).putDontShowAddExternalPoiMessage(true);
            }

            AddExteriorPoiTakePictureListener listener = (AddExteriorPoiTakePictureListener) getActivity();
            listener.onAddExteriorPoiTakePicturePositiveClick();
        }
        dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        return dialog;
    }


}
