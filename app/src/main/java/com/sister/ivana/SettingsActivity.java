package com.sister.ivana;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuItem;

import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;

import static com.sister.ivana.R.xml.preferences;

public class SettingsActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper> {
    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(preferences);
        }
    }

    private ActionBarUploadStatus actionBarStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        actionBarStatus = new ActionBarUploadStatus();
        actionBarStatus.onStart(this);
        populateUser();
        populateVersion();
    }

    @Override
    protected void onStop() {
        super.onStop();
        actionBarStatus.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vehicle_activity, menu);

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        MenuItem enableUploads = menu.findItem(R.id.action_enable_upload);
        enableUploads.setVisible(!preferences.areUploadsEnabled());
        MenuItem disableUploads = menu.findItem(R.id.action_disable_upload);
        disableUploads.setVisible(preferences.areUploadsEnabled());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);

        switch (item.getItemId()) {
            case R.id.action_logout:
                Util.logout(this);
                return true;

            case R.id.action_enable_upload:
                preferences.putUploadsEnabled(true);
                invalidateOptionsMenu();
                return true;

            case R.id.action_disable_upload:
                preferences.putUploadsEnabled(false);
                invalidateOptionsMenu();
                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                return false;
        }
    }

    private void populateUser() {
        SettingsFragment fragment = (SettingsFragment) getFragmentManager().findFragmentById(android.R.id.content);
        Preference userPreference = fragment.findPreference("user_display_field");

        final String title = String.format("Logged in as %s", new AppSharedPreferences(this).getCurrentUser());
        userPreference.setTitle(title);
    }

    private void populateVersion() {
        String version;
        try {
            PackageManager packageManager = getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            version = String.format("Version %s", packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Util.logError("Error reading version info", e);
            version = "Unknown version";
        }

        SettingsFragment fragment = (SettingsFragment) getFragmentManager().findFragmentById(android.R.id.content);
        Preference versionPreference = fragment.findPreference("version_display_field");
        versionPreference.setTitle(version);
    }
}
