package com.sister.ivana.messaging;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.SQLException;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteBaseService;
import com.j256.ormlite.stmt.PreparedUpdate;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.CameraBlankActivity;
import com.sister.ivana.EnvironmentSettingsActivity;
import com.sister.ivana.Util;
import com.sister.ivana.VehicleActivity;
import com.sister.ivana.VinSearchActivity;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.Peer;
import com.sister.ivana.database.UploadGroup;
import com.sister.ivana.database.Vehicle;

import net.sourceforge.opencamera.CameraActivity;

import org.w3c.dom.Text;

import timber.log.Timber;


public class UDPListenerService extends OrmLiteBaseService<DatabaseHelper> {
    public static final String BROADCAST_LISTEN_ADDRESS = "0.0.0.0";
    private DatagramSocket socket;
    private WifiManager.WifiLock wifiLock;
    private WifiManager.MulticastLock multicastLock;

    private void listenAndWaitAndThrowIntent(InetAddress broadcastIP, Integer port) throws IOException {
        byte[] recvBuf = new byte[512];
        if (socket == null || socket.isClosed()) {
            socket = new DatagramSocket(port);
            socket.setBroadcast(true);
        }

        DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
        Timber.i("Waiting for UDP broadcast");
        socket.receive(packet);

        String senderIP = packet.getAddress().getHostAddress();
        String message = new String(packet.getData()).trim();

        Timber.i("Received UDP broadcast from %s, message: %s", senderIP, message);
        onAdvertisementReceived(message);
    }

    private void onAdvertisementReceived(String json) {
        AdvertisementMessage message = null;
        try {
            Gson gson = new Gson();
            message = gson.fromJson(json, AdvertisementMessage.class);
        } catch (JsonParseException e) {
            Timber.e(e, "Exception parsing advertisement: %s", e.getLocalizedMessage());
            return;
        }

        if (message == null) {
            Timber.e("Error parsing advertisement");
            return;
        }

        final String remoteLibrary = message.getLibrary();
        if (TextUtils.isEmpty(remoteLibrary)) {
            Timber.e("Missing library in advertisement");
            return;
        }

        final String localLibrary = Util.getLibrary(getHelper());
        if (TextUtils.isEmpty(localLibrary)) {
            Timber.e("Missing local library");
            return;
        }

        if (!localLibrary.contentEquals(remoteLibrary)) {
            Timber.i("Ignoring advertisement for different library local: %s remote: %s", localLibrary, remoteLibrary);
            return;
        }

        final String deviceID = message.getDeviceID();
        if (TextUtils.isEmpty(deviceID)) {
            Timber.i("Ignoring advertisement with missing device ID");
            return;
        }

        if (deviceID.contentEquals(Util.getLogDeviceID(this))) {
            Timber.i("Ignoring advertisement from self");
            return;
        }

        final String address = message.getAddress();
        if (TextUtils.isEmpty(address)) {
            Timber.i("Ignoring advertisement with missing address");
            return;
        }

        updatePeerTable(deviceID, address);
    }

    private void updatePeerTable(String deviceID, String address) {
        try {
            final Peer peer = new Peer(deviceID, address, System.currentTimeMillis());
            getHelper().getPeerDao().createOrUpdate(peer);
            Timber.i("Added peer %s", peer);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Util.getBus().post(new NetworkService.PeerUpdate(peer));
                }
            });
        } catch (SQLException e) {
            Timber.e(e, "Failed updating peer table: %s", e.getLocalizedMessage());
        }
    }

    Thread UDPBroadcastThread;

    void startListenForUDPBroadcast() {
        if (UDPBroadcastThread != null) {
            Timber.i("already listening");
            return;
        }

        UDPBroadcastThread = new Thread(new Runnable() {
            public void run() {
                try {
                    AppSharedPreferences sharedPreferences = new AppSharedPreferences(UDPListenerService.this);
                    int port = sharedPreferences.getUDPCommunicationsPort();
                    InetAddress broadcastIP = InetAddress.getByName(BROADCAST_LISTEN_ADDRESS);
                    while (shouldRestartSocketListen) {
                        listenAndWaitAndThrowIntent(broadcastIP, port);
                    }
                } catch (IOException e) {
                    Timber.e(e, "UDPListenerService: no longer listening for UDP broadcasts cause of error %s", e.getMessage());
                    UDPBroadcastThread = null;
                }
            }
        });
        UDPBroadcastThread.start();
    }

    private Boolean shouldRestartSocketListen = true;

    @Override
    public void onDestroy() {
        Timber.i("onDestroy");
        shouldRestartSocketListen = false;
        socket.close();

        if (wifiLock != null) {
            wifiLock.release();
        }

        if (multicastLock != null) {
            multicastLock.release();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Background service running",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }

        Timber.i("onStartCommand");
        if (wifiLock == null) {
            final WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wifiLock = manager.createWifiLock(WifiManager.WIFI_MODE_FULL, "ivana.udp.listener");
            wifiLock.acquire();
        }

        if (multicastLock == null) {
            final WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            multicastLock = manager.createMulticastLock("ivana.udp.listener");
            multicastLock.acquire();
        }

        shouldRestartSocketListen = true;
        startListenForUDPBroadcast();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}