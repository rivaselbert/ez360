package com.sister.ivana.messaging

import timber.log.Timber
import java.io.DataInputStream
import java.io.IOException
import java.net.Socket

class MessageReceiver(val socket: Socket, val completion: (receiver: MessageReceiver) -> Unit, val callback: (payload: String) -> Unit) {
    val worker: Thread
    var running = true

    init {
        worker = Thread({read()})
        worker.start()
    }

    private fun read() {
        socket.use {
            try {
                val stream = DataInputStream(socket.getInputStream())
                stream.use {
                    while (running) {
                        val payload = stream.readUTF()
                        callback(payload)
                    }
                }
            } catch (e: IOException) {
                Timber.e(e, "Exception reading socket: %s", e.localizedMessage)
            }
        }

        completion(this)
    }

    fun shutdown() {
        running = false
        try {
            socket.close()
        } catch (e: IOException) {

        }
    }
}