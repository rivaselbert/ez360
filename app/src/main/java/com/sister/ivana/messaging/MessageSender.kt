package com.sister.ivana.messaging

import com.sister.ivana.AppSharedPreferences
import timber.log.Timber
import java.io.DataOutputStream
import java.io.IOException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import java.util.concurrent.BlockingDeque
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.LinkedBlockingQueue

class MessageSender(val address: InetAddress, val port: Int, val completion: (sender: MessageSender) -> Unit) {
    val worker: Thread
    val queue = LinkedBlockingQueue<String>()
    val socket = Socket()
    var running = true

    init {
        worker = Thread({write()})
        worker.start()
    }

    private fun write() {
        var stream: DataOutputStream? = null

        socket.use {
            try {
                socket.connect(InetSocketAddress(address, port))
                stream = DataOutputStream(socket.getOutputStream())
                stream.use {
                    while (running) {
                        val payload = queue.take()
                        stream?.writeUTF(payload)
                        stream?.flush()
                    }
                }
            } catch (e: IOException) {
                Timber.e(e, "Exception in writer %s: %s", address, e.localizedMessage)
            }
        }

        completion(this)
    }

    fun send(payload: String) {
        queue.add(payload)
    }

    fun shutdown() {
        running = false
        try {
            socket.close()
        } catch (e: IOException) {

        }
    }
}