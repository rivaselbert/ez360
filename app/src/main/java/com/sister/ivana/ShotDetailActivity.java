package com.sister.ivana;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShotDetailActivity extends AppCompatActivity implements DeletePhotoDialogFragment.DeletePhotoListener {
    public static final int RESULT_DELETE = 101;

    public static final String EXTRA_SHOT_INDEX = "EXTRA_SHOT_INDEX";

    private final float aspectRatio = 4f / 3f;
    private File image;
    private boolean havePhoto;
    private int index;

    private static final String EXTRA_IMAGE_PATHNAME = "EXTRA_IMAGE_PATHNAME";
    private static final String EXTRA_HAVE_PHOTO = "EXTRA_HAVE_PHOTO";
    private static final String EXTRA_INDEX = "EXTRA_INDEX";
    private static final String EXTRA_SHOOTING_INSTRUCTION = "EXTRA_SHOOTING_INSTRUCTION";

    @BindView(R.id.top_layout) RelativeLayout topLayout;
    @BindView(R.id.image_view) ImageView imageView;
    @BindView(R.id.button_close) ImageView closeButton;
    @BindView(R.id.button_delete) ImageView deleteButton;
    @BindView(R.id.top_label) TextView topLabel;
    @BindView(R.id.bottom_label) TextView bottomLabel;

    public static Intent createIntent(Context context, File image, boolean havePhoto, int index, String shootingInstruction) {
        Intent intent = new Intent(context, ShotDetailActivity.class);
        if (image != null) {
            intent.putExtra(EXTRA_IMAGE_PATHNAME, image.getAbsolutePath());
        }
        intent.putExtra(EXTRA_HAVE_PHOTO, havePhoto);
        intent.putExtra(EXTRA_INDEX, index);
        intent.putExtra(EXTRA_SHOOTING_INSTRUCTION, shootingInstruction);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_detail);
        ButterKnife.bind(this);

        final String pathname = getIntent().getStringExtra(EXTRA_IMAGE_PATHNAME);
        if (pathname != null) {
            image = new File(pathname);
        }
        havePhoto = getIntent().getBooleanExtra(EXTRA_HAVE_PHOTO, false);
        index = getIntent().getIntExtra(EXTRA_INDEX, -1);

        final String instruction = getIntent().getStringExtra(EXTRA_SHOOTING_INSTRUCTION);
        if (TextUtils.isEmpty(instruction)) {
            topLabel.setVisibility(View.GONE);
        } else {
            topLabel.setText(instruction);
            topLabel.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int height = (int) (metrics.heightPixels * 0.80);
        int width = Math.round(aspectRatio * (float) metrics.heightPixels);

        getWindow().setLayout(width, height);

        if (image != null) {
            Picasso.get()
                    .load(image)
                    .fit()
                    .centerCrop()
                    .into(imageView);
        }

        topLayout.setBackgroundColor(havePhoto ? Color.GREEN : Color.BLUE);

        if (!havePhoto) {
            deleteButton.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.button_close)
    void onCloseClick() {
        finish();
    }

    @OnClick(R.id.button_delete)
    void onDeleteClick() {
        final DeletePhotoDialogFragment fragment = DeletePhotoDialogFragment.newInstance();
        fragment.show(getFragmentManager(), "delete_photo");
    }

    @Override
    public void onDeletePhotoPositiveClick() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_SHOT_INDEX, index);
        setResult(RESULT_DELETE, intent);
        finish();
    }

    @Override
    public void onDeletePhotoNegativeClick() {
    }
}
