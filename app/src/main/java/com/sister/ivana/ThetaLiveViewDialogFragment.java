package com.sister.ivana;

import android.app.Dialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ThetaLiveViewDialogFragment extends AppDialogFragment {

    private Unbinder unbinder;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    public static ThetaLiveViewDialogFragment newInstance() {
        ThetaLiveViewDialogFragment fragment = new ThetaLiveViewDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, "Please wait");
        AppDialogFragment.setContentView(args, R.layout.layout_progress_bar);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.green), PorterDuff.Mode.MULTIPLY);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
