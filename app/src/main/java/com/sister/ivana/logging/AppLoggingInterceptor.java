package com.sister.ivana.logging;

import android.content.SharedPreferences;

import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

public class AppLoggingInterceptor implements Interceptor {
    private final HttpLoggingInterceptor.Logger logger = new HttpLoggingInterceptor.Logger() {
        @Override
        public void log(String message) {
            Timber.i(message);
        }
    };

    private final HttpLoggingInterceptor innerInterceptor = new HttpLoggingInterceptor(logger);

    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            if (s.contentEquals(AppSharedPreferences.KEY_IS_LOGGING_ENABLED)) {
                updateLogLevel(sharedPreferences.getBoolean(AppSharedPreferences.KEY_IS_LOGGING_ENABLED, false));
            }
        }
    };

    public AppLoggingInterceptor(AppSharedPreferences preferences) {
        preferences.registerOnSharedPreferenceChangeListener(preferenceListener);
        updateLogLevel(preferences.isLoggingEnabled());
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        return innerInterceptor.intercept(chain);
    }

    private void updateLogLevel(boolean enabled) {
        HttpLoggingInterceptor.Level level = (enabled || BuildConfig.DEBUG) ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;
        innerInterceptor.setLevel(level);
    }
}
