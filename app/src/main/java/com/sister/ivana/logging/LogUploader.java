package com.sister.ivana.logging;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.FileUtil;
import com.sister.ivana.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import timber.log.Timber;

public class LogUploader extends Handler {
    interface LogUploaderListener {
        void uploadsComplete(boolean success);
    }

    private static final String CURRENT_COMPRESSED_FILENAME = "log.txt.zip";
    private static final String CURRENT_FILENAME = "log.txt";

    private static final int MESSAGE_TYPE_START = 1;
    private static final int MESSAGE_TYPE_STOP = 2;
    private static final int MESSAGE_TYPE_UPLOAD_STATE_CHANGE = 3;
    private static final int MESSAGE_TYPE_UPLOAD_ERROR = 4;
    private static final String AWS_BUCKET = "sistertech-ivana";

    private final TransferUtility transferUtility;
    private final String library;
    private final String androidId;
    private LogUploaderListener listener;
    private final HashMap<Integer, File> pendingUploads = new HashMap<>();
    private final AppSharedPreferences sharedPreferences;
    private final boolean isUserRequested;
    private long modifiedTimestamp;
    private final LogTransferListener transferListener = new LogTransferListener(this);
    private boolean success = true;

    private boolean isStopped;

    private static class LogTransferListener implements TransferListener {
        private LogUploader uploader;

        public LogTransferListener(LogUploader uploader) {
            this.uploader = uploader;
        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Timber.d("onStateChanged id: %d state: %s", id, state);
            if (uploader != null) {
                uploader.sendStateChanged(id, state);
            }
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onError(int id, Exception ex) {
            Timber.e(ex, "onError id: " + id);
            if (uploader != null) {
                uploader.sendUploadError(id);
            }
        }
    }

    public LogUploader(Context context, Looper looper, String library, boolean isUserRequested, LogUploaderListener listener) {
        super(looper);

        this.library = TextUtils.isEmpty(library) ? "unknown_library" : library;
        this.listener = listener;
        this.isUserRequested = isUserRequested;
        this.androidId = Util.getLogDeviceID(context);
        final AmazonS3Client client = new AmazonS3Client(new BasicAWSCredentials(AppSharedPreferences.AWS_ACCESS, AppSharedPreferences.AWS_SECRET));
        this.transferUtility = new TransferUtility(client, context);
        this.sharedPreferences = new AppSharedPreferences(context);
    }

    /**
     * Both the media uploader and the log uploader depend on the AWS TransferUtility. We don't
     * want log uploads to interfere with normal functioning of app so media queue can use
     * this function to filer out log uploads.
     */
    public static boolean isLogTransfer(TransferObserver observer) {
        final String pathname = observer.getAbsoluteFilePath();
        if (pathname == null) {
            return false;
        }

        return pathname.startsWith(FileUtil.getLogDirectory().getAbsolutePath());
    }

    private static String getMessageTypeString(int type) {
        String str;

        switch (type) {
            case MESSAGE_TYPE_START:
                str = "MESSAGE_TYPE_START";
                break;
            case MESSAGE_TYPE_STOP:
                str = "MESSAGE_TYPE_STOP";
                break;
            case MESSAGE_TYPE_UPLOAD_STATE_CHANGE:
                str = "MESSAGE_TYPE_UPLOAD_STATE_CHANGE";
                break;
            case MESSAGE_TYPE_UPLOAD_ERROR:
                str = "MESSAGE_TYPE_UPLOAD_ERROR";
                break;

            default:
                str = Integer.toString(type);
        }
        return str;
    }

    @Override
    public void handleMessage(Message message) {
        Timber.d("handleMessage: %s", getMessageTypeString(message.what));

        switch (message.what) {
            case MESSAGE_TYPE_START:
                resumeExistingTransfers();
                uploadFiles();
                break;

            case MESSAGE_TYPE_STOP:
                isStopped = true;
                listener = null;
                stopUploads();
                getLooper().quit();
                break;

            case MESSAGE_TYPE_UPLOAD_STATE_CHANGE:
                // Check whether we are stopped because we can continue
                // to receive updates after we have canceled all uploads.
                // If we miss a state and don't delete a file we'll just
                // transfer it again next time we run.
                if (!isStopped) {
                    onStateChanged(message.arg1, (TransferState) message.obj);
                }
                break;

            case MESSAGE_TYPE_UPLOAD_ERROR:
                if (!isStopped) {
                    onError(message.arg1);
                }
                break;

            default:
                break;
        }
    }

    public void start() {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_START;
        sendMessage(message);
    }

    public void stop() {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_STOP;
        sendMessage(message);
    }

    private void sendStateChanged(int id, TransferState state) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_UPLOAD_STATE_CHANGE;
        message.arg1 = id;
        message.obj = state;
        sendMessage(message);
    }

    private void sendUploadError(int id) {
        final Message message = obtainMessage();
        message.what = MESSAGE_TYPE_UPLOAD_ERROR;
        message.arg1 = id;
        sendMessage(message);
    }

    /**
     * Old files are automatically zipped by the logger. The current file is not, though,
     * so we'll try to zip it before uploading it.
     */
    private File zip(File file) {
        File zipFile = null;
        FileOutputStream outputStream = null;
        FileInputStream inputStream = null;
        ZipOutputStream zipStream = null;
        try {
            final String outputPathname = file.getAbsoluteFile() + ".zip";
            outputStream = new FileOutputStream(outputPathname);
            inputStream = new FileInputStream(file);
            zipStream = new ZipOutputStream(outputStream);
            zipStream.putNextEntry(new ZipEntry(CURRENT_FILENAME));

            FileChannel inputChannel = inputStream.getChannel();
            WritableByteChannel outputChannel = Channels.newChannel(zipStream);
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            zipStream.closeEntry();

            zipFile = new File(outputPathname);
        } catch (IOException e) {
            Timber.e(e, "Error zipping log file");
        } finally {
            if (zipStream != null) {
                try {
                    zipStream.close();
                } catch (IOException ignored) {
                }
            }

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException ignored) {
                }
            }

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ignored) {
                }
            }
        }

        return zipFile;
    }

    private boolean shouldUploadFile(File file) {
        // Upload all zipped files
        if (file.getName().endsWith(".zip")) {
            return true;
        }

        // Upload current log file
        if (file.getName().contentEquals(CURRENT_FILENAME)) {
            // If the user requested the upload, don't check the last modified timestamp.
            if (isUserRequested) {
                return true;
            }

            // Check timestamp on last uploaded file so that we don't send the same log.txt
            // over and over after logs are disabled.
            final long lastUploaded = sharedPreferences.getLastUploadedLogFileTimestamp();
            final long current = file.lastModified();
            return lastUploaded != current;
        }

        return false;
    }

    private boolean isUploadPending(File file) {
        for (HashMap.Entry<Integer, File> entry : pendingUploads.entrySet()) {
            if (file.getAbsolutePath().contentEquals(entry.getValue().getAbsolutePath())) {
                return true;
            }
        }

        return false;
    }

    private void uploadFiles() {
        File[] files = FileUtil.getLogDirectory().listFiles();
        for (File file : files) {
            if (!shouldUploadFile(file)) {
                continue;
            }

            // Sometimes we fail to delete the zipped version of the current file after an upload
            // attempt. Don't try to upload it here because we are about generate a new zip file.
            if (file.getName().contentEquals(CURRENT_COMPRESSED_FILENAME)) {
                continue;
            }

            if (file.getName().contentEquals(CURRENT_FILENAME)) {
                modifiedTimestamp = file.lastModified();
                file = zip(file);
                if (file == null) {
                    continue;
                }
            }

            if (!isUploadPending(file)) {
                final String key = String.format(Locale.US, "%s/%s/%s/%s",
                        library, "logs", androidId, file.getName());
                TransferObserver observer = transferUtility.upload(AWS_BUCKET, key, file, CannedAccessControlList.PublicRead);
                observer.setTransferListener(transferListener);
                pendingUploads.put(observer.getId(), file);
                Timber.i("Started uploading %s", AppLogger.getTransferObserverString(observer));
            }
        }

        // Check status here in case we did not have any files to upload.
        checkStatus();
    }

    private void resumeExistingTransfers() {
        List<TransferObserver> uploads = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        for (TransferObserver observer : uploads) {
            if (!LogUploader.isLogTransfer(observer)) {
                continue;
            }

            pendingUploads.put(observer.getId(), new File(observer.getAbsoluteFilePath()));

            observer.setTransferListener(transferListener);
            final TransferState state = observer.getState();
            switch (state) {
                case PAUSED:
                    transferUtility.resume(observer.getId());
                    break;

                case COMPLETED:
                case FAILED:
                case CANCELED:
                    onStateChanged(observer.getId(), state);
            }
        }
    }

    private void onError(int id) {
        Timber.d("onError id:%d", id);
        pendingUploads.remove(id);
        success = false;
        checkStatus();
    }

    private void onStateChanged(int id, TransferState state) {
        Timber.d("onStateChanged id:%d state: %s", id, state);
        switch (state) {
            case COMPLETED:
            case FAILED:
            case CANCELED:
                File file = pendingUploads.remove(id);
                transferUtility.deleteTransferRecord(id);
                if (state == TransferState.COMPLETED && file != null) {
                    if (file.getName().contentEquals(CURRENT_COMPRESSED_FILENAME)) {
                        sharedPreferences.setLastUploadedLogFileTimestamp(modifiedTimestamp);
                    }

                    if (!file.delete()) {
                        Timber.e("Failed deleting file " + file.getAbsolutePath());
                    }
                }
                checkStatus();
                break;
        }
    }

    private void checkStatus() {
        if (pendingUploads.isEmpty() && listener != null) {
            listener.uploadsComplete(success);
        }
    }

    private void stopUploads() {
        List<TransferObserver> uploads = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        for (TransferObserver observer : uploads) {
            if (LogUploader.isLogTransfer(observer)) {
                observer.cleanTransferListener();
                File file = pendingUploads.get(observer.getId());
                if (file != null) {
                    if (file.getName().contentEquals(CURRENT_COMPRESSED_FILENAME)) {
                        // The current log file is constantly changing so cancel the upload and
                        // start fresh when we start uploading again.
                        transferUtility.cancel(observer.getId());
                    } else {
                        // Once the archived files are ready to upload they won't change so we
                        // can just pause the current upload.
                        transferUtility.pause(observer.getId());
                    }
                }
            }
        }
        transferListener.uploader = null;
    }
}
