package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;

public class ExteriorSpinOptionsTurntableDialogFragment extends AppDialogFragment {
    public interface SpinOptionsTurntableListener {
        void onSpinOptionsPositiveClick(boolean cutFramesFromVideo, boolean createFullMotionVideo);
    }

    @BindView(R.id.automated_pictures_radio_button) RadioButton automatedPicturesButton;
    @BindView(R.id.cut_frames_from_video_radio_button) RadioButton cutFromVideoButton;
    @BindView(R.id.create_video_checkbox) CheckBox createVideoCheckBox;

    private Unbinder unbinder;

    public static ExteriorSpinOptionsTurntableDialogFragment newInstance() {
        ExteriorSpinOptionsTurntableDialogFragment fragment = new ExteriorSpinOptionsTurntableDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_spinit_options_title);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_spinit_positive_button);
        AppDialogFragment.setContentView(args, R.layout.dialog_exterior_spin_options);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);

        final AppSharedPreferences preferences = new AppSharedPreferences(getContext());
        if (preferences.getCutPhotosFromVideo()) {
            cutFromVideoButton.setChecked(true);
            createVideoCheckBox.setEnabled(true);
        } else {
            automatedPicturesButton.setChecked(true);
            createVideoCheckBox.setEnabled(false);
        }
        createVideoCheckBox.setChecked(preferences.getSpinitCreateFullMotionVideo());

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == DialogInterface.BUTTON_POSITIVE) {
            // Save preferences
            final AppSharedPreferences preferences = new AppSharedPreferences(getContext());
            preferences.putCutPhotosFromVideo(cutFromVideoButton.isChecked());
            preferences.putSpinitCreateFullMotionVideo(createVideoCheckBox.isChecked());

            SpinOptionsTurntableListener listener = (SpinOptionsTurntableListener) getActivity();
            listener.onSpinOptionsPositiveClick(cutFromVideoButton.isChecked(), cutFromVideoButton.isChecked() && createVideoCheckBox.isChecked());
        }
        dismiss();
    }

    @OnCheckedChanged(R.id.cut_frames_from_video_radio_button)
    public void onCutFramesChanged() {
        createVideoCheckBox.setEnabled(cutFromVideoButton.isChecked());
    }

}
