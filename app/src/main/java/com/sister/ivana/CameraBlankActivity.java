package com.sister.ivana;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;
import com.sister.ivana.settings.SettingsServerComms;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CameraBlankActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper> {
    @BindView(R.id.camera_role_text_view) TextView cameraRoleEditText;

    private ActionBarUploadStatus actionBarStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_camera_blank);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        actionBarStatus = new ActionBarUploadStatus();
        actionBarStatus.onStart(this);
        cameraRoleEditText.setText(new AppSharedPreferences(this).getCameraRole().getTitle(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        SettingsServerComms settingsServerComms = new SettingsServerComms(this, getHelper());
        if (settingsServerComms.shouldPollSettings()) {
            settingsServerComms.startGetSettingsRequest(null);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        actionBarStatus.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vehicle_activity, menu);

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        MenuItem enableUploads = menu.findItem(R.id.action_enable_upload);
        enableUploads.setVisible(!preferences.areUploadsEnabled());
        MenuItem disableUploads = menu.findItem(R.id.action_disable_upload);
        disableUploads.setVisible(preferences.areUploadsEnabled());

        MenuItem logout = menu.findItem(R.id.action_logout);
        logout.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                Util.logout(this);
                return true;

            case R.id.action_enable_upload:
                return true;

            case R.id.action_disable_upload:
                return true;

            case R.id.action_settings:
                startActivity(new Intent(this, com.sister.ivana.settings.SettingsActivity.class));
                return true;

            case android.R.id.home:
//                finish();
                return true;

            default:
                return false;
        }
    }

}
