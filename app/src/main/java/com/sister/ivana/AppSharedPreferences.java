package com.sister.ivana;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sister.ivana.messaging.InterDeviceCommand;
import com.sister.ivana.messaging.NetworkService;
import com.sister.ivana.logging.CrashReporter;
import com.sister.ivana.settings.types.NumberSelectRowValue;

import net.sourceforge.opencamera.DraggableGridView;
import net.sourceforge.opencamera.FocusMode;

import java.util.HashMap;
import java.util.Locale;

public class AppSharedPreferences {
    @SuppressWarnings("SpellCheckingInspection")
    public static final String AWS_ACCESS = "AKIAIQ25UPQBVU7NCSUA";
    @SuppressWarnings("SpellCheckingInspection")
    public static final String AWS_SECRET = "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";

    public static final String KEY_LAST_SETTINGS_POLL = "last_settings_poll";
    public static final String KEY_LAST_VERSION_POLL = "last_version_poll";
    public static final String KEY_SETTINGS_FROM_SERVER = "settings_from_server";

    public static final String KEY_HAVE_TURN_TABLE = "have_turn_table";
    public static final String KEY_LENGTH_OF_SPIN = "length_of_spin";
    public static final String KEY_HOW_MANY_SPINS = "how_many_spins";
    public static final String KEY_FRAMES_PER_SPIN = "frames_per_spin";

    public static final String KEY_ALLOW_ANIMATED_PICTURES = "allow_animated_pictures";
    public static final String KEY_ANIMATED_FRAMES_PER_SECOND = "animated_frames_per_second";

    public static final String KEY_IS_CUT_FROM_VIDEO = "is_cut_from_video";
    public static final String KEY_SPIN_METHOD = "spin_method";
    public static final String KEY_SECONDS_BETWEEN_PICTURES = "seconds_between_pictures";
    public static final String KEY_FRAMES_IN_SPIN_NO_TT = "no_tt_frames_in_spin";
    public static final String KEY_AUTOMATED_EXTERNAL_PICTURES = "automated_external_pictures";

    public static final String KEY_MASTER_SLAVE_ENABLED = "master_slave_enabled";
    public static final String KEY_DEVICE_ROLE = "device_role";
    public static final String KEY_DEVICE_ALIAS = "device_alias";
    public static final String KEY_MASTER_TRIGGER = "master_trigger";
    public static final String KEY_UDP_COMMUNICATIONS_PORT = "udp_communications_port";
    public static final String KEY_SHARED_INPUT = "shared_input";
    public static final String KEY_IS_FIXED_CAMERA = "is_fixed_camera";
    public static final String KEY_COMPRESSION_RATIO = "compression_ratio";
    public static final String KEY_PICTURE_SIZE = "picture_size";
    public static final String KEY_IS_LOGGING_ENABLED = "logging_enabled";
    public static final String KEY_CAMERA_ID_STANDARD = "camera_id_standard";
    public static final String KEY_CAMERA_ID_WIDE = "camera_id_wide";
    public static final String KEY_COUNTDOWN_TIMER = "preference_countdown_timer";

    private static final String KEY_CURRENT_USER = "current_user";
    private static final String KEY_VIDEO_BITRATE = "preference_video_bitrate";
    private static final String KEY_FLASH_FORMAT = "flash_value_%d";
    private static final String KEY_WHITE_BALANCE = "preference_white_balance";
    private static final String KEY_IS_VIDEO = "is_video";
    private static final String KEY_CUT_PHOTOS_FROM_VIDEO = "cut_photos_from_video";
    private static final String KEY_SPINIT_CREATE_FULL_MOTION_VIDEO = "spinit_create_full_motion_video";
    private static final String KEY_LAST_SSID = "last_ssid";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_REMEMBER_ME_USERNAME = "remember_me_username";
    private static final String KEY_REMEMBER_ME_PASSWORD = "remember_me_password";
    private static final String KEY_SPIN_NO_TURNTABLE_METHOD = "spin_no_turntable_method";
    private static final String KEY_SPIN_NO_TURNTABLE_PICTURE_INTERVAL = "spin_no_turntable_picture_interval";
    private static final String KEY_SPIN_NO_TURNTABLE_EXTRACT_PICTURE_COUNT = "spin_no_turntable_extract_picture_count";
    private static final String KEY_SPIN_NO_TURNTABLE_AUTO_PICTURE_COUNT = "spin_no_turntable_auto_picture_count";
    private static final String KEY_SPIN_NO_TURNTABLE_VIDEO_ENABLED = "spin_no_turntable_video_enabled";
    private static final String KEY_FOCUS_MODE_SPIN = "focus_mode_spin";
    private static final String KEY_FOCUS_MODE_DETAIL_PICTURES = "focus_mode_detail_pictures";
    private static final String KEY_FOCUS_MODE_VIDEO = "focus_mode_video";
    private static final String KEY_CAMERA_ID = "camera_id";
    private static final String KEY_WALK_AROUND_GRID_LINES_X = "walk_around_grid_lines_x";
    private static final String KEY_WALK_AROUND_GRID_LINES_Y = "walk_around_grid_lines_y";
    private static final String KEY_CAMERA_ZOOM = "camera_zoom";
    private static final String KEY_LOG_FILE_LAST_MODIFIED = "log_file_last_modified";
    private static final String KEY_HDR_NEXT_WARNING = "hdr_next_warning";
    private static final String KEY_DONT_SHOW_LONG_CLICK_MESSAGE = "dont_show_long_click_message";
    private static final String KEY_DONT_SHOW_ADD_EXTERNAL_POI_MESSAGE = "dont_show_long_click_message";
    private static final String KEY_DONT_SHOW_ADD_PIP_MESSAGE = "dont_show_pip_message";
    private static final String KEY_DONT_SHOW_ADD_PIP_DETAIL_MESSAGE = "dont_show_pip_detail_message";
    private static final String KEY_MAX_CAPTURE_LENGTH = "max_capture_length";
    private static final String KEY_NUMBER_OF_PICS_EXTRACTED = "num_of_pics_extracted";

    private static final int DEFAULT_PICTURE_SIZE_WIDTH = 1920;
    private static final int DEFAULT_PICTURE_SIZE_HEIGHT = 1080;

    private static final int LOWER_PICTURE_SIZE_WIDTH = 1280;
    private static final int LOWER_PICTURE_SIZE_HEIGHT = 720;

    private static final int HIGHER_PICTURE_SIZE_WIDTH = 2048;
    private static final int HIGHER_PICTURE_SIZE_HEIGHT = 1152;

    private final SharedPreferences preferences;
    private final Context context;

    public AppSharedPreferences(Context context) {
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setDefaultValues(Context context) {
        PreferenceManager.setDefaultValues(context, R.xml.preferences, false);
    }

    public long getLastSettingsPoll() {
        return preferences.getLong(KEY_LAST_SETTINGS_POLL, 0);
    }

    public void updateLastSettingsPoll() {
        preferences.edit().putLong(KEY_LAST_SETTINGS_POLL, System.currentTimeMillis()).apply();
    }

    public long getLastVersionPoll() {
        return preferences.getLong(KEY_LAST_VERSION_POLL, 0);
    }

    public void updateLastVersionPoll() {
        preferences.edit().putLong(KEY_LAST_VERSION_POLL, System.currentTimeMillis()).apply();
    }

    public void storeServerSettings(JsonObject settingsJson) {
        String settingsAsString = null;
        if (settingsJson instanceof JsonElement) {
            JsonElement jsonElement = (JsonElement) settingsJson;
            settingsAsString = jsonElement.toString();
        } else {
            settingsAsString = settingsJson.getAsString();
        }
        preferences.edit().putString(KEY_SETTINGS_FROM_SERVER, settingsAsString).apply();
    }

    public JsonObject retrieveServerSettings() {
        JsonObject settingsJson = null;
        String settingsAsString = preferences.getString(KEY_SETTINGS_FROM_SERVER, null);
        if (settingsAsString != null) {
            settingsJson = new JsonParser().parse(settingsAsString).getAsJsonObject();
        }
        return settingsJson;
    }

    public String getKeyValue(String key) {
        return preferences.getString(key, "");
    }

    public void putKeyValue(String key, String value) {
        preferences.edit().putString(key, value).apply();
    }

    public int getIntKeyValue(String key, int defaultValue) {
        int value = defaultValue;
        try {
            value = preferences.getInt(key, defaultValue);
        } catch (ClassCastException e) {
            // protect against Settings that may have changed type
            e.printStackTrace();
        }
        return value;
    }

    public void putIntKeyValue(String key, int value) {
        preferences.edit().putInt(key, value).apply();
    }

    public boolean getBoolKeyValue(String key, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            value = preferences.getBoolean(key, defaultValue);
        } catch (ClassCastException e) {
            // protect against Settings that may have changed type
            e.printStackTrace();
        }
        return value;
    }

    public void putBoolKeyValue(String key, boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        preferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        preferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public boolean isLoggedIn() {
        return !TextUtils.isEmpty(getCurrentUser());
    }

    public boolean acceptSharedInput() {
        return preferences.getBoolean(KEY_SHARED_INPUT, true);
    }

    public boolean isMaster() {
        // 1 == Master
        return (1 == getIntKeyValue(AppSharedPreferences.KEY_DEVICE_ROLE, 1));
    }

    public int getUDPCommunicationsPort() {
        return preferences.getInt(KEY_UDP_COMMUNICATIONS_PORT, 11111);
    }

    public CameraRole getCameraRole() {
        if (!isMasterSlaveEnabled()) {
            return null;
        } else {
            return CameraRole.create(getIntKeyValue(KEY_DEVICE_ROLE, 0));
        }
    }

    public void putCameraRole(CameraRole role) {
        preferences.edit().putInt(KEY_DEVICE_ROLE, role.getCode()).apply();
    }

    public boolean isMasterSlaveEnabled() {
        return getBoolKeyValue(KEY_MASTER_SLAVE_ENABLED, false);
    }

    public void putMasterSlaveEnabled(boolean enabled) {
        preferences.edit().putBoolean(KEY_MASTER_SLAVE_ENABLED, enabled).apply();
    }

    public boolean triggerSlaves() {
        return getBoolKeyValue(AppSharedPreferences.KEY_MASTER_TRIGGER, true);
    }

    public String getCurrentUser() {
        return preferences.getString(KEY_CURRENT_USER, "");
    }

    public void putCurrentUser(String user) {
        if (TextUtils.isEmpty(user)) {
            throw new IllegalArgumentException("bad username");
        }

        preferences.edit().putString(KEY_CURRENT_USER, user).apply();
    }

    public void resetUser() {
        preferences.edit().putString(KEY_CURRENT_USER, null).apply();
        preferences.edit().putString(KEY_TOKEN, null).apply();
        preferences.edit().remove(KEY_LAST_SETTINGS_POLL).apply();
        CrashReporter.resetIdentifiers();
    }

    public void resetMasterSlave() {
        putMasterSlaveEnabled(false);
        putCameraRole(CameraRole.MASTER);
    }

    // This should probably be stored as a numeric type. However, it was stored as a string in
    // PixZero so I left it as is for now.
    public void putVideoBitrate(String bitrate) {
        preferences.edit().putString(KEY_VIDEO_BITRATE, bitrate).apply();
    }

    public String getFlash(int camera) {
        return preferences.getString(String.format(Locale.US, KEY_FLASH_FORMAT, camera), "");
    }

    public String getWhiteBalance() {
        return preferences.getString(KEY_WHITE_BALANCE, "");
    }

    public void putWhiteBalance(String whiteBalance) {
        preferences.edit().putString(KEY_WHITE_BALANCE, whiteBalance).apply();
    }

    public boolean isVideo() {
        return preferences.getBoolean(KEY_IS_VIDEO, false);
    }

    public void putIsVideo(boolean isVideo) {
        preferences.edit().putBoolean(KEY_IS_VIDEO, isVideo).apply();
    }

    public boolean allowAnimatedPictures() {
        return preferences.getBoolean(KEY_ALLOW_ANIMATED_PICTURES, false);
    }

    public boolean getUseTurntable() {
        return preferences.getBoolean(KEY_HAVE_TURN_TABLE, true);
    }

    public void putUseTurntable(boolean useTurntable) {
        preferences.edit().putBoolean(KEY_HAVE_TURN_TABLE, useTurntable).apply();
    }

    public boolean getCutPhotosFromVideo() {
        return preferences.getBoolean(KEY_CUT_PHOTOS_FROM_VIDEO, true);
    }

    public void putCutPhotosFromVideo(boolean cutPhotosFromVideo) {
        preferences.edit().putBoolean(KEY_CUT_PHOTOS_FROM_VIDEO, cutPhotosFromVideo).apply();
    }

    public boolean areUploadsEnabled() {
        return preferences.getBoolean(context.getString(R.string.preference_uploads_enabled), true);
    }

    public void putUploadsEnabled(final boolean autoUpload) {
        preferences.edit().putBoolean(context.getString(R.string.preference_uploads_enabled), autoUpload).apply();

        if (isMaster()) {
            InterDeviceCommand command = new InterDeviceCommand();
            command.data = new InterDeviceCommand.CommandData();
            command.data.command = autoUpload ? InterDeviceCommand.COMMAND_ENABLE_UPLOADS : InterDeviceCommand.COMMAND_DISABLE_UPLOADS;
            NetworkService.sendMessage(context, command);
        }
    }

    public boolean getSpinitCreateFullMotionVideo() {
        return preferences.getBoolean(KEY_SPINIT_CREATE_FULL_MOTION_VIDEO, false);
    }

    public void putSpinitCreateFullMotionVideo(boolean fullMotionVideo) {
        preferences.edit().putBoolean(KEY_SPINIT_CREATE_FULL_MOTION_VIDEO, fullMotionVideo).apply();
    }

    public boolean getDontShowLongClickMessage() {
        return preferences.getBoolean(KEY_DONT_SHOW_LONG_CLICK_MESSAGE, false);
    }

    public void putDontShowLongClickMessage(boolean longClickMessage) {
        preferences.edit().putBoolean(KEY_DONT_SHOW_LONG_CLICK_MESSAGE, longClickMessage).apply();
    }

    public boolean getDontShowAddExternalPoiMessage() {
        return preferences.getBoolean(KEY_DONT_SHOW_ADD_EXTERNAL_POI_MESSAGE, false);
    }

    public void putDontShowAddExternalPoiMessage(boolean longClickMessage) {
        preferences.edit().putBoolean(KEY_DONT_SHOW_ADD_EXTERNAL_POI_MESSAGE, longClickMessage).apply();
    }

    public boolean getDontShowPipMessage() {
        return preferences.getBoolean(KEY_DONT_SHOW_ADD_PIP_MESSAGE, false);
    }

    public void putDontShowPipMessage(boolean dontShow) {
        preferences.edit().putBoolean(KEY_DONT_SHOW_ADD_PIP_MESSAGE, dontShow).apply();
    }

    public boolean getDontShowPipDetailMessage() {
        return preferences.getBoolean(KEY_DONT_SHOW_ADD_PIP_DETAIL_MESSAGE, false);
    }

    public void putDontShowPipDetailMessage(boolean dontShow) {
        preferences.edit().putBoolean(KEY_DONT_SHOW_ADD_PIP_DETAIL_MESSAGE, dontShow).apply();
    }

    public int getMaxCaptureLength() {
        return preferences.getInt(KEY_MAX_CAPTURE_LENGTH, 4000);
    }

    public void putMaxCaptureLength(int maxCaptureLength){
        preferences.edit().putInt(KEY_MAX_CAPTURE_LENGTH, maxCaptureLength).apply();
    }

    public int getNumOfPicsExtracted(){
        return  preferences.getInt(KEY_NUMBER_OF_PICS_EXTRACTED,4);
    }

    public void putNumOfPicsExtracted(int numOfPicsExtracted) {
        preferences.edit().putInt(KEY_NUMBER_OF_PICS_EXTRACTED,numOfPicsExtracted).apply();
    }

    public int getSpinItVideoMaxDuration() {
        return preferences.getInt(KEY_LENGTH_OF_SPIN, 60);
    }

    public int getSpinitTotalFrames() {
        int defaultValue = 30;
        int framesPerSpin = preferences.getInt(KEY_FRAMES_PER_SPIN, defaultValue);

        //
        // Make sure 'framesPerSpin' is valid.  '0' will cause a crash.
        String[] spinValues = context.getResources().getStringArray(R.array.frames_per_spin_values);
        int max = -1;
        int min = -1;
        for (String spinValue : spinValues) {
            int value = 0;
            try {
                value = Integer.parseInt(spinValue);
            } catch (NumberFormatException e) {
            }
            if (value > 1 && (value > max || max == -1)) {
                max = value;
            }
            if (value > 1 && (value < min || min == -1)) {
                min = value;
            }
        }
        if (framesPerSpin < min || framesPerSpin > max) {
            // Get default value from serverSettings
            HashMap<String, String> map = new HashMap<>();
            map.put("key", KEY_FRAMES_PER_SPIN);
            NumberSelectRowValue numberSelectRowValue = new NumberSelectRowValue(map, null);
            Object defaultValueObject = numberSelectRowValue.getServerDefaultSetting(retrieveServerSettings());
            if (defaultValueObject != null && defaultValueObject instanceof Integer) {
                if ((int) defaultValueObject >= min && (int) defaultValueObject <= max) {
                    defaultValue = (int) defaultValueObject;
                }
            }

            framesPerSpin = defaultValue;
        }

        return framesPerSpin;
    }

    public String getThetaCameraSSID() {
        return preferences.getString(context.getString(R.string.preference_theta_camera_ssid), "");
    }

    public void putLastSSID(String ssid) {
        preferences.edit().putString(KEY_LAST_SSID, ssid).apply();
    }

    public String getLastSSID() {
        return preferences.getString(KEY_LAST_SSID, "");
    }

    public String getToken() {
        return preferences.getString(KEY_TOKEN, "");
    }

    public void putToken(String token) {
        preferences.edit().putString(KEY_TOKEN, token).apply();
    }

    public String getVideoCompression() {
        return preferences.getString(context.getString(R.string.preference_video_compression), "");
    }

    public String getVideoDimensions() {
        return preferences.getString(context.getString(R.string.preference_video_dimensions), "");
    }

    public int getDetailPictureQuality() {
        return preferences.getInt(KEY_COMPRESSION_RATIO, 70);
    }

    public int getPictureSize() {
        return preferences.getInt(KEY_PICTURE_SIZE, 2);
    }

    public int getPictureSizeWidth() {
        int sizeSetting = getPictureSize();
        int width = DEFAULT_PICTURE_SIZE_WIDTH;
        if (sizeSetting == 1) {
            width = HIGHER_PICTURE_SIZE_WIDTH;
        } else if (sizeSetting == 3) {
            width = LOWER_PICTURE_SIZE_WIDTH;
        }
        return width;
    }

    public int getPictureSizeHeight() {
        int sizeSetting = getPictureSize();
        int height = DEFAULT_PICTURE_SIZE_HEIGHT;
        if (sizeSetting == 1) {
            height = HIGHER_PICTURE_SIZE_HEIGHT;
        } else if (sizeSetting == 3) {
            height = LOWER_PICTURE_SIZE_HEIGHT;
        }
        return height;
    }

    public boolean shouldDelayVideoCompression() {
        return preferences.getBoolean(context.getString(R.string.preference_delay_video_compression), false);
    }

    public String getRememberMeUsername() {
        return preferences.getString(KEY_REMEMBER_ME_USERNAME, "");
    }

    public void putRememberMeUsername(String username) {
        preferences.edit().putString(KEY_REMEMBER_ME_USERNAME, username).apply();
    }

    public String getRememberMePassword() {
        return preferences.getString(KEY_REMEMBER_ME_PASSWORD, "");
    }

    public void putRememberMePassword(String password) {
        preferences.edit().putString(KEY_REMEMBER_ME_PASSWORD, password).apply();
    }

    public ExteriorSpinOptionsNoTurntableDialogFragment.SpinMethod getNoTurntableSpinMethod() {
        boolean isCutFromVideo = preferences.getBoolean(KEY_IS_CUT_FROM_VIDEO, true);
        int code = ExteriorSpinOptionsNoTurntableDialogFragment.SpinMethod.VIDEO.getCode();
        if (!isCutFromVideo) {
            code = ExteriorSpinOptionsNoTurntableDialogFragment.SpinMethod.AUTOMATIC_PICTURES.getCode();
        }

        int spinMethod = preferences.getInt(KEY_SPIN_METHOD, -1);
        if (spinMethod != -1) {
            code = spinMethod;
        }
        return ExteriorSpinOptionsNoTurntableDialogFragment.SpinMethod.create(code);
    }

    public void putNoTurntableSpinMethod(ExteriorSpinOptionsNoTurntableDialogFragment.SpinMethod method) {
        boolean isCutFromVideo = (method.getCode() == ExteriorSpinOptionsNoTurntableDialogFragment.SpinMethod.VIDEO.getCode());
        preferences.edit().putBoolean(KEY_IS_CUT_FROM_VIDEO, isCutFromVideo).apply();
        preferences.edit().putInt(KEY_SPIN_METHOD, method.getCode()).apply();
    }

    public int getSpinNoTurntablePictureInterval() {
        return preferences.getInt(KEY_SECONDS_BETWEEN_PICTURES, 2);
    }

    public void putSpinNoTurntablePictureInterval(int interval) {
        preferences.edit().putInt(KEY_SECONDS_BETWEEN_PICTURES, interval).apply();
    }

    public int getCountdownTimer() {
        return preferences.getInt(KEY_COUNTDOWN_TIMER, 5);
    }

    public void putCountdownTimer(int time) {
        preferences.edit().putInt(KEY_COUNTDOWN_TIMER, time).apply();
    }

    public int getSpinNoTurntableExtractPictureCount() {
        return preferences.getInt(KEY_FRAMES_IN_SPIN_NO_TT, 10);
    }

    public void putSpinNoTurntableExtractPictureCount(int count) {
        preferences.edit().putInt(KEY_FRAMES_IN_SPIN_NO_TT, count).apply();
    }

    public int getSpinNoTurntableAutoPictureCount() {
        return preferences.getInt(KEY_AUTOMATED_EXTERNAL_PICTURES, 20);
    }

    public void putSpinNoTurntableAutoPictureCount(int count) {
        preferences.edit().putInt(KEY_AUTOMATED_EXTERNAL_PICTURES, count).apply();
    }

    public boolean getSpinNoTurntableVideoEnabled() {
        return preferences.getBoolean(KEY_SPIN_NO_TURNTABLE_VIDEO_ENABLED, false);
    }

    public void putSpinNoTurntableVideoEnabled(boolean enabled) {
        preferences.edit().putBoolean(KEY_SPIN_NO_TURNTABLE_VIDEO_ENABLED, enabled).apply();
    }

    public FocusMode getSpinFocusMode() {
        final int code = preferences.getInt(KEY_FOCUS_MODE_SPIN, FocusMode.AUTO.getPreferenceCode());
        return FocusMode.create(code);
    }

    public void setSpinFocusMode(FocusMode mode) {
        preferences.edit().putInt(KEY_FOCUS_MODE_SPIN, mode.getPreferenceCode()).apply();
    }

    public FocusMode getDetailPicturesFocusMode() {
        final int code = preferences.getInt(KEY_FOCUS_MODE_DETAIL_PICTURES, FocusMode.MANUAL.getPreferenceCode());
        return FocusMode.create(code);
    }

    public void setDetailPicturesFocusMode(FocusMode mode) {
        preferences.edit().putInt(KEY_FOCUS_MODE_DETAIL_PICTURES, mode.getPreferenceCode()).apply();
    }

    public FocusMode getVideoFocusMode() {
        final int code = preferences.getInt(KEY_FOCUS_MODE_VIDEO, FocusMode.AUTO.getPreferenceCode());
        return FocusMode.create(code);
    }

    public void setVideoFocusMode(FocusMode mode) {
        preferences.edit().putInt(KEY_FOCUS_MODE_VIDEO, mode.getPreferenceCode()).apply();
    }

    public String getCameraID() {
        String id = preferences.getString(KEY_CAMERA_ID, "");
        if (TextUtils.isEmpty(id)) {
            final String androidID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (!TextUtils.isEmpty(androidID)) {
                id = androidID;
                setCameraID(id);
            }
        }
        return id;
    }

    private void setCameraID(String id) {
        preferences.edit().putString(KEY_CAMERA_ID, id).apply();
    }

    public int getCameraZoom() {
        int zoom = preferences.getInt(KEY_CAMERA_ZOOM, 0);
        return zoom;
    }

    public void setCameraZoom(int zoom) {
        preferences.edit().putInt(KEY_CAMERA_ZOOM, zoom).apply();
    }

    public void setWalkAroundGridLines(float percentX, float percentY) {
        preferences.edit().putFloat(KEY_WALK_AROUND_GRID_LINES_X, percentX).apply();
        preferences.edit().putFloat(KEY_WALK_AROUND_GRID_LINES_Y, percentY).apply();
    }

    public float getWalkAroundGridLinesX() {
        return preferences.getFloat(KEY_WALK_AROUND_GRID_LINES_X, DraggableGridView.DEFAULT_SIZE);
    }

    public float getWalkAroundGridLinesY() {
        return preferences.getFloat(KEY_WALK_AROUND_GRID_LINES_Y, DraggableGridView.DEFAULT_SIZE);
    }

    public boolean isLoggingEnabled() {
        return preferences.getBoolean(KEY_IS_LOGGING_ENABLED, false);
    }

    public long getLastUploadedLogFileTimestamp() {
        return preferences.getLong(KEY_LOG_FILE_LAST_MODIFIED, 0);
    }

    public void setLastUploadedLogFileTimestamp(long modified) {
        preferences.edit().putLong(KEY_LOG_FILE_LAST_MODIFIED, modified).apply();
    }

    public long getHdrNextWarning() {
        return preferences.getLong(KEY_HDR_NEXT_WARNING, 0);
    }

    public void setHdrNextWarning(long timestamp) {
        preferences.edit().putLong(KEY_HDR_NEXT_WARNING, timestamp).apply();
    }
}
