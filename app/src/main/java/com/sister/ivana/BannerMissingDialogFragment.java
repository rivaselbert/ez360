package com.sister.ivana;

import android.content.DialogInterface;
import android.os.Bundle;

import com.sister.ivana.dialog.AppDialogFragment;

public class BannerMissingDialogFragment extends AppDialogFragment {

    public static BannerMissingDialogFragment newInstance() {
        BannerMissingDialogFragment fragment = new BannerMissingDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, "No overlay found");
        AppDialogFragment.setMessage(args, "Please upload (using iContent) or contact support@sister.tv");
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_button_ok);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();
    }
}
