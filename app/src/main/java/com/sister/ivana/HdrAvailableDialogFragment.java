package com.sister.ivana;


import android.app.Dialog;
import android.graphics.Rect;
import android.os.Bundle;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class HdrAvailableDialogFragment extends AppDialogFragment {
    public interface HdrListener {
        void onHdrOk();

        void onHdrDoNotShowAgain();
    }

    private Unbinder unbinder;

    public static HdrAvailableDialogFragment newInstance() {
        HdrAvailableDialogFragment fragment = new HdrAvailableDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, "HDR 360");
        AppDialogFragment.setMessage(args, R.string.dialog_hdr_available_message);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_button_ok);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_hdr_available_button_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();

        // The dialog has a lot of text and it doesn't lay out nicely using the default
        // behavior so make it wider
        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int) (frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @OnClick(R.id.positive_button)
    public void onPositiveClick() {
        final HdrListener listener = (HdrListener) getActivity();
        listener.onHdrOk();
        dismiss();
    }

    @OnClick(R.id.negative_button)
    public void onNegativeClick() {
        final HdrListener listener = (HdrListener) getActivity();
        listener.onHdrDoNotShowAgain();
        dismiss();
    }
}
