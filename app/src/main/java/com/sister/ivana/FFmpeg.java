package com.sister.ivana;


import android.content.Context;
import android.media.MediaMetadataRetriever;

import com.netcompss.loader.LoadJNI;
import com.sister.ivana.logging.LogReceiver;
import com.sister.ivana.settings.SettingsAdapter;

import net.sourceforge.opencamera.ImageDescription;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Don't make Timber calls directly since this runs in a different process. Use LogReceiver to
 * pass the log statements to the main process
 */

public class FFmpeg {
    private static final String TAG = "FFmpeg";
    private static FFmpeg ffmpeg;
    private final LoadJNI loadJNI;

    private FFmpeg() {
        loadJNI = new LoadJNI();
    }

    public static FFmpeg getFFmpeg() {
        if (ffmpeg == null) {
            ffmpeg = new FFmpeg();
        }

        return ffmpeg;
    }

    public void compressFullMotionVideo(Context c, String sourcePath, String targetPath) {
        final AppSharedPreferences preferences = new AppSharedPreferences(c);
        try {
            LogReceiver.i(c, TAG, "compressFullMotionVideo source: %s target: %s compression: %s dimensions: %s", sourcePath, targetPath, preferences.getVideoCompression(), preferences.getVideoDimensions());
            String[] complexCommand = {
                    "ffmpeg",
                    "-y",
                    "-i",
                    sourcePath,
                    "-b",
                    preferences.getVideoCompression(),
                    "-r",
                    "29.97",
                    "-an",
                    "-preset",
                    "ultrafast",
                    "-s",
                    preferences.getVideoDimensions(),
                    "-sws_flags",
                    "fast_bilinear",
                    targetPath
            };

            String workFolder = c.getFilesDir().getAbsolutePath();
            loadJNI.run(complexCommand, workFolder, c);

            LogReceiver.i(c, TAG, "completed video compression");
        } catch (Throwable e) {
            Util.logError("Error transcoding video", e);
        }
    }

    public ArrayList<ImageDescription> extractFrames(Context context, String videoPath, String vin, long frameCount) {
        return extractFrames(context, videoPath, vin, frameCount, true);
    }

    public ArrayList<ImageDescription> extractFrames(Context context, String videoPath, String vin, long frameCount, boolean isSpinIt) {
        final AppSharedPreferences preferences = new AppSharedPreferences(context);

        LogReceiver.i(context, TAG, "extractFrames source: %s vin: %s frameCount: %d", videoPath, vin, frameCount);

        String path = FileUtil.getSpinItPicturesDirectory(vin).getAbsolutePath();
        if (!isSpinIt) {
            path = FileUtil.getAnimatedPicturesDirectory(vin).getAbsolutePath();
        }
        final String format = String.format("%s/%s.spd.%s.%%d.jpg",
                path,
                vin,
                new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss", Locale.US).format(new Date()));

        final MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(videoPath);
        float duration_seconds = getVideoDuration(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        mmr.release();

        float fps = (frameCount - 1) / duration_seconds;

        // TODO MAKE THE QUALITY QSCALE AND S 960X540 DYNAMIC BY SETTING IN PREF WINDOW.
        int width = preferences.getPictureSizeWidth();
        int height = preferences.getPictureSizeHeight();

        LogReceiver.i(context, TAG, "extractFrames duration: %f fps: %f width: %d height: %d", duration_seconds, fps, width, height);
        LogReceiver.i(context, TAG, "format: %s", format);

        String[] multi_image_cmd = {
                "ffmpeg",
                "-i",
                videoPath,
                "-vf",
                "fps=" + fps,
                "-qscale:v",
                "1",
                "-s",
                width + "x" + height,
                format
        };

        final ArrayList<ImageDescription> descriptions = new ArrayList<>();
        String workFolder2 = context.getFilesDir().getAbsolutePath();
        try {
            loadJNI.run(multi_image_cmd, workFolder2, context.getApplicationContext());
            LogReceiver.i(context, TAG, "completed frame extraction");
        } catch (Throwable e) {
            Util.logError("Error extracting frames from video", e);
            return descriptions;
        }

        for (int i = 1; ; i++) {
            final File image = new File(String.format(Locale.US, format, i));
            if (image.exists()) {
                descriptions.add(new ImageDescription(image, i));
            } else {
                break;
            }
        }

        return descriptions;
    }

    public ImageDescription convertGif(Context c, String sourcePath, String targetPath) {
        final AppSharedPreferences preferences = new AppSharedPreferences(c);
        try {
            int fps = preferences.getIntKeyValue(AppSharedPreferences.KEY_ANIMATED_FRAMES_PER_SECOND, SettingsAdapter.ANIMATED_FRAMES_PER_SECOND_DEFAULT);
            LogReceiver.i(c, TAG, "convertGif source: %s target: %s fps: %s dimensions: %s", sourcePath, targetPath, fps, preferences.getVideoDimensions());
            String[] complexCommand = {
                    "ffmpeg",
                    "-i",           // input
                    sourcePath,
//                  "-vf",          // video filter
//                  "scale=500:-1",
//                  "-t",           // output duration
//                  "10",
                    "-r",           // output frame rate (fps)
                    String.valueOf(fps),
                    targetPath
            };

            String workFolder = c.getFilesDir().getAbsolutePath();
            loadJNI.run(complexCommand, workFolder, c);

            LogReceiver.i(c, TAG, "completed video conversion to GIF");
        } catch (Throwable e) {
            Util.logError("Error transcoding video", e);
        }

        ImageDescription description = null;
        final File image = new File(targetPath);
        if (image.exists()) {
            description = new ImageDescription(image, 0);
        }

        return description;
    }

    private float getVideoDuration(String durationText) {
        float duration = 0;
        try {
            duration = Integer.parseInt(durationText);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        // Return in seconds
        return duration / 1000;
    }
}

