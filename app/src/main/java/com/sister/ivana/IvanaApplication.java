package com.sister.ivana;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.os.Process;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.messaging.AdvertiserService;
import com.sister.ivana.messaging.NetworkService;
import com.sister.ivana.messaging.UDPListenerService;
import com.sister.ivana.logging.AppLogger;
import com.sister.ivana.logging.CrashReporter;
import com.sister.ivana.webapi.AppVersionBody;

import java.util.List;

import io.fabric.sdk.android.Fabric;

public class IvanaApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Crashlytics crashlytics = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(this, crashlytics);
        setCrashLogIdentifiers();

        AppVersionBody.updateAppVersion(getApplicationContext());

        // Media operations happen in another process but we control all logging through the
        // main process so only initialize the logger from the main process.
        final int pid = Process.myPid();
        final ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
        if (processes != null) {
            for (ActivityManager.RunningAppProcessInfo info : processes) {
                // If we don't set these properties ORMLite tries to use slf4j and generates a lot of
                // debug data into our log file
                System.setProperty("com.j256.ormlite.logger.type", "LOCAL");
                System.setProperty("com.j256.ormlite.logger.level", "ERROR");

                if (info.pid == pid && "com.sister.ivana".contentEquals(info.processName)) {
                    AppLogger.getLogger().initialize(getApplicationContext());
                }
            }
        }

        Intent udpListener = new Intent(this, UDPListenerService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(udpListener);
        } else {
            startService(udpListener);
        }

        Intent advertiser = new Intent(this, AdvertiserService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(advertiser);
        } else {
            startService(advertiser);
        }

        Intent network = new Intent(this, NetworkService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(network);
        } else {
            startService(network);
        }
    }

    private void setCrashLogIdentifiers() {
        DatabaseHelper helper = null;
        try {
            helper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
            CrashReporter.setIdentifiers(this, helper);
        } finally {
            if (helper != null) {
                OpenHelperManager.releaseHelper();
            }
        }
    }
}
