package com.sister.ivana.firebase;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.SQLException;

import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.CameraBlankActivity;
import com.sister.ivana.EnvironmentSettingsActivity;
import com.sister.ivana.Util;
import com.sister.ivana.VehicleActivity;
import com.sister.ivana.VinSearchActivity;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.Vehicle;

import net.sourceforge.opencamera.CameraActivity;

import timber.log.Timber;


public class UDPListenerService extends Service {

    DatagramSocket socket;

    private void listenAndWaitAndThrowIntent(InetAddress broadcastIP, Integer port) throws Exception {
        byte[] recvBuf = new byte[15000];
        if (socket == null || socket.isClosed()) {
            socket = new DatagramSocket(port, broadcastIP);
            socket.setBroadcast(true);
        }

        DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
        Timber.i("Waiting for UDP broadcast");
        socket.receive(packet);

        String senderIP = packet.getAddress().getHostAddress();
        String message = new String(packet.getData()).trim();

        Timber.i("Received UDP broadcast from %s, message: %s", senderIP, message);
        onPayloadReceived(senderIP, message);

        socket.close();
    }

    private void onPayloadReceived(String senderIP, String jsonString) {
        Gson gson = new Gson();
        final InterDeviceCommand data = gson.fromJson(jsonString, InterDeviceCommand.class);

        final AppSharedPreferences sharedPreferences = new AppSharedPreferences(UDPListenerService.this);
        if (InterDeviceCommand.COMMAND_CHANGE_VIN.equals(data.data.command)) {

            // In addition to checking whether VIN sharing is enabled we check whether a download is active. This is to avoid having
            // the inventory download interrupted when the activity is recreated.
            if (sharedPreferences.isLoggedIn() && sharedPreferences.acceptSharedInput() && !VinSearchActivity.isInventoryDownloadActive()) {

                addVehicle(data.data.vin, data.data.project, data.data.library, data.data.isCameraOnly);

                Intent environmentSettingsActivity = new Intent(UDPListenerService.this, EnvironmentSettingsActivity.class);
                environmentSettingsActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Intent vinSearchActivity = new Intent(UDPListenerService.this, VinSearchActivity.class);
                Intent vehicleActivity = VehicleActivity.createIntent(UDPListenerService.this, data.data.vin);

                android.app.TaskStackBuilder.create(UDPListenerService.this)
                        .addNextIntentWithParentStack(environmentSettingsActivity)
                        .addNextIntentWithParentStack(vinSearchActivity)
                        .addNextIntentWithParentStack(vehicleActivity)
                        .startActivities();
            }
        }
        if (InterDeviceCommand.COMMAND_CHANGE_SLAVE_VIN.equals(data.data.command)) {
            // In addition to checking whether VIN sharing is enabled we check whether a download is active. This is to avoid having
            // the inventory download interrupted when the activity is recreated.
            if (sharedPreferences.isLoggedIn() && sharedPreferences.isMasterSlaveEnabled() && !sharedPreferences.isMaster() && !VinSearchActivity.isInventoryDownloadActive()) {
                addVehicle(data.data.vin, data.data.project, data.data.library, data.data.isCameraOnly);

                Intent cameraBlankActivity = new Intent(UDPListenerService.this, CameraBlankActivity.class);
                Intent cameraActivity = CameraActivity.createSlavePictureIntent(UDPListenerService.this, data.data.vin);

                TaskStackBuilder taskStackBuilder = android.app.TaskStackBuilder.create(UDPListenerService.this)
                        .addNextIntentWithParentStack(cameraBlankActivity)
                        .addNextIntentWithParentStack(cameraActivity);
                taskStackBuilder.startActivities();
            }
        } else if (InterDeviceCommand.COMMAND_ACTIVATE_CAMERA.equals(data.data.command)) {
            if (sharedPreferences.isLoggedIn() && !sharedPreferences.isMaster()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        // Event Bus must be accessed from the main thread
                        Util.getBus().post(data.slaveCaptureData);
                    }
                });
            }
        } else if (InterDeviceCommand.COMMAND_ENABLE_UPLOADS.equals(data.data.command)) {
            if (sharedPreferences.isLoggedIn() && !sharedPreferences.isMaster()) {
                sharedPreferences.putUploadsEnabled(true);
            }
        } else if (InterDeviceCommand.COMMAND_DISABLE_UPLOADS.equals(data.data.command)) {
            if (sharedPreferences.isLoggedIn() && !sharedPreferences.isMaster()) {
                sharedPreferences.putUploadsEnabled(false);
            }
        }
    }

    private void addVehicle(String vin, String project, String library, boolean isCameraOnly) {
        if (vin == null || project == null || library == null) {
            return;
        }

        try {
            final String libraryID = library.replace(",", "");
            final String projectID = project.replace(",", "");

            DatabaseHelper databaseHelper = null;
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
            databaseHelper.getVehicleDao().createIfNotExists(new Vehicle(vin, projectID, libraryID, isCameraOnly));
            OpenHelperManager.releaseHelper();
        } catch (SQLException e) {
            Util.logError("Database error adding new VIN", e);
        }
    }


    Thread UDPBroadcastThread;
    void startListenForUDPBroadcast() {
        UDPBroadcastThread = new Thread(new Runnable() {
            public void run() {
                try {
                    AppSharedPreferences sharedPreferences = new AppSharedPreferences(UDPListenerService.this);
                    int port = sharedPreferences.getUDPCommunicationsPort();
                    InetAddress broadcastIP = InetAddress.getByName(UDPSender.BROADCAST_LISTEN_ADDRESS);
                    while (shouldRestartSocketListen) {
                        listenAndWaitAndThrowIntent(broadcastIP, port);
                    }
                } catch (Exception e) {
                    Timber.i("UDPListenerService: no longer listening for UDP broadcasts cause of error %s", e.getMessage());
                }
            }
        });
        UDPBroadcastThread.start();
    }

    private Boolean shouldRestartSocketListen = true;

    @Override
    public void onDestroy() {
        shouldRestartSocketListen = false;
        socket.close();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        shouldRestartSocketListen = true;
        startListenForUDPBroadcast();
        Timber.i("UDPListenerService: Waiting for UDP broadcast");
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}