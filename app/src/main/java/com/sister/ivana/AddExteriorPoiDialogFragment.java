package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sister.ivana.dialog.AppAlertDialog;
import com.sister.ivana.dialog.AppDialogFragment;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AddExteriorPoiDialogFragment extends AppDialogFragment {
    public interface AddExteriorPoiDialogListener {

        void onAddExteriorPoiDialogNext(String title, String description);

    }

    @BindView(R.id.topContainer) ViewGroup topContainer;
    @BindView(R.id.titleEditText) EditText titleEditText;
    @BindView(R.id.descriptionEditText) EditText descriptionEditText;

    private Unbinder unbinder;

    public static AddExteriorPoiDialogFragment newInstance() {
        AddExteriorPoiDialogFragment fragment = new AddExteriorPoiDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, "NEW External Point of Interest");
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_next);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_cancel);
        AppDialogFragment.setContentView(args, R.layout.dialog_add_external_poi);
        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int) (frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);

        topContainer.invalidate();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i != DialogInterface.BUTTON_POSITIVE) {
            dismiss();
            return;
        }

        final String title = titleEditText.getText().toString().trim();
        if (TextUtils.isEmpty(title)) {
            AppDialogFragment.show((AppCompatActivity)getActivity(), "Title is required");
            return;
        }

        if (title.length() > 25) {
            AppDialogFragment.show((AppCompatActivity)getActivity(), "Max length for title is 25 characters");
            return;
        }

        final String description = descriptionEditText.getText().toString().trim();

        final AddExteriorPoiDialogListener listener = (AddExteriorPoiDialogListener) getActivity();
        listener.onAddExteriorPoiDialogNext(title, description);
        dismiss();
    }


}
