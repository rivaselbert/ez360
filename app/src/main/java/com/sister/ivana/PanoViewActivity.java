/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sister.ivana;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.panorama.Panorama;
import com.google.android.gms.panorama.PanoramaApi.PanoramaResult;

public class PanoViewActivity extends Activity implements ConnectionCallbacks,
        OnConnectionFailedListener {
    public Uri panoUri;
    private GoogleApiClient mClient;
    private boolean alreadyStarted = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClient = new GoogleApiClient.Builder(this, this, this)
                .addApi(Panorama.API)
                .build();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            panoUri = Uri.parse(extras.getString("pano_uri"));
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onStart() {
        // this is neccessary bc onBackPressed() method override doesnt work when implementing connection
        // callback, so users would otherwise end up hitting the back button many times to exit
        super.onStart();
        if (!alreadyStarted) {
            mClient.connect();
            alreadyStarted = true;
        } else {
            finish();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Panorama.PanoramaApi.loadPanoramaInfo(mClient, panoUri).setResultCallback(
                new ResultCallback<PanoramaResult>() {
                    @Override
                    public void onResult(PanoramaResult result) {
                        final Status status = result.getStatus();
                        if (status.isSuccess()) {
                            Intent viewerIntent = result.getViewerIntent();
                            if (viewerIntent != null) {
                                startActivity(viewerIntent);
                            }
                        } else {
                            Util.logError("Failed loading panorama info: " + status);
                        }
                    }
                });
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Util.logInfo("Panorama service connection suspended: " + cause);
    }

    @Override
    public void onConnectionFailed(ConnectionResult status) {
        Util.logError("Panorama service connection failed: " + status);
        finish();
    }

    @Override
    public void onStop() {
        super.onStop();
        mClient.disconnect();
    }
}
