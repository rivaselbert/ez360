package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;

import com.sister.ivana.dialog.AppDialogFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AddExteriorPoiSelectPicture extends AppDialogFragment {
    public interface AddExteriorPoiSelectPictureDialogListener {

        void onAddExteriorPoiSelectedPicture(int position);

        void onAddExteriorPoiSelectCancel();

    }

    private static final String KEY_THUMBNAILS = "KEY_THUMBNAILS";

    public class GridSpacing extends RecyclerView.ItemDecoration {
        private int spanCount;
        private int spacing;

        public GridSpacing(int spanCount, int spacing) {
            this.spanCount = spanCount;
            this.spacing = (int)(spacing / 2.0);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.left = spacing;
            outRect.right = spacing;
            outRect.bottom = spacing / 2;
            outRect.top = 0;
        }
    }

    @BindView(R.id.topContainer) ViewGroup topContainer;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.negative_button) Button cancelButton;

    private Unbinder unbinder;
    private ArrayList<String> thumbnailUrls;

    public static AddExteriorPoiSelectPicture newInstance(ArrayList<String> thumbnails) {
        AddExteriorPoiSelectPicture fragment = new AddExteriorPoiSelectPicture();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, "Tap to select the picture for this new POI");
        AppDialogFragment.setContentView(args, R.layout.dialog_add_external_poi_select_picture);

        args.putStringArrayList(KEY_THUMBNAILS, thumbnails);

        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int width = (int) (getResources().getDimension(R.dimen.shot_selector_width));
        width += (int) Math.ceil(32 * metrics.density);


        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);

        thumbnailUrls = getArguments().getStringArrayList(KEY_THUMBNAILS);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        final int spacing = (int)Math.ceil(6 * metrics.density);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));
        recyclerView.addItemDecoration(new GridSpacing(4, spacing));

        int width = (int) (getResources().getDimension(R.dimen.shot_selector_width));
        final AddExteriorPoiPictureAdapter adapter = new AddExteriorPoiPictureAdapter(Picasso.get(), (width - 4 * spacing), thumbnailUrls, new AddExteriorPoiPictureAdapter.AddExternalPoiPictureListener() {
            @Override
            public void shotClicked(int position) {
                final AddExteriorPoiSelectPictureDialogListener listener = (AddExteriorPoiSelectPictureDialogListener) getActivity();
                listener.onAddExteriorPoiSelectedPicture(position);
                dismiss();
            }
        });
        recyclerView.setAdapter(adapter);

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.negative_button)
    void onClickNegativeButton() {
        final AddExteriorPoiSelectPictureDialogListener listener = (AddExteriorPoiSelectPictureDialogListener) getActivity();
        listener.onAddExteriorPoiSelectCancel();
        dismiss();
    }
}
