package com.sister.ivana.database;

import android.text.TextUtils;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.sister.ivana.webapi.elasticsearch.InventoryResults;

import java.security.InvalidParameterException;
import java.util.List;

@SuppressWarnings("WeakerAccess")
@DatabaseTable(tableName = "vehicle")
public class Vehicle {
    public static final String COLUMN_VIN = "vin";
    public static final String COLUMN_PROJECT = "project";
    public static final String COLUMN_LIBRARY = "library";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_MAKE = "make";
    public static final String COLUMN_MODEL = "model";
    public static final String COLUMN_STOCK = "stock";
    public static final String COLUMN_MILES = "miles";
    public static final String COLUMN_NEW_USED = "newUsed";
    public static final String COLUMN_THUMBNAIL = "thumbnail";
    public static final String COLUMN_IS_CAMERA_ONLY = "isCameraOnly";

    /*
     * Most fields are stored as strings even if they are numeric data because they are mostly
     * for display purposes and are returned from the server as strings.
     */
    @DatabaseField(id = true, canBeNull = false, columnName = COLUMN_VIN)
    private String vin;

    @DatabaseField(canBeNull = false, columnName = COLUMN_PROJECT)
    private String project;

    @DatabaseField(canBeNull = false, columnName = COLUMN_LIBRARY)
    private String library;

    @DatabaseField(canBeNull = false, columnName = COLUMN_YEAR)
    private String year;

    @DatabaseField(canBeNull = false, columnName = COLUMN_MAKE)
    private String make;

    @DatabaseField(canBeNull = false, columnName = COLUMN_MODEL)
    private String model;

    @DatabaseField(canBeNull = false, columnName = COLUMN_STOCK)
    private String stock;

    @DatabaseField(canBeNull = false, columnName = COLUMN_MILES)
    private String miles;

    @DatabaseField(canBeNull = false, columnName = COLUMN_NEW_USED)
    private String newUsed;

    @DatabaseField(canBeNull = false, columnName = COLUMN_THUMBNAIL)
    private String thumbnail;

    @DatabaseField(canBeNull = false, columnName = COLUMN_IS_CAMERA_ONLY, defaultValue = "false")
    private boolean isCameraOnly;

    public static Vehicle create(InventoryResults.VehicleData vehicleData) {
        Vehicle vehicle = new Vehicle();
        vehicle.setVin(vehicleData.vin);
        vehicle.setLibrary(vehicleData.library_id);
        vehicle.setProject(vehicleData.project_id);
        vehicle.setYear(vehicleData.year);
        vehicle.setMake(vehicleData.make);
        vehicle.setModel(vehicleData.model);
        vehicle.setStock(vehicleData.stock_no);
        vehicle.setMiles(vehicleData.miles);
        vehicle.setNewUsed(vehicleData.new_used);
        vehicle.setThumbnail(extractThumbnail(vehicleData));
        return vehicle;
    }

    private static String getFirstImage(List<String> images) {
        String url = "";

        if (images != null) {
            for (String image : images) {
                if (!TextUtils.isEmpty(image)) {
                    url = image;
                    break;
                }
            }
        }

        return url;
    }

    /**
     * Find best match for a thumbnail image.
     */
    private static String extractThumbnail(InventoryResults.VehicleData vehicleData) {
        String url = getFirstImage(vehicleData.display_pics);
        if (!TextUtils.isEmpty(url)) {
            return url;
        }

        // spin_detail_pics is actually an array of array of images
        if (vehicleData.spin_detail_pics != null) {
            for (List<String> pictures : vehicleData.spin_detail_pics) {
                url = getFirstImage(pictures);
                if (!TextUtils.isEmpty(url)) {
                    return url;
                }
            }

        }

        url = getFirstImage(vehicleData.detail_pics);
        if (!TextUtils.isEmpty(url)) {
            return url;
        }

        url = getFirstImage(vehicleData.third_party_pics);
        if (!TextUtils.isEmpty(url)) {
            return url;
        }

        url = getFirstImage(vehicleData.stock_pics);
        if (!TextUtils.isEmpty(url)) {
            return url;
        }

        return "";
    }

    // Required by ORMLite
    public Vehicle() {
    }

    public Vehicle(String vin, String project, String library, boolean isCameraOnly) {
        this.vin = vin;
        this.project = project;
        this.library = library;
        this.isCameraOnly = isCameraOnly;
        year = "";
        make = "";
        model = "";
        stock = "";
        miles = "";
        newUsed = "";
        thumbnail = "";
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        if (vin == null) {
            throw new InvalidParameterException("vin can't be null");
        }
        this.vin = vin.toUpperCase();
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project != null ? project : "";
    }

    public String getLibrary() {
        return library;
    }

    public void setLibrary(String library) {
        this.library = library != null ? library : "";
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year != null ? year : "";
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make != null ? make : "";
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model != null ? model : "";
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock != null ? stock : "";
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles != null ? miles : "";
    }

    public String getNewUsed() {
        return newUsed;
    }

    public void setNewUsed(String newUsed) {
        this.newUsed = newUsed != null ? newUsed : "";
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isCameraOnly() {
        return isCameraOnly;
    }
}
