package com.sister.ivana.database;

import android.net.Uri;
import android.text.TextUtils;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.sister.ivana.Util;
import com.sister.ivana.webapi.elasticsearch.MagazineResults;

@DatabaseTable(tableName = "magazineShot")
public class MagazineShot {

    public static final String COLUMN_INDEX = "id";
    public static final String COLUMN_ORDER = "order";
    public static final String COLUMN_MAGAZINE_ID = "magazineID";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_INSTRUCTION = "instruction";
    public static final String COLUMN_FEATURE_CODE = "featureCode";

    @DatabaseField(generatedId = true, columnName = COLUMN_INDEX)
    private int id;

    @DatabaseField(canBeNull = false, columnName = COLUMN_ORDER)
    private long order;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoCreate = true, columnName = COLUMN_MAGAZINE_ID)
    private Magazine magazine;

    @DatabaseField(canBeNull = false, columnName = COLUMN_TITLE, defaultValue = "")
    private String title;

    @DatabaseField(canBeNull = false, columnName = COLUMN_DESCRIPTION, defaultValue = "")
    private String description;

    @DatabaseField(canBeNull = false, columnName = COLUMN_IMAGE, defaultValue = "")
    private String image;

    @DatabaseField(canBeNull = false, columnName = COLUMN_INSTRUCTION, defaultValue = "")
    private String instruction;

    @DatabaseField(canBeNull = false, columnName = COLUMN_FEATURE_CODE, defaultValue = "")
    private String featureCode;


    public static MagazineShot create(Magazine magazine, MagazineResults.MagazineShotData shotData, long order) {
        MagazineShot shot = new MagazineShot();
        shot.order = order;
        shot.magazine = magazine;
        shot.title = Util.enforceNonNull(shotData.feature_title);
        shot.description = Util.enforceNonNull(shotData.feature_description);
        shot.image = Util.enforceNonNull(shotData.reference_image);
        shot.instruction = Util.enforceNonNull(shotData.shooting_instruction);
        shot.featureCode = shotData.feature_code;
        return shot;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Uri getImageUri() {
        if (!TextUtils.isEmpty(image)) {
            return Uri.parse(image);
        } else {
            return null;
        }
    }

    public String getTitle() {
        return title;
    }

    public String getFeatureCode() {
        return featureCode;
    }

    public String getInstruction() {
        return instruction;
    }
}
