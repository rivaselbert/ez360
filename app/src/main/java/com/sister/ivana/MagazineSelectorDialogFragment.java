package com.sister.ivana;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sister.ivana.database.Magazine;
import com.sister.ivana.dialog.AppDialogFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MagazineSelectorDialogFragment extends DialogFragment {
    public interface MagazineSelectorListener {
        void onMagazineListenerPositiveClick(int magazineID, int requestCode);

        void onMagazineListenerNegativeClick(int requestCode);
    }

    private static final String KEY_VEHICLE_TYPES = "KEY_VEHICLE_TYPES";
    private static final String KEY_MAGAZINE_IDS = "KEY_MAGAZINE_IDS";
    private static final String KEY_DEFAULT_SELECTION = "KEY_DEFAULT_SELECTION";
    private static final String KEY_REQUEST_CODE = "KEY_REQUEST_CODE";

    private Unbinder unbinder;
    private ArrayList<Integer> magazineIDs;
    private int requestCode;

    @BindView(R.id.magazine_radio_group) RadioGroup magazineRadioGroup;

    public static MagazineSelectorDialogFragment newInstance(ArrayList<Magazine> magazines, int requestCode) {
        return newInstance(magazines, requestCode, -1);
    }

    public static MagazineSelectorDialogFragment newInstance(ArrayList<Magazine> magazines, int requestCode, int defaultSelection) {
        MagazineSelectorDialogFragment fragment = new MagazineSelectorDialogFragment();
        Bundle args = new Bundle();

        ArrayList<String> vehicleTypes = new ArrayList<>();
        ArrayList<Integer> magazineIDs = new ArrayList<>();

        for (Magazine magazine : magazines) {
            vehicleTypes.add(magazine.getVehicleType());
            magazineIDs.add(magazine.getID());
        }

        args.putStringArrayList(KEY_VEHICLE_TYPES, vehicleTypes);
        args.putIntegerArrayList(KEY_MAGAZINE_IDS, magazineIDs);
        args.putInt(KEY_DEFAULT_SELECTION, defaultSelection);
        args.putInt(KEY_REQUEST_CODE, requestCode);

        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity(), R.style.AppDialogTheme);
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_magazine_selector, null);
        dialog.setContentView(view);
        unbinder = ButterKnife.bind(this, dialog);

        ArrayList<String> vehicleTypes = getArguments().getStringArrayList(KEY_VEHICLE_TYPES);
        magazineIDs = getArguments().getIntegerArrayList(KEY_MAGAZINE_IDS);
        requestCode = getArguments().getInt(KEY_REQUEST_CODE);
        final int defaultSelection = getArguments().getInt(KEY_DEFAULT_SELECTION);

        int i = 0;
        for (String vehicleType : vehicleTypes) {
            RadioButton button = new RadioButton(getContext());
            button.setText(vehicleType);
            button.setTextSize(18);
            magazineRadioGroup.addView(button, new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            if (defaultSelection == i) {
                button.setChecked(true);
            }
            i++;
        }

        return dialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.negative_button)
    void onClickNegativeButton() {
        MagazineSelectorListener listener = (MagazineSelectorListener) getActivity();
        listener.onMagazineListenerNegativeClick(requestCode);
        dismiss();
    }

    @OnClick(R.id.positive_button)
    void onClickPositiveButton() {
        final int count = magazineRadioGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            RadioButton button = (RadioButton) magazineRadioGroup.getChildAt(i);
            if (button.isChecked()) {
                MagazineSelectorListener listener = (MagazineSelectorListener) getActivity();
                listener.onMagazineListenerPositiveClick(magazineIDs.get(i), requestCode);
                dismiss();
                return;
            }
        }

        AppDialogFragment.show((AppCompatActivity) getActivity(), "Please select a magazine.");
    }
}
