package net.sourceforge.opencamera;

import android.util.SparseArray;

import com.sister.ivana.database.MagazineShot;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

public class ShotCollection {
    static class AnimatedPicturePosition {
        int shotIndex;
        int frameIndex;

        public AnimatedPicturePosition(int shotIndex, int frameIndex) {
            this.shotIndex = shotIndex;
            this.frameIndex = frameIndex;
        }
    }

//    SparseArray<PoiShot> poiShots = new SparseArray<>();
    HashMap<Integer, PoiShot> poiShots = new HashMap<>();
    ArrayList<CustomPoiShot> customPoiShots = new ArrayList<>();
    ArrayList<DetailShot> detailShots = new ArrayList<>();
    ArrayList<PipShot> pipShots = new ArrayList<>();
    ArrayList<AnimatedPictureShot> animatedPictureShots = new ArrayList<>();

    AnimatedPicturePosition getAnimatedPicturePosition(int position) {
        int shotIndex = -1;
        int totalIndex = -1;
        for (AnimatedPictureShot shot : animatedPictureShots) {
            shotIndex++;

            if (shot.video != null) {
                totalIndex++;
            }

            if (position == totalIndex) {
                return new AnimatedPicturePosition(shotIndex, -1);
            }

            if (position <= totalIndex + shot.frameCount) {
                return new AnimatedPicturePosition(shotIndex, position - totalIndex - 1);
            }

            totalIndex += (shot.frameCount);
        }

        return null;
    }

    PoiShot getPoiShot(int index) {
        List<Integer> keys = new ArrayList(poiShots.keySet());
        Collections.sort(keys);
        return index < keys.size() ? poiShots.get(keys.get(index)) : null;
    }

    ArrayList<ImageDescription> getPoiImageDescriptions(MagazineState magazineState) {
        List<Integer> keys = new ArrayList(poiShots.keySet());
        Collections.sort(keys);

        ArrayList<ImageDescription> descriptions = new ArrayList<>();
        for (int key : keys) {
            final PoiShot shot = poiShots.get(key);
            final File image = shot.file;
            final String title;
            final String code;
            final MagazineShot magazineShot = magazineState.getMagazineShot(key);
            if (magazineShot != null) {
                title = magazineShot.getTitle();
                code = magazineShot.getFeatureCode();
            } else {
                title = "";
                code = "";
            }

            ImageDescription description = new ImageDescription(image, title, code, 0);
            descriptions.add(description);
        }

        return descriptions;
    }

    ArrayList<ImageDescription> getDetailImageDescriptions() {
        ArrayList<ImageDescription> descriptions = new ArrayList<>();
        for (DetailShot shot : detailShots) {
            descriptions.add(new ImageDescription(shot.file, 0));
        }
        return descriptions;
    }

    int getAnimatedPictureCount() {
        int count = 0;

        for (AnimatedPictureShot shot : animatedPictureShots) {
            if (shot.video != null) {
                count++;
            }

            count += shot.frameCount;
        }


        return count;
    }
}
