package net.sourceforge.opencamera;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Video.VideoColumns;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ZoomControls;

import com.j256.ormlite.stmt.QueryBuilder;
import com.sister.ivana.AddExteriorPoiDialogFragment;
import com.sister.ivana.AddExteriorPoiReview;
import com.sister.ivana.AddExteriorPoiSelectPicture;
import com.sister.ivana.AddExteriorPoiSelectPosition;
import com.sister.ivana.AddExteriorPoiTakePicture;
import com.sister.ivana.AddPipReview;
import com.sister.ivana.AddPipSelectPosition;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.BannerMissingDialogFragment;
import com.sister.ivana.CameraRole;
import com.sister.ivana.FileUtil;
import com.sister.ivana.LongClickCompleteDialogFragment;
import com.sister.ivana.PipShotDetailActivity;
import com.sister.ivana.PipTakeDetailPictureDialog;
import com.sister.ivana.PipTakeWidePictureDialog;
import com.sister.ivana.LongClickSettingDialogFragment;
import com.sister.ivana.R;
import com.sister.ivana.ShotDetailActivity;
import com.sister.ivana.Util;
import com.sister.ivana.VideoDetailActivity;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.Magazine;
import com.sister.ivana.database.MagazineShot;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;
import com.sister.ivana.database.Vehicle;
import com.sister.ivana.dialog.AppDialogFragment;
import com.sister.ivana.messaging.InterDeviceCommand;
import com.sister.ivana.messaging.NetworkService;
import com.sister.ivana.messaging.SlaveCancelCapture;
import com.sister.ivana.messaging.SlaveCaptureData;
import com.sister.ivana.messaging.SlaveDeleteMedia;
import com.sister.ivana.messaging.SlaveEnqueueMedia;
import com.sister.ivana.settings.ItemSelectedListener;
import com.sister.ivana.settings.SettingsServerComms;
import com.sister.ivana.upload.UploaderService;
import com.sister.ivana.LongClickIntroDialogFragment;
import com.sister.ivana.webapi.elasticsearch.ESWebService;
import com.sister.ivana.webapi.elasticsearch.InventoryResults;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.widget.RelativeLayout.ALIGN_PARENT_RIGHT;

class MyDebug {
    static final boolean LOG = true;
}

public class CameraActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper> implements
        BackDialogFragment.BackListener,
        LongClickIntroDialogFragment.LongClickIntroListener,
        AddExteriorPoiDialogFragment.AddExteriorPoiDialogListener,
        AddExteriorPoiSelectPicture.AddExteriorPoiSelectPictureDialogListener,
        AddExteriorPoiSelectPosition.AddExteriorPoiSelectPositionDialogListener,
        AddExteriorPoiTakePicture.AddExteriorPoiTakePictureListener,
        AddExteriorPoiReview.AddExteriorPoiReviewListener,
        PipTakeWidePictureDialog.PipTakeWidePictureListener,
        AddPipSelectPosition.AddPipSelectPositionDialogListener,
        PipTakeDetailPictureDialog.PipTakeDetailPictureListener,
        AddPipReview.AddPipReviewListener,
		LongClickSettingDialogFragment.LongClickSettingListener
 {
    class ShutterTouchListener implements View.OnTouchListener {
        private final long TAP_THRESHOLD = 300;
        private final long ANIMATED_GIF_THRESHOLD = 800;
        private final long ANIMATED_GIF_COUNTDOWN = 2000;
        private final long ANIMATED_GIF_MOVEMENT_THRESHOLD = 100;

        float startRightMargin;
        float startTopMargin;
        float touchStartX;
        float touchStartY;
        float parentWidth;
        float parentHeight;
        int leftThreshold;
        int bottomThreshold;
        long downTime;
        Timer animatedGifTimer;
        float movementDelta;
        AppSharedPreferences sharedPreferences;

        public ShutterTouchListener() {
            sharedPreferences = new AppSharedPreferences(CameraActivity.this);
            DisplayMetrics metrics = CameraActivity.this.getResources().getDisplayMetrics();
            leftThreshold = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, metrics));
            bottomThreshold = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, metrics));
        }

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                    if (adapter != null) {
                        adapter.setShowAll(false);
                        updateShowAllText();
                        final String message = galleryMessages.get(modeControlView.getMode());
                        galleryMessageTextView.setText(message);
                    }
                    
                    downTime = System.currentTimeMillis();
                    final View parent = (View) view.getParent();
                    parentWidth = parent.getWidth();
                    parentHeight = parent.getHeight();

                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    startRightMargin = lParams.rightMargin;
                    startTopMargin = lParams.topMargin;

                    touchStartX = event.getRawX();
                    touchStartY = event.getRawY();

                    if (modeControlView.getMode() == CaptureMode.ANIMATED_PICTURE) {
                        animatedGifTimer = new Timer();
                        animatedGifTimer.scheduleAtFixedRate(new TimerTask() {
                            @Override
                            public void run() {
                                animatedGifHandler();
                            }
                        }, 0, 100);
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    if (animatedGifTimer != null) {
                        animatedGifTimer.cancel();
                    }
                    if (isAnimatedGifActive) {
                        completeAnimatedGif();
                    } else if ((System.currentTimeMillis() - downTime) < TAP_THRESHOLD && (modeControlView.getMode() != CaptureMode.ANIMATED_PICTURE)) {
                        clickedTakePhoto(view);
                    }
                    isAnimatedGifActive = false;
                    movementDelta = 0;
                    updateCounter("0", false);
                    break;

                case MotionEvent.ACTION_MOVE:
                    float deltaX = event.getRawX() - touchStartX;
                    float deltaY = event.getRawY() - touchStartY;
                    movementDelta = Math.abs(deltaX) + Math.abs(deltaY);

                    int rightMargin = Math.round(startRightMargin - deltaX);
                    rightMargin = Math.max(rightMargin, 0);
                    rightMargin = Math.min(rightMargin, Math.round(parentWidth - leftThreshold));

                    int topMargin = Math.round(startTopMargin + deltaY);
                    topMargin = Math.max(topMargin, 0);
                    topMargin = Math.min(topMargin, Math.round(parentHeight - bottomThreshold));

                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    layoutParams.rightMargin = rightMargin;
                    layoutParams.leftMargin = 0;
                    layoutParams.topMargin = topMargin;
                    layoutParams.bottomMargin = 0;
                    view.setLayoutParams(layoutParams);
                    break;
            }

            return true;
        }

        private void animatedGifHandler() {
            if ((System.currentTimeMillis() - downTime) > ANIMATED_GIF_THRESHOLD) {
                if (!isAnimatedGifActive) {
                    // Display countdown timer
                    long countDownMillis = ANIMATED_GIF_COUNTDOWN - (System.currentTimeMillis() - downTime - ANIMATED_GIF_THRESHOLD);
                    if (countDownMillis <= 0) {
                        updateCounter("", false);
                        isAnimatedGifActive = true;
                        final Activity activity = CameraActivity.this;
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                animatedPictureStartTime = System.currentTimeMillis();
                                preview.takePicturePressed(true);
                            }});
                    } else {
                        int halfSeconds = (int)(countDownMillis / 500);
                        updateCounter(Integer.toString(halfSeconds), true);
                    }
                } else {
                    if (System.currentTimeMillis() - animatedPictureStartTime > sharedPreferences.getMaxCaptureLength()) {
                        final Activity activity = CameraActivity.this;
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                completeAnimatedGif();
                            }});
                    }
                }
            }

            if (!isAnimatedGifActive && movementDelta > ANIMATED_GIF_MOVEMENT_THRESHOLD) {
                animatedGifTimer.cancel();
                updateCounter("", false);
            }
        }

        private void completeAnimatedGif() {
            animatedGifTimer.cancel();

            if (!isAnimatedGifActive) {
                return;
            }

            preview.stopVideo(false);
            LongClickCompleteDialogFragment dialog = LongClickCompleteDialogFragment.newInstance(videoFile.getAbsolutePath());
            dialog.setListener(new ItemSelectedListener() {
                @Override
                public void itemSelected(String selectedItem) {
                    modeControlView.dimModeSelector();

                    final int frames = sharedPreferences.getNumOfPicsExtracted();
                    AnimatedPictureShot shot = new AnimatedPictureShot();
                    shot.video = videoFile;
                    shot.frameCount = frames;

                    ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                    int lastIndex = adapter.getSelectedIndex();
                    adapter.addAnimatedPictureShot(shot, vin);
                    updateAdapter(lastIndex, frames + 1);
                    CameraActivity.this.updateResultIntent();
                }
            });
            dialog.show(getSupportFragmentManager(), "dummy_tag");
            isAnimatedGifActive = false;
        }

        private void updateCounter(final String text, final boolean visible) {
            final Activity activity = CameraActivity.this;
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (animatedGifCountdownTextView != null) {
                        animatedGifCountdownTextView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        animatedGifCountdownTextView.setText(text);
                    }
                }
            });
        }
    }

    private final Preview.PreviewClient previewClient = new Preview.PreviewClient() {
        // Note that not all camera sessions end here. The Preview class also calls finishWithResult
        // directly in some cases and in some cases CameraActivity controls the finish. Eventually
        // it would be nice to consolidate the exit code into one place (probably this class).
        @Override
        public void captureComplete() {
            finish();
        }

        @Override
        public File getVideoFile() {
            return CameraActivity.this.getVideoFile();
        }

        @Override
        public File getImageFile() {
            return CameraActivity.this.getImageFile();
        }

        @Override
        public void onVideoCaptureStarted(File video) {
            CameraActivity.this.onVideoCaptureStarted(video);
        }

        @Override
        public void onVideoCaptureComplete() {
            final AppSharedPreferences sharedPreferences = new AppSharedPreferences(CameraActivity.this);
            if (sharedPreferences.isMasterSlaveEnabled() && !sharedPreferences.isMaster()) {
                shutterButton.setEnabled(true);
                shutterButton.setVisibility(View.VISIBLE);
                isCapturingVideo = false;
                if (lastEnqueueMediaCommand != null && ((System.currentTimeMillis() - lastEnqueueMediaCommand.timestamp) < 5000)) {
                    enqueueSpinitPictures();
                }
            }
            else {
                isCapturingVideo = false;
                if (!isCancelPending && !isAnimatedGifActive) {
                    if (Util.shouldSendSlaveCommand(CameraActivity.this)) {
                        InterDeviceCommand command = new InterDeviceCommand();
                        command.data = new InterDeviceCommand.CommandData();
                        command.data.command = InterDeviceCommand.COMMAND_ENQUEUE_MEDIA;
                        NetworkService.sendMessage(CameraActivity.this, command);
                    }
                    finish();
                }
            }
        }
    };

    private final ShotListAdapter.ShotListListener shotListListener = new ShotListAdapter.ShotListListener() {
        @Override
        public void poiShotClicked(int index) {
            CameraActivity.this.poiShotClicked(index);
        }

        @Override
        public void poiShotLongClicked(int index) {
            CameraActivity.this.poiShotLongClicked(index);
        }

        @Override
        public void addPoiClicked() {
            CameraActivity.this.addPoiClicked();
        }

        @Override
        public void pipShotClicked(int index) {
            CameraActivity.this.pipShotClicked(index);
        }

        @Override
        public void detailShotClicked(int index) {
            CameraActivity.this.detailShotClicked(index);
        }

        @Override
        public void animatedPictureClicked(int index) {
            CameraActivity.this.animatedPictureClicked(index);
        }

        @Override
        public void customPoiClicked(int index) {
            CameraActivity.this.customPoiClicked(index);
        }
    };

    public static final int HAVE_IMAGES_RESULT_CODE = 1002;
    public static final int HAVE_VIDEO_RESULT_CODE = 1003;
    public static final String EXTRA_PROJECT_ID = "PROJECT_ID";
    public static final String EXTRA_LIBRARY_ID = "LIBRARY_ID";
    public static final String EXTRA_MAGAZINE_ID = "EXTRA_MAGAZINE_ID";
    public static final String EXTRA_IMAGE_DESCRIPTIONS = "EXTRA_IMAGE_DESCRIPTIONS";
    public static final String EXTRA_CUSTOM_POIS = "EXTRA_CUSTOM_POIS";
    public static final String EXTRA_PIPS = "EXTRA_PIPS";
    public static final String EXTRA_ANIMATED_PICTURES = "EXTRA_ANIMATED_PICTURES";
    public static final String EXTRA_VIDEO_PATHNAME = "EXTRA_VIDEO_PATHNAME";

    private static final int SHOT_DETAIL_REQUEST_CODE = 1001;
    private static final int SHOT_PIP_DETAIL_REQUEST_CODE = 1004;
    private static final int SHOT_PICTURE_DETAIL_REQUEST_CODE = 1005;
    private static final int SHOT_ANIMATED_PICTURE_REQUEST_CODE = 1006;
    private static final int SHOT_CUSTOM_POI_REQUEST_CODE = 1007;
    private static final String EXTRA_VIN = "vin";
    private static final String EXTRA_IS_REMOTE = "is_remote";
    private static final String EXTRA_IS_VIDEO = "is_video";
    private static final String EXTRA_IS_FULL_MOTION_VIDEO = "isFullmo";
    private static final String EXTRA_IS_SINGLE_SHOT = "singleShot";
    private static final String EXTRA_AUTOMATIC_PICTURE_INTERVAL = "EXTRA_AUTOMATIC_PICTURE_INTERVAL";
    private static final String EXTRA_VIDEO_LIMIT_DURATION = "EXTRA_VIDEO_LIMIT_DURATION";
    private static final String EXTRA_SHUTTER_BUTTON_RESOURCE_ID = "EXTRA_SHUTTER_BUTTON_RESOURCE_ID";
    private static final String EXTRA_SHOW_IMAGE_COUNT = "EXTRA_SHOW_IMAGE_COUNT";
    private static final String EXTRA_TURNTABLE_AUTOMATED_PHOTO_MODE = "EXTRA_TURNTABLE_AUTOMATED_PHOTO_MODE";

    private static final String KEY_BANNER_VISIBLE = "KEY_BANNER_VISIBLE";

    private static final int SPINIT_PICTURE_SIZE_WIDTH = 1280;
    private static final int SPINIT_PICTURE_SIZE_HEIGHT = 720;

    @BindView(R.id.zoomSeekBar) SeekBar zoomSeekBar;
    @BindView(R.id.zoomSeekBarMinus) ImageButton zoomSeekBarMinus;
    @BindView(R.id.zoomSeekBarPlus) ImageButton zoomSeekBarPlus;
    @BindView(R.id.zoomSeekBarMinusBG) ImageView zoomSeekBarMinusBG;
    @BindView(R.id.zoomSeekBarPlusBG) ImageView zoomSeekBarPlusBG;
    @BindView(R.id.shotListRecyclerView) RecyclerView shotListRecyclerView;
    @BindView(R.id.shotListRecyclerViewContainer) RelativeLayout shotListRecyclerViewContainer;
    @BindView(R.id.shotListLeftScroll) ImageView shotListLeftScroll;
    @BindView(R.id.shotListRightScroll) ImageView shotListRightScroll;
    @BindView(R.id.take_photo) ImageButton shutterButton;
    @BindView(R.id.poi_check_box) CheckBox poiCheckBox;
    @BindView(R.id.imageCount) TextView imageCountTextView;
    @BindView(R.id.animatedGifCountdown) TextView animatedGifCountdownTextView;
    @BindView(R.id.whiteOverlay) View whiteOverlay;
    @BindView(R.id.banner_view) ImageView bannerView;
    @BindView(R.id.toggleBanner) ImageButton toggleBanner;
    @BindView(R.id.camera_role_text_view) TextView cameraRoleTextView;
    @BindView(R.id.mode_control_view) ModeControlView modeControlView;
    @BindView(R.id.gallery_message_text_view) TextView galleryMessageTextView;
    @BindView(R.id.show_all_text_view) TextView showAllTextView;

    private static final String TAG = "MainActivity";
    private SensorManager mSensorManager = null;
    private Sensor mSensorAccelerometer = null;
    private Sensor mSensorMagnetic = null;
    private LocationManager mLocationManager = null;
    private LocationListener locationListener = null;
    private Preview preview = null;
    private int current_orientation = 0;
    private OrientationEventListener orientationEventListener = null;
    private boolean supports_auto_stabilise = false;
    private boolean supports_force_video_4k = false;
    private ArrayList<String> save_location_history = new ArrayList<String>();
    private boolean camera_in_background = false; // whether the camera is covered by a fragment/dialog (such as settings or folder picker)
    private GestureDetector gestureDetector;
    private boolean screen_is_locked = false;
    private Map<Integer, Bitmap> preloaded_bitmap_resources = new Hashtable<Integer, Bitmap>();
    private PopupView popup_view = null;
    private ShotListAnimator shotListAnimator;
    String vin;
    String projectId = "";
    String libraryId = "";
    public boolean isSpinitSingleShotMode = false;
    public static boolean staticisSpinitSingleShotMode = false;
    boolean isGridVisible = false;
    public float spinitDuration = 68f;
    public float numSpinitShots = 45f;
    private ToastBoxer screen_locked_toast = new ToastBoxer();
    ToastBoxer changed_auto_stabilise_toast = new ToastBoxer();
    View gridOverlayView;
    public boolean isVideo;
    public boolean isFullmo;
    private boolean isAnimatedGifActive;
    // for testing:
    public boolean is_test = false;
    public boolean is_remote = false;
    TextView vinText;
    TextView shootingModeText;
    public Bitmap gallery_bitmap = null;
    ImageButton toggleGrid;
    ImageButton flashAutoButton;
    ImageButton flashOffButton;
    ImageButton flashOnButton;
    ImageButton selectFlashButton;
    ImageButton settingsButton;
    LinearLayout flashSelectorLayout;
    ImageButton selectFocusButton;
    ImageButton focusAutoButton;
    ImageButton focusManualButton;
    LinearLayout focusSelector;
    public AlertDialog levelDialog;
    public Handler controlHandler;
    private int defaultControlAnimationDelay = 500;
    private MagazineState magazineState;
    private List<MagazineShot> magazineShots;
    private ArrayList<ImageDescription> imageDescriptions;
    private ShotCollection shotCollection = new ShotCollection();
    private int automaticPictureInterval;
    private boolean limitVideoDuration;
    private int shutterButtonResourceID;
    private boolean showImageCount;
    private int imageCount;
    private boolean isCapturingVideo;
    private boolean isCancelPending;
    private boolean isTurntableAutomatedPhotoMode;
    private boolean startedAutoPictures;
    private File videoFile;
    private SlaveCaptureData slaveData;

    private int cameraIdStandard = -1;
    private int cameraIdWide = -1;
    private CaptureMode lastCaptureMode = CaptureMode.POI;
    private Map<CaptureMode, String> galleryMessages = new HashMap<>();
    private SlaveEnqueueMedia lastEnqueueMediaCommand;
    private CustomPoiShot currentCustomPoiShot;
    private PipShot currentPipShot;
    private Call<InventoryResults> thumbnailRequest;
    private ArrayList<String> thumbnailUrls;
    private ArrayList<String> spinUrls;
    private long animatedPictureStartTime;


    // We have a lot of different session configuration parameters that depend on the task the
    // user is performing and user settings. Examples include video vs pictures, timed vs untimed,
    // automated shutter vs manual shutter, etc.
    //
    // Currently these parameters are represented by a lot of different variables. Eventually it would
    // be good to represent all of this state in this session object.
    private Session session;

    ImageButton whiteBalanceSelector;

    public static Intent createIntent(Context context, String vin, boolean isVideo, boolean isFullMotionVideo, boolean isSingleShot, int timer) {
        // This is terrible but it is part of the legacy code and I'm leaving it for now. The starting
        // activity sets preferences before launching camera to communicate desired behavior.
        AppSharedPreferences preferences = new AppSharedPreferences(context);
        preferences.putIsVideo(isVideo);

        Intent intent = new Intent(context, CameraActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_IS_REMOTE, true);
        intent.putExtra(EXTRA_IS_VIDEO, isVideo);
        intent.putExtra(EXTRA_IS_FULL_MOTION_VIDEO, isFullMotionVideo);
        intent.putExtra(EXTRA_IS_SINGLE_SHOT, isSingleShot);
        intent.putExtra(EXTRA_VIDEO_LIMIT_DURATION, true);
        return intent;
    }

    public static Intent createSpinItVideoTurntableIntent(Context context, String vin, boolean replace) {
        Intent intent = createIntent(context, vin, true, false, false, 5);
        intent.putExtra(EXTRA_SHUTTER_BUTTON_RESOURCE_ID, R.drawable.video_go);

        new Session.Builder(intent)
                .setShowCountdownTimer(true)
                .setReplaceMedia(replace);

        return intent;
    }

    public static Intent createDetailPicturesIntent(Context context, String vin, int magazineID) {
        // This is terrible but it is part of the legacy code and I'm leaving it for now. The starting
        // activity sets preferences before launching camera to communicate desired behavior.
        AppSharedPreferences preferences = new AppSharedPreferences(context);
        preferences.putIsVideo(false);

        Intent intent = new Intent(context, CameraActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_IS_REMOTE, true);
        intent.putExtra(EXTRA_IS_VIDEO, false);
        intent.putExtra(EXTRA_IS_FULL_MOTION_VIDEO, false);
        intent.putExtra(EXTRA_IS_SINGLE_SHOT, false);
        intent.putExtra(EXTRA_MAGAZINE_ID, magazineID);
        intent.putExtra(EXTRA_VIDEO_LIMIT_DURATION, true);

        new Session.Builder(intent).enableShutterAnimation()
                                   .setHasShotMagazine(magazineID >= 0);
        if (preferences.allowAnimatedPictures()) {
            new Session.Builder(intent).enableAnimatedGif();
        }
        return intent;
    }

    public static Intent createDetailPicturesIntent(Context context, String vin) {
        // This is terrible but it is part of the legacy code and I'm leaving it for now. The starting
        // activity sets preferences before launching camera to communicate desired behavior.
        AppSharedPreferences preferences = new AppSharedPreferences(context);
        preferences.putIsVideo(false);

        Intent intent = new Intent(context, CameraActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_IS_REMOTE, true);
        intent.putExtra(EXTRA_IS_VIDEO, false);
        intent.putExtra(EXTRA_IS_FULL_MOTION_VIDEO, false);
        intent.putExtra(EXTRA_IS_SINGLE_SHOT, false);
        intent.putExtra(EXTRA_VIDEO_LIMIT_DURATION, true);

        new Session.Builder(intent).enableShutterAnimation()
                                   .declineSelectMagazine();
        if (preferences.allowAnimatedPictures()) {
            new Session.Builder(intent).enableAnimatedGif();
        }
        return intent;
    }

    public static Intent createSpinItNoTurntableManualPictureIntent(Context context, String vin) {
        Intent intent = new Intent(context, CameraActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_IS_REMOTE, true);
        intent.putExtra(EXTRA_IS_VIDEO, false);
        intent.putExtra(EXTRA_IS_FULL_MOTION_VIDEO, false);
        intent.putExtra(EXTRA_VIDEO_LIMIT_DURATION, true);
        intent.putExtra(EXTRA_SHUTTER_BUTTON_RESOURCE_ID, R.drawable.ic_camera_white_48dp);

        new Session.Builder(intent)
                .setPictureSize(SPINIT_PICTURE_SIZE_WIDTH, SPINIT_PICTURE_SIZE_HEIGHT)
                .setShowGridLines(true)
                .enableManualPictureMode();

        return intent;
    }

    public static Intent createSpinItNoTurntableAutomatedPictureIntent(Context context, String vin, int interval, int maxPictures) {
        Intent intent = new Intent(context, CameraActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_IS_REMOTE, true);
        intent.putExtra(EXTRA_IS_VIDEO, false);
        intent.putExtra(EXTRA_IS_FULL_MOTION_VIDEO, false);
        intent.putExtra(EXTRA_AUTOMATIC_PICTURE_INTERVAL, interval * 1000);
        intent.putExtra(EXTRA_VIDEO_LIMIT_DURATION, true);
        intent.putExtra(EXTRA_SHUTTER_BUTTON_RESOURCE_ID, R.drawable.shutter_auto);

        new Session.Builder(intent)
                .setPictureSize(SPINIT_PICTURE_SIZE_WIDTH, SPINIT_PICTURE_SIZE_HEIGHT)
                .setShowGridLines(true)
                .setMaxPictures(maxPictures)
                .setShowPicurePrompt(true)
                .enableAutomaticPictureMode();

        return intent;
    }

    public static Intent createSlavePictureIntent(Context context, String vin) {
        final Intent intent = createSpinItVideoTurntableIntent(context, vin, false);
        final CameraRole role = new AppSharedPreferences(context).getCameraRole();
        new Session.Builder(intent)
                .setVinToShow(vin)
                .setSlaveRole(role);
        return intent;
    }

    public static Intent createSpinItTurntableAutomatedPictureIntent(Context context, String vin) {
        Intent intent = createIntent(context, vin, false, false, true, 5);
        intent.putExtra(EXTRA_TURNTABLE_AUTOMATED_PHOTO_MODE, true);
        intent.putExtra(EXTRA_SHOW_IMAGE_COUNT, true);

        new Session.Builder(intent)
                .setShowCountdownTimer(true)
                .setPictureSize(SPINIT_PICTURE_SIZE_WIDTH, SPINIT_PICTURE_SIZE_HEIGHT);

        return intent;
    }

    public static Intent createUnlimitedVideoIntent(Context context, String vin) {
        AppSharedPreferences preferences = new AppSharedPreferences(context);
        preferences.putIsVideo(true);

        Intent intent = new Intent(context, CameraActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_IS_REMOTE, true);
        intent.putExtra(EXTRA_IS_VIDEO, true);
        intent.putExtra(EXTRA_IS_FULL_MOTION_VIDEO, false);
        intent.putExtra(EXTRA_IS_SINGLE_SHOT, false);
        intent.putExtra(EXTRA_VIDEO_LIMIT_DURATION, false);
        intent.putExtra(EXTRA_SHUTTER_BUTTON_RESOURCE_ID, R.drawable.video_go);

        new Session.Builder(intent)
                .setShowVideoPrompt(true)
                .setShowGridLines(true);

        return intent;
    }

    public static Intent createManualVideoIntent(Context context, String vin) {
        Intent intent = createIntent(context, vin, true, true, false, 5);
        intent.putExtra(EXTRA_SHUTTER_BUTTON_RESOURCE_ID, R.drawable.video_go);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        configureGalleryMessages();
        controlHandler = new Handler();
        long time_s = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);
        flashSelectorLayout = (LinearLayout) findViewById(R.id.selectFlashLayout);

        selectFlashButton = (ImageButton) findViewById(R.id.selectFlashButton);
        flashAutoButton = (ImageButton) findViewById(R.id.flashAutoButton);
        flashOffButton = (ImageButton) findViewById(R.id.flashOffButton);
        flashOnButton = (ImageButton) findViewById(R.id.flashOnButton);
        settingsButton = (ImageButton) findViewById(R.id.settingsButton);

        scanCameras();

        /*SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("preference_video_bitrate", "17000000");
        editor.apply();*/
        new AppSharedPreferences(this).putVideoBitrate("40000000");
        //Utils.saveToPrefs(MainActivity.this, "preference_video_bitrate", "default");

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // TODO: Implement settings
                // startActivity(new Intent(MainActivity.this, SettingsActivity.class));


            }
        });

        /*final String videoQuality = Utils.getStringFromPrefs(MainActivity.this, Preview.getVideoQualityPreferenceKey(0));
        System.out.println("==== xxvideoQuality ==================");
        System.out.println(videoQuality);
        Utils.saveToPrefs(MainActivity.this,videoQuality,"1");*/

        selectFlashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] items = {"Auto Flash", "Flash On", "flash Off"};
                String flashValue = new AppSharedPreferences(CameraActivity.this).getFlash(0);
                int checkedIndex = -1;
                if (flashValue.contains("flash_auto")) {
                    checkedIndex = 0;

                } else if (flashValue.contains("flash_on")) {
                    checkedIndex = 1;

                } else if (flashValue.contains("flash_off")) {
                    checkedIndex = 2;

                }
                AlertDialog.Builder builder = new AlertDialog.Builder(CameraActivity.this);
                builder.setTitle("Select Flash Mode");
                builder.setSingleChoiceItems(items, checkedIndex, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {


                        switch (item) {
                            case 0:
                                selectFlashButton.setImageResource(R.drawable.ic_flash_auto_white_36dp);
                                preview.updateFlash("flash_auto", true);
                                break;
                            case 1:

                                selectFlashButton.setImageResource(R.drawable.ic_flash_on_white_36dp);
                                preview.updateFlash("flash_on", true);
                                break;
                            case 2:
                                selectFlashButton.setImageResource(R.drawable.ic_flash_off_white_36dp);
                                preview.updateFlash("flash_off", true);

                        }

                        levelDialog.dismiss();
                        //TODO RV for hiding contronls
                        //delayControlsHide();
                    }
                });
                levelDialog = builder.create();
                levelDialog.show();


            }

        });


        focusSelector = (LinearLayout) findViewById(R.id.selectFocusLayout);

        selectFocusButton = (ImageButton) findViewById(R.id.focusSelectButton);


        selectFocusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] items = {"Auto Focus (for Spin and Walk-Around)", "Manual Focus (Tap on Screen to Focus)"};
                FocusMode mode = getDefaultFocusMode();
                int checkedIndex = -1;
                if (mode == FocusMode.AUTO) {
                    checkedIndex = 0;
                } else if (mode == FocusMode.MANUAL) {
                    checkedIndex = 1;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(CameraActivity.this);
                builder.setTitle(getString(R.string.focus_selector_dialog_title));
                builder.setSingleChoiceItems(items, checkedIndex, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 0:
                                setDefaultFocusMode(FocusMode.AUTO);
                                preview.updateFocus("focus_mode_auto", true, true);
                                selectFocusButton.setImageResource(R.drawable.ic_center_focus_strong_white_36dp_auto);

                                break;
                            case 1:
                                setDefaultFocusMode(FocusMode.MANUAL);
                                preview.updateFocus("focus_mode_manual", true, true);
                                selectFocusButton.setImageResource(R.drawable.ic_center_focus_strong_white_36dp_manual);
                                break;
                        }

                        levelDialog.dismiss();
                    }
                });
                levelDialog = builder.create();
                levelDialog.show();
            }
        });


        whiteBalanceSelector = (ImageButton) findViewById(R.id.popup);
        whiteBalanceSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> supported_white_balances = preview.getSupportedWhiteBalances();
                String whiteBalanceValue = new AppSharedPreferences(CameraActivity.this).getWhiteBalance();
                int checkedIndex = -1;

                for (int x = 0; x < supported_white_balances.size(); x++) {
                    String s = supported_white_balances.get(x);
                    if (s.contains("custom") || s.contains("temp")) {
                        supported_white_balances.remove(x);
                        x--;
                    }
                }

                String[] _items = new String[supported_white_balances.size()];
                for (int x = 0; x < supported_white_balances.size(); x++) {
                    if (whiteBalanceValue.equals(supported_white_balances.get(x))) {
                        checkedIndex = x;
                    }
                    _items[x] = supported_white_balances.get(x);
                }

                final String[] items = _items;
                final String wbKey = Preview.getWhiteBalancePreferenceKey();
                AlertDialog.Builder builder = new AlertDialog.Builder(CameraActivity.this);
                builder.setTitle(getString(R.string.select_white_balance_dialog_title));
                builder.setSingleChoiceItems(items, checkedIndex, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        new AppSharedPreferences(CameraActivity.this).putWhiteBalance(items[item]);

                        Camera.Parameters parameters = preview.getCamera().getParameters();


                        parameters.setWhiteBalance(items[item]);
                        preview.setCameraParameters(parameters);

                        levelDialog.dismiss();
                    }
                });
                levelDialog = builder.create();
                levelDialog.show();


            }
        });

        if (getIntent() != null && getIntent().getExtras() != null) {
            is_test = getIntent().getExtras().getBoolean("test_project");
            if (MyDebug.LOG)
                Log.d(TAG, "is_test: " + is_test);
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String numFrames = sharedPreferences.getString(getResources().getString(R.string.prefs_deltaTimeKey, 45), "NULL");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            vin = extras.getString("vin");
            projectId = extras.getString(EXTRA_PROJECT_ID, "");
            if (TextUtils.isEmpty(projectId)) {
                try {
                    final Vehicle vehicle = getHelper().getVehicleDao().queryForId(vin);
                    if (vehicle != null) {
                        projectId = vehicle.getProject();
                    }
                } catch (SQLException e) {
                    Util.logError("Error getting project from database", e);
                }
            }
            libraryId = extras.getString(EXTRA_LIBRARY_ID, "");
            isSpinitSingleShotMode = extras.getBoolean("singleShot", false);
            staticisSpinitSingleShotMode = extras.getBoolean("singleShot", false);
            isVideo = extras.getBoolean(EXTRA_IS_VIDEO, false);
            isFullmo = extras.getBoolean(EXTRA_IS_FULL_MOTION_VIDEO, false);
            is_remote = extras.getBoolean(EXTRA_IS_REMOTE, false);
            automaticPictureInterval = extras.getInt(EXTRA_AUTOMATIC_PICTURE_INTERVAL);
            shutterButtonResourceID = extras.getInt(EXTRA_SHUTTER_BUTTON_RESOURCE_ID, -1);
            limitVideoDuration = extras.getBoolean(EXTRA_VIDEO_LIMIT_DURATION, true);
            showImageCount = extras.getBoolean(EXTRA_SHOW_IMAGE_COUNT, false);
            isTurntableAutomatedPhotoMode = extras.getBoolean(EXTRA_TURNTABLE_AUTOMATED_PHOTO_MODE);

            session = Session.Builder.create(extras);
        }
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        if (MyDebug.LOG) {
            Log.d(TAG, "standard max memory = " + activityManager.getMemoryClass() + "MB");
            Log.d(TAG, "large max memory = " + activityManager.getLargeMemoryClass() + "MB");
        }
        //if( activityManager.getMemoryClass() >= 128 ) { // test
        if (activityManager.getLargeMemoryClass() >= 128) {
            supports_auto_stabilise = true;
        }

        // hack to rule out phones unlikely to have 4K video, so no point even offering the option!
        // both S5 and Note 3 have 128MB standard and 512MB large heap (tested via Samsung RTL), as does Galaxy K Zoom
        // also added the check for having 128MB standard heap, to support modded LG G2, which has 128MB standard, 256MB large - see https://sourceforge.net/p/opencamera/tickets/9/
        if (activityManager.getMemoryClass() >= 128 || activityManager.getLargeMemoryClass() >= 512) {
            supports_force_video_4k = true;
        }

        setWindowFlagsForCamera();

        // read save locations
        save_location_history.clear();
        int save_location_history_size = sharedPreferences.getInt("save_location_history_size", 0);
        if (MyDebug.LOG)
            Log.d(TAG, "save_location_history_size: " + save_location_history_size);
        for (int i = 0; i < save_location_history_size; i++) {
            String string = sharedPreferences.getString("save_location_history_" + i, null);
            if (string != null) {
                if (MyDebug.LOG)
                    Log.d(TAG, "save_location_history " + i + ": " + string);
                save_location_history.add(string);
            }
        }
        // also update, just in case a new folder has been set
        updateFolderHistory();
        //updateFolderHistory("/sdcard/Pictures/OpenCameraTest");

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            if (MyDebug.LOG)
                Log.d(TAG, "found accelerometer");
            mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        } else {
            if (MyDebug.LOG)
                Log.d(TAG, "no support for accelerometer");
        }
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
            if (MyDebug.LOG)
                Log.d(TAG, "found magnetic sensor");
            mSensorMagnetic = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        } else {
            if (MyDebug.LOG)
                Log.d(TAG, "no support for magnetic sensor");
        }

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //updateGalleryIcon();
        clearSeekBar();

        preview = new Preview(this, savedInstanceState, isVideo, session, previewClient);
        String df = preview.convertFlashValueToMode(preview.getFlashPreferenceKey(preview.getCameraId()));


        ((ViewGroup) findViewById(R.id.preview)).addView(preview);

        orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                CameraActivity.this.onOrientationChanged(orientation);
            }
        };

        gestureDetector = new GestureDetector(this, new MyGestureDetector());
        preloadIcons(R.array.flash_icons);

        configureMagazine(getIntent().getExtras());
        if (magazineState == null) {
            imageDescriptions = new ArrayList<>();
        }

        if (!session.hasShotMagazine()) {
            poiCheckBox.setVisibility(View.GONE);
        }
        poiCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                final CaptureMode mode;
                if (isChecked) {
                    mode = lastCaptureMode;
                    final String message = galleryMessages.get(lastCaptureMode);
                    galleryMessageTextView.setText(message);
                } else {
                    mode = CaptureMode.DETAIL;
                    final String message = galleryMessages.get(CaptureMode.DETAIL);
                    galleryMessageTextView.setText(message);
                }

                modeControlView.swapShotList(mode);
            }
        });

        showAllTextView.setPaintFlags(showAllTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        showAllTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                boolean isShowAll = adapter.toggleShowAll();
                updateShowAllText();
                if (adapter.isShowAllEnabled()) {
                    galleryMessageTextView.setText("");
                } else {
                    final String message = galleryMessages.get(lastCaptureMode);
                    galleryMessageTextView.setText(message);
                }
                modeControlView.swapShotList(modeControlView.getMode());
            }
        });

        final boolean isBannerVisible = (savedInstanceState != null) ? savedInstanceState.getBoolean(KEY_BANNER_VISIBLE, false) : false;
        setBannerVisibility(isBannerVisible);

        final CameraRole role = session.getCameraRole();
        if (role != null) {
            cameraRoleTextView.setText(role.getTitle(this));
        }

        if (magazineState != null) {
            modeControlView.setListener(new ModeControlView.ModeControlListener() {
                @Override
                public void onModeSelected(CaptureMode mode) {
                    if (mode != CaptureMode.DETAIL) {
                        lastCaptureMode = mode;
                    }

                    // Switch out of "show all" when user changes mode
                    ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                    adapter.setShowAll(false);
                    updateShowAllText();
                    adapter.setMode(modeControlView.getMode());

                    final String message = galleryMessages.get(mode);
                    galleryMessageTextView.setText(message);

                    // Delete intermediate state if user switches modes in middle of operation.
                    currentCustomPoiShot = null;
                    currentPipShot = null;

                    switch (mode) {
                        case ANIMATED_PICTURE:
                            onAnimatedPictureSelected();
                            break;
                        case PIP:
                            onPipSelected();
                            break;
                    }
                }
            });

            modeControlView.setDependancies(session, shutterButton, poiCheckBox, shotListRecyclerView);
            modeControlView.dimModeSelector();
        } else {
            modeControlView.setVisibility(View.GONE);
            showAllTextView.setVisibility(View.GONE);
        }
    }

    private void updateShowAllText() {
        ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
        if (adapter != null) {
            showAllTextView.setText(adapter.isShowAllEnabled() ? "Don't show all" : "Show all");
        }
    }

    private void onAnimatedPictureSelected() {
        final AppSharedPreferences preferences = new AppSharedPreferences(CameraActivity.this);

        if (!preferences.getDontShowLongClickMessage()) {
            LongClickIntroDialogFragment dialog = LongClickIntroDialogFragment.newInstance();
            dialog.show(getFragmentManager(), "long_click_intro");
        }
        else {
            LongClickSettingDialogFragment dialog = LongClickSettingDialogFragment.newInstance();
            dialog.show(getFragmentManager(), "long_click_setting");
        }
    }

    private void onPipSelected() {
        if (new AppSharedPreferences(this).getDontShowPipMessage()) {
            currentPipShot = new PipShot();
            return;
        }
        PipTakeWidePictureDialog dialog = PipTakeWidePictureDialog.newInstance();
        dialog.show(getFragmentManager(), "pip_selected");
    }

    @Override
    public void onLongClickIntroPositiveClick(int magazineID, int requestCode) {
        Util.logInfo("*** vin onLongClickIntroPositiveClick");
        LongClickSettingDialogFragment dialog = LongClickSettingDialogFragment.newInstance();
        dialog.show(getFragmentManager(), "long_click_setting");
    }

    @Override
    public void onLongClickIntroNegativeClick() {
        Util.logInfo("*** vin onLongClickIntroNegativeClick");
    }

    @Override
    public void onLongClickSettingPositiveClick(int magazineID, int requestCode){
        Util.logInfo("*** vin onLongClickSettingPositiveClick");
    }

    @Override
    public void onLongClickSettingNegativeClick(){
        Util.logInfo("*** vin onLongClickSettingNegativeClick");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Util.getBus().register(this);
        final FocusMode mode = getDefaultFocusMode();
        selectFocusButton.setImageResource(mode == FocusMode.AUTO ? R.drawable.ic_center_focus_strong_white_36dp_auto : R.drawable.ic_center_focus_strong_white_36dp_manual);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Util.getBus().unregister(this);
    }

    @Override
    public void onAddExteriorPoiDialogNext(String title, String description) {
        currentCustomPoiShot = new CustomPoiShot();
        currentCustomPoiShot.title = title;
        currentCustomPoiShot.description = description;

        AddExteriorPoiSelectPicture dialog = AddExteriorPoiSelectPicture.newInstance(thumbnailUrls);
        dialog.show(getFragmentManager(), "add_exterior_poi_select_picture");
    }

    @Override
    public void onAddExteriorPoiSelectedPicture(int position) {
        String thumbnail = thumbnailUrls.get(position);

        // For now there is not a one-to-one correspondence between the list of URLs we get down
        // and the list of thumbnails we get down. I assume in the final version there will be.
        // For now correlate a thumbnail with the full image by comparing the base filenames.
        thumbnail = thumbnail.replace("THUMB.", "");
        thumbnail = thumbnail.substring(thumbnail.lastIndexOf("/") + 1, thumbnail.length());

        int spinPosition = -1;
        for (int i = 0; i < spinUrls.size(); i++) {
            if (spinUrls.get(i).contains(thumbnail)) {
                spinPosition = i;
                break;
            }
        }

        if (spinPosition == -1) {
            currentCustomPoiShot = null;
            return;
        }

        currentCustomPoiShot.position = spinPosition;
        AddExteriorPoiSelectPosition dialog = AddExteriorPoiSelectPosition.newInstance(spinUrls.get(spinPosition));
        dialog.show(getFragmentManager(), "add_exterior_poi_select_picture");
    }

    @Override public void onAddExteriorPoiSelectCancel() {
        currentCustomPoiShot = null;
    }

    @Override
    public void onAddExteriorPoiSelectedPosition(float x, float y) {
        currentCustomPoiShot.positionX = x;
        currentCustomPoiShot.positionY = y;

        if (new AppSharedPreferences(this).getDontShowAddExternalPoiMessage()) {
            currentCustomPoiShot.waitingForPhoto = true;
        } else {
            AddExteriorPoiTakePicture dialog = AddExteriorPoiTakePicture.newInstance();
            dialog.show(getFragmentManager(), "add_exterior_poi_take_picture");
        }
    }

    @Override
    public void onAddExteriorPoiSelectPositionCancel() {
        currentCustomPoiShot = null;
    }

    @Override
    public void onAddPipSelectedPosition(float x, float y) {
        currentPipShot.positionX = x;
        currentPipShot.positionY = y;

       if (new AppSharedPreferences(this).getDontShowPipDetailMessage()) {
           currentPipShot.waitingForDetailPhoto = true;

       } else {
           final PipTakeDetailPictureDialog dialog = PipTakeDetailPictureDialog.newInstance();
           dialog.show(getFragmentManager(), "add_pip_detail_picture");
       }
    }

    @Override
    public void onAddPipSelectPositionCancel() {
        currentPipShot = new PipShot();
    }

    @Override
    public void onPipTakeDetailPicturePositiveClick() {
        currentPipShot.waitingForDetailPhoto = true;
    }

    @Override
    public void onPipTakeDetailPictureNegativeClick() {
        currentPipShot = new PipShot();
    }

    @Override
    public void onAddExteriorPoiTakePicturePositiveClick() {
        currentCustomPoiShot.waitingForPhoto = true;
    }

    @Override
    public void onAddExteriorPoiReviewSubmit(String pathname, boolean addToMagazine) {
        currentCustomPoiShot.filename = pathname;

        currentCustomPoiShot.addToMagazine = addToMagazine;
        ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
        if (adapter != null) {
            adapter.addCustomPoiShot(currentCustomPoiShot);
            shotListRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
            adapter.notifyItemChanged(adapter.getItemCount() - 1);
            updateResultIntent();
        }

        currentCustomPoiShot = new CustomPoiShot();
    }

    @Override
    public void onAddExteriorPoiReviewCancel() {
        currentCustomPoiShot = null;
    }

    @Override
    public void onPipTakeWidePicturePositiveClick() {
        currentPipShot = new PipShot();
    }

    @Override
    public void onAddPipReviewSubmit() {
        modeControlView.dimModeSelector();

        ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
        adapter.addPipShot(currentPipShot);
        updateAdapter(adapter.getSelectedIndex() - 1);
        currentPipShot = new PipShot();
        updateResultIntent();
    }

    @Override
    public void onAddPipReviewCancel() {
        currentPipShot = new PipShot();
    }

    private void configureGalleryMessages() {
        if (!galleryMessages.isEmpty()) {
            return;
        }

        galleryMessages.put(CaptureMode.POI, "Showing POI Only");
        galleryMessages.put(CaptureMode.PIP, "Showing PIP Only");
        galleryMessages.put(CaptureMode.ANIMATED_PICTURE, "Showing Animated Pictures Only");
        galleryMessages.put(CaptureMode.DETAIL, "Showing Pictures Only");
    }

    private void setBannerVisibility(boolean isVisible) {
        bannerView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        toggleBanner.setBackgroundResource(isVisible ? R.drawable.ic_banner_on_60dp : R.drawable.ic_banner_off_60dp);
    }

    private void scanCameras() {
        AppSharedPreferences preferences = new AppSharedPreferences(this);
        cameraIdStandard = preferences.getIntKeyValue(AppSharedPreferences.KEY_CAMERA_ID_STANDARD, -1);
        cameraIdWide = preferences.getIntKeyValue(AppSharedPreferences.KEY_CAMERA_ID_WIDE, -1);
        if (cameraIdStandard >= 0) {
            return;
        }

        float cameraAngle = -1;
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0 ; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Camera cameraTmp = Camera.open(i);
                float hAngle = cameraTmp.getParameters().getHorizontalViewAngle();
                float vAngle = cameraTmp.getParameters().getVerticalViewAngle();
                Timber.i("Camera scan: id=" + i + ", hAngle=" + hAngle + ", vAngle=" + vAngle);
                if (cameraIdStandard == -1) {
                    cameraIdStandard = i;
                    cameraAngle = hAngle;
                } else {
                    if (cameraAngle > hAngle) {
                        cameraIdWide = cameraIdStandard;
                        cameraIdStandard = i;
                    } else {
                        cameraIdWide = i;
                    }
                }
                cameraTmp.release();
            }
        }

        Timber.i("Detected standard angle lens: " + cameraIdStandard);
        Timber.i("Detected wide angle lens: " + cameraIdWide);

        preferences.putIntKeyValue(AppSharedPreferences.KEY_CAMERA_ID_STANDARD, cameraIdStandard);
        preferences.putIntKeyValue(AppSharedPreferences.KEY_CAMERA_ID_WIDE, cameraIdWide);
    }

    boolean isLimitedVideo() {
        return limitVideoDuration;
    }

    int getAutomaticPictureInterval() {
        return automaticPictureInterval;
    }

    /**
     *  Check whether we have the inventory data we need to create a POI.
     */
    private boolean isAddPoiCandidate() {
        boolean isCandidate = false;
        Vehicle vehicle = null;
        try {
            vehicle = getHelper().getVehicleDao().queryForId(vin);
            if (vehicle != null) {
                // The back end needs certain data to be present in order to create POIs.
                isCandidate = !TextUtils.isEmpty(vehicle.getMake()) && !TextUtils.isEmpty(vehicle.getModel());
            } else {
                Util.logError("Couldn't find vehicle in database");
            }
        } catch (SQLException e) {
            Util.logError("Error finding car by VIN", e);
        }

        return isCandidate;
    }

    private void configureMagazine(Bundle extras) {
        final int magazineID = extras.getInt(EXTRA_MAGAZINE_ID, -1);
        if (session.declinedSelectMagazine()) {
            poiCheckBox.setVisibility(View.GONE);
            magazineShots = new ArrayList<MagazineShot>();
        } else {
            try {
                final QueryBuilder<Magazine, Integer> magazineBuilder = getHelper().getMagazineDao().queryBuilder();
                magazineBuilder.where().eq(Magazine.COLUMN_INDEX, magazineID);

                final QueryBuilder<MagazineShot, Integer> shotBuilder = getHelper().getMagazineShotDao().queryBuilder();
                shotBuilder
                        .orderBy(Magazine.COLUMN_ORDER, true);
                shotBuilder.join(magazineBuilder);

                magazineShots = getHelper().getMagazineShotDao().query(shotBuilder.prepare());
            } catch (SQLException e) {
                Util.logError("Error getting shot list", e);
                return;
            }
        }

        if ((magazineShots == null || magazineShots.isEmpty()) && !session.declinedSelectMagazine()) {
            return;
        }

        if (isAddPoiCandidate()) {
            // Start network request to see if spin thumbnails are available.
            startThumbnailRequest();
        }

        magazineState = new MagazineState(magazineShots);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        shotListRecyclerView.setLayoutManager(layoutManager);
        ShotListAdapter adapter = new ShotListAdapter(getApplicationContext(), magazineState, shotCollection, magazineShots);
        shotListAnimator = new ShotListAnimator(magazineState);
        shotListRecyclerView.setAdapter(adapter);
        shotListRecyclerView.setItemAnimator(shotListAnimator);
        shotListRecyclerViewContainer.setVisibility(View.VISIBLE);

        shotListRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                modeControlView.dimModeSelector();
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }
        });

        RecyclerView.OnScrollListener shotListScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    //End of list
                    shotListRightScroll.setVisibility(View.GONE);
                } else {
                    shotListRightScroll.setVisibility(View.VISIBLE);
                }

                if (pastVisibleItems == 0) {
                    shotListLeftScroll.setVisibility(View.GONE);
                } else {
                    shotListLeftScroll.setVisibility(View.VISIBLE);
                }
            }
        };
        shotListRecyclerView.addOnScrollListener(shotListScrollListener);
    }

    private void startThumbnailRequest() {
        thumbnailRequest = new ESWebService().getVehicle(this, vin);
        thumbnailRequest.enqueue(new Callback<InventoryResults>() {
            @Override
            public void onResponse(Call<InventoryResults> call, Response<InventoryResults> response) {
                thumbnailRequest = null;
                final InventoryResults results = response.body();
                if (results.hits != null && results.hits.hits != null && !results.hits.hits.isEmpty()) {
                    InventoryResults.VehicleHit vehicle = results.hits.hits.get(0);
                    if (vehicle._source != null
                            && vehicle._source.thumbnail_pics != null
                            && !vehicle._source.thumbnail_pics.isEmpty()
                            && !vehicle._source.spin_pics.isEmpty()) {
                        // If thumbnails are available allow the user to create external POIs.
                        thumbnailUrls = new ArrayList<String>(vehicle._source.thumbnail_pics);
                        spinUrls = new ArrayList<String>(vehicle._source.spin_pics.get(0));
                        if (magazineState != null) {
                            ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                            adapter.enableAddPoi();
                            shotListRecyclerView.getAdapter().notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<InventoryResults> call, Throwable t) {
                thumbnailRequest = null;
                Timber.e(t, "Failed getting thumbnails for %s", vin);
            }
        });

    }

    private void poiShotClicked(int index) {
        if (null != shotCollection.poiShots.get(index)) {
            // If the user has taken a picture, treat this like a long click.
            poiShotLongClicked(index);
        } else {
            // No picture has been taken, so select this index.
            ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
            adapter.setSelectedIndex(index);


//            if (!magazineState.hasMagazine()) {
//                magazineState.setSelectedIndex(magazineState.getPictureCount());
//            }

            if (adapter != null) {
                // We don't notify on specific indexes here because we don't want an animation
                // when just changing selected shot.
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void poiShotLongClicked(int index) {
        // PoiShot shot = shotCollection.poiShots.valueAt(index);
        PoiShot shot = shotCollection.poiShots.get(index);
        if (shot != null) {
            final Intent intent = ShotDetailActivity.createIntent(this, shot.file, true, index, "");
            startActivityForResult(intent, SHOT_DETAIL_REQUEST_CODE);
            return;
        } else {
            final MagazineShot magazineShot = magazineShots.get(index);
            final Uri uri = magazineShot.getImageUri();
            final File image = FileUtil.getLocalImageFile(uri);
            String instruction = magazineShot.getInstruction();
            final Intent intent = ShotDetailActivity.createIntent(this, image, false, index, instruction);
            startActivityForResult(intent, SHOT_DETAIL_REQUEST_CODE);
            return;
        }
    }

    private void addPoiClicked() {
        AddExteriorPoiDialogFragment dialog = AddExteriorPoiDialogFragment.newInstance();
        dialog.show(getFragmentManager(), "add_exterior_poi");
    }

    private void pipShotClicked(int index) {
        final PipShot shot = shotCollection.pipShots.get(index);
        final Intent intent = PipShotDetailActivity.createIntent(this, new File(shot.detailImagePathname), new File(shot.wideImagePathname), index);
        startActivityForResult(intent, SHOT_PIP_DETAIL_REQUEST_CODE);
    }

    private void detailShotClicked(int index) {
        DetailShot shot = shotCollection.detailShots.get(index);
        final Intent intent = ShotDetailActivity.createIntent(this, shot.file, true, index, "");
        startActivityForResult(intent, SHOT_PICTURE_DETAIL_REQUEST_CODE);
    }

     private void animatedPictureClicked(int index) {
         final ShotCollection.AnimatedPicturePosition position = shotCollection.getAnimatedPicturePosition(index);
         if (position == null) {
             return;
         }

         final AnimatedPictureShot shot = shotCollection.animatedPictureShots.get(position.shotIndex);
         if (position.frameIndex > -1) {
             // It is possible that a shot is tapped on before it is extracted. In that case
             // just ignore the touch.
             if (position.frameIndex < shot.frames.size()) {
                 final File image = shot.frames.get(position.frameIndex);
                 if (image != null) {
                     final Intent intent = ShotDetailActivity.createIntent(this, image, true, index, "");
                     startActivityForResult(intent, SHOT_ANIMATED_PICTURE_REQUEST_CODE);
                 }
             }
         } else {
             final Intent intent = VideoDetailActivity.createIntent(this, shot.video, index);
             startActivityForResult(intent, SHOT_ANIMATED_PICTURE_REQUEST_CODE);
         }
     }

     private void customPoiClicked(int index) {
         CustomPoiShot shot = shotCollection.customPoiShots.get(index);
         final Intent intent = ShotDetailActivity.createIntent(this, new File(shot.filename), true, index, "");
         startActivityForResult(intent, SHOT_CUSTOM_POI_REQUEST_CODE);
     }

    private void preloadIcons(int icons_id) {
        long time_s = System.currentTimeMillis();
        String[] icons = getResources().getStringArray(icons_id);
        for (int i = 0; i < icons.length; i++) {
            int resource = getResources().getIdentifier(icons[i], null, this.getApplicationContext().getPackageName());
            Bitmap bm = BitmapFactory.decodeResource(getResources(), resource);
            this.preloaded_bitmap_resources.put(resource, bm);
        }

    }

    @Override
    protected void onDestroy() {
        // Need to recycle to avoid out of memory when running tests - probably good practice to do anyway
        for (Map.Entry<Integer, Bitmap> entry : preloaded_bitmap_resources.entrySet()) {
            if (MyDebug.LOG)
                Log.d(TAG, "recycle: " + entry.getKey());
            entry.getValue().recycle();
        }
        preloaded_bitmap_resources.clear();

        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // Some Bluetooth selfie sticks send both VOLUME_UP and KEYCODE_ENTER. We only need
        // VOLUME_UP. If we don't discared KEYCODE_ENTER it will cause some of the controls
        // on the camera screen like the flash button to be activated.
        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (MyDebug.LOG)
            Log.d(TAG, "onKeyDown: " + keyCode);
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN: {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                String volume_keys = sharedPreferences.getString("preference_volume_keys", "volume_take_photo");
                if (volume_keys.equals("volume_take_photo")) {
                    takePicture();
                    return true;
                } else if (volume_keys.equals("volume_focus")) {
                    preview.requestAutoFocus();
                    return true;
                } else if (volume_keys.equals("volume_zoom")) {
                    if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
                        this.preview.zoomIn();
                    else
                        this.preview.zoomOut();
                    return true;
                } else if (volume_keys.equals("volume_exposure")) {
                    if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
                        this.preview.changeExposure(1, true);
                    else
                        this.preview.changeExposure(-1, true);
                    return true;
                } else if (volume_keys.equals("volume_auto_stabilise")) {
                    if (this.supports_auto_stabilise) {
                        boolean auto_stabilise = sharedPreferences.getBoolean("preference_auto_stabilise", false);
                        auto_stabilise = !auto_stabilise;
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("preference_auto_stabilise", auto_stabilise);
                        editor.apply();
//						String message = getResources().getString(R.string.preference_auto_stabilise) + ": " +  "on");
//						preview.showToast(changed_auto_stabilise_toast, message);
                    } else {
                        preview.showToast(changed_auto_stabilise_toast, R.string.auto_stabilise_not_supported);
                    }
                    return true;
                } else if (volume_keys.equals("volume_really_nothing")) {
                    // do nothing, but still return true so we don't change volume either
                    return true;
                }
                // else do nothing here, but still allow changing of volume (i.e., the default behaviour)
                break;
            }
            case KeyEvent.KEYCODE_MENU: {
                // needed to support hardware menu button
                // tested successfully on Samsung S3 (via RTL)
                // see http://stackoverflow.com/questions/8264611/how-to-detect-when-user-presses-menu-key-on-their-android-device
                openSettings();
                return true;
            }
            case KeyEvent.KEYCODE_CAMERA: {
                if (event.getRepeatCount() == 0) {
                    clickedTakePhoto(shutterButton);
                    return true;
                }
            }
            case KeyEvent.KEYCODE_FOCUS: {
                preview.requestAutoFocus();
                return true;
            }
            case KeyEvent.KEYCODE_ZOOM_IN: {
                preview.zoomIn();
                return true;
            }
            case KeyEvent.KEYCODE_ZOOM_OUT: {
                preview.zoomOut();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private SensorEventListener accelerometerListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            preview.onAccelerometerSensorChanged(event);
        }
    };

    private SensorEventListener magneticListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            preview.onMagneticSensorChanged(event);
        }
    };


    private void setupLocationListener() {
        if (MyDebug.LOG)
            Log.d(TAG, "setupLocationListener");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        // Define a listener that responds to location updates
        boolean store_location = sharedPreferences.getBoolean("preference_location", false);
        if (store_location && locationListener == null) {
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    if (MyDebug.LOG)
                        Log.d(TAG, "onLocationChanged");
                    preview.locationChanged(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            // see https://sourceforge.net/p/opencamera/tickets/1/
            if (mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            }
            if (mLocationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        } else if (!store_location && locationListener != null) {
            if (this.locationListener != null) {
                mLocationManager.removeUpdates(locationListener);
                locationListener = null;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
        if (adapter != null) {
            adapter.setListener(shotListListener);
        };

        mSensorManager.registerListener(accelerometerListener, mSensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(magneticListener, mSensorMagnetic, SensorManager.SENSOR_DELAY_NORMAL);
        orientationEventListener.enable();
        setupLocationListener();
        if (!this.camera_in_background) {
            // immersive mode is cleared when app goes into background
            setImmersiveMode(true);
        }
        layoutUI();
        //updateGalleryIcon(); // update in case images deleted whilst idle
        preview.onResume();

        shutterButton.setOnTouchListener(new ShutterTouchListener());

        // Slave camera might stay on camera screen all of the time so update settings when
        // we resume due to a VIN change or other reason.
        final AppSharedPreferences pref = new AppSharedPreferences(this);
        if (pref.isMasterSlaveEnabled() && !pref.isMaster()) {
            SettingsServerComms settingsServerComms = new SettingsServerComms(this, getHelper());
            if (settingsServerComms.shouldPollSettings()) {
                settingsServerComms.startGetSettingsRequest(null);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Android will deliver a click event after onPause has been called. This can happen if
        // you tap on a shot twice in quick succession, in which case we try to start the
        // shot detail activity twice. Guard against that by resetting the click listener
        // after the first click.
        ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
        if (adapter != null) {
            adapter.setListener(null);
        };

        closePopup();
        mSensorManager.unregisterListener(accelerometerListener);
        mSensorManager.unregisterListener(magneticListener);
        orientationEventListener.disable();
        if (this.locationListener != null) {
            mLocationManager.removeUpdates(locationListener);
            locationListener = null;
        }
        // reset location, as may be out of date when resumed - the location listener is reinitialised when resuming
        preview.resetLocation();
        preview.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SHOT_DETAIL_REQUEST_CODE) {
            if (resultCode == ShotDetailActivity.RESULT_DELETE) {
                final int index = data.getIntExtra(ShotDetailActivity.EXTRA_SHOT_INDEX, -1);
                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                if (adapter != null) {
                    adapter.removePoiShot(index);
                }
            }
        } else if (requestCode == SHOT_PIP_DETAIL_REQUEST_CODE) {
            if (resultCode == PipShotDetailActivity.RESULT_DELETE) {
                final int index = data.getIntExtra(ShotDetailActivity.EXTRA_SHOT_INDEX, -1);
                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                if (adapter != null) {
                    adapter.removePipShot(index);
                }
            }
        } else if (requestCode == SHOT_PICTURE_DETAIL_REQUEST_CODE) {
            if (resultCode == PipShotDetailActivity.RESULT_DELETE) {
                final int index = data.getIntExtra(ShotDetailActivity.EXTRA_SHOT_INDEX, -1);
                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                if (adapter != null) {
                    adapter.removeDetailShot(index);
                }
            }
        } else if (requestCode == SHOT_ANIMATED_PICTURE_REQUEST_CODE) {
            if (resultCode == PipShotDetailActivity.RESULT_DELETE) {
                final int index = data.getIntExtra(ShotDetailActivity.EXTRA_SHOT_INDEX, -1);
                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                if (adapter != null) {
                    adapter.removeAnimatedPictureShot(index);
                }
            }
        } else if (requestCode == SHOT_CUSTOM_POI_REQUEST_CODE) {
            if (resultCode == ShotDetailActivity.RESULT_DELETE) {
                final int index = data.getIntExtra(ShotDetailActivity.EXTRA_SHOT_INDEX, -1);
                shotCollection.customPoiShots.remove(index);
                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        }

        updateResultIntent();
    }

    private void toggleButtonsEnabled(Boolean enabled) {
        ImageButton tb = (ImageButton) findViewById(R.id.toggleBanner);
        ImageButton tg = (ImageButton) findViewById(R.id.toggleGrid);
        ImageButton fab = (ImageButton) findViewById(R.id.flashAutoButton);
        ImageButton fonb = (ImageButton) findViewById(R.id.flashOnButton);
        ImageButton foffb = (ImageButton) findViewById(R.id.flashOffButton);
        ImageButton sflb = (ImageButton) findViewById(R.id.selectFlashButton);
        ImageButton sb = (ImageButton) findViewById(R.id.settingsButton);
        ImageButton sfob = (ImageButton) findViewById(R.id.focusSelectButton);
        tb.setEnabled(enabled);
        tg.setEnabled(enabled);
        fab.setEnabled(enabled);
        fonb.setEnabled(enabled);
        foffb.setEnabled(enabled);
        sflb.setEnabled(enabled);
        sb.setEnabled(enabled);
        sfob.setEnabled(enabled);
    }

    public void delayControlsHide() {
        Runnable r = new Runnable() {
            public void run() {
                hideControls(defaultControlAnimationDelay);
            }
        };
        controlHandler.postDelayed(r, 2000);
    }

    public void hideControls(int delay) {
        final View viewControls = findViewById(R.id.popup_controls);
        //viewControls.setVisibility(LinearLayout.VISIBLE);
        //toggleButtonsEnabled(false);
        /*Animation animation = AnimationUtils.loadAnimation(this, R.anim.left_out);
        viewControls.setAnimation(animation);
        viewControls.animate();
        animation.start();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                //viewControls.setClickable(false);
                View control_show = findViewById(R.id.control_show);
                control_show.setVisibility(Button.VISIBLE);
            }
        });*/
        ObjectAnimator mSlidInAnimator = ObjectAnimator.ofFloat(viewControls, "translationX", -100);

        mSlidInAnimator.setDuration(delay);
        mSlidInAnimator.start();
        View control_show = findViewById(R.id.control_show);
        control_show.setVisibility(Button.VISIBLE);
    }

    public void showControls(View view) {
        View viewControls = findViewById(R.id.popup_controls);
        //toggleButtonsEnabled(true);
        //Animation animation = AnimationUtils.loadAnimation(this, R.anim.right_in);
        //viewControls.setAnimation(animation);
        //viewControls.animate();

        ObjectAnimator mSlidInAnimator = ObjectAnimator.ofFloat(viewControls, "translationX", 0);
        mSlidInAnimator.setDuration(500);
        mSlidInAnimator.start();
        View control_show = findViewById(R.id.control_show);
        control_show.setVisibility(Button.INVISIBLE);
        /*animation.start();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                Runnable r = new Runnable() {
                    public void run() {
                        System.out.println("runrunrun==================");
                        hideControls();
                    }
                };
                controlHandler.postDelayed(r, 2000);
            }
        });*/

    }

    public void layoutUI() {
        if (MyDebug.LOG)
            Log.d(TAG, "layoutUI");
        this.preview.updateUIPlacement();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String ui_placement = sharedPreferences.getString("preference_ui_placement", "ui_right");
        boolean ui_placement_right = ui_placement.equals("ui_right");
        if (MyDebug.LOG)
            Log.d(TAG, "ui_placement: " + ui_placement);
        // new code for orientation fixed to landscape
        // the display orientation should be locked to landscape, but how many degrees is that?
        int rotation = this.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        // getRotation is anti-clockwise, but current_orientation is clockwise, so we add rather than subtract
        // relative_orientation is clockwise from landscape-left
        //int relative_orientation = (current_orientation + 360 - degrees) % 360;
        int relative_orientation = (current_orientation + degrees) % 360;
        if (MyDebug.LOG) {
            Log.d(TAG, "    current_orientation = " + current_orientation);
            Log.d(TAG, "    degrees = " + degrees);
            Log.d(TAG, "    relative_orientation = " + relative_orientation);
        }
        int ui_rotation = (360 - relative_orientation) % 360;
        preview.setUIRotation(ui_rotation);
        int align_left = RelativeLayout.ALIGN_LEFT;
        int align_right = RelativeLayout.ALIGN_RIGHT;
        //int align_top = RelativeLayout.ALIGN_TOP;
        //int align_bottom = RelativeLayout.ALIGN_BOTTOM;
        int left_of = RelativeLayout.LEFT_OF;
        int right_of = RelativeLayout.RIGHT_OF;
        int above = RelativeLayout.ABOVE;
        int below = RelativeLayout.BELOW;
        int align_parent_left = RelativeLayout.ALIGN_PARENT_LEFT;
        int align_parent_right = ALIGN_PARENT_RIGHT;
        int align_parent_top = RelativeLayout.ALIGN_PARENT_TOP;
        int align_parent_bottom = RelativeLayout.ALIGN_PARENT_BOTTOM;
        if (!ui_placement_right) {
            //align_top = RelativeLayout.ALIGN_BOTTOM;
            //align_bottom = RelativeLayout.ALIGN_TOP;
            above = RelativeLayout.BELOW;
            below = RelativeLayout.ABOVE;
            align_parent_top = RelativeLayout.ALIGN_PARENT_BOTTOM;
            align_parent_bottom = RelativeLayout.ALIGN_PARENT_TOP;
        }
        {
//			View view = findViewById(R.id.settings);
//			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();
////			layoutParams.addRule(align_parent_left, 0);
////			layoutParams.addRule(align_parent_right, RelativeLayout.TRUE);
////			layoutParams.addRule(align_parent_top, RelativeLayout.TRUE);
////			layoutParams.addRule(align_parent_bottom, 0);
////			layoutParams.addRule(left_of, 0);
////			layoutParams.addRule(right_of, 0);
////			view.setLayoutParams(layoutParams);
//			view.setRotation(ui_rotation);
//
//			view = findViewById(R.id.gallery);
//			layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();
////			layoutParams.addRule(align_parent_top, RelativeLayout.TRUE);
////			layoutParams.addRule(align_parent_bottom, 0);
////			layoutParams.addRule(left_of, R.id.settings);
////			layoutParams.addRule(right_of, 0);
////			view.setLayoutParams(layoutParams);
//			view.setRotation(ui_rotation);

            View view = findViewById(R.id.popup);
//			layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();
//			layoutParams.addRule(align_parent_top, RelativeLayout.TRUE);
//			layoutParams.addRule(align_parent_bottom, 0);
//			layoutParams.addRule(left_of, R.id.gallery);
//			layoutParams.addRule(right_of, 0);
//			view.setLayoutParams(layoutParams);
            view.setRotation(ui_rotation);


            view = findViewById(R.id.exposure);
//			layoutParams = (RelativeLayout.LayoutParams)view.getLayoutParams();

            view.setRotation(ui_rotation);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) shutterButton.getLayoutParams();
            layoutParams.addRule(align_parent_left, 0);
            layoutParams.addRule(align_parent_right, RelativeLayout.TRUE);
            shutterButton.setLayoutParams(layoutParams);
//            shutterButton.setRotation(ui_rotation);

            view = findViewById(R.id.zoom);
            layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
            layoutParams.addRule(align_parent_left, 0);
            layoutParams.addRule(align_parent_right, RelativeLayout.TRUE);
            layoutParams.addRule(align_parent_top, 0);
            layoutParams.addRule(align_parent_bottom, RelativeLayout.TRUE);
            view.setLayoutParams(layoutParams);
            view.setRotation(180.0f); // should always match the zoom_seekbar, so that zoom in and out are in the same directions

            view = findViewById(R.id.zoom_seekbar);
            layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
            layoutParams.addRule(align_left, 0);
            layoutParams.addRule(align_right, R.id.zoom);
            layoutParams.addRule(above, R.id.zoom);
            layoutParams.addRule(below, 0);
            view.setLayoutParams(layoutParams);
        }

        {
            // set seekbar info
            View view = findViewById(R.id.seekbar);
            view.setRotation(ui_rotation);

            int width_dp = 0;
            if (ui_rotation == 0 || ui_rotation == 180) {
                width_dp = 300;
            } else {
                width_dp = 200;
            }
            int height_dp = 50;
            final float scale = getResources().getDisplayMetrics().density;
            int width_pixels = (int) (width_dp * scale + 0.5f); // convert dps to pixels
            int height_pixels = (int) (height_dp * scale + 0.5f); // convert dps to pixels
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
            lp.width = width_pixels;
            lp.height = height_pixels;
            view.setLayoutParams(lp);

            view = findViewById(R.id.seekbar_zoom);
            view.setRotation(ui_rotation);
            view.setAlpha(0.5f);
            // n.b., using left_of etc doesn't work properly when using rotation (as the amount of space reserved is based on the UI elements before being rotated)
            if (ui_rotation == 0) {
                view.setTranslationX(0);
                view.setTranslationY(height_pixels);
            } else if (ui_rotation == 90) {
                view.setTranslationX(-height_pixels);
                view.setTranslationY(0);
            } else if (ui_rotation == 180) {
                view.setTranslationX(0);
                view.setTranslationY(-height_pixels);
            } else if (ui_rotation == 270) {
                view.setTranslationX(height_pixels);
                view.setTranslationY(0);
            }
        }

        {
            zoomSeekBar.setRotation(ui_rotation);

            int width_dp = 0;
            if (ui_rotation == 0 || ui_rotation == 180) {
                width_dp = 300;
            } else {
                width_dp = 200;
            }
            int height_dp = 50;
            final float scale = getResources().getDisplayMetrics().density;
            int width_pixels = (int) (width_dp * scale + 0.5f); // convert dps to pixels
            int height_pixels = (int) (height_dp * scale + 0.5f); // convert dps to pixels
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) zoomSeekBar.getLayoutParams();
            lp.width = width_pixels;
            lp.height = height_pixels;
            zoomSeekBar.setLayoutParams(lp);
        }

        {
            View view = findViewById(R.id.popup_container);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();

            view.setRotation(ui_rotation);
            // reset:
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            if (MyDebug.LOG) {
                Log.d(TAG, "popup view width: " + view.getWidth());
                Log.d(TAG, "popup view height: " + view.getHeight());
            }
            if (ui_rotation == 0 || ui_rotation == 180) {
                view.setPivotX(view.getWidth() / 2.0f);
                view.setPivotY(view.getHeight() / 2.0f);
            } else {
                view.setPivotX(view.getWidth());
                view.setPivotY(ui_placement_right ? 0.0f : view.getHeight());
                if (ui_placement_right) {
                    if (ui_rotation == 90)
                        view.setTranslationY(view.getWidth());
                    else if (ui_rotation == 270)
                        view.setTranslationX(-view.getHeight());
                } else {
                    if (ui_rotation == 90)
                        view.setTranslationX(-view.getHeight());
                    else if (ui_rotation == 270)
                        view.setTranslationY(-view.getWidth());
                }
            }
            //TODO RV for hiding contronls
            //hideControls(0);
        }

        {
            //DisplayImageOptions options = new DisplayImageOptions.Builder().cacheOnDisc().build();
            String tmPath = "file://";
            Log.d("=============tmPath", tmPath);
            try {
                File banner = FileUtil.getBannerFile(projectId);
                Picasso.get().load(banner).into(bannerView);
            } catch (Exception e) {
                System.out.println("Exception==================");
                System.out.println(e);
            }

            toggleGrid = (ImageButton) findViewById(R.id.toggleGrid
            );
            gridOverlayView = (View) findViewById(R.id.grid4x4_view);
            isGridVisible = session.isShowGridLines();
            gridOverlayView.setVisibility(isGridVisible ? View.VISIBLE : View.INVISIBLE);
            toggleGrid.setImageResource(isGridVisible ?  R.drawable.ic_grid_on_white_36dp : R.drawable.ic_grid_off_white_36dp);
            toggleGrid.setOnClickListener(new Button.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (isGridVisible) {
                        gridOverlayView.setVisibility(View.GONE);
                        toggleGrid.setImageResource(R.drawable.ic_grid_off_white_36dp);
                        //toggleBanner.setText("SHOW BANNER");
                        isGridVisible = false;
                    } else {
                        gridOverlayView.setVisibility(View.VISIBLE);
                        toggleGrid.setImageResource(R.drawable.ic_grid_on_white_36dp);
                        //toggleBanner.setText("HIDE BANNER");
                        isGridVisible = true;
                    }
                }
            });


            if (preview != null) {
                final int drawable;
                if (shutterButtonResourceID != -1) {
                    drawable = shutterButtonResourceID;
                } else if (preview.isVideo()) {
                    drawable = R.drawable.take_video_selector;
                } else if (isSpinitSingleShotMode) {
                    drawable = R.drawable.shutter_auto;
                } else {
                    drawable = R.drawable.take_photo_selector;
                }

                shutterButton.setImageResource(drawable);
            }
        }
    }

    private void onOrientationChanged(int orientation) {
        /*if( MyDebug.LOG ) {
            Log.d(TAG, "onOrientationChanged()");
			Log.d(TAG, "orientation: " + orientation);
			Log.d(TAG, "current_orientation: " + current_orientation);
		}*/
        if (orientation == OrientationEventListener.ORIENTATION_UNKNOWN)
            return;
        int diff = Math.abs(orientation - current_orientation);
        if (diff > 180)
            diff = 360 - diff;
        // only change orientation when sufficiently changed
        if (diff > 60) {
            orientation = (orientation + 45) / 90 * 90;
            orientation = orientation % 360;
            if (orientation != current_orientation) {
                this.current_orientation = orientation;
                if (MyDebug.LOG) {
                    Log.d(TAG, "current_orientation is now: " + current_orientation);
                }
                layoutUI();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (MyDebug.LOG)

            Log.d(TAG, "onConfigurationChanged()");
        // configuration change can include screen orientation (landscape/portrait) when not locked (when settings is open)
        // needed if app is paused/resumed when settings is open and device is in portrait mode
        preview.setCameraDisplayOrientation(this);
        super.onConfigurationChanged(newConfig);
    }

    @OnClick(R.id.zoomButton)
    void onZoomButtonClick() {
        if (zoomSeekBar.getVisibility() != View.VISIBLE && preview.getCamera() != null) {
            clearSeekBar();

            zoomSeekBar.setMax(preview.getMaxZoomFactor());
            zoomSeekBar.setProgress(preview.getZoomFactor());
            zoomSeekBar.setVisibility(View.VISIBLE);
            if (cameraIdWide >= 0) {
                zoomSeekBarMinus.setVisibility(View.VISIBLE);
                zoomSeekBarPlus.setVisibility(View.VISIBLE);
                zoomSeekBarMinusBG.setVisibility(preview.getCameraId() == cameraIdWide ? View.VISIBLE : View.GONE);
                zoomSeekBarPlusBG.setVisibility(preview.getCameraId() == cameraIdStandard ? View.VISIBLE : View.GONE);
            }
            zoomSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    onZoomSeekBarChanged();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        } else {
            zoomSeekBar.setVisibility(View.GONE);
            zoomSeekBarMinus.setVisibility(View.GONE);
            zoomSeekBarMinusBG.setVisibility(View.GONE);
            zoomSeekBarPlus.setVisibility(View.GONE);
            zoomSeekBarPlusBG.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.zoomSeekBarMinus)
    void onZoomSeekBarMinusClick() {
        Timber.i("Wide angle lens selected");
        zoomSeekBarMinusBG.setVisibility(View.VISIBLE);
        zoomSeekBarPlusBG.setVisibility(View.GONE);
        preview.switchCamera(cameraIdWide);
    }

    @OnClick(R.id.zoomSeekBarPlus)
    void onZoomSeekBarPlusClick() {
        Timber.i("Standard lens selected");
        zoomSeekBarMinusBG.setVisibility(View.GONE);
        zoomSeekBarPlusBG.setVisibility(View.VISIBLE);
        preview.switchCamera(cameraIdStandard);
    }

    @OnClick(R.id.toggleBanner)
    void onToggleBannerClick() {
        File banner = FileUtil.getBannerFile(projectId);
        if (banner == null || !banner.exists()) {
            final BannerMissingDialogFragment fragment = BannerMissingDialogFragment.newInstance();
            fragment.show(getFragmentManager(), "banner_missing");
        } else {
            setBannerVisibility(!(bannerView.getVisibility() == View.VISIBLE));
        }
    }

    private void onZoomSeekBarChanged() {
        preview.zoomTo(zoomSeekBar.getProgress(), false);
    }

    public void clickedTakePhoto(View view) {
        final AppSharedPreferences sharedPreferences = new AppSharedPreferences(this);

        Timber.i("clickedTakePhoto master/slave: %b master: %b trigger slaves: %b", sharedPreferences.isMasterSlaveEnabled(), sharedPreferences.isMaster(), sharedPreferences.triggerSlaves());

        // Only activate slaves if we are extracting photos from video and using a turntable.
        if (sharedPreferences.isLoggedIn()
                && Util.shouldSendSlaveCommand(this)
                && isVideo
                && !isFullmo
                && sharedPreferences.getUseTurntable()) {
            Vehicle vehicle = null;
            try {
                vehicle = getHelper().getVehicleDao().queryForId(vin);
                if (vehicle == null) {
                    Util.logError("Couldn't find vehicle in database");
                }
            } catch (SQLException e) {
                Util.logError("Error finding car by VIN", e);
            }

            if (!isCapturingVideo) {
                InterDeviceCommand command = new InterDeviceCommand();
                command.data = new InterDeviceCommand.CommandData();
                command.data.command = InterDeviceCommand.COMMAND_ACTIVATE_CAMERA;

                SlaveCaptureData captureData = new SlaveCaptureData();
                captureData.vin = vehicle.getVin();
                captureData.year = vehicle.getYear();
                captureData.make = vehicle.getMake();
                captureData.model = vehicle.getModel();
                captureData.project = vehicle.getProject();
                captureData.library = vehicle.getLibrary();
                captureData.replace = session.shouldReplaceMedia();
                command.slaveCaptureData = captureData;

                NetworkService.sendMessage(this, command);
            }

            this.takePicture();
            return;
        }

        if (isSpinitSingleShotMode) {
            // Shutter button doesn't do anything in automatic picture mode so hide it.
            shutterButton.setVisibility(View.GONE);
            this.takePicture();
        } else if (session.isAutomaticPictureMode()) {
            if (!startedAutoPictures) {
                shutterButton.setImageResource(R.drawable.shutter_auto_stop);
                startedAutoPictures = true;
                takePicture();
            } else {
                if (imageDescriptions.isEmpty()) {
                    finish();
                } else {
                    isCancelPending = true;
                    preview.onPause();
                    showCaptureInterruptedDialog();
                }
            }

        } else {
            if (modeControlView.getVisibility() == View.VISIBLE) {
                modeControlView.dimModeSelector();
            }
            this.takePicture();
        }
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void startSlaveCapture(SlaveCaptureData data) {
        slaveData = data;
        isVideo = true;
        isFullmo = false;
        isSpinitSingleShotMode = false;

        takePicture();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void cancelSlaveCapture(SlaveCancelCapture command) {
        preview.stopVideo(true);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void deleteSlaveMedia(SlaveDeleteMedia command) {
        deleteMedia();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void slaveEnqueueMedia(SlaveEnqueueMedia command) {
        if (isCapturingVideo) {
            // The captures aren't perfectly synchronized so the master might have stopped and sent
            // the command to send the media before the client is done capturing.
            lastEnqueueMediaCommand = command;
        } else {
            CameraActivity.this.enqueueSpinitPictures();
        }
    }

    public void clickedSwitchCamera(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedSwitchCamera");
        this.closePopup();
        this.preview.switchCamera();
    }

    public void clickedSwitchVideo(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedSwitchVideo");
        this.closePopup();
        this.preview.switchVideo(true, true);
    }

    void clearSeekBar() {
        View view = findViewById(R.id.seekbar);
        view.setVisibility(View.GONE);
        view = findViewById(R.id.seekbar_zoom);
        view.setVisibility(View.GONE);
    }

    void clearZoomSeekBar() {
        zoomSeekBar.setVisibility(View.GONE);
        zoomSeekBarMinus.setVisibility(View.GONE);
        zoomSeekBarMinusBG.setVisibility(View.GONE);
        zoomSeekBarPlus.setVisibility(View.GONE);
        zoomSeekBarPlusBG.setVisibility(View.GONE);
    }

    void setSeekBarExposure() {
        SeekBar seek_bar = ((SeekBar) findViewById(R.id.seekbar));

        final int min_exposure = preview.getMinimumExposure();
        seek_bar.setMax(preview.getMaximumExposure() - min_exposure);
        seek_bar.setProgress(preview.getCurrentExposure() - min_exposure);
    }

    public void clickedExposure(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedExposure");
        this.closePopup();
        SeekBar seek_bar = ((SeekBar) findViewById(R.id.seekbar));
        final TextView seek100 = (TextView) findViewById(R.id.seekbar100);
        int visibility = seek_bar.getVisibility();
        if (visibility == View.GONE && preview.getCamera() != null && preview.supportsExposures()) {
            clearZoomSeekBar();
            final int min_exposure = preview.getMinimumExposure();
            seek_bar.setVisibility(View.VISIBLE);
            seek100.setVisibility(View.VISIBLE);
            setSeekBarExposure();
            seek_bar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (MyDebug.LOG)
                        Log.d(TAG, "exposure seekbar onProgressChanged");
                    seek100.setText(String.valueOf(progress) + " /10");
                    preview.setExposure(min_exposure + progress, false);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });

            ZoomControls seek_bar_zoom = (ZoomControls) findViewById(R.id.seekbar_zoom);
            seek_bar_zoom.setVisibility(View.VISIBLE);
            seek_bar_zoom.setOnZoomInClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    preview.changeExposure(1, true);
                }
            });
            seek_bar_zoom.setOnZoomOutClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    preview.changeExposure(-1, true);
                }
            });
        } else if (visibility == View.VISIBLE) {
            clearSeekBar();
            seek100.setVisibility(View.INVISIBLE);
        }
    }

    public void clickedExposureLock(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedExposureLock");
        this.preview.toggleExposureLock();
    }

    public void clickedSettings(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedSettings");
        openSettings();
    }

    public boolean popupIsOpen() {
        if (popup_view != null) {
            return true;
        }
        return false;
    }

    void closePopup() {
        if (MyDebug.LOG)
            Log.d(TAG, "close popup");
        if (popupIsOpen()) {
            ViewGroup popup_container = (ViewGroup) findViewById(R.id.popup_container);
            popup_container.removeAllViews();
            popup_view.close();
            popup_view = null;
        }
    }

    Bitmap getPreloadedBitmap(int resource) {
        Bitmap bm = this.preloaded_bitmap_resources.get(resource);
        return bm;
    }

    public void clickedPopupSettings(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedPopupSettings");
        final ViewGroup popup_container = (ViewGroup) findViewById(R.id.popup_container);
        if (popupIsOpen()) {
            closePopup();
            return;
        }
        if (preview.getCamera() == null) {
            if (MyDebug.LOG)
                Log.d(TAG, "camera not opened!");
            return;
        }

        if (MyDebug.LOG)
            Log.d(TAG, "open popup");

        clearSeekBar();
        preview.cancelTimer(); // best to cancel any timer, in case we take a photo while settings window is open, or when changing settings

        final long time_s = System.currentTimeMillis();

        {
            // prevent popup being transparent
            popup_container.setBackgroundColor(Color.BLACK);
            popup_container.setAlpha(0.95f);
        }

        popup_view = new PopupView(this);
        popup_container.addView(popup_view);

        // need to call layoutUI to make sure the new popup is oriented correctly
        // but need to do after the layout has been done, so we have a valid width/height to use
        popup_container.getViewTreeObserver().addOnGlobalLayoutListener(
                new OnGlobalLayoutListener() {
                    @SuppressWarnings("deprecation")
                    @SuppressLint("NewApi")
                    @Override
                    public void onGlobalLayout() {
                        if (MyDebug.LOG)
                            Log.d(TAG, "onGlobalLayout()");
                        if (MyDebug.LOG)
                            Log.d(TAG, "time after global layout: " + (System.currentTimeMillis() - time_s));
                        layoutUI();
                        if (MyDebug.LOG)
                            Log.d(TAG, "time after layoutUI: " + (System.currentTimeMillis() - time_s));
                        // stop listening - only want to call this once!
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                            popup_container.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            popup_container.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }

                        ScaleAnimation animation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
                        animation.setDuration(100);
                        popup_container.setAnimation(animation);
                    }
                }
        );

        if (MyDebug.LOG)
            Log.d(TAG, "time to create popup: " + (System.currentTimeMillis() - time_s));
    }

    private void openSettings() {
        if (MyDebug.LOG)
            Log.d(TAG, "openSettings");
        closePopup();
        preview.cancelTimer(); // best to cancel any timer, in case we take a photo while settings window is open, or when changing settings
        preview.stopVideo(false); // important to stop video, as we'll be changing camera parameters when the settings window closes

        Bundle bundle = new Bundle();
        bundle.putInt("cameraId", this.preview.getCameraId());
        putBundleExtra(bundle, "white_balances", this.preview.getSupportedWhiteBalances());
        if (this.preview.getCamera() != null) {
            bundle.putString("parameters_string", this.preview.getCamera().getParameters().flatten());
        }

        List<Camera.Size> preview_sizes = this.preview.getSupportedPreviewSizes();
        if (preview_sizes != null) {
            int[] widths = new int[preview_sizes.size()];
            int[] heights = new int[preview_sizes.size()];
            int i = 0;
            for (Camera.Size size : preview_sizes) {
                widths[i] = size.width;
                heights[i] = size.height;
                i++;
            }
            bundle.putIntArray("preview_widths", widths);
            bundle.putIntArray("preview_heights", heights);
        }

        List<Camera.Size> sizes = this.preview.getSupportedPictureSizes();
        if (sizes != null) {
            int[] widths = new int[sizes.size()];
            int[] heights = new int[sizes.size()];
            int i = 0;
            for (Camera.Size size : sizes) {
                widths[i] = size.width;
                heights[i] = size.height;
                i++;
            }
            bundle.putIntArray("resolution_widths", widths);
            bundle.putIntArray("resolution_heights", heights);
        }

        List<String> video_quality = this.preview.getSupportedVideoQuality();
        if (video_quality != null) {
            String[] video_quality_arr = new String[video_quality.size()];
            String[] video_quality_string_arr = new String[video_quality.size()];
            int i = 0;
            for (String value : video_quality) {
                video_quality_arr[i] = value;
                video_quality_string_arr[i] = this.preview.getCamcorderProfileDescription(value);
                i++;
            }
            bundle.putStringArray("video_quality", video_quality_arr);
            bundle.putStringArray("video_quality_string", video_quality_string_arr);
        }

        List<Camera.Size> video_sizes = this.preview.getSupportedVideoSizes();
        if (video_sizes != null) {
            int[] widths = new int[video_sizes.size()];
            int[] heights = new int[video_sizes.size()];
            int i = 0;
            for (Camera.Size size : video_sizes) {
                widths[i] = size.width;
                heights[i] = size.height;
                i++;
            }
            bundle.putIntArray("video_widths", widths);
            bundle.putIntArray("video_heights", heights);
        }

        putBundleExtra(bundle, "flash_values", this.preview.getSupportedFlashValues());
        putBundleExtra(bundle, "focus_values", this.preview.getSupportedFocusValues());

        setWindowFlagsForSettings();

    }

    public void updateForSettings() {
        updateForSettings(null);
    }

    public void updateForSettings(String toast_message) {
        if (MyDebug.LOG) {
            Log.d(TAG, "updateForSettings()");
            if (toast_message != null) {
                Log.d(TAG, "toast_message: " + toast_message);
            }
        }
        String saved_focus_value = null;
        if (preview.getCamera() != null && preview.isVideo() && !preview.focusIsVideo()) {
            saved_focus_value = preview.getCurrentFocusValue(); // n.b., may still be null
            // make sure we're into continuous video mode
            // workaround for bug on Samsung Galaxy S5 with UHD, where if the user switches to another (non-continuous-video) focus mode, then goes to Settings, then returns and records video, the preview freezes and the video is corrupted
            // so to be safe, we always reset to continuous video mode, and then reset it afterwards
            preview.updateFocusForVideo(false);
        }
        if (MyDebug.LOG)
            Log.d(TAG, "saved_focus_value: " + saved_focus_value);

        updateFolderHistory();

        // update camera for changes made in prefs - do this without closing and reopening the camera app if possible for speed!
        // but need workaround for Nexus 7 bug, where scene mode doesn't take effect unless the camera is restarted - I can reproduce this with other 3rd party camera apps, so may be a Nexus 7 issue...
        boolean need_reopen = false;
        if (preview.getCamera() != null) {
            Camera.Parameters parameters = preview.getCamera().getParameters();
            if (MyDebug.LOG)
                Log.d(TAG, "scene mode was: " + parameters.getSceneMode());
            String key = Preview.getSceneModePreferenceKey();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String value = sharedPreferences.getString(key, Camera.Parameters.SCENE_MODE_AUTO);

            if (!value.equals(parameters.getSceneMode())) {
                if (MyDebug.LOG)
                    Log.d(TAG, "scene mode changed to: " + value);
                need_reopen = true;
            }
        }

        layoutUI(); // needed in case we've changed left/right handed UI
        setupLocationListener(); // in case we've enabled GPS
        if (need_reopen || preview.getCamera() == null) { // if camera couldn't be opened before, might as well try again
            preview.onPause();
            preview.onResume(toast_message);
        } else {
            preview.setCameraDisplayOrientation(this); // need to call in case the preview rotation option was changed
            preview.pausePreview();
            preview.setupCamera(toast_message);
        }

        if (saved_focus_value != null) {
            if (MyDebug.LOG)
                Log.d(TAG, "switch focus back to: " + saved_focus_value);
            preview.updateFocus(saved_focus_value, true, false);
        }
    }

    boolean cameraInBackground() {
        return this.camera_in_background;
    }

    MyPreferenceFragment getPreferenceFragment() {
        MyPreferenceFragment fragment = (MyPreferenceFragment) getFragmentManager().findFragmentByTag("PREFERENCE_FRAGMENT");
        return fragment;
    }

    @Override
    public void onBackPressed() {
        if (screen_is_locked) {
            preview.showToast(screen_locked_toast, R.string.screen_is_locked);
            return;
        }

        if (getPreferenceFragment() != null) {
            setWindowFlagsForCamera();
            updateForSettings();
            super.onBackPressed();
            return;
        }

        if (popupIsOpen()) {
            closePopup();
            return;
        }

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        if (preferences.isMasterSlaveEnabled() && !preferences.isMaster()) {
            // Users will generally not be interacting directly with slave cameras so if
            // the user backs out don't display the back dialog
            super.onBackPressed();
            return;
        }

        if (isCapturingVideo
                || (isTurntableAutomatedPhotoMode && !imageDescriptions.isEmpty())
                || (session.isAutomaticPictureMode() && !imageDescriptions.isEmpty())) {
            isCancelPending = true;
            preview.onPause();
            showCaptureInterruptedDialog();
            return;
        }

        super.onBackPressed();
    }

    private void showCaptureInterruptedDialog() {
        BackDialogFragment fragment = new BackDialogFragment();
        fragment.setCancelable(false);
        fragment.show(getFragmentManager(), "cancel_dialog");
    }

    //@TargetApi(Build.VERSION_CODES.KITKAT)
    private void setImmersiveMode(boolean on) {
        // Andorid 4.4 immersive mode disabled for now, as not clear of a good way to enter and leave immersive mode, and "sticky" might annoy some users
        /*if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ) {
            if( on )
        		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
        	else
        		getWindow().getDecorView().setSystemUiVisibility(0);
        }*/
        if (on)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        else
            getWindow().getDecorView().setSystemUiVisibility(0);
    }

    private void setWindowFlagsForCamera() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // force to landscape mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        // keep screen active - see http://stackoverflow.com/questions/2131948/force-screen-on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (sharedPreferences.getBoolean("preference_show_when_locked", true)) {
            // keep Open Camera on top of screen-lock (will still need to unlock when going to gallery or settings)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        }

        // set screen to max brightness - see http://stackoverflow.com/questions/11978042/android-screen-brightness-max-value
        // done here rather than onCreate, so that changing it in preferences takes effect without restarting app
        {
            WindowManager.LayoutParams layout = getWindow().getAttributes();
            if (sharedPreferences.getBoolean("preference_max_brightness", true)) {
                layout.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
            } else {
                layout.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
            }
            getWindow().setAttributes(layout);
        }

        setImmersiveMode(true);

        camera_in_background = false;
    }

    private void setWindowFlagsForSettings() {
        // allow screen rotation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        // revert to standard screen blank behaviour
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // settings should still be protected by screen lock
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        {
            WindowManager.LayoutParams layout = getWindow().getAttributes();
            layout.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
            getWindow().setAttributes(layout);
        }

        setImmersiveMode(false);

        camera_in_background = true;
    }

    class Media {
        public long id;
        public boolean video;
        public Uri uri;
        public long date;
        public int orientation;

        Media(long id, boolean video, Uri uri, long date, int orientation) {
            this.id = id;
            this.video = video;
            this.uri = uri;
            this.date = date;
            this.orientation = orientation;
        }
    }

    private Media getLatestMedia(boolean video) {
        Media media = null;
        Uri baseUri = video ? Video.Media.EXTERNAL_CONTENT_URI : Images.Media.EXTERNAL_CONTENT_URI;
        Uri query = baseUri.buildUpon().appendQueryParameter("limit", "1").build();
        String[] projection = video ? new String[]{VideoColumns._ID, VideoColumns.DATE_TAKEN} : new String[]{ImageColumns._ID, ImageColumns.DATE_TAKEN, ImageColumns.ORIENTATION, ImageColumns.DATE_TAKEN};
        String selection = video ? "" : ImageColumns.MIME_TYPE + "='image/jpeg'";
        String order = video ? VideoColumns.DATE_TAKEN + " DESC," + VideoColumns._ID + " DESC" : ImageColumns.DATE_TAKEN + " DESC," + ImageColumns._ID + " DESC";
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(query, projection, selection, null, order);
            if (cursor != null && cursor.moveToFirst()) {
                long id = cursor.getLong(0);
                long date = cursor.getLong(1);
                int orientation = video ? 0 : cursor.getInt(2);
                Uri uri = ContentUris.withAppendedId(baseUri, id);
                if (MyDebug.LOG)
                    Log.d(TAG, "found most recent uri for " + (video ? "video" : "images") + ": " + uri);
                media = new Media(id, video, uri, date, orientation);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return media;
    }

    private Media getLatestMedia() {
        Media image_media = getLatestMedia(false);
        Media video_media = getLatestMedia(true);
        Media media = null;
        if (image_media != null && video_media == null) {
            if (MyDebug.LOG)
                Log.d(TAG, "only found images");
            media = image_media;
        } else if (image_media == null && video_media != null) {
            if (MyDebug.LOG)
                Log.d(TAG, "only found videos");
            media = video_media;
        } else if (image_media != null && video_media != null) {
            if (MyDebug.LOG) {
                Log.d(TAG, "found images and videos");
                Log.d(TAG, "latest image date: " + image_media.date);
                Log.d(TAG, "latest video date: " + video_media.date);
            }
            if (image_media.date >= video_media.date) {
                if (MyDebug.LOG)
                    Log.d(TAG, "latest image is newer");
                media = image_media;
            } else {
                if (MyDebug.LOG)
                    Log.d(TAG, "latest video is newer");
                media = video_media;
            }
        }
        return media;
    }

    public void updateGalleryIconToBlank() {
//		if( MyDebug.LOG )
//			Log.d(TAG, "updateGalleryIconToBlank");
//    	ImageButton galleryButton = (ImageButton) this.findViewById(R.id.gallery);
//	    int bottom = galleryButton.getPaddingBottom();
//	    int top = galleryButton.getPaddingTop();
//	    int right = galleryButton.getPaddingRight();
//	    int left = galleryButton.getPaddingLeft();
//	    /*if( MyDebug.LOG )dfgdfg
//			Log.d(TAG, "padding: " + bottom);*/
//	    galleryButton.setImageBitmap(null);
//		galleryButton.setImageResource(R.drawable.gallery_icon_dark);
//		// workaround for setImageResource also resetting padding, Android bug
//		galleryButton.setPadding(left, top, right, bottom);
//		gallery_bitmap = null;
    }

    public void updateGalleryIconToBitmap(Bitmap bitmap) {
//		if( MyDebug.LOG )
//			Log.d(TAG, "updateGalleryIconToBitmap");
//    	ImageButton galleryButton = (ImageButton) this.findViewById(R.id.gallery);
//		galleryButton.setImageBitmap(bitmap);
//		gallery_bitmap = bitmap;
    }

    public void updateGalleryIcon() {
        if (MyDebug.LOG)
            Log.d(TAG, "updateGalleryIcon");
        long time_s = System.currentTimeMillis();
        Media media = getLatestMedia();
        Bitmap thumbnail = null;
        if (media != null) {
            if (media.video) {
                thumbnail = Video.Thumbnails.getThumbnail(getContentResolver(), media.id, Video.Thumbnails.MINI_KIND, null);
            } else {
                thumbnail = Images.Thumbnails.getThumbnail(getContentResolver(), media.id, Images.Thumbnails.MINI_KIND, null);
            }
            if (thumbnail != null) {
                if (media.orientation != 0) {
                    if (MyDebug.LOG)
                        Log.d(TAG, "thumbnail size is " + thumbnail.getWidth() + " x " + thumbnail.getHeight());
                    Matrix matrix = new Matrix();
                    matrix.setRotate(media.orientation, thumbnail.getWidth() * 0.5f, thumbnail.getHeight() * 0.5f);
                    try {
                        Bitmap rotated_thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                        // careful, as rotated_thumbnail is sometimes not a copy!
                        if (rotated_thumbnail != thumbnail) {
                            thumbnail.recycle();
                            thumbnail = rotated_thumbnail;
                        }
                    } catch (Throwable t) {
                        if (MyDebug.LOG)
                            Log.d(TAG, "failed to rotate thumbnail");
                    }
                }
            }
        }
        if (thumbnail != null) {
            if (MyDebug.LOG)
                Log.d(TAG, "set gallery button to thumbnail");
            updateGalleryIconToBitmap(thumbnail);
        } else {
            if (MyDebug.LOG)
                Log.d(TAG, "set gallery button to blank");
            updateGalleryIconToBlank();
        }
        if (MyDebug.LOG)
            Log.d(TAG, "time to update gallery icon: " + (System.currentTimeMillis() - time_s));
    }

    public void clickedGallery(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedGallery");
        //Intent intent = new Intent(Intent.ACTION_VIEW, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Uri uri = null;
        Media media = getLatestMedia();
        if (media != null) {
            uri = media.uri;
        }

        if (uri != null) {
            // check uri exists
            if (MyDebug.LOG)
                Log.d(TAG, "found most recent uri: " + uri);
            try {
                ContentResolver cr = getContentResolver();
                ParcelFileDescriptor pfd = cr.openFileDescriptor(uri, "r");
                if (pfd == null) {
                    if (MyDebug.LOG)
                        Log.d(TAG, "uri no longer exists (1): " + uri);
                    uri = null;
                }
                pfd.close();
            } catch (IOException e) {
                if (MyDebug.LOG)
                    Log.d(TAG, "uri no longer exists (2): " + uri);
                uri = null;
            }
        }
        if (uri == null) {
            uri = Images.Media.EXTERNAL_CONTENT_URI;
        }
        if (!is_test) {
            // don't do if testing, as unclear how to exit activity to finish test (for testGallery())
            if (MyDebug.LOG)
                Log.d(TAG, "launch uri:" + uri);
            final String REVIEW_ACTION = "com.android.camera.action.REVIEW";
            try {
                // REVIEW_ACTION means we can view video files without autoplaying
                Intent intent = new Intent(REVIEW_ACTION, uri);
                this.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                if (MyDebug.LOG)
                    Log.d(TAG, "REVIEW_ACTION intent didn't work, try ACTION_VIEW");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                // from http://stackoverflow.com/questions/11073832/no-activity-found-to-handle-intent - needed to fix crash if no gallery app installed
                //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("blah")); // test
                if (intent.resolveActivity(getPackageManager()) != null) {
                    this.startActivity(intent);
                } else {
                    preview.showToast(null, R.string.no_gallery_app);
                }
            }
        }
    }

    public void updateFolderHistory() {
        String folder_name = getSaveLocation();
        updateFolderHistory(folder_name);
    }

    private void updateFolderHistory(String folder_name) {
        while (save_location_history.remove(folder_name)) {
        }
        save_location_history.add(folder_name);
        while (save_location_history.size() > 6) {
            save_location_history.remove(0);
        }
        writeSaveLocations();
    }

    public void clearFolderHistory() {
        save_location_history.clear();
        updateFolderHistory(); // to re-add the current choice, and save
    }

    private void writeSaveLocations() {
        if (MyDebug.LOG)
            Log.d(TAG, "writeSaveLocations");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("save_location_history_size", save_location_history.size());
        if (MyDebug.LOG)
            Log.d(TAG, "save_location_history_size = " + save_location_history.size());
        for (int i = 0; i < save_location_history.size(); i++) {
            String string = save_location_history.get(i);
            editor.putString("save_location_history_" + i, string);
        }
        editor.apply();
    }

    private void longClickedGallery() {
        if (MyDebug.LOG)
            Log.d(TAG, "longClickedGallery");
        if (save_location_history.size() <= 1) {
            return;
        }
        final int theme = android.R.style.Theme_Black_NoTitleBar_Fullscreen;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, theme);
        alertDialog.setTitle(R.string.choose_save_location);
        CharSequence[] items = new CharSequence[save_location_history.size() + 1];
        int index = 0;
        // save_location_history is stored in order most-recent-last
        for (int i = 0; i < save_location_history.size(); i++) {
            items[index++] = save_location_history.get(save_location_history.size() - 1 - i);
        }
        final int clear_index = index;
        items[index++] = getResources().getString(R.string.clear_folder_history);
        /*final int new_index = index;
        items[index++] = getResources().getString(R.string.new_save_location);*/
        alertDialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == clear_index) {
                    if (MyDebug.LOG)
                        Log.d(TAG, "selected clear save history");
                    new AlertDialog.Builder(CameraActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(R.string.clear_folder_history)
                            .setMessage(R.string.clear_folder_history_question)
                            .setPositiveButton(R.string.answer_yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (MyDebug.LOG)
                                        Log.d(TAG, "confirmed clear save history");
                                    clearFolderHistory();
                                }
                            })
                            .setNegativeButton(R.string.answer_no, null)
                            .show();
                    setWindowFlagsForCamera();
                }
                /*else if( which == new_index ) {
                    if( MyDebug.LOG )
						Log.d(TAG, "selected choose new folder");
		    		FolderChooserDialog fragment = new FolderChooserDialog();
		    		fragment.setStyle(DialogFragment.STYLE_NORMAL, theme);
		    		fragment.show(getFragmentManager(), "FOLDER_FRAGMENT");
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					//DialogFragment newFragment = MyDialogFragment.newInstance();
					FolderChooserDialog fragment = new FolderChooserDialog();
					ft.add(R.id.prefs_container, fragment);
					ft.commit();
		    		fragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							if( MyDebug.LOG )
								Log.d(TAG, "FolderChooserDialog dismissed");
							setWindowFlagsForCamera();
						}
;		    		});
				}*/
                else {
                    if (MyDebug.LOG)
                        Log.d(TAG, "selected: " + which);
                    if (which >= 0 && which < save_location_history.size()) {
                        String save_folder = save_location_history.get(save_location_history.size() - 1 - which);
                        if (MyDebug.LOG)
                            Log.d(TAG, "changed save_folder from history to: " + save_folder);
                        preview.showToast(null, getResources().getString(R.string.changed_save_location) + "\n" + save_folder);
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(CameraActivity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("preference_save_location", save_folder);
                        editor.apply();
                        updateFolderHistory(); // to move new selection to most recent
                    }
                    setWindowFlagsForCamera();
                }
            }
        });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface arg0) {
                setWindowFlagsForCamera();
            }
        });
        alertDialog.show();
        //getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        setWindowFlagsForSettings();
    }

    static private void putBundleExtra(Bundle bundle, String key, List<String> values) {
        if (values != null) {
            String[] values_arr = new String[values.size()];
            int i = 0;
            for (String value : values) {
                values_arr[i] = value;
                i++;
            }
            bundle.putStringArray(key, values_arr);
        }
    }

    public void clickedShare(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedShare");
        this.preview.clickedShare();
    }

    public void clickedTrash(View view) {
        if (MyDebug.LOG)
            Log.d(TAG, "clickedTrash");
        this.preview.clickedTrash();
        // Calling updateGalleryIcon() immediately has problem that it still returns the latest image that we've just deleted!
        // But works okay if we call after a delay. 100ms works fine on Nexus 7 and Galaxy Nexus, but set to 500 just to be safe.
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //updateGalleryIcon();
            }
        }, 500);
    }

    private void takePicture() {
        if (MyDebug.LOG)
            Log.d(TAG, "takePicture");
        closePopup();

        if (magazineState != null) {
            if (currentCustomPoiShot == null) {
                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                final int selected = adapter.getSelectedIndex();
                if (selected < 0) {
                    AppDialogFragment.show(this, "No Shot Selected", "Select a shot before taking photo");
                    return;
                }
            }
        }

        if (isCapturingVideo) {
            isCancelPending = true;

            if (Util.shouldSendSlaveCommand(this)) {
                InterDeviceCommand command = new InterDeviceCommand();
                command.data = new InterDeviceCommand.CommandData();
                command.data.command = InterDeviceCommand.COMMAND_CANCEL_CAPTURE;
                NetworkService.sendMessage(this, command);
            }
            preview.onPause();
            showCaptureInterruptedDialog();
            return;
        }

        shutterButton.setEnabled(false);
        if (session.isShutterAnimationEnabled()) {
            showShutterAnimation();
        } else {
            preview.takePicturePressed();
        }
    }

    private void showShutterAnimation() {
        // App is used in noisy environments and users cannot always hear shutter sound so we
        // provide visual feedback that shutter button was tapped.
        final Preview p = preview;
        whiteOverlay.setAlpha(1f);
        whiteOverlay.animate().alpha(0f).setDuration(200).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                // Wait until the animation is complete before capturing the image because the
                // capture process spends a lot of time on the main thread so the animation
                // won't run smoothly while it is happening.
                p.takePicturePressed();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    void lockScreen() {
//		((ViewGroup) findViewById(R.id.locker)).setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View arg0, MotionEvent event) {
//                return gestureDetector.onTouchEvent(event);
//                //return true;
//            }
//		});
//		screen_is_locked = true;
    }

    void unlockScreen() {
//		((ViewGroup) findViewById(R.id.locker)).setOnTouchListener(null);
//		screen_is_locked = false;
    }

    boolean isScreenLocked() {
        return screen_is_locked;
    }

    class MyGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (MyDebug.LOG)
                    Log.d(TAG, "from " + e1.getX() + " , " + e1.getY() + " to " + e2.getX() + " , " + e2.getY());
                final ViewConfiguration vc = ViewConfiguration.get(CameraActivity.this);
                //final int swipeMinDistance = 4*vc.getScaledPagingTouchSlop();
                final float scale = getResources().getDisplayMetrics().density;
                final int swipeMinDistance = (int) (160 * scale + 0.5f); // convert dps to pixels
                final int swipeThresholdVelocity = vc.getScaledMinimumFlingVelocity();
                if (MyDebug.LOG) {
                    Log.d(TAG, "from " + e1.getX() + " , " + e1.getY() + " to " + e2.getX() + " , " + e2.getY());
                    Log.d(TAG, "swipeMinDistance: " + swipeMinDistance);
                }
                float xdist = e1.getX() - e2.getX();
                float ydist = e1.getY() - e2.getY();
                float dist2 = xdist * xdist + ydist * ydist;
                float vel2 = velocityX * velocityX + velocityY * velocityY;
                if (dist2 > swipeMinDistance * swipeMinDistance && vel2 > swipeThresholdVelocity * swipeThresholdVelocity) {
                    preview.showToast(screen_locked_toast, R.string.unlocked);
                    unlockScreen();
                }
            } catch (Exception e) {
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            preview.showToast(screen_locked_toast, R.string.screen_is_locked);
            return true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        Timber.i("onSaveInstanceState entered");
        super.onSaveInstanceState(state);
        if (this.preview != null) {
            preview.onSaveInstanceState(state);
        }

        state.putBoolean(KEY_BANNER_VISIBLE, bannerView.getVisibility() == View.VISIBLE);
    }

     /*
      * Merge shots according to order taken.  Since the filename of the picture is based on timestamp,
      * we can sort by filename.
      */
     private static ArrayList<ImageDescription> mergeShots(ArrayList<ImageDescription> left, ArrayList<ImageDescription> right) {
         ArrayList<ImageDescription> shots = new ArrayList<>(left.size() + right.size());

         for (ImageDescription d : left) {
             shots.add(d);
         }

         for (ImageDescription d : right) {
             shots.add(d);
         }

         // Make sure the pictures are ordered in the same order they were taken
         Collections.sort(shots, new Comparator<ImageDescription>() {
             @Override
             public int compare(ImageDescription i1, ImageDescription i2) {
                 return i1.image.getName().compareToIgnoreCase(i2.image.getName());
             }
         });

         return shots;
     }

    private static boolean isPOIShot(ImageDescription description) {
        return !TextUtils.isEmpty(description.featureCode) || !TextUtils.isEmpty(description.featureTitle);

    }

    private void updateResultIntent() {
        Intent intent = new Intent();
        if (magazineState != null) {
            intent.putParcelableArrayListExtra(EXTRA_CUSTOM_POIS, shotCollection.customPoiShots);
            intent.putParcelableArrayListExtra(EXTRA_PIPS, shotCollection.pipShots);
            intent.putParcelableArrayListExtra(EXTRA_ANIMATED_PICTURES, shotCollection.animatedPictureShots);
            intent.putParcelableArrayListExtra(EXTRA_IMAGE_DESCRIPTIONS, getImageDescriptions());
        } else {
            intent.putParcelableArrayListExtra(EXTRA_IMAGE_DESCRIPTIONS, imageDescriptions);
        }
        setResult(HAVE_IMAGES_RESULT_CODE, intent);
    }

    private ArrayList<ImageDescription> getImageDescriptions() {
        ArrayList<ImageDescription> poiImages = shotCollection.getPoiImageDescriptions(magazineState);
        ArrayList<ImageDescription> detailImages = shotCollection.getDetailImageDescriptions();
        return mergeShots(poiImages, detailImages);
    }

    private void enqueueSpinitPictures() {
        if (slaveData == null) {
            Timber.e("Slave is missing data required to enqueue media");
            return;
        }

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        final Intent intent = UploaderService.createSpinItVideoIntent(this,
                slaveData.vin,
                slaveData.year,
                slaveData.make,
                slaveData.model,
                slaveData.project,
                slaveData.library,
                slaveData.replace,
                true,
                videoFile.getAbsolutePath(),
                new AppSharedPreferences(this).getSpinitTotalFrames(),
                preferences.getSpinNoTurntableAutoPictureCount(),
                preferences.getSpinItVideoMaxDuration(),
                preferences.getSpinitTotalFrames(),
                preferences.getCameraRole().getSymbol());
        startService(intent);
    }

    public void broadcastFile(File file, boolean is_new_picture, boolean is_new_video) {
        // note that the new method means that the new folder shows up as a file when connected to a PC via MTP (at least tested on Windows 8)
        if (file.isDirectory()) {
            this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.fromFile(file)));
            // ACTION_MEDIA_MOUNTED no longer allowed on Android 4.4! Gives: SecurityException: Permission Denial: not allowed to send broadcast android.intent.action.MEDIA_MOUNTED
            // note that we don't actually need to broadcast anything, the folder and contents appear straight away (both in Gallery on device, and on a PC when connecting via MTP)
            // also note that we definitely don't want to broadcast ACTION_MEDIA_SCANNER_SCAN_FILE or use scanFile() for folders, as this means the folder shows up as a file on a PC via MTP (and isn't fixed by rebooting!)
        } else {
            // both of these work fine, but using MediaScannerConnection.scanFile() seems to be preferred over sending an intent
            //this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
            MediaScannerConnection.scanFile(this, new String[]{file.getAbsolutePath()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            if (MyDebug.LOG) {
                                Log.d("ExternalStorage", "Scanned " + path + ":");
                                Log.d("ExternalStorage", "-> uri=" + uri);
                            }
                        }
                    }
            );

            int lastIndex = -1;
            if (is_new_picture) {
                shutterButton.setEnabled(true);

                if (currentCustomPoiShot != null && currentCustomPoiShot.waitingForPhoto) {
                    AddExteriorPoiReview dialog = AddExteriorPoiReview.newInstance(file.getAbsolutePath());
                    dialog.show(getFragmentManager(), "add_exterior_poi_review");
                    return;
                }

                if (magazineState != null && modeControlView.getMode() == CaptureMode.PIP) {
                    if (currentPipShot.waitingForDetailPhoto) {
                        currentPipShot.detailImagePathname = file.getAbsolutePath();
                        final AddPipReview dialog = AddPipReview.newInstance(currentPipShot.wideImagePathname, currentPipShot.detailImagePathname);
                        dialog.show(getFragmentManager(), "add_pip_review");
                    } else {
                        currentPipShot.wideImagePathname = file.getAbsolutePath();
                        AddPipSelectPosition dialog = AddPipSelectPosition.newInstance(currentPipShot.wideImagePathname);
                        dialog.show(getFragmentManager(), "add_pip_select_position");
                    }
                    return;
                }

                ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
                if (magazineState != null && modeControlView.getMode() == CaptureMode.POI) {
                    lastIndex = adapter.getSelectedIndex();

                    final PoiShot shot = new PoiShot(file, CaptureMode.POI);
                    adapter.addPoiShot(shot);
                } else if (modeControlView.getMode() == CaptureMode.DETAIL) {
                    lastIndex = adapter.getSelectedIndex();
                    final DetailShot shot = new DetailShot(file, CaptureMode.DETAIL);
                    adapter.addDetailShot(shot);
                } else {
                    imageDescriptions.add(new ImageDescription(file, imageDescriptions.size()));
                }

                updateResultIntent();

                if (showImageCount) {
                    imageCount++;
                    imageCountTextView.setText(String.valueOf(imageCount));
                    imageCountTextView.setVisibility(View.VISIBLE);
                }

                ContentValues values = new ContentValues();
                values.put(ImageColumns.TITLE, file.getName().substring(0, file.getName().lastIndexOf(".")));
                values.put(ImageColumns.DISPLAY_NAME, file.getName());
                values.put(ImageColumns.DATE_TAKEN, System.currentTimeMillis());
                values.put(ImageColumns.MIME_TYPE, "image/jpeg");

                if (isSpinitSingleShotMode) values.put(ImageColumns.DESCRIPTION, vin + "spd");

                else {
                    values.put(ImageColumns.DESCRIPTION, vin + "dp");
                }
                // TODO: orientation
                values.put(ImageColumns.DATA, file.getAbsolutePath());
                Location location = null;//preview.getLocation();
                if (location != null) {
                    values.put(ImageColumns.LATITUDE, location.getLatitude());
                    values.put(ImageColumns.LONGITUDE, location.getLongitude());
                }
                try {

                    this.getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
                } catch (Throwable th) {
                    // This can happen when the external volume is already mounted, but
                    // MediaScanner has not notify MediaProvider to add that volume.
                    // The picture is still safe and MediaScanner will find it and
                    // insert it into MediaProvider. The only problem is that the user
                    // cannot click the thumbnail to review the picture.
                    Log.e(TAG, "Failed to write MediaStore" + th);
                }
            } else if (is_new_video) {
                if (isAnimatedGifActive) {
                    updateResultIntent();
                }
            }

            updateAdapter(lastIndex);
        }
    }

    private void updateAdapter(int lastIndex) {
        updateAdapter(lastIndex, 1);
    }

     private void updateAdapter(int lastIndex, int changed) {
         ShotListAdapter adapter = (ShotListAdapter) shotListRecyclerView.getAdapter();
         if (adapter != null) {
             // Make sure selected shot is visible
             final int selectedPosition = adapter.getSelectedIndex();
             shotListRecyclerView.scrollToPosition(selectedPosition);
             if (lastIndex >= 0) {
                 for (int i = 0; i < changed; i++) {
                     adapter.notifyItemChanged(lastIndex + i);
                 }
             }
             adapter.notifyItemChanged(selectedPosition);
         }
     }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private String getSaveLocation() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String folder_name = sharedPreferences.getString("preference_save_location", "OpenCamera");
        return folder_name;
    }

    static File getBaseFolder(Context c, String v) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        final boolean saved_is_video = new AppSharedPreferences(c).isVideo();
        if (saved_is_video) {
            return FileUtil.getSpinitDirectory(v);
        } else if (CameraActivity.staticisSpinitSingleShotMode) {
            return FileUtil.getSpinItPicturesDirectory(v);
        } else {
            return FileUtil.getDetailPicturesDirectory(v);
        }
    }

    static File getImageFolder(Context c, String folder_name, String v) {
        File file = null;
        if (folder_name.length() > 0 && folder_name.lastIndexOf('/') == folder_name.length() - 1) {
            // ignore final '/' character
            folder_name = folder_name.substring(0, folder_name.length() - 1);
        }
        //if( folder_name.contains("/") ) {
        if (folder_name.startsWith("/")) {
            file = new File(folder_name);
        } else {
            file = new File(getBaseFolder(c, v), folder_name);
        }
        /*if( MyDebug.LOG ) {
            Log.d(TAG, "folder_name: " + folder_name);
			Log.d(TAG, "full path: " + file);
		}*/
        return file;
    }

    public File getImageFolder() {
        String folder_name = getSaveLocation();
        return getImageFolder(this, folder_name, vin);
    }

    @SuppressLint("SimpleDateFormat")
    public File getImageFile() {
        if (isSpinitSingleShotMode) {
            return FileUtil.getNextSpinItPictureFile(vin);
        } else {
            return FileUtil.getNextDetailPictureFile(vin);
        }
    }

    public File getVideoFile() {
        if (isAnimatedGifActive) {
            return FileUtil.getNextAnimatedPictureFile(vin);
        } else if (!isFullmo) {
            return FileUtil.getNextSpinItVideoFile(vin);
        } else {
            return FileUtil.getNextFullMotionVideoFile(vin);
        }
    }

    public boolean supportsAutoStabilise() {
        return this.supports_auto_stabilise;
    }

    public boolean supportsForceVideo4K() {
        return this.supports_force_video_4k;
    }

    @SuppressWarnings("deprecation")
    public long freeMemory() { // return free memory in MB
        try {
            File image_folder = this.getImageFolder();
            StatFs statFs = new StatFs(image_folder.getAbsolutePath());
            // cast to long to avoid overflow!
            long blocks = statFs.getAvailableBlocks();
            long size = statFs.getBlockSize();
            long free = (blocks * size) / 1048576;
            /*if( MyDebug.LOG ) {
                Log.d(TAG, "freeMemory blocks: " + blocks + " size: " + size + " free: " + free);
			}*/
            return free;
        } catch (IllegalArgumentException e) {
            // can fail on emulator, at least!
            return -1;
        }
    }

    public static String getDonateLink() {
        return "https://play.google.com/store/apps/details?id=harman.mark.donation";
    }

    /*public static String getDonateMarketLink() {
        return "market://details?id=harman.mark.donation";
    }*/

    public Preview getPreview() {
        return this.preview;
    }

    // for testing:
    public ArrayList<String> getSaveLocationHistory() {
        return this.save_location_history;
    }

    public LocationListener getLocationListener() {
        return this.locationListener;
    }

    public void onCameraClosed() {
        if (!isCancelPending) {
            finishWithResult(false);
        }
    }

    public void finishWithResult() {
        this.finishWithResult(false);
    }

    public void finishWithResult(boolean onBack) {
        Bundle conData = new Bundle();
        conData.putString("vin", vin);
        conData.putString(EXTRA_PROJECT_ID, projectId);
        conData.putString(EXTRA_LIBRARY_ID, libraryId);

        Intent intent = new Intent();
        intent.putExtras(conData);
        setResult(RESULT_OK, intent);
        if (!onBack) finish();
    }

    void onVideoCaptureStarted(File video) {
        videoFile = video;

        final Intent intent = new Intent();
        intent.putExtra(EXTRA_VIDEO_PATHNAME, videoFile.getAbsolutePath());
        setResult(isAnimatedGifActive ? HAVE_IMAGES_RESULT_CODE : HAVE_VIDEO_RESULT_CODE, intent);

        isCapturingVideo = true;
        if (!isAnimatedGifActive) {
            shutterButton.setImageResource(R.drawable.video_stop);
        }
        shutterButton.setEnabled(true);
    }

    @Override
    public void onBackUploadContent() {
        if (Util.shouldSendSlaveCommand(this)) {
            InterDeviceCommand command = new InterDeviceCommand();
            command.data = new InterDeviceCommand.CommandData();
            command.data.command = InterDeviceCommand.COMMAND_ENQUEUE_MEDIA;
            NetworkService.sendMessage(this, command);
        }
        finish();
    }

    @Override
    public void onBackCancel() {
        if (Util.shouldSendSlaveCommand(this)) {
            InterDeviceCommand command = new InterDeviceCommand();
            command.data = new InterDeviceCommand.CommandData();
            command.data.command = InterDeviceCommand.COMMAND_DELETE_MEDIA;
            NetworkService.sendMessage(this, command);
        }

        deleteMedia();
        setResult(RESULT_CANCELED);
        finish();
    }

    private void deleteMedia() {
        if (videoFile != null) {
            if (!videoFile.delete()) {
                Timber.e("onBackCancel failed deleting video file " + videoFile.getAbsolutePath());
            } else {
                Timber.d("onBackCancel deleted video file " + videoFile.getAbsolutePath());
            }
        }

        if (imageDescriptions != null) {
            for (ImageDescription description : imageDescriptions) {
                if (!description.image.delete()) {
                    Timber.e("onBackCancel failed deleting image file " + description.image.getAbsolutePath());
                } else {
                    Timber.d("onBackCancel deleted image file " + description.image.getAbsolutePath());
                }
            }
        }
    }

    String getCameraFocusMode() {
        FocusMode mode = getDefaultFocusMode();
        return mode == FocusMode.AUTO ? "focus_mode_auto" : "focus_mode_manual";
    }

    private FocusMode getDefaultFocusMode() {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        FocusMode mode;
        if (isTurntableAutomatedPhotoMode
                || session.isAutomaticPictureMode()
                || (isVideo && !isFullmo)) {
            mode = preferences.getSpinFocusMode();
        } else if (isFullmo) {
            mode = preferences.getVideoFocusMode();
        } else {
            mode = preferences.getDetailPicturesFocusMode();
        }
        return mode;
    }

    private void setDefaultFocusMode(FocusMode mode) {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        if (isTurntableAutomatedPhotoMode
                || session.isAutomaticPictureMode()
                || (isVideo && !isFullmo)) {
            preferences.setSpinFocusMode(mode);
        } else if (isFullmo) {
            preferences.setVideoFocusMode(mode);
        } else {
            preferences.setDetailPicturesFocusMode(mode);
        }
    }


}
