package net.sourceforge.opencamera;

import java.io.File;

public interface Shot {
    File getShotListThumbnail();
}
