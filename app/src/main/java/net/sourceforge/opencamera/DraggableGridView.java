package net.sourceforge.opencamera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.sister.ivana.AppSharedPreferences;

import java.util.HashSet;

public class DraggableGridView extends View {

    private static final String TAG = "DraggableGridView";
    public static final float DEFAULT_SIZE = 0.20f;

    private class CircleArea {
        int radius;
        int centerX;
        int centerY;

        CircleArea(int centerX, int centerY, int radius) {
            this.radius = radius;
            this.centerX = centerX;
            this.centerY = centerY;
        }

        public void updatePercent(float percentX, float percentY) {
            percentX = Math.min(Math.max(percentX, 0.10f), 0.40f);
            percentY = Math.min(Math.max(percentY, 0.10f), 0.40f);

            if (this.centerX < screenCenterX && this.centerY < screenCenterY) {
                this.centerX = (int)(screenWidth * percentX);
                this.centerY = (int)(screenHeight * percentY);
            } else if (this.centerX >= screenCenterX && this.centerY < screenCenterY) {
                this.centerX = (int)(screenWidth - (screenWidth * percentX));
                this.centerY = (int)(screenHeight * percentY);
            } else if (this.centerX < screenCenterX && this.centerY >= screenCenterY) {
                this.centerX = (int)(screenWidth * percentX);
                this.centerY = (int)(screenHeight - (screenHeight * percentY));
            } if (this.centerX >= screenCenterX && this.centerY >= screenCenterY) {
                this.centerX = (int)(screenWidth - (screenWidth * percentX));
                this.centerY = (int)(screenHeight - (screenHeight * percentY));
            }
        }

        @Override
        public String toString() {
            return "Circle[" + centerX + ", " + centerY + ", " + radius + "]";
        }
    }

    private Paint mCirclePaint;
    private Paint mLinePaint;

    private final static int RADIUS_LIMIT = 100;

    private static final int CIRCLES_LIMIT = 4;

    private HashSet<CircleArea> mCircles = new HashSet<CircleArea>(CIRCLES_LIMIT);
    private SparseArray<CircleArea> mCirclePointer = new SparseArray<CircleArea>(CIRCLES_LIMIT);

    private int screenWidth;
    private int screenHeight;
    private int screenCenterX;
    private int screenCenterY;
    private float percentX;
    private float percentY;

    private AppSharedPreferences preferences;

    public DraggableGridView(final Context ct) {
        super(ct);

        init(ct);
    }

    public DraggableGridView(final Context ct, final AttributeSet attrs) {
        super(ct, attrs);

        init(ct);
    }

    public DraggableGridView(final Context ct, final AttributeSet attrs, final int defStyle) {
        super(ct, attrs, defStyle);

        init(ct);
    }

    private void init(final Context ct) {
        mCirclePaint = new Paint();
        mCirclePaint.setColor(Color.GRAY);
        mCirclePaint.setStrokeWidth(6);
        mCirclePaint.setStyle(Paint.Style.STROKE);

        mLinePaint = new Paint();
        mLinePaint.setColor(Color.GRAY);
        mLinePaint.setStrokeWidth(6);
        mLinePaint.setStyle(Paint.Style.FILL);

        WindowManager wm = (WindowManager) ct.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;

        preferences = new AppSharedPreferences(ct);
        percentX = preferences.getWalkAroundGridLinesX();
        percentY = preferences.getWalkAroundGridLinesY();
        float initialPercent = 0.20f;

        screenCenterX = screenWidth / 2;
        screenCenterY = screenHeight / 2;

        mCircles.add(new CircleArea((int)(screenWidth * initialPercent), (int)(screenHeight * initialPercent), 100));
        mCircles.add(new CircleArea((int)(screenWidth - (screenWidth * initialPercent)), (int)(screenHeight * initialPercent), 100));
        mCircles.add(new CircleArea((int)(screenWidth * initialPercent), (int)(screenHeight - (screenHeight * initialPercent)), 100));
        mCircles.add(new CircleArea((int)(screenWidth - (screenWidth * initialPercent)), (int)(screenHeight - (screenHeight * initialPercent)), 100));

        for (CircleArea circle : mCircles) {
            circle.updatePercent(percentX, percentY);
        }
    }

    @Override
    public void onDraw(final Canvas canv) {
        for (CircleArea circle : mCircles) {
            canv.drawLine((float)(circle.centerX) - 8.0f, (float)(circle.centerY) - 8.0f, (float)(circle.centerX) - 8.0f, (float)(circle.centerY) - 25.0f, mLinePaint);
            canv.drawLine((float)(circle.centerX) - 8.0f, (float)(circle.centerY) - 8.0f, (float)(circle.centerX) - 25.0f, (float)(circle.centerY) - 8.0f, mLinePaint);

            canv.drawLine((float)(circle.centerX) + 8.0f, (float)(circle.centerY) + 8.0f, (float)(circle.centerX) + 8.0f, (float)(circle.centerY) + 25.0f, mLinePaint);
            canv.drawLine((float)(circle.centerX) + 8.0f, (float)(circle.centerY) + 8.0f, (float)(circle.centerX) + 25.0f, (float)(circle.centerY) + 8.0f, mLinePaint);

            canv.drawLine((float)(circle.centerX), 0.0f, (float)(circle.centerX), (float)screenHeight, mLinePaint);
            canv.drawLine(0.0f, (float)(circle.centerY), (float)screenWidth, (float)(circle.centerY), mLinePaint);
        }

        canv.drawCircle(screenCenterX, screenCenterY, RADIUS_LIMIT, mCirclePaint);
        canv.drawLine((float)(screenCenterX), screenCenterY - RADIUS_LIMIT, (float)(screenCenterX), screenCenterY + RADIUS_LIMIT, mLinePaint);
        canv.drawLine(screenCenterX - RADIUS_LIMIT, (float)(screenCenterY), screenCenterX + RADIUS_LIMIT, (float)(screenCenterY), mLinePaint);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        boolean handled = false;

        CircleArea touchedCircle;
        int xTouch;
        int yTouch;
        int pointerId;
        int actionIndex = event.getActionIndex();

        // get touch event coordinates and make transparent circle from it
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                // it's the first pointer, so clear all existing pointers data
                clearCirclePointer();

                xTouch = (int) event.getX(0);
                yTouch = (int) event.getY(0);

                // check if we've touched inside some circle
                touchedCircle = obtainTouchedCircle(xTouch, yTouch);
                if (touchedCircle != null) {
                    touchedCircle.centerX = xTouch;
                    touchedCircle.centerY = yTouch;
                    mCirclePointer.put(event.getPointerId(0), touchedCircle);

                    invalidate();
                    handled = true;
                }

                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                // It secondary pointers, so obtain their ids and check circles
                pointerId = event.getPointerId(actionIndex);

                xTouch = (int) event.getX(actionIndex);
                yTouch = (int) event.getY(actionIndex);

                // check if we've touched inside some circle
                touchedCircle = obtainTouchedCircle(xTouch, yTouch);

                mCirclePointer.put(pointerId, touchedCircle);
                touchedCircle.centerX = xTouch;
                touchedCircle.centerY = yTouch;
                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_MOVE:
                final int pointerCount = event.getPointerCount();

                for (actionIndex = 0; actionIndex < pointerCount; actionIndex++) {
                    // Some pointer has moved, search it by pointer id
                    pointerId = event.getPointerId(actionIndex);

                    xTouch = (int) event.getX(actionIndex);
                    yTouch = (int) event.getY(actionIndex);

                    touchedCircle = mCirclePointer.get(pointerId);

                    if (null != touchedCircle) {

                        if (xTouch < screenCenterX) {
                            percentX = (float)xTouch / (float)screenWidth;
                        } else {
                            percentX = ((float)screenWidth - (float)xTouch) / (float)screenWidth;
                        }

                        if (yTouch < screenCenterY) {
                            percentY = (float)yTouch / (float)screenHeight;
                        } else {
                            percentY = ((float)screenHeight - (float)yTouch) / (float)screenHeight;
                        }

                        for (CircleArea circle : mCircles) {
                            circle.updatePercent(percentX, percentY);
                        }

                    }
                }
                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_UP:
                preferences.setWalkAroundGridLines(percentX, percentY);
                clearCirclePointer();
                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_POINTER_UP:
                // not general pointer was up
                pointerId = event.getPointerId(actionIndex);

                mCirclePointer.remove(pointerId);
                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_CANCEL:
                handled = true;
                break;

            default:
                // do nothing
                break;
        }

        return super.onTouchEvent(event) || handled;
    }

    private void clearCirclePointer() {
        mCirclePointer.clear();
    }

    private CircleArea obtainTouchedCircle(final int xTouch, final int yTouch) {
        CircleArea touchedCircle = getTouchedCircle(xTouch, yTouch);

        return touchedCircle;
    }

    private CircleArea getTouchedCircle(final int xTouch, final int yTouch) {
        CircleArea touched = null;

        for (CircleArea circle : mCircles) {
            if ((circle.centerX - xTouch) * (circle.centerX - xTouch) + (circle.centerY - yTouch) * (circle.centerY - yTouch) <= circle.radius * circle.radius) {
                touched = circle;
                break;
            }
        }

        return touched;
    }

}
