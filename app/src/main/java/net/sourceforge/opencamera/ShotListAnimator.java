package net.sourceforge.opencamera;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ShotListAnimator extends RecyclerView.ItemAnimator {
    private static final float SCALE_FACTOR = 1.8f;
    private static final long SCALE_UP_DURATION = 100;
    private static final long SCALE_DOWN_DURATION = 300;
    private static final float ANIMATION_ELEVATION = 1;

    private MagazineState magazineState;

    public ShotListAnimator(MagazineState magazineState) {
        this.magazineState = magazineState;
    }

    public void replaceMagazineState(MagazineState magazineState) {
        this.magazineState = magazineState;
    }

    @Override
    public boolean animateDisappearance(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull ItemHolderInfo preLayoutInfo, @Nullable ItemHolderInfo postLayoutInfo) {
        return false;
    }

    @Override
    public boolean animateAppearance(@NonNull RecyclerView.ViewHolder viewHolder, @Nullable ItemHolderInfo preLayoutInfo, @NonNull ItemHolderInfo postLayoutInfo) {
        return false;
    }

    @Override
    public boolean animatePersistence(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull ItemHolderInfo preLayoutInfo, @NonNull ItemHolderInfo postLayoutInfo) {
        return false;
    }

    @Override
    public boolean animateChange(@NonNull RecyclerView.ViewHolder oldHolder, @NonNull RecyclerView.ViewHolder newHolder, @NonNull ItemHolderInfo preLayoutInfo, @NonNull ItemHolderInfo postLayoutInfo) {
        if (!(newHolder instanceof ShotListHolder)) {
            return false;
        }

        ShotListHolder shotListHolder = (ShotListHolder) newHolder;
        if (shotListHolder.shouldAnimate()) {
            startScaleUp(newHolder.itemView);
        }
        
        return false;
    }

    private void startScaleUp(View view) {
        view.setPivotY(view.getHeight());
        final float startElevation = view.getElevation();
        view.setElevation(ANIMATION_ELEVATION);
        ViewCompat.animate(view)
                .scaleX(SCALE_FACTOR)
                .scaleY(SCALE_FACTOR)
                .setDuration(SCALE_UP_DURATION)
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        startScaleDown(view, startElevation);
                    }

                    @Override
                    public void onAnimationCancel(View view) {
                        view.setScaleX(1);
                        view.setScaleY(1);
                        view.setElevation(startElevation);
                    }
                })
                .start();
    }

    private void startScaleDown(View view, final float startElevation) {
        ViewCompat.animate(view)
                .scaleX(1)
                .scaleY(1)
                .setDuration(SCALE_DOWN_DURATION)
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        view.setElevation(startElevation);
                    }

                    @Override
                    public void onAnimationCancel(View view) {
                        view.setElevation(startElevation);
                    }
                })
                .start();
    }

    @Override
    public void runPendingAnimations() {

    }

    @Override
    public void endAnimation(RecyclerView.ViewHolder item) {

    }

    @Override
    public void endAnimations() {

    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
