package net.sourceforge.opencamera;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sister.ivana.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddPoiHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public interface AddPoiListener {
        void addPoiClicked();
    }

    private final AddPoiListener listener;

    public AddPoiHolder(Context context, View itemView, AddPoiListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.top_layout)
    public void onClick(View view) {
        listener.addPoiClicked();
    }
}
