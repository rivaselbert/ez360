package net.sourceforge.opencamera;

import java.io.File;

class DetailShot implements  Shot {
    File file;
    CaptureMode type;

    DetailShot(File file, CaptureMode type) {
        this.file = file;
    }

    @Override
    public File getShotListThumbnail() {
        return file;
    }
}
