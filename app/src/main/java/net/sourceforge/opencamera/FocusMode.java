package net.sourceforge.opencamera;


public enum FocusMode {
    AUTO(0), MANUAL(1);

    private final int preferenceCode;

    FocusMode(int preferenceCode) {
        this.preferenceCode = preferenceCode;
    }

    public int getPreferenceCode() {
        return preferenceCode;
    }

    public static FocusMode create(int preferenceCode) {
        switch (preferenceCode) {
            case 0:
                return AUTO;
            case 1:
                return MANUAL;
            default:
                return null;
        }
    }
}
