package net.sourceforge.opencamera;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sister.ivana.FileUtil;
import com.sister.ivana.R;
import com.sister.ivana.database.MagazineShot;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class ShotListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public interface ShotListener {
        void shotClicked(CaptureMode mode, int index);

        void shotLongClicked(CaptureMode mode, int index);
    }

    private final int textBackgroundColor;
    private final ShotListener listener;
    private int index;
    private final Picasso picasso;
    private boolean hasImage;
    private boolean shouldAnimate;
    private CaptureMode mode;
    @BindView(R.id.top_layout) RelativeLayout topLayout;
    @BindView(R.id.shot_image_view) ImageView shotImageView;
    @BindView(R.id.title_text_view) TextView titleTextView;

    public ShotListHolder(Context context, View itemView, ShotListener listener) {
        super(itemView);
        this.picasso  = Picasso.get();
        this.listener = listener;
        this.textBackgroundColor = context.getColor(R.color.translucent_black);
        ButterKnife.bind(this, itemView);
    }

    public CaptureMode getMode() {
        return mode;
    }

    public void bindMagazinePlaceholder(MagazineShot shot, int index, int imageWidth, int imageHeight) {
        this.index = index;
        File image = null;
        final Uri uri = shot.getImageUri();
        if (uri != null) {
            image = FileUtil.getLocalImageFile(uri);
        }

        updateImage(image, imageWidth, imageHeight);
        titleTextView.setText(shot.getTitle());
        titleTextView.setBackgroundColor(textBackgroundColor);
        shouldAnimate = false;
        mode = CaptureMode.POI;
    }

    public void bindPoiShot(PoiShot shot, String title, int index, int imageWidth, int imageHeight) {
        mode = CaptureMode.POI;
        this.index = index;
        updateImage(shot.file, imageWidth, imageHeight);
        titleTextView.setText(title);
        titleTextView.setBackgroundColor(textBackgroundColor);
        shouldAnimate = true;
    }

    public void bindCustomPoiShot(CustomPoiShot shot, boolean showTitle, int index, int imageWidth, int imageHeight) {
        mode = CaptureMode.POI;
        this.index = index;
        updateImage(new File(shot.filename), imageWidth, imageHeight);
        if (showTitle) {
            titleTextView.setText(shot.title);
            titleTextView.setBackgroundColor(textBackgroundColor);
        } else {
            titleTextView.setText("");
            titleTextView.setBackgroundColor(0);
        }
        shouldAnimate = true;
    }

    public void bindDetail(Shot shot, String title, int index, int width, int height) {
        mode = CaptureMode.DETAIL;
        bindShot(shot, title, index, width, height);
    }

    public void bindPip(Shot shot, String title, int index, int width, int height) {
        mode = CaptureMode.PIP;
        bindShot(shot, title, index, width, height);
    }

    private void bindShot(Shot shot, String title, int index, int width, int height) {
        this.index = index;
        titleTextView.setText(title);
        titleTextView.setBackgroundColor(0);

        if (shot == null) {
            shotImageView.setImageBitmap(null);
            shouldAnimate = false;
        } else {

            picasso.load(shot.getShotListThumbnail())
                    .resize(width, height)
                    .centerCrop()
                    .noFade()
                    .into(shotImageView);
            shouldAnimate = true;
        }
    }

    public void bindAnimatedPictureShot(AnimatedPictureShot shot, String title, int index, int width, int height) {
        mode = CaptureMode.ANIMATED_PICTURE;
        this.index = index;
        titleTextView.setText(title);
        titleTextView.setBackgroundColor(0);

        if (shot == null) {
            shotImageView.setImageBitmap(null);
            shouldAnimate = false;
        } else {
            shotImageView.setImageBitmap(extractThumbnailFromVideo(shot.video, width, height));
            shouldAnimate = true;
        }
    }

    public void bindAnimatedPictureFrame(File file, String title, int index, int width, int height) {
        mode = CaptureMode.ANIMATED_PICTURE;
        this.index = index;
        titleTextView.setText(title);
        titleTextView.setBackgroundColor(0);

        if (file == null) {
            shotImageView.setImageBitmap(null);
            shouldAnimate = false;
        } else {
            picasso.load(file)
                    .resize(width, height)
                    .centerCrop()
                    .noFade()
                    .into(shotImageView);
            shouldAnimate = false;
        }
    }

    public void setShotState(MagazineState.ShotState state) {
        switch (state) {
            case CURRENT:
                topLayout.setBackgroundColor(Color.BLUE);
                break;
            case NOT_TAKEN:
                topLayout.setBackgroundColor(0);
                break;
            case TAKEN:
                topLayout.setBackgroundColor(Color.GREEN);
                break;
        }
    }

    public boolean shouldAnimate() {
        return shouldAnimate;
    }

    private void updateImage(File image, int imageWidth, int imageHeight) {
        if (image != null) {
            if (image.getAbsolutePath().toLowerCase().endsWith(".mp4")) {
                shotImageView.setImageBitmap(extractThumbnailFromVideo(image, imageWidth, imageHeight));
            } else {
                picasso.load(image)
                        .resize(imageWidth, imageHeight)
                        .centerCrop()
                        .noFade()
                        .into(shotImageView);
            }
        } else {
            shotImageView.setImageBitmap(null);
        }
    }

    private Bitmap extractThumbnailFromVideo(File image, int imageWidth, int imageHeight) {
        Bitmap thumbnail = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(image.getPath());
            thumbnail = retriever.getFrameAtTime(1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            // corrupt video file?
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            // corrupt video file?
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
                // ignore
            }
        }
        if( thumbnail != null) {
            int width = thumbnail.getWidth();
            int height = thumbnail.getHeight();
            float scale = (float) imageWidth / width;
            int new_width = Math.round(scale * width);
            int new_height = Math.round(scale * height);
            Bitmap scaled_thumbnail = Bitmap.createScaledBitmap(thumbnail, new_width, new_height, true);
            // careful, as scaled_thumbnail is sometimes not a copy!
            if( scaled_thumbnail != thumbnail ) {
                thumbnail.recycle();
                thumbnail = scaled_thumbnail;
            }

            try {
                String jpgFilename = image.getAbsolutePath().replace(".mp4", ".jpg");
                FileOutputStream fOut = new FileOutputStream(new File(jpgFilename));
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                fOut.flush();
                fOut.close();
            } catch (Exception ex) {
                // ignore
            }
        }
        return thumbnail;
    }

    @OnClick(R.id.top_layout)
    public void onClick(View view) {
        if (mode != null) {
            listener.shotClicked(mode, index);
        }
    }

    @OnLongClick(R.id.top_layout)
    public boolean onLongClick(View view) {
        if (mode != null) {
            listener.shotLongClicked(mode, index);
        }
        return true;
    }

    public int getIndex() {
        return index;
    }
}
