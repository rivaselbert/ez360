package net.sourceforge.opencamera;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomPoiShot implements Parcelable {
    public String title;
    public String description;
    public int position;
    public float positionX;
    public float positionY;
    public String filename;
    public boolean addToMagazine;
    boolean waitingForPhoto;

    public CustomPoiShot() {
    }

    public CustomPoiShot(Parcel in) {
        title = in.readString();
        description = in.readString();
        position = in.readInt();
        positionX = in.readFloat();
        positionY = in.readFloat();
        filename = in.readString();
        addToMagazine = in.readInt() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeInt(position);
        parcel.writeFloat(positionX);
        parcel.writeFloat(positionY);
        parcel.writeString(filename);
        parcel.writeInt(addToMagazine ? 1 : 0);
    }

    public static final Parcelable.Creator<CustomPoiShot> CREATOR
            = new Parcelable.Creator<CustomPoiShot>() {
        public CustomPoiShot createFromParcel(Parcel in) {
            return new CustomPoiShot(in);
        }

        public CustomPoiShot[] newArray(int size) {
            return new CustomPoiShot[size];
        }
    };
}
